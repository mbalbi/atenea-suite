# módulo dialogs.py
"""
Popus durante el GUI

creado por: equipo Atenea-UBATIC
"""
import sys, os
import numpy as np

from PyQt5.QtGui import QDoubleValidator
from PyQt5 import QtCore, QtGui, QtWidgets

from .figurasUI import *

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

class saveDialog(QtWidgets.QMessageBox):

    def __init__(self, parent=None):
        super(saveDialog, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Question)
        self.setStandardButtons(QtWidgets.QMessageBox.Yes | \
                                QtWidgets.QMessageBox.No| \
                                QtWidgets.QMessageBox.Cancel)
        self.setWindowTitle('Guardar archivo')
        self.setText('Quiere guardar el archvo?')

    def exec_(self):
        ret = self.exec()
        if ret == QtWidgets.QMessageBox.Yes:
            ok = 1
        elif ret == QtWidgets.QMessageBox.No:
            ok = 0
        elif ret == QtWidgets.QMessageBox.Cancel:
            ok = 2
        return ok

class errorDialog(QtWidgets.QMessageBox):

    def __init__(self, parent=None):
        super(errorDialog, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Critical)
        self.setStandardButtons(QtWidgets.QMessageBox.Ok)
        self.setWindowTitle('Error')

    def error_catch(self, info, ex=None):
        """

        :return:
        """
        template = "Ocurrió un error de tipo {0}. Argumentos:\n{1!r}"
        if ex:
            message = template.format(type(ex).__name__, ex.args)
            self.setInformativeText(message)
        self.setText(info)
        self.exec_()
        return

class figuraDialog(QtWidgets.QDialog):

    def __init__(self):
        super(figuraDialog, self).__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.error_dialog = errorDialog()
        self.setWindowTitle('Crear figura')
        # Validate numerical values in lineedits
        self.DoubleValidation()

        # Error message
        self.error_dialog = errorDialog()

        # Setear grupo de botones de tipo de figura
        self.tipo_group = QtWidgets.QButtonGroup(self)
        self.tipo_group.addButton(self.ui.rectToolButton)
        self.tipo_group.addButton(self.ui.triToolButton)
        self.tipo_group.addButton(self.ui.circToolButton)
        self.tipo_group.addButton(self.ui.polyToolButton)
        self.ui.rectToolButton.setChecked(True)
        self.ui.rectToolButton.clicked.connect(self.updateFigura)
        self.ui.triToolButton.clicked.connect(self.updateFigura)
        self.ui.circToolButton.clicked.connect(self.updateFigura)
        self.ui.polyToolButton.clicked.connect(self.updateFigura)
        self.updateFigura()

        # Setear botones de tabla de poligono
        self.ui.addRowButton.clicked.connect(self.addrow2table)
        self.ui.removeRowButton.clicked.connect(self.removerow2table)

        # Setear que las columnas del arbol se autoexpandan
        self.ui.tableWidget.setSizeAdjustPolicy(
                QtWidgets.QAbstractScrollArea.AdjustToContents)
        # self.ui.tableWidget.horizontalHeader().setSectionResizeMode(
        #     QtWidgets.QHeaderView.ResizeToContents)
        # self.ui.tableWidget.horizontalHeader().setStretchLastSection(False)

    def updateFigura(self):
        self.ui.geomComboBox.clear()
        if self.ui.rectToolButton.isChecked():
            self.ftipo = 'Rect'
            items = ['g','A','B','C','D']
            self.ui.labelGeom2.setText('b')
            self.ui.labelGeom3.setText('h')
            self.ui.labelGeom2.setVisible(True)
            self.ui.geom2Edit.setVisible(True)
            self.ui.labelGeom3.setVisible(True)
            self.ui.geom3Edit.setVisible(True)
            self.ui.frameCoords.setVisible(False)
            self.ui.rectFrame.setVisible(True)
            self.ui.frame_5.setVisible(True)
            self.ui.label_7.setPixmap(QtGui.QPixmap(
                    resource_path("icons/fig_rect.png")))
        elif self.ui.triToolButton.isChecked():
            self.ftipo = 'Tri'
            items = ['g','A','B','C']
            self.ui.labelGeom2.setText('b')
            self.ui.labelGeom3.setText('h')
            self.ui.labelGeom2.setVisible(True)
            self.ui.geom2Edit.setVisible(True)
            self.ui.labelGeom3.setVisible(True)
            self.ui.geom3Edit.setVisible(True)
            self.ui.frameCoords.setVisible(False)
            self.ui.rectFrame.setVisible(True)
            self.ui.frame_5.setVisible(True)
            self.ui.label_7.setPixmap(QtGui.QPixmap(
                        resource_path("icons/fig_tri.png")))
        elif self.ui.circToolButton.isChecked():
            self.ftipo = 'Circ'
            items = ['g','A','B','C','D']
            self.ui.labelGeom2.setText('d')
            self.ui.labelGeom2.setVisible(True)
            self.ui.geom2Edit.setVisible(True)
            self.ui.labelGeom3.setVisible(False)
            self.ui.geom3Edit.setVisible(False)
            self.ui.frameCoords.setVisible(False)
            self.ui.rectFrame.setVisible(True)
            self.ui.frame_5.setVisible(True)
            self.ui.label_7.setPixmap(QtGui.QPixmap(
                        resource_path("icons/fig_circ.png")))
        elif self.ui.polyToolButton.isChecked():
            self.ftipo = 'Poly'
            items = ['g','A','B','C','D']
            self.ui.labelGeom2.setVisible(False)
            self.ui.geom2Edit.setVisible(False)
            self.ui.labelGeom3.setVisible(False)
            self.ui.geom3Edit.setVisible(False)
            self.ui.frameCoords.setVisible(True)
            self.ui.rectFrame.setVisible(False)
            self.ui.frame_5.setVisible(False)
            self.ui.label_7.setPixmap(QtGui.QPixmap(
                        resource_path("icons/fig_poly.png")))
        # Definir opciones del combobox interno
        self.ui.geomComboBox.addItems(items)

    def accept(self):
        self.ok = True
        self.geometria = {}
        # Leer tipo
        if self.ui.radioButtonLleno.isChecked():
            prop = 'Lleno'
        elif self.ui.radioButtonHueco.isChecked():
            prop = 'Hueco'
        self.tipo = {'Tipo':self.ftipo,'Prop':prop}
        if self.ui.rectToolButton.isChecked() or \
           self.ui.triToolButton.isChecked() or \
           self.ui.circToolButton.isChecked():

            # Leer geometria
            geom = self.ui.geomComboBox.currentText()
            if self.ui.xEdit.displayText():
                x = float(self.ui.xEdit.displayText())
            else:
                self.error_dialog.error_catch('Falta coordenada x')
                return
            if self.ui.yEdit.displayText():
                y = float(self.ui.yEdit.displayText())
            else:
                self.error_dialog.error_catch('Falta coordenada y')
                return
            self.geometria[geom] = [x,y]

            if self.ui.geom1Edit.displayText():
                alpha = float(self.ui.geom1Edit.displayText())
            else:
                self.error_dialog.error_catch('Falta coordenada ángulo de rotación')
                return
            self.geometria['alpha'] = alpha

            label1 = self.ui.labelGeom2.text()
            if self.ui.geom2Edit.displayText():
                p1 = float(self.ui.geom2Edit.displayText())
            else:
                self.error_dialog.error_catch('Falta {}'.format(label1))
                return
            self.geometria[label1] = p1

            if self.ui.labelGeom3.isVisible():
                label2 = self.ui.labelGeom3.text()
                if self.ui.geom3Edit.displayText():
                    p2 = float(self.ui.geom3Edit.displayText())
                else:
                    self.error_dialog.error_catch('Falta {}'.format(label2))
                    return
                self.geometria[label2] = p2

        elif self.ui.polyToolButton.isChecked():
            rows = self.ui.tableWidget.rowCount()
            # Leer geometria
            xy = []
            for i in range(rows):
                try:
                    xy_row = [float(self.ui.tableWidget.item(i,a).text()) for a in range(2)]
                except Exception as ex:
                    self.error_dialog.error_catch(
                        'Faltan completar celdas en fila {}'.format(str(i+1)))
                    return
                xy.append(xy_row)
            self.geometria['xy'] = xy
            if self.ui.discretizacionEdit.displayText():
                self.geometria['discr'] = int(
                            self.ui.discretizacionEdit.displayText())
            else:
                self.error_dialog.error_catch('Falta valor de discretización')
                return

        QtWidgets.QDialog.accept(self)

    def addrow2table(self):
        rowPosition = self.ui.tableWidget.rowCount()
        self.ui.tableWidget.insertRow(rowPosition)

    def removerow2table(self):
        rowPosition = self.ui.tableWidget.rowCount()
        if rowPosition > 3:
            self.ui.tableWidget.removeRow(rowPosition-1)

    def DoubleValidation(self):
        onlyDouble = QtGui.QDoubleValidator()
        self.ui.xEdit.setValidator(onlyDouble)
        self.ui.yEdit.setValidator(onlyDouble)
        self.ui.geom1Edit.setValidator(onlyDouble)
        self.ui.geom2Edit.setValidator(onlyDouble)
        self.ui.geom3Edit.setValidator(onlyDouble)
        self.ui.discretizacionEdit.setValidator(onlyDouble)

    def reject(self):
        self.ok = False
        self.tipo = {}
        self.geometria = {}
        QtWidgets.QDialog.reject(self)

    def exec_(self):
        super(figuraDialog, self).exec_()
        return self.ok, self.tipo, self.geometria


def about_dialog():
    ui = aboutDialog()
    ui.exec_()

class aboutDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.ui = about_ui()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.close)

class about_ui(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(600, 400)
        Dialog.setModal(True)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Dialog)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_4 = QtWidgets.QLabel(self.frame_2)
        self.label_4.setText("")
        self.label_4.setPixmap(QtGui.QPixmap(
                    resource_path("icons/AteneaSec_icon_large.png")))
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.horizontalLayout.addWidget(self.frame_2)
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout.setSpacing(15)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.textBrowser = QtWidgets.QTextBrowser(self.frame)
        self.textBrowser.setReadOnly(True)
        self.textBrowser.setTextInteractionFlags(
            QtCore.Qt.TextBrowserInteraction)
        self.textBrowser.setOpenExternalLinks(True)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        self.frame_3 = QtWidgets.QFrame(self.frame)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20,
                                           QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.pushButton = QtWidgets.QPushButton(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                           QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_3.addWidget(self.pushButton)
        self.verticalLayout.addWidget(self.frame_3)
        self.horizontalLayout.addWidget(self.frame)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Sobre AteneaSec"))
        self.label.setText(_translate("Dialog", "AteneaSec"))
        self.label_2.setText(_translate("Dialog", "Versión 1.7"))
        self.label_3.setText(_translate("Dialog", "Bajo licencia GNU-GPL-v3"))
        self.textBrowser.setHtml(_translate("Dialog",
                                            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.1pt; font-weight:400; font-style:normal;\">\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Desarrollado por el Departamento de Estabilidad de la Facultad de ingeniería de la UBA (Argentina)</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">para el proyecto UBATIC 2018-2019</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Director del proyecto: Raúl D. Bertero</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Equipo de desarrollo: Mariano Balbi, Agustín Bertero, Juan Mussat, Joaquín Ortiz, Felipe López Rivarola, Ariel Terlisky, Marcial Zapater, Felipe Medán y Santiago Bertero</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Web del proyecto: </span><a href=\"https://campusgrado.fi.uba.ar/course/view.php?id=258\"><span style=\" font-size:8pt; text-decoration: underline; color:#0000ff;\">Atenea Campus (FIUBA)</span></a></p></body></html>"))
        self.pushButton.setText(_translate("Dialog", "OK"))
