import sys, os
from PyQt5 import QtWidgets, QtGui, QtCore

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def createStatusBar(self):
    # Add to the status bar
    # Tool Button de mejorar precision
    self.ui.increasePrecisionToolButton = QtWidgets.QToolButton()
    self.ui.increasePrecisionToolButton.setToolTip('Incrementar precisión de resultados')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/increase_precision_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.increasePrecisionToolButton.setIcon(icon)
    self.ui.increasePrecisionToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.increasePrecisionToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.increasePrecisionToolButton.setAutoRaise(True)
    self.ui.increasePrecisionToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.increasePrecisionToolButton.setObjectName(
        "increaseArrowsToolButton")
    self.ui.increasePrecisionToolButton.setChecked(True)
    self.ui.increasePrecisionToolButton.clicked.connect(
        lambda: self.modifyPrecision(1))
    # Tool Button de achicar precision
    self.ui.decreasePrecisionToolButton = QtWidgets.QToolButton()
    self.ui.decreasePrecisionToolButton.setToolTip('Reducir precisión de resultados')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/decrease_precision_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.decreasePrecisionToolButton.setIcon(icon)
    self.ui.decreasePrecisionToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.decreasePrecisionToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.decreasePrecisionToolButton.setAutoRaise(True)
    self.ui.decreasePrecisionToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.decreasePrecisionToolButton.setObjectName(
        "decreaseArrowsToolButton")
    self.ui.decreasePrecisionToolButton.setChecked(True)
    self.ui.decreasePrecisionToolButton.clicked.connect(
        lambda: self.modifyPrecision(-1))
    # Tool Button de agrandar textos
    self.ui.increaseTextsToolButton = QtWidgets.QToolButton()
    self.ui.increaseTextsToolButton.setToolTip('Aumentar tamaño de textos')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/increase_letter_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.increaseTextsToolButton.setIcon(icon)
    self.ui.increaseTextsToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.increaseTextsToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.increaseTextsToolButton.setAutoRaise(True)
    self.ui.increaseTextsToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.increaseTextsToolButton.setObjectName(
        "increaseArrowsToolButton")
    self.ui.increaseTextsToolButton.setChecked(True)
    self.ui.increaseTextsToolButton.clicked.connect(
        lambda: self.modifyTextsSize(1))
    # Tool Button de achicar textos
    self.ui.decreaseTextsToolButton = QtWidgets.QToolButton()
    self.ui.decreaseTextsToolButton.setToolTip('Reducir tamaño de textos')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/decrease_letter_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.decreaseTextsToolButton.setIcon(icon)
    self.ui.decreaseTextsToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.decreaseTextsToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.decreaseTextsToolButton.setAutoRaise(True)
    self.ui.decreaseTextsToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.decreaseTextsToolButton.setObjectName(
        "decreaseArrowsToolButton")
    self.ui.decreaseTextsToolButton.setChecked(True)
    self.ui.decreaseTextsToolButton.clicked.connect(
        lambda: self.modifyTextsSize(-1))

    # Add widgets
    # Frame for precision tools
    frame1 = QtWidgets.QFrame()
    HLayout1 = QtWidgets.QHBoxLayout(frame1)
    HLayout1.setContentsMargins(0, 0, 0, 0)
    HLayout1.setSpacing(0)
    HLayout1.addWidget(self.ui.increasePrecisionToolButton,0)
    HLayout1.addWidget(self.ui.decreasePrecisionToolButton,1)
    frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
    frame1.setLayout(HLayout1)
    self.ui.statusBar.addPermanentWidget(frame1)
    # Frame for text size tools
    frame1 = QtWidgets.QFrame()
    HLayout1 = QtWidgets.QHBoxLayout(frame1)
    HLayout1.setContentsMargins(0, 0, 0, 0)
    HLayout1.setSpacing(0)
    HLayout1.addWidget(self.ui.increaseTextsToolButton,0)
    HLayout1.addWidget(self.ui.decreaseTextsToolButton,1)
    frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
    frame1.setLayout(HLayout1)
    self.ui.statusBar.addPermanentWidget(frame1)
    # Label de coordenadas del mouse en el canvas
    self.coordsLabel = QtWidgets.QLabel()
    self.ui.statusBar.addWidget(self.coordsLabel)