# modulo interactiveCanvas.py
"""
Este modulo define la clase "AteneaCanvasQt" que es un widget de Pyqt para el
builder Qt.

creado por: equipo Atenea-UBATIC
"""
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import proj3d
import matplotlib.transforms as transforms
from matplotlib.patches import Polygon

from ..core.graphsec import flecha

from PyQt5.QtCore import Qt

class Tensiones3dCanvas(FigureCanvas):
    """
    Matplotlib qt backend canvas qidget con clases para la interacción con el
    usuario para insertar dentro de un Qt GUI.

    Argumentos:
        - parent [QWidget]: QWidget que está una jerarquía arriba en el GUI
        - layout [QLayout widget]: layout widget donde estará inserto el canvas
        - grid [False / list]: False para crear el canvas sin grilla de puntos.
                               Lista [dx,dy,Nx,Ny] para crear una grilla de
                               Nx x Ny puntos espaciados dx y dy respectivamente
        - QSizePolicy [QWidget.QsizePolicy]: QsizePolicy object
        - coordsLabel [QLabel]: QLabel donde actualizar la posición del cursor
                                dentro del canvas
        - zoom_base_scale [float]: multplicador para definir el zoom interactivo

    Atributos:
        - fig [matplotlib.figure.Figure]: Matplotlib figure object
        - coordsLabel [QLabel]: QLabel donde actualizar la posición del cursor
                                dentro del canvas
        - ax [fig.axes]: objeto axes de matplotlib figure
        - grid [False or list]: False para crear el canvas sin grilla de puntos.
                                Lista [dx,dy,Nx,Ny] para crear una grilla de
                                Nx x Ny puntos espaciados dx y dy respectivamente
        - gridon [bool]: Define si la grilla está visible o no
        - grid_flatten [dict]: {'xgrid':xgrid,'ygrid':ygrid} donde xgrid e ygrid
                               son np.arrays con las coordenadas de los puntos
                               de la grilla
        - interactions [dict]: Diccionario con todos los objetos que agregan
                               interactividad al objeto axes (self.ax).
                               'nodedrawer': dibuja nodos con el mouse
                               'bardrawer': dibuja barras entre nodos existentes
                               'pan': panea clickeando el mouse y arrastrando
                               'zoom': hace zoom usando el scroll
                               'pickall': selecciona objetos del axes
                               'picknodes': selecciona objetos del axes, solo nodos
                               'pickbars': selecciona objetos del axes, solo barras
        - nodos [list]: Lista de patch.Rectanfgle de todos los nodos dibujados
                        en el canvas
        - barras [list]: Lista de mlines.Line2d de todas las barras dibujadas
                         en el canvas

    Metodos:
        - onMotion(event): Define el evento de escribir las coordenadas del mouse
                           durante el movimiento del mouse
        - enablePan(event): Define el evento de habilitar el modo 'pan' cuando
                            se oprime la tecla 'control'
        - disablePan(event): Define el evento de deshabilitar el 'pan' y volver
                             al modo previo cuando se suelta la tecla 'control'
        - setMode(mode, QCursor): conecta el modo indicado en el string 'mode'
                                  y le asigna el cursor QCursor
        - disconnectAll(): Desconecta todas las funcionalidades
        - setBounds(bounds): Setea los límites del canvas
        - drawGrid(grid): Dibuja la grilla de puntos definida por el list grid
        - setGridOn(gridon): Prende y apaga la visibilidad de la grilla según
                             el boolean 'gridon'
        - clearCanvas(): Elimina todos las barras y nodos

    """
    def __init__(self, parent=None, layout=None, grid=False, QSizePolicy=None,
                 coordsLabel=None, zoom_base_scale=0.05):


        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        if parent:
            self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        if QSizePolicy:
            FigureCanvas.setSizePolicy(self,QSizePolicy,QSizePolicy)
        # Detect key press in canvas
        FigureCanvas.setFocusPolicy(self, Qt.ClickFocus)
        FigureCanvas.setFocus(self)
        # Detect mouse movement in canvas
        self.setMouseTracking(True)

        # Label widget para escribir coordenadas del mouse en statusbar
        if coordsLabel:
            self.coordsLabel = coordsLabel

        # Initialize and format axes properties
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.ax.set_proj_type('ortho')
        self.ax.axis([0,2,0,2])
        self.ax.set_xlim3d(0,6)
        self.ax.set_ylim3d(0,6)
        self.ax.set_zlim3d(0,6)
        #self.ax.set_aspect('equal', adjustable='box', anchor='C')
        self.ax.set_axis_off()

        # Create interactive capabilities
        self.interactions = {}

        self.interactions['zoom'] = Zoom3D(self.ax,zoom_base_scale)
        self.interactions['pan'] = Pan3D(self.ax)
        self.setMode('rotate', Qt.OpenHandCursor)

        # Set pan mode with control key
        self.fig.canvas.mpl_connect('key_press_event',self.enablePan)
        self.fig.canvas.mpl_connect('key_release_event',self.disablePan)
        self.fig.canvas.mpl_connect('motion_notify_event',self.onMotion)

    def enablePan(self,event):
        """
        Habilitar paneo cuando se apreta la tecla control
        """
        if event.inaxes != self.ax: return
        if not event.key == 'control': return
        prev_mode = self.current_mode
        prev_cursor = self.current_cursor
        self.setMode('pan', Qt.ClosedHandCursor)
        self.current_mode = prev_mode
        self.current_cursor = prev_cursor

    def disablePan(self,event):
        """
        Habilitar paneo cuando se apreta la tecla control
        """
        if event.inaxes != self.ax: return
        if not event.key == 'control': return
        self.setMode(self.current_mode, self.current_cursor)

    def onMotion(self,event):
        prev_button = self.ax.button_pressed
        if event.inaxes != self.ax:
            if self.coordsLabel:
                self.coordsLabel.setText('x=  ,y=  ,z=  ')
                self.coordsLabel.update()
            return
        mousex = event.xdata
        mousey = event.ydata
        # Update del label
        if self.coordsLabel:
            if not prev_button: # Si no está haciendo rotate, imprimir coordenadas xyz
                self.ax.button_pressed = -1
            self.coordsLabel.setText(self.ax.format_coord(mousex,mousey))
            self.coordsLabel.update()
            self.ax.button_pressed = prev_button

    def setMode(self, mode, QCursor=None, **kwargs):
        self.current_mode = mode
        self.current_cursor = QCursor
        # Setear cursor
        if QCursor:
            self.setCursor(QCursor)
        # Reiniciar modos
        self.disconnectAll()
        # Setear modo
        if mode == 'pan':
            self.ax.mouse_init(rotate_btn=None, zoom_btn=None)
            self.interactions['pan'].connect()
            self.interactions['zoom'].connect()
        elif mode == 'rotate':
            self.ax.mouse_init(rotate_btn=1, zoom_btn=None)
            self.interactions['zoom'].connect()

    def disconnectAll(self):
        for interaction in self.interactions.values():
            interaction.disconnect()

    def clearCanvas(self, nodos={}, barras={}):
        # eliminar todos los artistas del ax
        self.ax.clear()
        self.ax.set_proj_type('ortho')
        self.ax.axis([0,2,0,2])
        self.ax.set_xlim3d(0,6)
        self.ax.set_ylim3d(0,6)
        self.ax.set_zlim3d(0,6)
        self.ax.set_aspect('equal', adjustable='box', anchor='C')
        self.ax.set_axis_off()

class seccionesCanvas(FigureCanvas):
    """
    """
    def __init__(self, parent=None, layout=None, QSizePolicy=None,
                 coordsLabel=None, originArrows=False):
        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        FigureCanvas.setSizePolicy(self, QSizePolicy, QSizePolicy)
        # FigureCanvas.setMaximumHeight(self, 250)
        FigureCanvas.setMinimumHeight(self, 1)

        # Label widget para escribir coordenadas del mouse en statusbar
        self.coordsLabel = coordsLabel

        # Initialize and format axes properties
        self.ax = self.fig.add_subplot(111)
        self.ax.set_frame_on(True)
        self.ax.set_aspect('equal', adjustable='datalim', anchor='C')
        self.ax.tick_params(axis='both', which = 'both', direction = 'out',
                labelsize=4, pad=0, length=4,
                bottom=True, top=True, left=True, right=True,
                labelbottom=True,labeltop=True,labelleft=True,labelright=True)
        self.ax.xaxis.set_tick_params(width=0.2)
        self.ax.yaxis.set_tick_params(width=0.2)
        for axis in ['top', 'bottom', 'left', 'right']:
            self.ax.spines[axis].set_linewidth(0.2)

        if originArrows:
            self.originArrows()
            self.ax.axis([-1,5,-1,4])

        # Connect Pan y Zoom
        self.pan = Pan(self.ax)
        self.pan.connect()
        self.zoom = Zoom(self.ax,1.2)
        self.zoom.connect()
        self.fig.canvas.mpl_connect('motion_notify_event',self.onMotion)

    def onMotion(self,event):
        if event.inaxes != self.ax:
            if self.coordsLabel:
                self.coordsLabel.setText("x= ,y= ")
                self.coordsLabel.update()
            return
        mousex = event.xdata
        mousey = event.ydata
        # Update del label
        if self.coordsLabel:
            self.coordsLabel.setText(
                    "x=%04.2f ,y=%04.2f " % (mousex, mousey))
            self.coordsLabel.update()

    def originArrows(self):
        # Transform
        self.trans = (self.ax.get_figure().dpi_scale_trans +
                      transforms.ScaledTranslation(0, 0, self.ax.transData))
        # Flechas
        self.flechax = flecha(self.ax,(0,0), (0.3,0),
                    color = 'r', forma = 'recta', text='x', fontsize=8,
                    textoffset=0.05, zorder=5)
        self.flechay = flecha(self.ax, (0,0), (0,0.3),
                    color = 'r', forma = 'recta', text='y', fontsize=8,
                    textoffset=0.05, zorder=5)
        # Setear transformación
        self.flechax.flecha[0].set_transform(self.trans)
        self.flechax.texto.set_transform(self.trans)
        self.flechay.flecha[0].set_transform(self.trans)
        self.flechay.texto.set_transform(self.trans)

    def zoom2extent(self, margin=0.1, figure_ratio=None):
        tol = 0.01
        xcoords = []
        ycoords = []
        for child in self.ax.get_children():
            if type(child) is Polygon:
                xcoords.extend(list(child.get_xy()[:,0]))
                ycoords.extend(list(child.get_xy()[:,1]))
        xmin = min(xcoords)
        xmax = max(xcoords)
        ymin = min(ycoords)
        ymax = max(ycoords)
        #
        if (xmin == xmax) and (ymin == ymax):
            return
        xmargin = (xmax-xmin)*margin + tol
        ymargin = (ymax-ymin)*margin + tol
        dx = xmax-xmin+2*xmargin
        dy = ymax-ymin+2*ymargin
        # Respetar constrained layout
        if not figure_ratio:
            figure_ratio = self.fig.get_size_inches()[1] / \
                        self.fig.get_size_inches()[0]
        if dy/dx > figure_ratio:
            dx = dy/figure_ratio
            new_margin = (dx - (xmax-xmin))/2
            self.ax.axis([xmin-new_margin, xmax+new_margin,
                          ymin-ymargin, ymax+ymargin])
        else:
            dy = dx*figure_ratio
            new_margin = (dy - (ymax-ymin))/2
            self.ax.axis([xmin-xmargin, xmax+xmargin,
                          ymin-new_margin, ymax+new_margin])
        self.fig.canvas.draw_idle()

class curvasCanvas(FigureCanvas):
    """
    """
    def __init__(self, parent=None, layout=None, QSizePolicy=None):
        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        FigureCanvas.setSizePolicy(self, QSizePolicy, QSizePolicy)
        # FigureCanvas.setMaximumHeight(self, 250)
        FigureCanvas.setMinimumHeight(self, 1)

        # Initialize and format axes properties
        self.ax = self.fig.subplots(3, 1, sharex='col')
        self.ax = list(self.ax)
        # self.ax[-1].set_xlabel('angulo',fontsize=4)
        # self.fig.subplots_adjust(hspace=0.2)
        ylabels = ['Jxy','J12','Juv']
        i = 0
        for ax in self.ax:
            ax.tick_params(axis='both', which = 'both', labelsize=4, pad=0,
                           length=2)
            ax.set_ylabel(ylabels[i], fontsize=4)
            ax.xaxis.set_tick_params(width=0.2)
            ax.yaxis.set_tick_params(width=0.2)
            for axis in ['top', 'bottom', 'left', 'right']:
                ax.spines[axis].set_linewidth(0.2)
            i += 1

class Zoom:
    """
    Clase que agrega la capacidad de hacer zoom in y out con el scroll del mouse

    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - base_scale [float]: Define la escala para agrandar o achicar el zoom

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - base_scale: mismo del argumento
        - cur_xlim: axes xlim
        - cur_ylim: axes ylim


    Métodos:
        - zoom(): Callback del 'scroll event'. Agranda y achica los limites del
                  canvas
        - connect(): Conecta los callbacks
        - disconnect(): Desconecta los callbacks

    Signals:
        -

    """
    def __init__(self, ax, base_scale = 1.3):
        self.cur_xlim = None
        self.cur_ylim = None
        self.ax = ax
        self.base_scale = base_scale

        self.fig = ax.get_figure() # get the figure of interest

        self.connect()
        self.disconnect()

    def zoom(self, event):

        cur_xlim = self.ax.get_xlim()
        cur_ylim = self.ax.get_ylim()

        xdata = event.xdata # get event x location
        ydata = event.ydata # get event y location

        if event.button == 'down':
            # deal with zoom in
            scale_factor = self.base_scale
        elif event.button == 'up':
            # deal with zoom out
            scale_factor = 1 / self.base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print (event.button)

        new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
        new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

        relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
        rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

        self.ax.set_xlim([xdata-new_width*(1-relx), xdata+new_width*(relx)])
        self.ax.set_ylim([ydata-new_height*(1-rely), ydata+new_height*(rely)])
        self.ax.figure.canvas.draw()

    def connect(self):
        self._cidscroll = self.fig.canvas.mpl_connect('scroll_event', self.zoom)

    def disconnect(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidscroll)

class Pan:
    """
    Clase que agrega la capacidad de panear en el canvas arrastrando el click
    del mouse

    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - press [set]: set con x0, y0, event.xdata, event.ydata
        - xpress [float]: Coordenada x cuando se clickea
        - ypress [float]: Coordenada y cuando se clickea
        - keypress [False o str]: nombre de la tecla que activa el pan. Si
                                  keypress=False, entonces el pan está siempre
                                  activado

    Métodos:
        - onKeyPress(): Callback del 'key press event'. Llama a connect_pan()
        - onKeyRelease(): Callback del 'key release event'. Llama a
                          disconnect_pan()
        - onPress(): Lee límites del gráfico y punto donde se presionó el mouse
        - onRelease(): Actualiza el canvas
        - onMotion(): Redefine límites del gráfico
        - connect_pan(): Conecta los callback de mouse press,release y motion
        - disconnect_pan(): Desconecta los callback de mouse press,release y motion
        - connect(keypress): Conecta los callbacks
        - disconnect(): Desconecta los callbacks

    Signals:
        -

    """
    def __init__(self,ax):
        self.ax = ax
        self.press = None
        self.xpress = None
        self.ypress = None
        self.x0 = None
        self.y0 = None

        self.fig = self.ax.get_figure() # get the figure of interest

        # El Objeto se inicializa desconectado
        self.connect()
        self.disconnect()


    def onKeyPress(self,event):
        if event.inaxes != self.ax: return
        if not event.key == self.keypress: return
        self.connect_pan()

    def onKeyRelease(self,event):
        if not event.key == self.keypress: return
        self.disconnect_pan()

    def onPress(self,event):
        if event.inaxes != self.ax: return
        if not event.button == 1: return
        self.cur_xlim = self.ax.get_xlim()
        self.cur_ylim = self.ax.get_ylim()
        self.press = event.xdata, event.ydata
        self.xpress, self.ypress = self.press

    def onRelease(self,event):
        self.press = None
        self.fig.canvas.draw_idle()

    def onMotion(self,event):
        if self.press is None: return
        if event.inaxes != self.ax: return
        dx = event.xdata - self.xpress
        dy = event.ydata - self.ypress
        self.cur_xlim -= dx
        self.cur_ylim -= dy
        self.ax.set_xlim(self.cur_xlim)
        self.ax.set_ylim(self.cur_ylim)

        self.fig.canvas.draw_idle()

    def connect(self, keypress=False):
        """connect events"""
        self.keypress = keypress
        if not self.keypress:
            self.connect_pan()
            return
        self._cidkeypress = self.fig.canvas.mpl_connect('key_press_event',
                                                        self.onKeyPress)
        self._cidkeyrelease = self.fig.canvas.mpl_connect('key_release_event',
                                                        self.onKeyRelease)

    def disconnect(self):
        if not self.keypress:
            self.disconnect_pan()
            return
        self.fig.canvas.mpl_disconnect(self._cidkeypress)
        self.fig.canvas.mpl_disconnect(self._cidkeyrelease)

    def connect_pan(self):
        self._cidmotion = self.fig.canvas.mpl_connect('motion_notify_event',
                                                  self.onMotion)
        self._cidpress = self.fig.canvas.mpl_connect('button_press_event',
                                                 self.onPress)
        self._cidrelease = self.fig.canvas.mpl_connect('button_release_event',
                                                   self.onRelease)

    def disconnect_pan(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidmotion)
        self.fig.canvas.mpl_disconnect(self._cidpress)
        self.fig.canvas.mpl_disconnect(self._cidrelease)

class Zoom3D:
    """
    Clase que agrega la capacidad de hacer zoom in y out con el scroll del mouse

    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - base_scale [float]: Define la escala para agrandar o achicar el zoom

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - base_scale: mismo del argumento
        - cur_xlim: axes xlim
        - cur_ylim: axes ylim


    Métodos:
        - zoom(): Callback del 'scroll event'. Agranda y achica los limites del
                  canvas
        - connect(): Conecta los callbacks
        - disconnect(): Desconecta los callbacks

    Signals:
        -

    """

    def __init__(self, ax, base_scale = 1.3):

        super().__init__()

        self.cur_xlim = None
        self.cur_ylim = None
        self.ax = ax
        self.base_scale = base_scale

        self.fig = ax.get_figure() # get the figure of interest

        self.connect()
        self.disconnect()

    def get_xyz(self, xd, yd):
        """
        Given the 2D view coordinates attempt to guess a 3D coordinate.
        Looks for the nearest edge to the point and then assumes that
        the point is at the same z location as the nearest point on the edge.
        """

        if self.ax.M is None:
            return ''

        # if self.ax.button_pressed in self.ax._rotate_btn:
        #     return 'azimuth=%d deg, elevation=%d deg ' % (self.ax.azim, self.ax.elev)
        #     # ignore xd and yd and display angles instead

        p = (xd, yd)
        edges = self.ax.tunit_edges()
        #lines = [proj3d.line2d(p0,p1) for (p0,p1) in edges]
        ldists = [(proj3d.line2d_seg_dist(p0, p1, p), i) for \
                i, (p0, p1) in enumerate(edges)]
        ldists.sort()
        # nearest edge
        edgei = ldists[0][1]

        p0, p1 = edges[edgei]

        # scale the z value to match
        x0, y0, z0 = p0
        x1, y1, z1 = p1
        d0 = np.hypot(x0-xd, y0-yd)
        d1 = np.hypot(x1-xd, y1-yd)
        dt = d0+d1
        z = d1/dt * z0 + d0/dt * z1

        x, y, z = proj3d.inv_transform(xd, yd, z, self.ax.M)

        xs = self.ax.format_xdata(x)
        ys = self.ax.format_ydata(y)
        zs = self.ax.format_zdata(z)
        return x, y, z

    def zoom(self, event):

        cur_xlim = self.ax.get_xlim()
        cur_ylim = self.ax.get_ylim()
        cur_zlim = self.ax.get_zlim()
        xdata, ydata, zdata = self.get_xyz(event.xdata,event.ydata)

        if event.button == 'down':
            # deal with zoom in
            scale_factor = self.base_scale
        elif event.button == 'up':
            # deal with zoom out
            scale_factor = 1 / self.base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print (event.button)

        new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
        new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor
        new_deep = (cur_zlim[1] - cur_zlim[0]) * scale_factor

        relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
        rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])
        relz = (cur_zlim[1] - zdata)/(cur_zlim[1] - cur_zlim[0])

        self.ax.set_xlim([xdata-new_width*(1-relx), xdata+new_width*(relx)])
        self.ax.set_ylim([ydata-new_height*(1-rely), ydata+new_height*(rely)])
        self.ax.set_zlim([zdata-new_deep*(1-relz), zdata+new_deep*(relz)])

        self.ax.figure.canvas.draw()

    def connect(self):
        self._cidscroll = self.fig.canvas.mpl_connect('scroll_event', self.zoom)

    def disconnect(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidscroll)

class Pan3D:
    """
    Clase que agrega la capacidad de panear en el canvas arrastrando el click
    del mouse

    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - press [set]: set con x0, y0, event.xdata, event.ydata
        - xpress [float]: Coordenada x cuando se clickea
        - ypress [float]: Coordenada y cuando se clickea
        - keypress [False o str]: nombre de la tecla que activa el pan. Si
                                  keypress=False, entonces el pan está siempre
                                  activado

    Métodos:
        - onKeyPress(): Callback del 'key press event'. Llama a connect_pan()
        - onKeyRelease(): Callback del 'key release event'. Llama a
                          disconnect_pan()
        - onPress(): Lee límites del gráfico y punto donde se presionó el mouse
        - onRelease(): Actualiza el canvas
        - onMotion(): Redefine límites del gráfico
        - connect_pan(): Conecta los callback de mouse press,release y motion
        - disconnect_pan(): Desconecta los callback de mouse press,release y motion
        - connect(keypress): Conecta los callbacks
        - disconnect(): Desconecta los callbacks

    Signals:
        -

    """
    def __init__(self,ax):
        self.ax = ax
        self.press = None
        self.xpress = None
        self.ypress = None
        self.x0 = None
        self.y0 = None

        self.fig = self.ax.get_figure() # get the figure of interest

        # El Objeto se inicializa desconectado
        self.connect()
        self.disconnect()

    def get_xyz(self, xd, yd):
        """
        Given the 2D view coordinates attempt to guess a 3D coordinate.
        Looks for the nearest edge to the point and then assumes that
        the point is at the same z location as the nearest point on the edge.
        """

        if self.ax.M is None:
            return ''

        if self.ax.button_pressed in self.ax._rotate_btn:
            return 'azimuth=%d deg, elevation=%d deg ' % (self.ax.azim, self.ax.elev)
            # ignore xd and yd and display angles instead

        p = (xd, yd)
        edges = self.ax.tunit_edges()
        #lines = [proj3d.line2d(p0,p1) for (p0,p1) in edges]
        ldists = [(proj3d.line2d_seg_dist(p0, p1, p), i) for \
                i, (p0, p1) in enumerate(edges)]
        ldists.sort()
        # nearest edge
        edgei = ldists[0][1]
        p0, p1 = edges[edgei]

        # scale the z value to match
        x0, y0, z0 = p0
        x1, y1, z1 = p1
        d0 = np.hypot(x0-xd, y0-yd)
        d1 = np.hypot(x1-xd, y1-yd)
        dt = d0+d1
        z = d1/dt * z0 + d0/dt * z1

        x, y, z = proj3d.inv_transform(xd, yd, z, self.ax.M)

        xs = self.ax.format_xdata(x)
        ys = self.ax.format_ydata(y)
        zs = self.ax.format_zdata(z)
        return x, y, z

    def onKeyPress(self,event):
        if event.inaxes != self.ax: return
        if not event.key == self.keypress: return
        self.connect_pan()

    def onKeyRelease(self,event):
        if not event.key == self.keypress: return
        self.disconnect_pan()

    def onPress(self,event):
        if event.inaxes != self.ax: return
        if not event.button == 1: return
        self.cur_xlim = self.ax.get_xlim()
        self.cur_ylim = self.ax.get_ylim()
        self.cur_zlim =self.ax.get_zlim()
        # self.press = event.xdata, event.ydata
        # self.xpress, self.ypress = self.press
        self.xpress, self.ypress, self.zpress = self.get_xyz(event.xdata,event.ydata)
        self.press = self.xpress, self.ypress, self.zpress

    def onRelease(self,event):
        self.press = None
        self.fig.canvas.draw_idle()

    def onMotion(self,event):
        if self.press is None: return
        if event.inaxes != self.ax: return
        xf, yf, zf = self.get_xyz(event.xdata, event.ydata)
        dx = xf - self.xpress
        dy = yf - self.ypress
        dz = zf - self.zpress
        self.cur_xlim -= dx
        self.cur_ylim -= dy
        self.cur_zlim -= dz
        self.ax.set_xlim(self.cur_xlim)
        self.ax.set_ylim(self.cur_ylim)
        self.ax.set_zlim(self.cur_zlim)
        self.fig.canvas.draw_idle()

    def connect(self, keypress=False):
        """connect events"""
        self.keypress = keypress
        if not self.keypress:
            self.connect_pan()
            return
        self._cidkeypress = self.fig.canvas.mpl_connect('key_press_event',
                                                        self.onKeyPress)
        self._cidkeyrelease = self.fig.canvas.mpl_connect('key_release_event',
                                                        self.onKeyRelease)

    def disconnect(self):
        if not self.keypress:
            self.disconnect_pan()
            return
        self.fig.canvas.mpl_disconnect(self._cidkeypress)
        self.fig.canvas.mpl_disconnect(self._cidkeyrelease)

    def connect_pan(self):
        self._cidmotion = self.fig.canvas.mpl_connect('motion_notify_event',
                                                  self.onMotion)
        self._cidpress = self.fig.canvas.mpl_connect('button_press_event',
                                                 self.onPress)
        self._cidrelease = self.fig.canvas.mpl_connect('button_release_event',
                                                   self.onRelease)

    def disconnect_pan(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidmotion)
        self.fig.canvas.mpl_disconnect(self._cidpress)
        self.fig.canvas.mpl_disconnect(self._cidrelease)