# module postprocess.py
"""
Created on Sun Jul 22 12:57:31 2018

@author: Mariano
"""
import numpy as np
from .sectionlib import Figura,Seccion

def save_txt(filename, figuras):
    f = open(filename,"w+")

    for key, figura in figuras.items():
        f.write("#" + "\t" + str(key)+"\n"+"tipo" + "\t" + figura.tipo['Tipo'] + "\t" + figura.tipo['Prop'] + "\n")
        string = "geometria" + "\n"
        for key2 in figura.geometria:
            string += key2
            if type(figura.geometria[key2]) is np.ndarray:
                for i in range(0,np.size(figura.geometria[key2])):
                    string += "\t"
                    string += str(figura.geometria[key2][i])
                string += "\n"
            elif type(figura.geometria[key2]) is list:
                for point in figura.geometria[key2]:
                    if type(point) is list:
                        for coord in point:
                            string += "\t"
                            string += str(coord)
                    else:
                        string += "\t"
                        string += str(point)
                string += "\n"
            else:
                string += "\t"
                string += str(figura.geometria[key2])
                string += "\n"
        f.write(string)

    f.close()

def read_txt(filename):
    f = open(filename,"r")
    tipo = []
    geometria = []
    nfig = []
    counter = -1
    for line in f:
        l = line.rstrip("\n")
        l = l.split("\t")
        if l[0] == '#':
            nfig.append(0)
            geometria.append(0)
            tipo.append(0)
            counter += 1
            nfig[counter] = int(l[1])
            geometria[counter] = {}
            tipo[counter] = {}
        if l[0] == 'tipo':
            tipo[counter].update({'Tipo':l[1],'Prop':l[2]})
        if l[0] == 'g':
            geometria[counter].update({l[0]:np.array([float(l[1]),float(l[2])])})
        if l[0] == 'A':
            geometria[counter].update({l[0]:np.array([float(l[1]),float(l[2])])})
        if l[0] == 'B':
            geometria[counter].update({l[0]:np.array([float(l[1]),float(l[2])])})
        if l[0] == 'C':
            geometria[counter].update({l[0]:np.array([float(l[1]),float(l[2])])})
        if l[0] == 'D':
            geometria[counter].update({l[0]:np.array([float(l[1]),float(l[2])])})
        if l[0] == 'alpha':
            geometria[counter].update({l[0]:float(l[1])})
        if l[0] == 'b':
            geometria[counter].update({l[0]:float(l[1])})
        if l[0] == 'h':
            geometria[counter].update({l[0]:float(l[1])})
        if l[0] == 'd':
            geometria[counter].update({l[0]:float(l[1])})
        if l[0] == 'xy':
            xy = []
            for i in range(1,int((len(l)-1)/2+1)):
                xy.append([float(l[2*i-1]),float(l[2*i])])
            geometria[counter].update({l[0]:np.array(xy)})
        if l[0] == 'discr':
            geometria[counter].update({l[0]:int(l[1])})
    figs = {}
    for i in range(0,counter+1):
        fign = Figura(tipo[i],geometria[i])
        figs.update({nfig[i]:fign})
    seccionn = Seccion(figs)

    return seccionn, figs