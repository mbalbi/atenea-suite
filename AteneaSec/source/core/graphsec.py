# module graphsec.py
"""
Módulo de funciones de ploteo de secciones.

creado por: equipo Atenea-UBATIC
"""
             
import numpy as np
import matplotlib.tri as tri
import matplotlib.pyplot as plt
from matplotlib.text import Text
from matplotlib.patches import Polygon
from matplotlib.patches import FancyArrowPatch
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from shapely.geometry import Polygon as shplypol
from shapely.geometry import Point
from mpl_toolkits.mplot3d.art3d import Line3DCollection
from matplotlib.collections import LineCollection
import matplotlib

def shiftedColorMap(cmap, start=0, midpoint=0.5, stop=1.0, name='shiftedcmap'):
    '''
    Function to offset the "center" of a colormap. Useful for
    data with a negative min and positive max and you want the
    middle of the colormap's dynamic range to be at zero.

    Input
    -----
      cmap : The matplotlib colormap to be altered
      start : Offset from lowest point in the colormap's range.
          Defaults to 0.0 (no lower offset). Should be between
          0.0 and `midpoint`.
      midpoint : The new center of the colormap. Defaults to 
          0.5 (no shift). Should be between 0.0 and 1.0. In
          general, this should be  1 - vmax / (vmax + abs(vmin))
          For example if your data range from -15.0 to +5.0 and
          you want the center of the colormap at 0.0, `midpoint`
          should be set to  1 - 5/(5 + 15)) or 0.75
      stop : Offset from highest point in the colormap's range.
          Defaults to 1.0 (no upper offset). Should be between
          `midpoint` and 1.0.
    '''
    cdict = {
        'red': [],
        'green': [],
        'blue': [],
        'alpha': []
    }

    # regular index to compute the colors
    reg_index = np.linspace(start, stop, 257)

    # shifted index to match the data
    shift_index = np.hstack([
        np.linspace(0.0, midpoint, 128, endpoint=False), 
        np.linspace(midpoint, 1.0, 129, endpoint=True)
    ])

    for ri, si in zip(reg_index, shift_index):
        r, g, b, a = cmap(ri)

        cdict['red'].append((si, r, r))
        cdict['green'].append((si, g, g))
        cdict['blue'].append((si, b, b))
        cdict['alpha'].append((si, a, a))

    newcmap = matplotlib.colors.LinearSegmentedColormap(name, cdict)
    plt.register_cmap(cmap=newcmap)

    return newcmap

class flecha():

    def __init__(self, axes, pos1, pos2, forma, color, text='', textoffset=0,
                 fontsize=12, zorder=1):

        style ="Simple,tail_width=1,head_width=8,head_length=8"
        kw = dict(arrowstyle=style, color=color, linewidth=0.5, zorder=zorder)
        if forma == 'recta':
            self.flecha = [FancyArrowPatch(pos1,pos2,**kw)]
        elif forma == 'recta_doble':
            flecha1 = FancyArrowPatch(pos1,pos2,**kw)
            pos22 = [pos1[0]+(pos2[0]-pos1[0])*0.8,pos1[1]+(pos2[1]-pos1[1])*0.8]
            flecha2 = FancyArrowPatch(pos1,pos22,**kw)
            self.flecha = [flecha1, flecha2]
        elif forma == 'curva -':
            self.flecha = [FancyArrowPatch(pos1,pos2,connectionstyle="arc3,rad=-0.8",**kw)]
        elif forma == 'curva +':
            self.flecha = [FancyArrowPatch(pos1,pos2,connectionstyle="arc3,rad=0.8",**kw)]
        for artist in self.flecha:
            axes.add_patch(artist)
        # Plot texto
        self.length = np.sqrt((pos2[1]-pos1[1])**2 + (pos2[0]-pos1[0])**2)
        postext = [pos1[0]+(pos2[0]-pos1[0])*(1+textoffset),
                   pos1[1]+(pos2[1]-pos1[1])*(1+textoffset)]
        self.texto = Text(postext[0], postext[1], text=text, fontsize=fontsize,
                          color=color, zorder=zorder)
        axes.add_artist(self.texto)

    def set_visible(self, visible):
        for artist in self.flecha:
            artist.set_visible(visible)
        self.texto.set_visible(visible)
        
    def remove(self):
        for artist in self.flecha:
            artist.remove()
        self.texto.remove()

def plot_seccion(seccion, ax=None, princ=True, uv=True, length_scale=0.2,
                 fontsize=12, colors=['k','b','g','r'], textoffset=0.1):
    """
    """
    if not ax:
        fig, ax = plt.subplots()
    
    secartists = []
    for xy in seccion.lleno:
        facecol=(0.6, 0.6, 0.6, 1)
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        secartists.append(artist)
    
    for xy in seccion.vacio:
        facecol=(1, 1, 1, 1)
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        secartists.append(artist)
        
    for xy in seccion.error:
        facecol=(1, 0, 0, 1)
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        secartists.append(artist)
    seccion.sec_artists = secartists

    ax.autoscale(True)
    ax.set_aspect('equal')
    
    # Plot ejes yz
    seccion.yz_artists = plot_yz(ax, seccion, length_scale=length_scale, 
                         fontsize=fontsize, color=colors[0], 
                         textoffset=textoffset)
    # Plot ejes principales
    seccion.princ_artists = plot_princ(ax, seccion, length_scale=length_scale, 
                                       fontsize=fontsize, color=colors[1],
                                       textoffset=textoffset)
    # Plot ejes uv
    seccion.uv_artists = plot_uv(ax, seccion, length_scale=length_scale, 
                         fontsize=fontsize, color=colors[2],
                         textoffset=textoffset)
    # # Plotear ejes globales
    # xy_artists = plot_Global(ax, seccion, length_scale=0.7*length_scale, 
    #                          fontsize=fontsize, color=colors[3],
    #                          textoffset=textoffset/2)
    # Plot baricentro
    seccion.g_artists = ax.plot(seccion.coords['g'][0], seccion.coords['g'][1],
                        color='k', marker='x')
    
    if not princ:
        for artist in seccion.princ_artists:
            artist.set_visible(False)
    if not uv:
        for artist in seccion.uv_artists:
            artist.set_visible(False)
    
    return ax

def plot_yz(ax, seccion, length_scale=0.2, fontsize=12, color='k', textoffset=0):
    # Obtener dimensiones máximas de seccion
    XY = np.empty((0,2))
    for artist in seccion.sec_artists:
        xy = artist.get_xy()
        XY = np.vstack((XY, xy))
    dx = np.max(XY[:,0]) - np.min(XY[:,0])
    dy = np.max(XY[:,1]) - np.min(XY[:,1])
    DF = 1.5*max(dx,dy)
    # DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
    #          abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    flechaz = flecha(ax,seccion.coords['g'],
                (seccion.coords['g'][0]-DF*length_scale, seccion.coords['g'][1]),
                 color = color, forma = 'recta', text='y', fontsize=fontsize,
                 textoffset=textoffset)
    flechay = flecha(ax,seccion.coords['g'],
                (seccion.coords['g'][0],seccion.coords['g'][1]-DF*length_scale),
                color = color,forma = 'recta', text='z', fontsize=fontsize,
                textoffset=textoffset)
    
    return [flechaz,flechay]

def plot_princ(ax, seccion, length_scale=0.2, fontsize=12, color='b', textoffset=0):
    """
    """

    # Plot flechas
    RotMat = np.array([[np.cos(seccion.princ['theta']*np.pi/180),
                            -np.sin(seccion.princ['theta']*np.pi/180)],
                       [np.sin(seccion.princ['theta']*np.pi/180),
                            np.cos(seccion.princ['theta']*np.pi/180)]])
    # Obtener dimensiones máximas de seccion
    XY = np.empty((0,2))
    for artist in seccion.sec_artists:
        xy = artist.get_xy()
        XY = np.vstack((XY, xy))
    dx = np.max(XY[:,0]) - np.min(XY[:,0])
    dy = np.max(XY[:,1]) - np.min(XY[:,1])
    DF = 1.2*max(dx,dy)
    # DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
    #          abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    
    if seccion.ejes[2]==1:
        labela = '1'
        labelb = '2'
        
    else: 
        labela = '2'
        labelb = '1'
    
    posf = np.transpose(np.dot(RotMat,
            np.transpose((seccion.coords['g'][0]-DF*length_scale,
            seccion.coords['g'][1]))-np.transpose(seccion.coords['g']))) + \
            seccion.coords['g']
    flechaa = flecha(ax, seccion.coords['g'], posf, color=color, 
                          forma='recta', text=labela, fontsize=fontsize,
                          textoffset=textoffset)
    
    posf = np.transpose(np.dot(RotMat,
        np.transpose((seccion.coords['g'][0],seccion.coords['g'][1]-DF*length_scale)) - \
        np.transpose(seccion.coords['g'])))+seccion.coords['g']
    flechab = flecha(ax,seccion.coords['g'], posf, color=color, 
                          forma='recta', text=labelb, fontsize=fontsize,
                          textoffset=textoffset)
    

    return [flechaa,flechab]

def plot_uv(ax, seccion, length_scale=0.2, fontsize=1, color='g', textoffset=0):

    RotMat = np.array([[np.cos(seccion.uv['beta']*np.pi/180),
                            -np.sin(seccion.uv['beta']*np.pi/180)],    
                        [np.sin(seccion.uv['beta']*np.pi/180), 
                            np.cos(seccion.uv['beta']*np.pi/180)]])
    
    # Obtener dimensiones máximas de seccion
    XY = np.empty((0,2))
    for artist in seccion.sec_artists:
        xy = artist.get_xy()
        XY = np.vstack((XY, xy))
    dx = np.max(XY[:,0]) - np.min(XY[:,0])
    dy = np.max(XY[:,1]) - np.min(XY[:,1])
    DF = 1.8*max(dx,dy)
    # DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
    #          abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    
    # Plot flechas
    posf = np.transpose(np.dot(RotMat,
        np.transpose((seccion.coords['g'][0]-DF*length_scale,
                      seccion.coords['g'][1])) - \
        np.transpose(seccion.coords['g'])))+seccion.coords['g']
    flechav = flecha(ax, seccion.coords['g'], posf, color=color, forma='recta',
                     text='u', fontsize=fontsize, textoffset=textoffset)
    
    posf = np.transpose(np.dot(RotMat,
        np.transpose((seccion.coords['g'][0],seccion.coords['g'][1]-DF*length_scale)) - \
        np.transpose(seccion.coords['g'])))+seccion.coords['g']
    flechau = flecha(ax,seccion.coords['g'], posf, color=color, forma='recta',
                     text='v', fontsize=fontsize, textoffset=textoffset)
             

    return [flechau,flechav]

def plot_Mom(ax, seccion, length_scale=0.2, fontsize=1, color='g', textoffset=0):
    
    # Plot flechas    
    DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
             abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    
    Mom = max(abs(seccion.solicitaciones['Mz']),abs(seccion.solicitaciones['My']))
    
    posf1 = seccion.coords['g']+length_scale*DF*seccion.solicitaciones['My']*np.array([-1,0])/Mom
    flechaa = flecha(ax, seccion.coords['g'], posf1, color=color, 
                          forma='recta_doble', text='My', fontsize=fontsize,
                          textoffset=textoffset)
    
    posf2 = seccion.coords['g']+length_scale*DF*seccion.solicitaciones['Mz']*np.array([0,-1])/Mom
    flechab = flecha(ax,seccion.coords['g'], posf2, color=color, 
                          forma='recta_doble', text='Mz', fontsize=fontsize,
                          textoffset=textoffset)
    
    posf3 = -seccion.coords['g']+posf1+posf2
    flechac = flecha(ax,seccion.coords['g'], posf3, color=color, 
                          forma='recta_doble', text='MR', fontsize=fontsize,
                          textoffset=textoffset)
    
    return [flechaa,flechab,flechac]
    
def plot_Global(ax, seccion, length_scale=0.2, fontsize=12, color='r', textoffset=0):
    DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
             abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    
    flechax = flecha(ax,(0,0), (DF*length_scale,0),
                 color = color, forma = 'recta', text='X', fontsize=fontsize,
                 textoffset=textoffset)
    flechay = flecha(ax, (0,0), (0,DF*length_scale),
                color = color,forma = 'recta', text='Y', fontsize=fontsize,
                textoffset=textoffset)
                      
    return [flechax,flechay]

    
def plot_juv(seccion, ax=None, juv=True, j12=True, colors=['b','g']):
    
    # Calcular
    j1 = seccion.props['jy']
    j2 = seccion.props['jz']
    j12 = seccion.props['jyz']
    theta = []
    ju = []
    jv = []
    juv = []
    for i in range (0,3600):
        theta.append(i/10-180)
        ju.append(((j1 + j2)/2) + ((j1-j2)/2) *np.cos(2*theta[i]*np.pi/180) - \
                    j12*np.sin(2*theta[i]*np.pi/180))
        jv.append(((j1 + j2)/2) - ((j1-j2)/2) *np.cos(2*theta[i]*np.pi/180) + \
                    j12*np.sin(2*theta[i]*np.pi/180))
        juv.append(((j1-j2)/2) *np.sin(2*theta[i]*np.pi/180) + \
                    j12*np.cos(2*theta[i]*np.pi/180))
    
    # Plotear
    if not ax:
        fig, ax = plt.subplots(3,sharex=True)
    ax[0].plot(theta,ju)
    ax[1].plot(theta,jv)
    ax[2].plot(theta,juv)
    plt.xlabel('Theta (deg)')
    ax[0].set_ylabel('ju')
    ax[1].set_ylabel('jv')
    ax[2].set_ylabel('juv')
    ax[0].get_figure().align_ylabels
    plt.xlim((-180,180))
    mrktheta = []
    mrkbeta = []
    for i in range(3):
        ax[i].axvline(x=0,color='k')
        ax[i].grid()
        mrktheta.append(ax[i].axvline(x=seccion.princ['theta'], color=colors[0]))
        mrkbeta.append(ax[i].axvline(x=seccion.uv['beta'], color=colors[1]))
    
    if juv:
        for i in range(3):
            mrkbeta[i].set_visible(True)
            
    if j12:
        for i in range(3):
            mrktheta[i].set_visible(True)

    return mrktheta, mrkbeta, ax
    
def plot_sigmas(seccion, ax=None, princ=True, mom=True, en=True, lf=True, cp=True,
                par=True, tot=True, rscale=0.1, length_scale=0.2,
                fontsize=12, size = 50, colors=['k','b','g'], textoffset=0.1):
    """
    """
    if not ax:
        fig, ax = plt.subplots()
    
    x = []
    y = []
    z = []
    artists = []
    
    for index in seccion.sigmas:
            x.append(index[0][0])
            y.append(index[0][1])
            z.append(index[1])
    
    # cbar_min = -np.max(np.absolute(np.array(z)))
    # cbar_max = np.max(np.absolute(np.array(z)))
    cbar_max = max(np.array(z))
    cbar_min = min(np.array(z))
    if cbar_min>0:
        cbar_min = min(cbar_min,0)
    elif cbar_max<0:
        cbar_max = max(cbar_max,0)
    
    orig_cmap = matplotlib.cm.coolwarm
    mid = 1 - cbar_max / (cbar_max + abs(cbar_min))
    shifted_cmap = shiftedColorMap(orig_cmap, midpoint=mid, name='shifted')
    
    for xy in seccion.vacio:
        facecol=(1, 1, 1, 1)
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        artists += [artist]
    
    for xy in seccion.lleno:
        facecol=(0.6, 0.6, 0.6, 0)
        clippath = Path(xy)
        patch = PathPatch(clippath, facecolor='none')
        ax.add_patch(patch)
        triang = tri.Triangulation(x, y)
        artist2 = ax.tricontourf(triang,z, 100, cmap=shifted_cmap)  
        for c in artist2.collections:
            c.set_clip_path(patch)
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        artists += [artist]
    
    for xy in seccion.error:
        facecol=(1, 0, 0, 1)
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        artists += [artist]
    
    cbar = None
    if cbar_max != cbar_min:
        cbar = ax.get_figure().colorbar(artist2)
        cbar.ax.tick_params(labelsize=fontsize-2)

    ax.autoscale(True)
    ax.set_aspect('equal')
    
    # Plot ejes yz
    yz_artists = plot_yz(ax, seccion, length_scale=length_scale, 
                         fontsize=fontsize, color=colors[0], textoffset=textoffset)
    artists += yz_artists
    # Plot ejes principales
    princ_artists = plot_princ(ax, seccion, length_scale=length_scale, 
                         fontsize=fontsize, color=colors[1], textoffset=textoffset)
    artists += princ_artists

    if seccion.Mprinc['M1'] != 0 or seccion.Mprinc['M2'] != 0:
        mom_artists = plot_Mom(ax, seccion, length_scale=length_scale/2, 
                         fontsize=fontsize, color=colors[2], textoffset=2*textoffset)
        en_artists = plot_EN(ax, seccion, fontsize=fontsize, color=colors[0],
                            textoffset=textoffset/8)
        lf_artists = plot_LF(ax, seccion, fontsize=fontsize, color=colors[0],
                            textoffset=textoffset/8)
        tot_artists = plot_totsigmas(ax, seccion, rscale = rscale, fontsize=fontsize,
                                    color=colors[1], textoffset=textoffset/100)
        par_artists = plot_parsigmas(ax, seccion, rscale = rscale, fontsize=fontsize,
                                    color=colors[0], textoffset=textoffset/100)
        artists += mom_artists + en_artists + lf_artists + tot_artists + par_artists
    else:
        mom_artists = []
        en_artists = []
        lf_artists = []
        tot_artists = []
        if seccion.Mprinc['N'] != 0:
            par_artists = plot_parsigmas(ax, seccion, rscale = rscale,
                                    fontsize=fontsize, color=colors[0], 
                                    textoffset=textoffset/100)
            artists += par_artists
        else:
            par_artists = []
            
    if seccion.Mprinc['N'] != 0:
        cp_artists = plot_CP(ax, seccion, size=size,fontsize=fontsize,
                             color=colors[0], textoffset=textoffset/100)
        artists += cp_artists
    else:
        cp_artists = []
    
    ax.autoscale(True)
    ax.set_aspect('equal')
    
    A2 = abs(ax.get_ylim()[0]-ax.get_ylim()[1])
    A1 = abs(ax.get_xlim()[0]-ax.get_xlim()[1])
    if A1 > A2:
        ax.set_ylim([ax.get_ylim()[0]-(1.1*A1-A2)*(1/2),
                     ax.get_ylim()[1]+(1.1*A1-A2)*(1/2)])
        ax.set_xlim([ax.get_xlim()[0]-(0.1*A1)*(1/2),
                    ax.get_xlim()[1]+(0.1*A1)*(1/2)])
    else:
        ax.set_xlim([ax.get_xlim()[0]-(1.1*A2-A1)*(1/2),
                    ax.get_xlim()[1]+(1.1*A2-A1)*(1/2)])
        ax.set_ylim([ax.get_ylim()[0]-(0.1*A2)*(1/2),
                    ax.get_ylim()[1]+(0.1*A2)*(1/2)])
    
    if not princ:
        for artist in princ_artists:
            artist.set_visible(False)
    if not mom:
        for artist in mom_artists:
            artist.set_visible(False)
    if not en:
        for artist in en_artists:
            artist.set_visible(False)
    if not lf:
        for artist in lf_artists:
            artist.set_visible(False)
    if not cp:
        for artist in cp_artists:
            artist.set_visible(False)
    if not par:
        for artist in par_artists:
            artist.set_visible(False)
    if not tot:
        for artist in tot_artists:
            artist.set_visible(False)
    
    artists_dict = {'mom':mom_artists,'en':en_artists, 'lf':lf_artists,
                    'cp':cp_artists, 'par':par_artists, 'tot':tot_artists,
                    'princ':princ_artists}
    
    return artists, artists_dict, cbar, ax


def plot_LF(ax, seccion, fontsize=12, color='k', textoffset=0):
        DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
             abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
        line = Polygon([(seccion.coords['g']-(DF/2)*seccion.aux['LF']),
                        (seccion.coords['g']+(DF/2)*seccion.aux['LF'])],
                        lw=1,ls=':', color=color)

        postext = [(seccion.coords['g']+((2*textoffset+1)*DF/2)*seccion.aux['LF'])]
        texto = Text(postext[0][0], postext[0][1], text='LF', fontsize=fontsize, 
                          color=color)
        ax.add_artist(texto)
        ax.add_patch(line)
        
        return [line,texto]
        
def plot_EN(ax, seccion, fontsize=12, color='k', textoffset=0):
        DF = np.sqrt((ax.get_ylim()[0]-ax.get_ylim()[1])**2+(ax.get_xlim()[0]-ax.get_xlim()[1])**2)
        line = Polygon([(seccion.aux['Int']-(DF/2)*seccion.aux['EN']),(seccion.aux['Int']+(DF/2)*seccion.aux['EN'])],lw=1,ls='-',color=color)

        postext = [(seccion.aux['Int']-((2*textoffset+1)*DF/2)*seccion.aux['EN'])]
        texto = Text(postext[0][0], postext[0][1], text='EN', fontsize=fontsize, 
                          color=color)
        ax.add_artist(texto)
        ax.add_patch(line)
        
        return [line,texto]
        
def plot_CP(ax, seccion, size = 50, fontsize=12, color='k', textoffset=0.002):
    punto = seccion.aux['CP']
    if seccion.Mprinc['N'] > 0:
        style = 'o'
    else:
        style = 'X'
    CP = ax.scatter(punto[0],punto[1],color=color,s=size,marker=style)     
    postext = [(punto - textoffset*np.array([1,1]))]
    texto = Text(postext[0][0], postext[0][1], text='CP', fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto)
    
    return [CP,texto]

def plot_parsigmas(ax, seccion, rscale = 0.08, fontsize=12, color='k', textoffset=0.002):

    d1 = []
    d2 = []
    d3 = []
    d4 = []
    z1 = []
    z2 = []
    z3 = []

    DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
        abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    scale = rscale*DF

    for index in seccion.sigmas:
        dd1 = np.dot(seccion.coords['g']-index[0],seccion.ejes[0])
        d1.append(dd1)
        dd2 = np.dot(seccion.coords['g']-index[0],seccion.ejes[1])
        d2.append(dd2)
        dd3 = np.dot(seccion.coords['g']-index[0],np.array([0,1]))
        d3.append(dd3)
        dd4 = np.dot(seccion.coords['g']-index[0],np.array([1,0]))
        d4.append(dd4)
        sigma1 = -seccion.ejes[2]*((seccion.Mprinc['M1']*dd2/seccion.princ['j1']))
        sigma2 = seccion.ejes[2]*((seccion.Mprinc['M2']*dd1/seccion.princ['j2']))
        sigma3 = seccion.Mprinc['N']/seccion.props['a']
        z1.append(sigma1)
        z2.append(sigma2)
        z3.append(sigma3)
            
    zmax1 = np.max(np.absolute(np.array(z1)))
    zmax2 = np.max(np.absolute(np.array(z2)))
    zmax3 = np.max(np.absolute(np.array(z3)))
    zmax = max(zmax1,zmax2,zmax3)
    lxy1 = []
    lxy2 = []
    lxy3 = []
    for i in range (0,len(z1)):
        lxy1.append([d2[i],scale*z1[i]/zmax])
        lxy2.append([d1[i],scale*z2[i]/zmax])
        lxy3.append([d3[i],scale*z3[i]/zmax])
    lxy1.sort(key = lambda lxy1: lxy1[0])
    lxy1.insert(0,[lxy1[0][0],0])
    lxy1.append([lxy1[-1][0],0])
    
    lxy2.sort(key = lambda lxy2: lxy2[0])
    lxy2.insert(0,[lxy2[0][0],0])
    lxy2.append([lxy2[-1][0],0])
    
    lxy3.sort(key = lambda lxy3: lxy3[0])
    lxy3.insert(0,[lxy3[0][0],0])
    lxy3.append([lxy3[-1][0],0])
    
    xy1 = np.array(lxy1)
    xy2 = np.array(lxy2)
    xy3 = np.array(lxy3)
    
    if seccion.ejes[2]==1:
        alpha1 = seccion.princ['theta']+90
        alpha2 = seccion.princ['theta']
        
    else:
        alpha1 = seccion.princ['theta']
        alpha2 = seccion.princ['theta']+90
		
    alpha3 = -90
    
    
    RotMat2 = np.array([[np.cos(alpha2*np.pi/180),
                            -np.sin(alpha2*np.pi/180)],    
                        [np.sin(alpha2*np.pi/180), 
                            np.cos(alpha2*np.pi/180)]])
    
    RotMat1 = np.array([[np.cos(alpha1*np.pi/180),
                            -np.sin(alpha1*np.pi/180)],    
                        [np.sin(alpha1*np.pi/180), 
                            np.cos(alpha1*np.pi/180)]])
    
    RotMat3 = np.array([[np.cos(alpha3*np.pi/180),
                            -np.sin(alpha3*np.pi/180)],    
                        [np.sin(alpha3*np.pi/180), 
                            np.cos(alpha3*np.pi/180)]])
    
    xy2 = np.transpose(np.dot(RotMat2,np.transpose(xy2)))+seccion.coords['g'] + 1.4*np.max(np.absolute(np.array(d2)))*seccion.ejes[1]
    
    xy1 = np.transpose(np.dot(RotMat1,np.transpose(xy1)))+seccion.coords['g'] + 1.4*np.max(np.absolute(np.array(d1)))*seccion.ejes[0]
    
    xy3 = np.transpose(np.dot(RotMat3,np.transpose(xy3))) + seccion.coords['g'] + 1.4*np.max(np.absolute(np.array(d4)))*np.array([1,0])
    
    line2 = Polygon(xy2,True,lw=2,ls='-',color=color,alpha=0.5)
    line1 = Polygon(xy1,True,lw=2,ls='-',color=color,alpha=0.5)
    line3 = Polygon(xy3,True,lw=2,ls='-',color=color,alpha=0.5)
    
    postext = xy1[1]
    texto1 = Text(postext[0], postext[1], text=str(format(zmax*lxy1[1][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto1)
    texto1.set_visible(True)
    
    postext = xy1[-2]
    texto2 = Text(postext[0], postext[1], text=str(format(zmax*lxy1[-2][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto2)
    texto2.set_visible(True)
    
    postext = xy2[1]
    texto3 = Text(postext[0], postext[1], text=str(format(zmax*lxy2[1][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto3)
    texto3.set_visible(True)
    
    postext = xy2[-2]
    texto4 = Text(postext[0], postext[1], text=str(format(zmax*lxy2[-2][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto4)
    texto4.set_visible(True)
    
    postext = xy3[-2]
    texto5 = Text(postext[0], postext[1], text=str(format(zmax*lxy3[-2][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto5)
    texto5.set_visible(True)
    
    ax.add_patch(line1)
    ax.add_patch(line2)
    ax.add_patch(line3)
    
    return [line1,line2,line3,texto1,texto2,texto3,texto4,texto5]
    
def plot_totsigmas(ax, seccion, rscale = 0.1, fontsize=12, color='b', textoffset=0.002):
    
    d1 = []
    d2 = []
    z1 = []
    
    DF = max(abs(ax.get_ylim()[0]-ax.get_ylim()[1]),
            abs(ax.get_xlim()[0]-ax.get_xlim()[1]))
    scale = rscale*DF
    
    alpha = -np.arctan(seccion.aux['EN'][0]/seccion.aux['EN'][1])*180/np.pi-180
    
    RotMat = np.array([[np.cos(alpha*np.pi/180),
                            -np.sin(alpha*np.pi/180)],    
                        [np.sin(alpha*np.pi/180), 
                            np.cos(alpha*np.pi/180)]])
    
    RotMat2 = np.array([[np.cos((alpha+180)*np.pi/180),
                            -np.sin((alpha+180)*np.pi/180)],    
                        [np.sin((alpha+180)*np.pi/180), 
                            np.cos((alpha+180)*np.pi/180)]])
    
    EN = np.transpose(np.dot(RotMat,np.transpose([1,0])))

    for index in seccion.sigmas:
        dd1 = np.dot(seccion.coords['g']-index[0],EN)
        d1.append(dd1)
        dd2 = np.dot(seccion.coords['g']-index[0],np.array([-EN[1],EN[0]]))
        d2.append(dd2)
        z1.append(index[1])
            
    zmax = np.max(np.absolute(np.array(z1)))
    lxy1 = []

    for i in range (0,len(z1)):
        lxy1.append([d1[i],scale*z1[i]/zmax])
    lxy1.sort(key = lambda lxy1: lxy1[0])
    lxy1.insert(0,[lxy1[0][0],0])
    lxy1.append([lxy1[-1][0],0])
    
    xy1 = np.array(lxy1)
    
    xy1 = np.transpose(np.dot(RotMat2,np.transpose(xy1)))+seccion.coords['g'] + 1.4*np.max(np.absolute(np.array(d2)))*-np.array([-EN[1],EN[0]])
    
    line1 = Polygon(xy1,True,lw=2,ls='-',color=color,alpha=0.5)
    
    postext = xy1[1]
    texto1 = Text(postext[0], postext[1], text=str(format(zmax*lxy1[1][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto1)
    texto1.set_visible(True)
    
    postext = xy1[-2]
    texto2 = Text(postext[0], postext[1], text=str(format(zmax*lxy1[-2][1]/scale,"<6.2E")), fontsize=fontsize, 
                          color=color)
    ax.add_artist(texto2)
    texto2.set_visible(True)
    
    ax.add_patch(line1)
    
    return [line1,texto1,texto2]
    
def plot_fig(figura,ax=None,length_scale=0.2, fontsize=12, color='r',
             textoffset=0.1, facecol=(0.6, 0.6, 0.6, 1)):
    """
    """
    if not ax:
        fig, ax = plt.subplots()
        xy_artists = plot_Global(ax, figura, length_scale=2*length_scale, 
                         fontsize=fontsize, color=color, textoffset=textoffset/2)
    
    artists = []
    if figura.tipo['Tipo'] == 'Rect':
        poly = shplypol([tuple(figura.coords['A']),
                         tuple(figura.coords['B']),
                         tuple(figura.coords['C']),
                         tuple(figura.coords['D'])])
    
        if figura.tipo['Prop'] == 'Lleno':
            facecol=facecol
        
        if figura.tipo['Prop'] == 'Hueco':
            facecol=(1,1,1,1)
            
        artist = Polygon(np.array(list(poly.exterior.coords)), True, fc=facecol,
                        ec='black', lw=0.5) 
        ax.add_patch(artist)
        artists.append(artist)
        
    if figura.tipo['Tipo'] == 'Tri':
        poly = shplypol([tuple(figura.coords['A']),
                         tuple(figura.coords['B']),
                         tuple(figura.coords['C'])])
    
        if figura.tipo['Prop'] == 'Lleno':
            facecol=facecol
        
        if figura.tipo['Prop'] == 'Hueco':
            facecol=(1,1,1,1)
            
        artist = Polygon(np.array(list(poly.exterior.coords)), True, fc=facecol,
                         ec='black', lw=0.5) 
        ax.add_patch(artist)
        artists.append(artist)
        
    if figura.tipo['Tipo'] == 'Circ':
        poly = Point(tuple(
            figura.coords['g'])).buffer(figura.geometria['d']/2)
        
        if figura.tipo['Prop'] == 'Lleno':
            facecol=facecol
        
        if figura.tipo['Prop'] == 'Hueco':
            facecol=(1,1,1,1)
        
        xy = np.array(list(poly.exterior.coords))
        artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.5) 
        ax.add_patch(artist)
        artists.append(artist)
        
    if figura.tipo['Tipo'] == 'Poly':
        cord = []
        for x in range(len(figura.coords['poly'])):
            cord.append(tuple(figura.coords['poly'][x]))
        poly2 = shplypol(cord)
        
        a = figura.discr['xf']
        b = figura.discr['yf']
        for x in range(len(figura.discr['xy'])):
            A = tuple(figura.discr['xy'][x] + np.array([-a/2,-b/2]))
            B = tuple(figura.discr['xy'][x] + np.array([a/2,-b/2]))
            C = tuple(figura.discr['xy'][x] + np.array([a/2,b/2]))
            D = tuple(figura.discr['xy'][x] + np.array([-a/2,b/2]))
            poly = shplypol([A,B,C,D])
            
            if figura.tipo['Prop'] == 'Lleno':
                facecol=facecol
            
            if figura.tipo['Prop'] == 'Hueco':
                facecol=(1,1,1,1)
            
            xy = np.array(list(poly.exterior.coords))
            artist = Polygon(xy, True, fc=facecol, ec='black', lw=0.25) 
            ax.add_patch(artist)
            artists.append(artist)
            
        xy = np.array(list(poly2.exterior.coords))
        artist2 = Polygon(xy, True, fc=(1,1,1,0), ec='black', lw=0.5)
        ax.add_patch(artist2)
        artists.append(artist2)
    
    # Plotear centro de gravedad
    gartist = ax.plot(figura.coords['g'][0], figura.coords['g'][1],
                      color='k', marker='x', markersize=4)

    return artists, gartist[0], ax
    
def plot_sigma3d(seccion, ax=None, length_scale=0.2, fontsize=12, 
                 colors=['k','b','k']):
    
    if not ax:
        fig = plt.figure()
        ax  = fig.add_subplot(111,projection='3d')
        
    Mprinc = seccion.Mprinc
    ejes = seccion.ejes
    
    sigmax = []
    sigmin = []
    xmax = []
    xmin = []
    ymax = []
    ymin = []

    for xy in seccion.lleno:
        verts = []
        verts2 = []
        for coord in xy:
            dist1 = np.dot(coord-seccion.coords['g'],ejes[0])
            dist2 = np.dot(coord-seccion.coords['g'],ejes[1])
            sigma = (Mprinc['N']/seccion.props['a'])+ejes[2]*((Mprinc['M1']*dist2/seccion.princ['j1'])-(Mprinc['M2']*dist1/seccion.princ['j2']))
            
            verts.append((coord[0], coord[1]))
            verts2.append((coord[0], coord[1], sigma))
            
        poly  = LineCollection([verts], alpha=0.5,colors=colors[0])
        poly.set_facecolor([0.6,0.6,0.6])
            
        poly2  = Line3DCollection([verts2], alpha=0.5,colors=colors[1])
        poly2.set_facecolor('b')

        ax.add_collection3d(poly, zs=0)
        ax.add_collection3d(poly2)
        
        sigmax.append(max(np.array(verts2)[:,2]))
        sigmin.append(min(np.array(verts2)[:,2]))
        xmax.append(max(np.array(verts2)[:,0]))
        xmin.append(min(np.array(verts2)[:,0]))
        ymax.append(max(np.array(verts2)[:,1]))
        ymin.append(min(np.array(verts2)[:,1]))
        
    ax.autoscale(True)
    sig_min = np.min(sigmin)
    sig_max = np.max(sigmax)
    x_min = np.min(xmin)
    x_max = np.max(xmax)
    y_min = np.min(ymin)
    y_max = np.max(ymax)

    ax.set_zlim([min(sig_min,0),max(sig_max,0)])
    ax.set_xlim([min(x_min,y_min),max(x_max,y_max)])
    ax.set_ylim([min(x_min,y_min),max(x_max,y_max)])
    
    DF = np.sqrt((ax.get_ylim()[0]-ax.get_ylim()[1])**2+(ax.get_xlim()[0]-ax.get_xlim()[1])**2)
    P1 = seccion.aux['Int']-(DF/2)*seccion.aux['EN']
    P2 = seccion.aux['Int']+(DF/2)*seccion.aux['EN']
    LN = []
    if not np.isnan(P1).any():
        LN.append((P1[0], P1[1]))
        LN.append((P2[0], P2[1]))
        
        en  = LineCollection([LN], alpha=1,colors=colors[2])
        en.set_facecolor('k')
    
        ax.add_collection3d(en, zs=0)
        
    ax.set_axis_off()
    
    return ax