# module sigmalib.py
"""
Módulo de libreria de tensiones.

creado por: equipo Atenea-UBATIC
"""

import numpy as np 

class Sigmas:
    
    def __init__(self,seccion,solicitaciones):
        self.seccion = seccion
        self.solicitaciones = solicitaciones
        self.sigmas = {}
        self.Mprinc = {}
        self.defejes()
        self.computeMprinc()
        self.computeSigma()
    
    def defejes(self):
        seccion = self.seccion
        theta = -seccion.princ['theta']
        
        RotMat = np.array([[np.cos(theta*np.pi/180),
                        -np.sin(theta*np.pi/180)],    
                        [np.sin(theta*np.pi/180), 
                         np.cos(theta*np.pi/180)]])
    
        if seccion.princ['j1'] > seccion.props['jy']:
            eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,1]))))
            eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([1,0]))))
            signo = 1
        
        elif seccion.princ['j1'] < seccion.props['jy']:
            eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,1]))))
            eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([1,0]))))  
            signo = -1
        
        elif seccion.princ['j1'] == seccion.props['jy']:
            if seccion.props['jy'] < seccion.props['jz']:
                eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,1]))))
                eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([1,0]))))
                signo = -1
                
            else:
                eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,1]))))
                eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([1,0]))))
                signo = 1
        self.ejes = [eje1,eje2,signo]
        
    def computeMprinc(self):
        solicitaciones = self.solicitaciones
        ejes = self.ejes
        
        M1 = np.dot([solicitaciones['My'],solicitaciones['Mz']],ejes[0])
        M2 = np.dot([solicitaciones['My'],solicitaciones['Mz']],ejes[1])
        self.Mprinc = {'N':solicitaciones['N'],'M1':M1,'M2':M2}
                
    def computeSigma(self):
        Mprinc = self.Mprinc
        seccion = self.seccion
        ejes = self.ejes

        sigmas = {}
        for key in seccion.figs:
            for key2 in seccion.figs[key].coords:
                dist1 = np.dot(seccion.figs[key].coords[key2]-seccion.coords['g'],ejes[0])
                dist2 = np.dot(seccion.figs[key].coords[key2]-seccion.coords['g'],ejes[1])
                sigma = (Mprinc['N']/seccion.props['a'])+ejes[2]*((Mprinc['M1']*dist2/seccion.princ['j1'])-(Mprinc['M2']*dist1/seccion.princ['j2']))
                sigmas.update({str(key)+key2:sigma})
        self.sigmas = sigmas