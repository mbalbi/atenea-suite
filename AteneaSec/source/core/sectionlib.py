# módulo sectionlib.py
"""
Módulo de librería de secciones.

creado por: equipo de Atenea-UBATIC
"""
import numpy as np 
import matplotlib.path    as mpath
from shapely.geometry import Polygon as shplypol
from shapely.geometry import Point
from shapely.ops import polygonize, unary_union

from .graphsec import plot_seccion, plot_uv, plot_juv, plot_sigmas, plot_fig
from .graphsec import plot_sigma3d

class Figura:
    """
    Clase que define las figuras que forman secciones.
    
    Argumentos:
        - coords [list]: Lista con las coordenadas del baricentro [Z,Y]
        - tipo [string]: Tipo de figura (i.e., 'Rect', 'Cir','Pol')
        - b [float]: Base/dimensión en y (para 'Tipo':'Rect')
        - h [float]: Altura/dimensión en z (para 'Tipo':'Rect'). 
        - d [float]: Diámetro (para 'Tipo':'Cir')
        - prop [string]: lleno ('Lleno') o hueco ('Hueco')
        
    Atributos:
        - props:
        - coords:
    
    """
    def __init__(self,tipo,geometria):
        """
        """
        self.tipo = tipo
        self.geometria = geometria
        self.coords = {}
        self.props = {}
        self.computeProps()
        
    def computeProps(self):
        geometria = self.geometria
        coords = self.coords
        tipo = self.tipo
        if tipo['Tipo'] == 'Rect':
            # revisar que no falte informacion
            requeridos = {'h','b'}
            if not requeridos <= set(geometria):
                raise ValueError('Revisar input de la seccion')
            else:
                if tipo['Prop'] == 'Lleno':  
                    area = geometria['h']*geometria['b']
                    j1 = (geometria['h'] ** 3) * geometria['b'] / 12
                    j2 = (geometria['b'] ** 3) * geometria['h'] / 12
                    j12 = 0
                    jy = ((j1 + j2)/2) + \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jz = ((j1 + j2)/2) - \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jyz = ((j1-j2)/2) *np.sin(-2*geometria['alpha']*np.pi/180) \
                          + j12*np.cos(-2*geometria['alpha']*np.pi/180)
                    self.props = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                elif tipo['Prop'] == 'Hueco':
                    area = -1*geometria['h']*geometria['b']
                    j1 = -1*(geometria['h'] ** 3) * geometria['b'] / 12
                    j2 = -1*(geometria['b'] ** 3) * geometria['h'] / 12
                    j12 = -1*0
                    jy = ((j1 + j2)/2) + \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jz = ((j1 + j2)/2) - \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jyz = ((j1-j2)/2)*np.sin(-2*geometria['alpha']*np.pi/180) + \
                          j12*np.cos(-2*geometria['alpha']*np.pi/180)
                    self.props = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                else:
                    raise ValueError('Revisar input de la seccion')
                    
            RotMat = np.array([[np.cos(geometria['alpha']*np.pi/180),
                                    -np.sin(geometria['alpha']*np.pi/180)],
                               [np.sin(geometria['alpha']*np.pi/180),
                                    np.cos(geometria['alpha']*np.pi/180)]])
                    
            if 'g' in geometria.keys():
                G = np.array(geometria['g'])
                A = np.array(geometria['g']) + (np.array([-geometria['b']/2,-geometria['h']/2]))
                B = np.array(geometria['g']) + (np.array([geometria['b']/2,-geometria['h']/2]))
                C = np.array(geometria['g']) + (np.array([geometria['b']/2,geometria['h']/2]))
                D = np.array(geometria['g']) + (np.array([-geometria['b']/2,geometria['h']/2]))
                aux = G
                
            elif 'A' in geometria.keys():
                G = np.array(geometria['A']) + (np.array([geometria['b']/2,geometria['h']/2]))
                A = np.array(geometria['A'])
                B = np.array(geometria['A']) + (np.array([geometria['b'],0]))
                C = np.array(geometria['A']) + (np.array([geometria['b'],geometria['h']]))
                D = np.array(geometria['A']) + (np.array([0,geometria['h']]))
                aux = A
                
            elif 'B' in geometria.keys():
                G = np.array(geometria['B']) + (np.array([-geometria['b']/2,geometria['h']/2]))
                A = np.array(geometria['B']) + (np.array([-geometria['b'],0]))
                B = np.array(geometria['B'])
                C = np.array(geometria['B']) + (np.array([0,geometria['h']]))
                D = np.array(geometria['B']) + (np.array([-geometria['b'],geometria['h']]))
                aux = B
                
            elif 'C' in geometria.keys():
                G = np.array(geometria['C']) + (np.array([-geometria['b']/2,-geometria['h']/2]))
                A = np.array(geometria['C']) + (np.array([-geometria['b'],-geometria['h']]))
                B = np.array(geometria['C']) + (np.array([0,-geometria['h']]))
                C = np.array(geometria['C'])
                D = np.array(geometria['C']) + (np.array([-geometria['b'],0]))
                aux = C

            elif 'D' in geometria.keys():
                G = np.array(geometria['D']) + (np.array([geometria['b']/2,-geometria['h']/2]))
                A = np.array(geometria['D']) + (np.array([0,-geometria['h']]))
                B = np.array(geometria['D']) + (np.array([geometria['b'],-geometria['h']]))
                C = np.array(geometria['D']) + (np.array([geometria['b'],0]))
                D = np.array(geometria['D'])
                aux = D

            self.coords = {'g':np.transpose(np.dot(RotMat,
                                    np.transpose(G)-np.transpose(aux)))+aux,
                        'A':np.transpose(np.dot(RotMat,
                                    np.transpose(A)-np.transpose(aux)))+aux, 
                        'B':np.transpose(np.dot(RotMat,
                                    np.transpose(B)-np.transpose(aux)))+aux, 
                        'C':np.transpose(np.dot(RotMat,
                                    np.transpose(C)-np.transpose(aux)))+aux, 
                        'D':np.transpose(np.dot(RotMat,
                                    np.transpose(D)-np.transpose(aux)))+aux}

        elif tipo['Tipo'] == 'Circ':
            # revisar que no falte informacion
            requeridos = {'d'}
            if not requeridos <= set(geometria):
                raise ValueError('Revisar input de la seccion')
                
            else:
                if tipo['Prop'] == 'Lleno':  
                    area = np.pi*((geometria['d']) ** 2) / 4
                    j1 = np.pi*(geometria['d'] ** 4) /64
                    j2 = np.pi*(geometria['d'] ** 4) /64
                    j12 = 0
                    jy = ((j1 + j2)/2) + \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jz = ((j1 + j2)/2) - \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jyz = ((j1-j2)/2)*np.sin(-2*geometria['alpha']*np.pi/180) + \
                          j12*np.cos(-2*geometria['alpha']*np.pi/180)
                    self.props = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                elif tipo['Prop'] == 'Hueco':
                    area = -1*np.pi*((geometria['d']) ** 2) / 4
                    j1 = -1*np.pi*(geometria['d'] ** 4) /64
                    j2 = -1*np.pi*(geometria['d'] ** 4) /64
                    j12 = -1*0
                    jy = ((j1 + j2)/2) + \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jz = ((j1 + j2)/2) - \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) + \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jyz = ((j1-j2)/2)*np.sin(-2*geometria['alpha']*np.pi/180) + \
                          j12*np.cos(-2*geometria['alpha']*np.pi/180)
                    self.props = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                else:
                    raise ValueError('Revisar input de la seccion')
                    
            RotMat = np.array([[np.cos(geometria['alpha']*np.pi/180),
                                    -np.sin(geometria['alpha']*np.pi/180)],
                               [np.sin(geometria['alpha']*np.pi/180),
                                    np.cos(geometria['alpha']*np.pi/180)]])
                    
            if 'g' in geometria.keys():
                G = np.array(geometria['g'])
                A = np.array(geometria['g']) + (np.array([-geometria['d']/2,0]))
                B = np.array(geometria['g']) + (np.array([0,-geometria['d']/2]))
                C = np.array(geometria['g']) + (np.array([geometria['d']/2,0]))
                D = np.array(geometria['g']) + (np.array([0,geometria['d']/2]))
                aux = G
                
            elif 'A' in geometria.keys():
                G = np.array(geometria['A']) + (np.array([geometria['d']/2,0]))
                A = np.array(geometria['A'])
                B = np.array(geometria['A']) + (np.array([geometria['d']/2,-geometria['d']/2]))
                C = np.array(geometria['A']) + (np.array([geometria['d'],0]))
                D = np.array(geometria['A']) + (np.array([geometria['d']/2,geometria['d']/2]))
                aux = A
                
            elif 'D' in geometria.keys():
                G = np.array(geometria['D']) + (np.array([0,-geometria['d']/2]))
                A = np.array(geometria['D']) + (np.array([-geometria['d']/2,geometria['d']/2]))
                B = np.array(geometria['D'])
                C = np.array(geometria['D']) + (np.array([geometria['d']/2,geometria['d']/2]))
                D = np.array(geometria['D']) + (np.array([0,-geometria['d']]))
                aux = D
                
            elif 'C' in geometria.keys():
                G = np.array(geometria['C']) + (np.array([-geometria['d']/2,0]))
                A = np.array(geometria['C']) + (np.array([-geometria['d'],0]))
                B = np.array(geometria['C']) + (np.array([-geometria['d']/2,-geometria['d']/2]))
                C = np.array(geometria['C'])
                D = np.array(geometria['C']) + (np.array([-geometria['d']/2,geometria['d']/2]))
                aux = C

            elif 'B' in geometria.keys():
                G = np.array(geometria['B']) + (np.array([0,geometria['d']/2]))
                A = np.array(geometria['B']) + (np.array([-geometria['d']/2,-geometria['d']/2]))
                B = np.array(geometria['B']) + (np.array([0,geometria['d']]))
                C = np.array(geometria['B']) + (np.array([geometria['d']/2,-geometria['d']/2]))
                D = np.array(geometria['B'])
                aux = B

            self.coords = {'g':np.transpose(np.dot(RotMat,
                                    np.transpose(G)-np.transpose(aux)))+aux,
                        'A':np.transpose(np.dot(RotMat,
                                    np.transpose(A)-np.transpose(aux)))+aux, 
                        'B':np.transpose(np.dot(RotMat,
                                    np.transpose(B)-np.transpose(aux)))+aux, 
                        'C':np.transpose(np.dot(RotMat,
                                    np.transpose(C)-np.transpose(aux)))+aux, 
                        'D':np.transpose(np.dot(RotMat,
                                    np.transpose(D)-np.transpose(aux)))+aux}

        elif tipo['Tipo'] == 'Tri':
            # revisar que no falte informacion
            requeridos = {'h','b'}
            if not requeridos <= set(geometria):
                raise ValueError('Revisar input de la seccion')
                
            else:
                if tipo['Prop'] == 'Lleno':  
                    area = geometria['h']*geometria['b']/2
                    j1 = (geometria['h'] ** 3) * geometria['b'] / 36
                    j2 = (geometria['b'] ** 3) * geometria['h'] / 36
                    j12 = -(geometria['b'] ** 2) * (geometria['h'] ** 2) / 72
                    jy = ((j1 + j2)/2) + \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jz = ((j1 + j2)/2) - \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jyz = ((j1-j2)/2)*np.sin(-2*geometria['alpha']*np.pi/180) + \
                          j12*np.cos(-2*geometria['alpha']*np.pi/180)
                    self.props = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                elif tipo['Prop'] == 'Hueco':
                    area = -geometria['h']*geometria['b']/2
                    j1 = -(geometria['h'] ** 3) * geometria['b'] / 36
                    j2 = -(geometria['b'] ** 3) * geometria['h'] / 36
                    j12 = (geometria['b'] ** 2) * (geometria['h'] ** 2) / 72
                    jy = ((j1 + j2)/2) + \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jz = ((j1 + j2)/2) - \
                         ((j1-j2)/2)*np.cos(-2*geometria['alpha']*np.pi/180) - \
                         j12*np.sin(-2*geometria['alpha']*np.pi/180)
                    jyz = ((j1-j2)/2)*np.sin(-2*geometria['alpha']*np.pi/180) + \
                          j12*np.cos(-2*geometria['alpha']*np.pi/180)
                    self.props = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                else:
                    raise ValueError('Revisar input de la seccion')
                    
            RotMat = np.array([[np.cos(geometria['alpha']*np.pi/180),
                                    -np.sin(geometria['alpha']*np.pi/180)], 
                               [np.sin(geometria['alpha']*np.pi/180),
                                    np.cos(geometria['alpha']*np.pi/180)]])
                    
            if 'g' in geometria.keys():
                G = np.array(geometria['g'])
                A = np.array(geometria['g']) + (np.array([-geometria['b']/3, -geometria['h']/3]))
                B = np.array(geometria['g']) + (np.array([2*geometria['b']/3, -geometria['h']/3]))
                C = np.array(geometria['g']) + (np.array([-geometria['b']/3, 2*geometria['h']/3]))
                aux = G
                
            elif 'A' in geometria.keys():
                G = np.array(geometria['A']) + (np.array([geometria['b']/3, geometria['h']/3]))
                A = np.array(geometria['A'])
                B = np.array(geometria['A']) + (np.array([geometria['b'],0]))
                C = np.array(geometria['A']) + (np.array([0,geometria['h']]))
                aux = A
                
            elif 'B' in geometria.keys():
                G = np.array(geometria['B']) + (np.array([-2*geometria['b']/3,geometria['h']/3]))
                A = np.array(geometria['B']) + (np.array([-geometria['b'],0]))
                B = np.array(geometria['B'])
                C = np.array(geometria['B']) + (np.array([-geometria['b'],geometria['h']]))
                aux = B
                
            elif 'C' in geometria.keys():
                G = np.array(geometria['C']) + (np.array([geometria['b']/3,-2*geometria['h']/3]))
                A = np.array(geometria['C']) + (np.array([0,-geometria['h']]))
                B = np.array(geometria['C']) + (np.array([geometria['b'],-geometria['h']]))
                C = np.array(geometria['C'])
                aux = C
            
            self.coords = {'g':np.transpose(np.dot(RotMat,
                                    np.transpose(G)-np.transpose(aux)))+aux,
                        'A':np.transpose(np.dot(RotMat,
                                    np.transpose(A)-np.transpose(aux)))+aux,
                        'B':np.transpose(np.dot(RotMat,
                                    np.transpose(B)-np.transpose(aux)))+aux,
                        'C':np.transpose(np.dot(RotMat,
                                    np.transpose(C)-np.transpose(aux)))+aux}
                                    
        elif tipo['Tipo'] == 'Poly':
            # Agregar codigo de agus aca!!
            # revisar que no falte informacion
            requeridos = {'xy','discr'}
            if not requeridos <= set(geometria):
                raise ValueError('Revisar input de la seccion')
                
            else:
                xy   = self.geometria['xy']
                discr = self.geometria['discr']
                poly = np.array(xy)
                # limites de la grilla
                xmin = np.min(poly[:,0])
                xmax = np.max(poly[:,0])
                ymin = np.min(poly[:,1])
                ymax = np.max(poly[:,1])
                # puntos extremos de las fibras
                xcorner = np.linspace(xmin,xmax,discr+1)
                ycorner = np.linspace(ymin,ymax,discr+1)
                # puntos baricentricos de las fibras
                xcenter = (xcorner[0:discr]+xcorner[1:discr+1])/2
                ycenter = (ycorner[0:discr]+ycorner[1:discr+1])/2
                # definir grilla
                xc,yc = np.meshgrid(xcenter,ycenter)
                xc    = xc.flatten()
                yc    = yc.flatten()
                # definir path e identificar fibras dentro de la seccion
                path = mpath.Path(xy)
                points = np.column_stack((xc,yc))
                inside = path.contains_points(points)
                plist = np.where(inside)[0]
                xinside = xc[plist]
                yinside = yc[plist]
                # definir propiedades de las fibras
                xf = (xmax-xmin)/discr
                yf = (ymax-ymin)/discr
                af = xf*yf
                coords = np.array([xinside,yinside])
                coords = coords.T
                nfib   = np.size(coords,0)
                # Determinar baricentro
                area = nfib*af
                Sx   = 0
                Sy   = 0
                for ii in range(nfib):
                    Sx = Sx + af*coords[ii,1]
                    Sy = Sy + af*coords[ii,0]
                    
                xg = Sy/area
                yg = Sx/area
                G  = np.array([xg,yg])
                
                # Determinar momentos de inercia
                coordsyz = G-coords
                jy     = 0
                jz     = 0
                jyz    = 0
                for ii in range(nfib):
                    jy = jy+af*coordsyz[ii,1]**2
                    jz = jz+af*coordsyz[ii,0]**2
                    jyz = jyz+af*coordsyz[ii,1]*coordsyz[ii,0]
                
                # Resumir datos
                self.props  = {'a':area,'jy':jy,'jz':jz,'jyz':jyz}
                self.coords = {'g':G,'poly':poly}
                self.discr  = {'xy':coords,'xf':xf,'yf':yf}
    
    def plot(self, ax=None, length_scale=0.2, fontsize=12, color='r', 
                 textoffset=0.1, facecol=(0.6, 0.6, 0.6, 1)):
        self.artists, self.gartist, self.ax = plot_fig(self, ax=ax, 
                        length_scale=length_scale, fontsize=fontsize, 
                        color=color, textoffset=textoffset, facecol=facecol)

class Seccion:
    """
    """
    def __init__(self,figs):
        self.figs = figs
        self.props = {}
        self.princ = {}
        self.vacio = []
        self.lleno = []
        self.error = []
        self.cbar = []
        self.sigma_artists_dict = {}
        self.sigma_artists = []
        self.computeProps()
        self.defejes()
        self.createpolys()
        self.RotAxis(0)
        
        
    def computeProps(self):
        gg = np.array([0,0])
        aa = 0
        jjz = 0
        jjy = 0
        jjyz = 0
        for fig in self.figs.values():
            aa = aa + fig.props['a']
            gg = gg +  fig.coords['g']*fig.props['a']
        gg = gg/aa
        for fig in self.figs.values():
            jjz = jjz + fig.props['jz'] + fig.props['a']*((fig.coords['g'][0]-gg[0])**2)
            jjy = jjy + fig.props['jy'] + fig.props['a']*((fig.coords['g'][1]-gg[1])**2)
            jjyz = jjyz + fig.props['jyz'] + \
                fig.props['a']*(fig.coords['g'][1]-gg[1])*(fig.coords['g'][0]-gg[0])
        if jjyz == 0:
            theta = 0
        elif jjy == jjz:
            theta = 45
        else:
            theta = np.arctan(-2*jjyz/(jjy-jjz))/2*180/np.pi
        jj1 = (jjy + jjz)/2 + np.sqrt(((jjy-jjz)/2)**2+jjyz**2)
        jj2 = (jjy + jjz)/2 - np.sqrt(((jjy-jjz)/2)**2+jjyz**2)
        self.coords = {'g':gg}
        self.props = {'a':aa,'jy':jjy,'jz':jjz,'jyz':jjyz}
        self.princ = {'j1':jj1,'j2':jj2,'theta':theta}
        
    def createpolys(self):
        seccion_pol = {}
        seccion = self
        for key, figura in seccion.figs.items():
            if figura.tipo['Tipo'] == 'Rect':
                poly = shplypol([tuple(figura.coords['A']),
                                 tuple(figura.coords['B']),
                                 tuple(figura.coords['C']),
                                 tuple(figura.coords['D'])])
            if figura.tipo['Tipo'] == 'Tri':
                poly = shplypol([tuple(figura.coords['A']),
                                 tuple(figura.coords['B']),
                                 tuple(figura.coords['C'])])
            if figura.tipo['Tipo'] == 'Circ':
                poly = Point(tuple(
                    figura.coords['g'])).buffer(figura.geometria['d']/2)
            if figura.tipo['Tipo'] == 'Poly':
                cord = []
                for x in range(len(figura.coords['poly'])):
                    cord.append(tuple(figura.coords['poly'][x]))
                poly = shplypol(cord)
                
            seccion_pol[key] = poly
            
        ext_list = [x.exterior for x in seccion_pol.values()]
        interpolys = list(polygonize(unary_union(ext_list)))
            
        for poly in interpolys:
            xy= np.array(list(poly.exterior.coords))
            checkfill = 0
            for key, figura in seccion_pol.items():
                if (poly.intersection(figura)).contains(poly) == True:
                    if seccion.figs[key].tipo['Prop'] == 'Lleno':
                        checkfill = checkfill + 1
                    if seccion.figs[key].tipo['Prop'] == 'Hueco':
                        checkfill = checkfill - 1
            if checkfill == 0:
                self.vacio.append(xy)
            if checkfill == 1:
                self.lleno.append(xy)
            if checkfill not in [0,1]: 
                self.error.append(xy)

    def RotAxis(self,beta):
        """
        """
        j1 = self.props['jy']
        j2 = self.props['jz']
        j12 = self.props['jyz']
        ju = ((j1 + j2)/2) + ((j1-j2)/2) *np.cos(2*beta*np.pi/180) - \
             j12*np.sin(2*beta*np.pi/180)
        jv = ((j1 + j2)/2) - ((j1-j2)/2) *np.cos(2*beta*np.pi/180) + \
             j12*np.sin(2*beta*np.pi/180)
        juv = ((j1-j2)/2) *np.sin(2*beta*np.pi/180) + \
              j12*np.cos(2*beta*np.pi/180)
        self.uv = {'a':self.props['a'],'ju':ju,'jv':jv,'juv':juv,'beta':beta}
        
    def RotSec(self,gamma):
        """
        """
        j1 = self.props['jy']
        j2 = self.props['jz']
        j12 = self.props['jyz']
        jy = ((j1 + j2)/2) + ((j1-j2)/2) *np.cos(-2*gamma*np.pi/180) - \
             j12*np.sin(-2*gamma*np.pi/180)
        jz = ((j1 + j2)/2) - ((j1-j2)/2) *np.cos(-2*gamma*np.pi/180) + \
             j12*np.sin(-2*gamma*np.pi/180)
        jyz = ((j1-j2)/2)*np.sin(-2*gamma*np.pi/180) + \
              j12*np.cos(-2*gamma*np.pi/180)
        self.gamma = {'a':self.props['a'],'jy':jy,'jz':jz,'jyz':jyz}

    def defejes(self):
        theta = self.princ['theta']
        
        RotMat = np.array([[np.cos(theta*np.pi/180),
                        -np.sin(theta*np.pi/180)],    
                        [np.sin(theta*np.pi/180), 
                         np.cos(theta*np.pi/180)]])
        
        j1 = self.props['jy']
        j2 = self.props['jz']
        j12 = self.props['jyz']
        """
		reviso si la inercia aumenta o disminuye con el angulo theta
		para definir cual eje es j1 y cual j2
		"""
        ju1 = ((j1 + j2)/2) + ((j1-j2)/2) *np.cos(theta*np.pi/180) - \
             j12*np.sin(theta*np.pi/180)
        ju2 = ((j1 + j2)/2) + ((j1-j2)/2) *np.cos(2*theta*np.pi/180) - \
             j12*np.sin(2*theta*np.pi/180)
    
        if ju2 > ju1:
            eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([-1,0]))))
            eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,-1]))))
            signo = 1
        
        elif ju2 < ju1:
            eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([-1,0]))))
            eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,-1]))))  
            signo = -1
        
        else:
            if self.props['jy'] < self.props['jz']:
                eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([-1,0]))))
                eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,-1]))))
                signo = -1
            else:
                eje1 = np.transpose(np.dot(RotMat,np.transpose(np.array([-1,0]))))
                eje2 = np.transpose(np.dot(RotMat,np.transpose(np.array([0,-1]))))
                signo = 1
        self.ejes = [eje1,eje2,signo]
        
    def computeMprinc(self,solicitaciones):
        ejes = self.ejes
        
        M1 = np.dot([-solicitaciones['My'],-solicitaciones['Mz']],ejes[0])
        M2 = np.dot([-solicitaciones['My'],-solicitaciones['Mz']],ejes[1])
        self.Mprinc = {'N':solicitaciones['N'],'M1':M1,'M2':M2}        
                
    def computeSigma(self, My=0, Mz=0, N=0):
        self.solicitaciones = {'My':My, 'Mz':Mz, 'N':N}
        self.computeMprinc(self.solicitaciones)
        Mprinc = self.Mprinc
        ejes = self.ejes

        sigmas = []
        for xy in self.lleno:
            for coord in xy:
                dist1 = np.dot(coord-self.coords['g'],ejes[0])
                dist2 = np.dot(coord-self.coords['g'],ejes[1])
                sigma = (Mprinc['N']/self.props['a'])+ejes[2]*((Mprinc['M1']*dist2/self.princ['j1'])-(Mprinc['M2']*dist1/self.princ['j2']))
                sigmas.append([coord,sigma])
        self.sigmas = sigmas
        self.computeAux()
        self.computeAux2()
        
    def computeXYSigma(self,coords):
        Mprinc = self.Mprinc
        ejes = self.ejes
        coord = -coords
        
        dist1 = np.dot(coord,ejes[0])
        dist2 = np.dot(coord,ejes[1])
        sigmaN = (Mprinc['N']/self.props['a'])
        sigmaM1 = ejes[2]*(Mprinc['M1']*dist2/self.princ['j1'])
        sigmaM2 = -ejes[2]*(Mprinc['M2']*dist1/self.princ['j2'])
        sigma = sigmaN + sigmaM1 + sigmaM2

        return {'yz':coords,'sigmaN':sigmaN, 'sigmaM1':sigmaM1,
                        'sigmaM2':sigmaM2,'sigma':sigma}
    
    def computeAux(self):
        Mprinc = self.Mprinc
        ejes = self.ejes
        princ = self.princ
        
        if Mprinc['N']==0 and Mprinc['M1']==0 and Mprinc['M2']==0:
            VectorLF = np.nan
            VectorEN = np.nan
            Int = np.nan
            cp = np.nan
            
        elif Mprinc['M1']==0 and Mprinc['M2']==0:
            VectorLF = np.nan
            VectorEN = np.nan
            Int = np.nan
            cp = self.coords['g']
            
        else:     
            VectorM = (Mprinc['M1']*ejes[0]+Mprinc['M2']*ejes[1])/np.sqrt(Mprinc['M1']**2+Mprinc['M2']**2)
            
            if Mprinc['N'] == 0:
                VectorLF = np.array([-VectorM[1],VectorM[0]])/np.linalg.norm(np.array([-VectorM[1],VectorM[0]]))
                cp = np.nan
                
            else:
                e2 = ejes[2]*Mprinc['M1']/Mprinc['N']
                e1 = -ejes[2]*Mprinc['M2']/Mprinc['N']
                cp = (e1*ejes[0]+e2*ejes[1]+self.coords['g'])
                VectorLF = (cp-self.coords['g'])/np.linalg.norm(cp-self.coords['g'])      
    
            if Mprinc['M2'] == 0:
                punto1 = np.array([0, -ejes[2]*Mprinc['N']*princ['j1']/(Mprinc['M1']*self.props['a'])])
                punto2 = np.array([1, -ejes[2]*Mprinc['N']*princ['j1']/(Mprinc['M1']*self.props['a'])])
            elif Mprinc['M1'] == 0:
                punto1 = np.array([ejes[2]*Mprinc['N']*princ['j2']/(Mprinc['M2']*self.props['a']), 0])
                punto2 = np.array([ejes[2]*Mprinc['N']*princ['j2']/(Mprinc['M2']*self.props['a']), 1]) 
            else:
                if Mprinc['N'] == 0:
                    punto1 = np.array([0,0])
                    punto2 = np.array([1, Mprinc['M2']*princ['j1']/(Mprinc['M1']*princ['j2'])])
                else:
                    punto1 = np.array([ejes[2]*Mprinc['N']*princ['j2']/(Mprinc['M2']*self.props['a']), 0])
                    punto2 = np.array([0, -ejes[2]*Mprinc['N']*princ['j1']/(Mprinc['M1']*self.props['a'])])
    
            p1 = (punto1[0]*ejes[0]+punto1[1]*ejes[1])
            p2 = (punto2[0]*ejes[0]+punto2[1]*ejes[1])
            VectorEN = (p2-p1)/np.linalg.norm(p2-p1)
                    
            matrizA = np.transpose(np.array([VectorLF,-VectorEN]))
            matrizB = np.transpose(p1)
            X = np.dot(np.linalg.inv(matrizA),matrizB)
            Int = + self.coords['g']+X[0]*VectorLF
            #        Chequeo
            Int2 = p1 + self.coords['g'] + X[1]*VectorEN
        
        self.aux = {'LF':VectorLF,'CP':cp,'EN':VectorEN,'Int':Int}
        
    def computeAux2(self):
        aux = self.aux
        self.aux2 = {}
        
        aux2 = {}
        
        if aux['LF'] is np.nan:
            aux2.update({'LF':np.nan})
        else:
            if aux['LF'][0] == 0:
                deg = 90
            else:
                deg = np.arctan(aux['LF'][1]/aux['LF'][0])*180/np.pi
            aux2.update({'LF':deg})
        
        if aux['EN'] is np.nan:
            aux2.update({'EN':np.nan})
        else:
            if aux['EN'][0] == 0:
                deg = 90
            else:
                deg = np.arctan(aux['EN'][1]/aux['EN'][0])*180/np.pi
            aux2.update({'EN':deg})
            
        if aux['Int'] is np.nan:
            aux2.update({'Dist':np.nan})
        else:
            distInt = np.linalg.norm(aux['Int']-self.coords['g'])
            alpha = aux2['EN']-aux2['LF']
            if alpha <= 90:
                Dist = distInt * abs(np.sin(alpha))
            else:
                Dist = distInt * abs(np.cos(alpha))
            aux2.update({'Dist':Dist})
            
        if aux['CP'] is np.nan:
            aux2['CPyz'] = np.nan
        else:
            aux2['CPyz'] = (self.aux['CP'] - self.coords['g'])*(-1)

        self.aux2 = aux2
            
    def plot_seccion(self, ax=None, princ=True, uv=True, length_scale=0.2,
                     fontsize=12, colors=['k','b','g','r'], textoffset=0.1):
        self.ax_seccion = plot_seccion(self, ax=ax, 
                                    princ=princ, uv=uv, length_scale=length_scale,
                                    fontsize=fontsize, colors=colors, 
                                    textoffset=textoffset)
    
    def plot_uv(self, ax=None, length_scale=0.2, fontsize=12, color='g',
                textoffset=0.1):
        self.uv_artists = plot_uv(ax, self, length_scale=length_scale, 
                                  fontsize=fontsize, color=color, 
                                  textoffset=textoffset)
    
    def plot_juv(self, ax=None, juv=True, j12=True, colors=['b','g']):
        self.mrktheta, self.mrkbeta, ax = plot_juv(self, ax=ax, juv=juv, j12=j12,
                                                   colors=colors)
        
    def plot_sigmas(self, ax=None, princ=True, mom=True,  en=True, lf=True,
                    cp=True, par=True, tot=True, rscale=0.1, length_scale=0.2,
                    fontsize=12, size = 50, colors=['k','b','g'], textoffset=0.1):
        self.sigma_artists, self.sigma_artists_dict, \
                self.cbar, self.ax_sigmas = plot_sigmas(self, ax=ax,
                                princ=princ, mom=mom, en=en, lf=lf, cp=cp, 
                                par=par, tot=tot, rscale=rscale,
                                length_scale=length_scale, fontsize=fontsize, 
                                size = size, colors=colors, textoffset=textoffset)
        
    def plot_sigma3d(self, ax=None, length_scale=0.2, fontsize=12,
                     colors=['k','b','g']):
        self.ax_3d = plot_sigma3d(self, ax=ax, length_scale=length_scale,
                                  fontsize=fontsize, colors=colors)
                                  
    def remove_artists(self, artists):
        for artist in artists:
            artist.remove()

"""
"""