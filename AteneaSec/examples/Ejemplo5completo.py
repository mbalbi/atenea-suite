import numpy as np
from AteneaSec.sectionlib import Figura,Seccion
import matplotlib.pyplot as plt
from AteneaSec.leerescribir import save_txt

"""
Ejemplos
"""

a = 3
b = 3 + 0.00001

# coords = {'g':,'alpha':0}
tipo = {'Tipo':'Tri','Prop':'Lleno'}
geometria = {'g':np.array([0,0]),'alpha':0,'b':a,'h':a}
figura1 = Figura(tipo,geometria)

fig1 = {1:figura1}
seccion = Seccion(fig1)
#print('Seccion 1a')
#print(seccion.coords)
#print(seccion.props)
#print(seccion.princ)

seccion.RotAxis(35)
seccion.plot_seccion(princ=True, uv=True, length_scale=0.2, fontsize=12,
                     colors=['k','b','g','r'], textoffset=0.2)
seccion.plot_juv(juv=True, j12=True, colors=['b','g'])

My = 1
Mz = 1
N = 1

solicitaciones = {'N':N,'My':My,'Mz':Mz}
seccion.computeSigma(solicitaciones)
#Revisar coherencia en los resultados
#print(seccion.Mprinc)
#print(seccion.ejes)
#print(seccion.sigmas)
#print(seccion.aux)
seccion.plot_sigmas(princ=True, mom=True, en=True,lf=True, cp=True, par=True,
                    tot=True, length_scale=0.2, fontsize=12, size=50,
                    colors=['k','b','g'], textoffset=0.2)


figura1.plot_fig(length_scale=0.2,fontsize=12, color='r', textoffset=0.2)

seccion.plot_sigma3d()

#save_txt('Ejemplo1.txt',seccion)

plt.show()