# -*- coding: utf-8 -*-
"""
Created on Wed Nov 13 16:44:02 2019

@author: santi
Figu"""

from leerescribir import read_txt

seccion1 = read_txt('Ejemplo1.txt')
seccion2 = read_txt('Ejemplo2.txt')
seccion3 = read_txt('Ejemplo3.txt')

seccion1.plot_seccion(princ=True, uv=True, length_scale=0.2, fontsize=12,
                     colors=['k','b','g','r'], textoffset=0.2)
seccion2.plot_seccion(princ=True, uv=True, length_scale=0.2, fontsize=12,
                     colors=['k','b','g','r'], textoffset=0.2)
seccion3.plot_seccion(princ=True, uv=True, length_scale=0.2, fontsize=12,
                     colors=['k','b','g','r'], textoffset=0.2)