import numpy as np
from AteneaSec.sectionlib import Figura,Seccion
import matplotlib.pyplot as plt
from AteneaSec.leerescribir import save_txt

"""
Ejemplos
"""

a = 80/1000
t = 3.2/1000
h = 200/1000

# coords = {'g':,'alpha':0}
tipo = {'Tipo':'Rect','Prop':'Lleno'}
geometria = {'g':np.array([a/2,t/2]),'alpha':0,'b':a,'h':t}
figura1 = Figura(tipo,geometria)
#print('Figura 1a')
#print(figura1.props)
#print(figura1.coords)

# coords = {'g':np.array([-(a-t/2),-h/2]),'alpha':0}
tipo = {'Tipo':'Rect','Prop':'Lleno'}
geometria = {'g':np.array([(a-t/2),h/2]),'alpha':0,'b':t,'h':h-2*t}
figura2 = Figura(tipo,geometria)
#print('Figura 2a')
#print(figura2.props)
#print(figura2.coords)

# coords = {'g':np.array([-(a-t)-a/2,-(h-t/2)]),'alpha':0}
tipo = {'Tipo':'Rect','Prop':'Lleno'}
geometria = {'g':np.array([(a-t)+a/2,(h-t/2)]), 'alpha':0, 'b':a, 'h':t}
figura3 = Figura(tipo,geometria)
#print('Figura 3a')
#print(figura3.props)
#print(figura3.coords)

fig1 = {1:figura1,2:figura2,3:figura3}
seccion = Seccion(fig1)
#print('Seccion 1a')
#print(seccion.coords)
#print(seccion.props)
#print(seccion.princ)

seccion.RotAxis(35)
seccion.plot_seccion(princ=True, uv=True, length_scale=0.2, fontsize=12,
                     colors=['k','b','g','r'], textoffset=0.2)
seccion.plot_juv(juv=True, j12=True, colors=['b','g'])

My = 0.1
Mz = 0.1
N = 1

solicitaciones = {'N':N,'My':My,'Mz':Mz}
seccion.computeSigma(solicitaciones)
#Revisar coherencia en los resultados
#print(seccion.Mprinc)
#print(seccion.ejes)
#print(seccion.sigmas)
#print(seccion.aux)
seccion.plot_sigmas(princ=True, mom=True, en=True,lf=True, cp=True, par=True,
                    tot=True, length_scale=0.2, fontsize=12, size=50,
                    colors=['k','b','g'], textoffset=0.2)


figura1.plot_fig(length_scale=0.2,fontsize=12, color='r', textoffset=0.2)
figura2.plot_fig(ax=figura1.ax,length_scale=0.2,fontsize=12, color='r', textoffset=0.2)
figura3.plot_fig(ax=figura1.ax,length_scale=0.2,fontsize=12, color='r', textoffset=0.2)

seccion.plot_sigma3d()

save_txt('Ejemplo1.txt',seccion)

plt.show()