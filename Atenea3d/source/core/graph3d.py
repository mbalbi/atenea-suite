# module graph3d.py
"""
Módulo de funciones de ploteo.

creado por: equipo Atenea-UBATIC
"""

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.patches import Polygon
from matplotlib.lines import Line2D
from matplotlib.patches import FancyArrow, FancyArrowPatch
from matplotlib.text import Text
from matplotlib.collections import PolyCollection

from mpl_toolkits.mplot3d import proj3d, art3d

import matplotlib.transforms as transforms

from .auxiliar import to_precision

class FrameBar:
    """
    Clase de artist que combina un Line2D y circular Patches para formar
    una barra con 0, 1 o 2 articulaciones en sus extremos
    """

    def __init__(self, axes, xdata, ydata, zdata, elemname='3DFrame',
                 rel=[0,0,0,0,0,0], **kwargs):

        self.ax = axes
        self.artists = []
        self.release_radius = 6
        self.release_distance = 0.05
        self.color = kwargs['color']
        self.zorder = kwargs['zorder']
        self.linewidth = kwargs['lw']

        self.elemname = elemname
        if self.elemname == '3DFrame':
            linestyle = '-'
            self.releases = rel
        elif self.elemname == '3DTruss':
            linestyle = '--'
            self.releases = [0,1,1,0,1,1]

        # Create Line3D artist
        self.baraxis = art3d.Line3D(xdata, ydata, zdata, linewidth=self.linewidth,
                                    linestyle=linestyle, color=self.color)
        self.baraxis.set_zorder(self.zorder)
        self.ax.add_artist(self.baraxis)
        self.rel = []
        self.compute_circle_coords()
        self.add_releases(self.releases)

    def compute_circle_coords(self):
        xdata = self.baraxis._verts3d[0]
        ydata = self.baraxis._verts3d[1]
        zdata = self.baraxis._verts3d[2]

        dx = xdata[1] - xdata[0]
        dy = ydata[1] - ydata[0]
        dz = zdata[1] - zdata[0]
        self.L = np.sqrt(dx ** 2 + dy ** 2 + dz ** 2)
        if self.L == 0:
            self.L = 0.000001
        # Releases artists location
        self.rel_init_coords = (xdata[0] + self.release_distance * dx,
                                ydata[0] + self.release_distance * dy,
                                zdata[0] + self.release_distance * dz)
        self.rel_center_coords = (xdata[0] + 0.5 * dx,
                                  ydata[0] + 0.5 * dy,
                                  zdata[0] + 0.5 * dz)
        self.rel_end_coords = (xdata[1] - self.release_distance * dx,
                               ydata[1] - self.release_distance * dy,
                               zdata[1] - self.release_distance * dz)

    def add_releases(self, releases):
        self.clean_releases()
        if releases[1] or releases[2]:
            artist = self.ax.plot3D([self.rel_init_coords[0]], [self.rel_init_coords[1]],
                                    [self.rel_init_coords[2]], marker='o',
                                    fillstyle='full',
                                    markersize=self.release_radius,
                                    markeredgecolor='k', markerfacecolor='w',
                                    zorder=self.zorder + 1)
            self.rel.append(artist[0])
        if releases[4] or releases[5]:
            artist = self.ax.plot3D([self.rel_end_coords[0]], [self.rel_end_coords[1]],
                                    [self.rel_end_coords[2]], marker='o',
                                    fillstyle='full',
                                    markersize=self.release_radius,
                                    markeredgecolor='k', markerfacecolor='w',
                                    zorder=self.zorder + 1)
            self.rel.append(artist[0])
        if releases[3]:
            artist = self.ax.plot3D([self.rel_center_coords[0]],
                                    [self.rel_center_coords[1]],
                                    [self.rel_center_coords[2]], marker='^',
                                    fillstyle='full',
                                    markersize=self.release_radius,
                                    markeredgecolor='k', markerfacecolor='r',
                                    zorder=self.zorder + 1)
            self.rel.append(artist[0])
        self.artists = [self.baraxis] + self.rel

    def clean_releases(self):
        self.artists = [self.baraxis]
        for rel in self.rel:
            rel.remove()
        self.rel = []

    def set_releases_data(self):
        self.compute_circle_coords()
        if self.releases[1] or self.releases[2]:
            if self.releases[4] or self.releases[5]:
                if self.releases[3]:
                    self.rel[0].set_data(self.rel_init_coords[0], self.rel_init_coords[1], self.rel_init_coords[2])
                    self.rel[1].set_data(self.rel_end_coords[0], self.rel_end_coords[1], self.rel_end_coords[2])
                    self.rel[2].set_data(self.rel_center_coords[0], self.rel_center_coords[1], self.rel_center_coords[2])
                else:
                    self.rel[0].set_data(self.rel_init_coords[0], self.rel_init_coords[1], self.rel_init_coords[2])
                    self.rel[1].set_data(self.rel_end_coords[0], self.rel_end_coords[1], self.rel_end_coords[2])
            else:
                if self.releases[3]:
                    self.rel[0].set_data(self.rel_init_coords[0], self.rel_init_coords[1], self.rel_init_coords[2])
                    self.rel[1].set_data(self.rel_center_coords[0], self.rel_center_coords[1], self.rel_center_coords[2])
                else:
                    self.rel[0].set_data(self.rel_init_coords[0], self.rel_init_coords[1], self.rel_init_coords[2])
        else:
            if self.releases[4] or self.releases[5]:
                if self.releases[3]:
                    self.rel[0].set_data(self.rel_end_coords[0], self.rel_end_coords[1], self.rel_end_coords[2])
                    self.rel[1].set_data(self.rel_center_coords[0], self.rel_center_coords[1], self.rel_center_coords[2])
                else:
                    self.rel[0].set_data(self.rel_end_coords[0], self.rel_end_coords[1], self.rel_end_coords[2])
            else:
                if self.releases[3]:
                    self.rel[0].set_data(self.rel_center_coords[0], self.rel_center_coords[1], self.rel_center_coords[2])
                else:
                    pass

    def set_picker(self, picker):
        self.baraxis.set_picker(picker)

    def set_zorder(self, zorder):
        self.baraxis.set_zorder(zorder)
        for artist in self.rel:
            artist.set_zorder(zorder + 1)

    def set_data(self, xdata, ydata):
        self.baraxis.set_data(xdata, ydata)
        self.set_releases_data()

    def set_animated(self, animated):
        for artist in self.artists:
            artist.set_animated(animated)

    def remove(self):
        for artist in self.artists:
            artist.remove()

    def get_data(self):
        return self.baraxis.get_data()

    def draw_artist(self):
        for artist in self.artists:
            self.ax.draw_artist(artist)

    def set_color(self, color):
        for artist in self.artists:
            artist.set_color(color)
            artist.set_markeredgecolor(color)

    def get_xydata(self):
        return self.baraxis.get_xydata()

    def get_color(self):
        return self.baraxis.get_color()

class FlechaAtenea3d:
    """
    ax: objeto axis de matplotlib
    x: [xi,xf]
    y: [yi,yf]
    z: [zi,zf]
    length: largo de la flecha en unidades fisicas
    reverse: bool. 0:alejandose del nodos; 1:acercandose al nodo
    offset: separacion del nodo
    label: texto
    fontsize: tamaño del texto

    """

    def __init__(self, ax, coords, dir, length=1, reverse=0, offset=0, label='',
                 fontsize=8, tipo='simple', visible=True, *args, **kwargs):
        self.ax = ax
        self.coords = coords
        dir = np.array(dir) / np.linalg.norm(dir)
        self.tipo = tipo
        if reverse: dir = [-i for i in dir]
        if tipo == 'simple':
            self.flecha = [Arrow3D(coords, dir, length=length, reverse=reverse,
                                   offset=offset, visible=visible, *args,
                                    **kwargs)]
        elif tipo == 'doble':
            flecha1 = Arrow3D(coords, dir, length=length, reverse=reverse,
                              offset=offset, visible=visible, *args, **kwargs)
            if reverse: offset += 0.2
            flecha2 = Arrow3D(coords, dir, length=length * 0.8, reverse=reverse,
                              offset=offset, visible=visible, *args, **kwargs)
            self.flecha = [flecha1, flecha2]
        for flecha in self.flecha:
            self.ax.add_artist(flecha)
        self.create_annotation(label, fontsize, visible=visible, reverse=reverse)

    def create_annotation(self, label, fontsize, visible=True, reverse=0):
        xs, ys, zs = self.flecha[0]._verts3d
        if reverse:
            x, y, z =xs[0], ys[0], zs[0]
        else:
            x, y, z =xs[1], ys[1], zs[1]
        # if self.flecha[0].reverse: x, y, z = xs[0], ys[0], zs[0]
        # if not self.flecha[0].reverse: x, y, z = xs[1], ys[1], zs[1]
        dx = (xs[1] - xs[0])
        dy = (ys[1] - ys[0])
        dz = (zs[1] - zs[0])
        dir = (dx, dy, dz)
        self.text = self.ax.text(x, y, z, label, dir, fontsize=fontsize,
                                 visible=visible)

    def set_arrowstyle(self, arrowstyle):
        for flecha in self.flecha:
            flecha.set_arrowstyle(arrowstyle)

    def update_length(self, length):
        l = self.flecha[0].length
        for flecha in self.flecha:
            flecha.update_length(length/l, self.coords)
        xs, ys, zs = self.flecha[0]._verts3d
        if self.flecha[0].reverse: x, y, z = xs[0], ys[0], zs[0]
        if not self.flecha[0].reverse: x, y, z = xs[1], ys[1], zs[1]
        self.text.set_x(x)
        self.text.set_y(y)
        self.text.set_3d_properties(z)

    def set_zorder(self, zorder):
        for flecha in self.flecha:
            flecha.set_zorder(zorder)
        self.text.set_zorder(zorder + 1)

    def set_visible(self, visible):
        for flecha in self.flecha:
            flecha.set_visible(visible)
        self.text.set_visible(visible)

    def set_text_visible(self, visible):
        self.text.set_visible(visible)

    def set_arrow_visible(self, visible):
        for flecha in self.flecha:
            flecha.set_visible(visible)

    def set_fontsize(self, fontsize):
        self.text.set_fontsize(fontsize)

    def remove(self):
        for flecha in self.flecha:
            flecha.remove()
        self.text.remove()

class curvedArrow3D:

    def __init__(self, ax, center, dir, r=1, reverse=0, label='', fontsize=5,
                 *args, **kwargs):

        self.ax = ax
        self.center = center
        self.dir = dir
        self.r = r
        self.reverse = reverse
        self.plot_arrow(*args, **kwargs)
        self.create_annotation(label, fontsize)

    def plot_arrow(self, *args, **kwargs):
        # Compute rotation matrix from (0,0,1) to dir
        a = np.array((0, 0, 1))
        b = np.array(self.dir)
        v = np.cross(a, b)
        s = np.sqrt(np.dot(a, a.T))
        c = np.dot(a, b.T)
        vp = np.array([[0, -v[2], v[1]],
                       [v[2], 0, -v[0]],
                       [-v[1], v[0], 0]])
        R = np.eye(len(a)) + vp + np.dot(vp, vp) * (1 - c) / s

        self.Np = 15
        rad = np.linspace(0, np.pi, self.Np)
        if self.reverse:
            rad = np.linspace(np.pi, 0, self.Np)
        # Curva en coordenadas locales en 2D
        xl = self.r * np.cos(rad)
        yl = self.r * np.sin(rad)
        zl = np.zeros(np.size(xl))
        # Rotacion
        xyz = np.stack((xl, yl, zl), axis=1)
        XYZ = np.dot(xyz, R.T)
        # Traslacion al centro
        XYZ += np.array(self.center)
        self.flecha = self.ax.plot(XYZ[:, 0], XYZ[:, 1], XYZ[:, 2], *args,
                                    **kwargs)
        # Dibujar punta de flecha
        if not self.reverse:
            xf = XYZ[-1, 0]
            yf = XYZ[-1, 1]
            zf = XYZ[-1, 2]
        if self.reverse:
            xf = XYZ[1, 0]
            yf = XYZ[1, 1]
            zf = XYZ[1, 2]
        self.head = self.ax.plot([xf], [yf], [zf],
                                 marker='v', markersize=5, *args, **kwargs)
        self.XYZ = XYZ

    def create_annotation(self, label, fontsize):
        if not self.reverse:
            x = self.XYZ[-1, 0]
            y = self.XYZ[-1, 1]
            z = self.XYZ[-1, 2]
        if self.reverse:
            x = self.XYZ[1, 0]
            y = self.XYZ[1, 1]
            z = self.XYZ[1, 2]
        self.text = self.ax.text(x, y, z, label, self.dir, fontsize=fontsize)

    def update_length(self, scale):
        xyz = self.XYZ - np.array(self.center)
        xyz_scaled = xyz * scale
        self.r = scale*self.r
        self.XYZ = xyz_scaled + np.array(self.center)
        self.flecha[0].set_xdata(self.XYZ[:, 0])
        self.flecha[0].set_ydata(self.XYZ[:, 1])
        self.flecha[0].set_3d_properties(self.XYZ[:, 2])
        # Move arrow head
        self.head[0].set_xdata(self.XYZ[-1, 0])
        self.head[0].set_ydata(self.XYZ[-1, 1])
        self.head[0].set_3d_properties(self.XYZ[-1, 2])
        # Move text
        if not self.reverse:
            xf = self.XYZ[-1, 0]
            yf = self.XYZ[-1, 1]
            zf = self.XYZ[-1, 2]
        if self.reverse:
            xf = self.XYZ[1, 0]
            yf = self.XYZ[1, 1]
            zf = self.XYZ[1, 2]
        self.text.set_x(xf)
        self.text.set_y(yf)
        self.text.set_3d_properties(zf)

    def set_zorder(self, zorder):
        self.flecha[0].set_zorder(zorder)
        self.text.set_zorder(zorder + 1)

    def set_visible(self, visible):
        self.flecha[0].set_visible(visible)
        self.text.set_visible(visible)

    def set_text_visible(self, visible):
        self.text.set_visible(visible)

    def set_arrow_visible(self, visible):
        self.flecha[0].set_visible(visible)

    def set_fontsize(self, fontsize):
        self.text.set_fontsize(fontsize)

class Arrow3D(FancyArrowPatch):

    def __init__(self, coords, dir, length=1, reverse=0, offset=0,
                 *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = ([coords[0], coords[0] + dir[0]],
                         [coords[1], coords[1] + dir[1]],
                         [coords[2], coords[2] + dir[2]])
        self.reverse = reverse
        if self.reverse: self.reverse_dir()
        self.add_offset(offset)
        self.calc_length()
        scale = length/self.length
        self.update_length(scale, coords)


    def draw(self, renderer):
        """
        Transforma a la flecha2D en una flecha en 3D
        """
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

    def add_offset(self, offset):
        self.calc_length()
        xs, ys, zs = self._verts3d
        dx = offset*(xs[1]-xs[0])/self.length
        dy = offset*(ys[1]-ys[0])/self.length
        dz = offset*(zs[1]-zs[0])/self.length
        if self.reverse:
            dx = -dx
            dy = -dy
            dz = -dz
        self._verts3d = ([xs[0]+dx,xs[1]+dx],
                         [ys[0]+dy,ys[1]+dy],
                         [zs[0]+dz,zs[1]+dz])

    def calc_length(self):
        xs, ys, zs = self._verts3d
        self.length = np.sqrt((xs[1]-xs[0])**2 +
                     (ys[1]-ys[0])**2 +
                     (zs[1]-zs[0])**2)

    def reverse_dir(self):
        xs, ys, zs = self._verts3d
        self._verts3d = ([xs[1],xs[0]],
                         [ys[1],ys[0]],
                         [zs[1],zs[0]])

    def update_length(self, scale, coords):
        xs, ys, zs = self._verts3d
        # Distancias del nodo al inicio de la flecha
        dx1 = (xs[0] - coords[0]) * scale
        dy1 = (ys[0] - coords[1]) * scale
        dz1 = (zs[0] - coords[2]) * scale
        # Distancias del nodo al final de la flecha
        dx2 = dx1 + (xs[1] - xs[0]) * scale
        dy2 = dy1 + (ys[1] - ys[0]) * scale
        dz2 = dz1 + (zs[1] - zs[0]) * scale
        self._verts3d = ([coords[0] + dx1, coords[0] + dx2],
                         [coords[1] + dy1, coords[1] + dy2],
                         [coords[2] + dz1, coords[2] + dz2])
        self.calc_length()
        return

class cargadistribuidaAtenea:

    def __init__(self, axes, pinit, winit, pfin, wfin, dir, color, arrowstyle,
                 fontsize, offset, length=1, visible=True, zorder=5):
        """
        La lista de coordenadas de los vértices del polígono están en el
        siguiente orden:
            1- Punto inicial de flecha en el nodo inicial
            2- Punto final de flecha en el nodo inicial
            3- Punto final de flecha en el nodo final
            4- Punto inicial de flecha en el nodo final
        :param axes:
        :param xy:
        :param color:
        :param arrowstyle:
        """

        # Parámetros
        self.winit = winit
        self.wfin = wfin
        self.pinit = np.array(pinit)
        self.pfin = np.array(pfin)
        self.length = length

        # Versor de direccion
        self.dir = np.array(dir)/np.linalg.norm(dir)

        # Tamaño de las flechas
        self.set_arrow_sizes()

        # Dibujar polígono de carga distribuida
        if self.arrow1_dim:
            p1 = pinit - np.sign(self.arrow1_dim) * self.dir * offset * self.length
            p4 = pfin - np.sign(self.arrow1_dim) * self.dir * offset * self.length
        else:
            p1 = pinit - np.sign(self.arrow2_dim) * self.dir * offset * self.length
            p4 = pfin - np.sign(self.arrow2_dim) * self.dir * offset * self.length
        p2 = p1 - self.dir * self.arrow1_dim
        p3 = p4 - self.dir * self.arrow2_dim
        self.xyz = [[tuple(p1), tuple(p2), tuple(p3), tuple(p4)]]

        # Polygon Artist
        self.polygon = art3d.Poly3DCollection(self.xyz, visible=visible,
                                              alpha=0.3, facecolor=color,
                                              zorder=zorder)
        axes.add_collection3d(self.polygon)

        # Flecha inicial
        if self.arrow1_dim:
            dir1 = self.dir*self.arrow1_dim
            self.flecha_i = FlechaAtenea3d(axes, pinit, dir1,
                                label=str(self.winit),
                                offset=offset*self.length/abs(self.arrow1_dim),
                                fontsize=fontsize, length=abs(self.arrow1_dim),
                                lw=1, color=color, visible=visible,
                                zorder=zorder, reverse=True)
            self.flecha_i.set_arrowstyle(arrowstyle)

        # Flecha final
        if self.arrow2_dim:
            dir2 = self.dir * self.arrow2_dim
    # Para cuando es cruzada, que el offset se aplique para el lado del poligono
            signo_offset = 1
            if self.arrow1_dim:
                if np.dot(dir1,dir2) < 0: signo_offset=-1
            self.flecha_f = FlechaAtenea3d(axes, pfin, dir2, label=str(self.wfin),
                    offset=signo_offset*offset*self.length/abs(self.arrow2_dim),
                    fontsize=fontsize, length=abs(self.arrow2_dim), lw=1,
                    color=color, visible=visible, zorder=zorder, reverse=True)
            self.flecha_f.set_arrowstyle(arrowstyle)

    def set_arrow_sizes(self):
        if np.abs(self.winit) >= np.abs(self.wfin):
            if self.winit == 0: # Los dos son cero
                self.arrow1_dim = 0
                self.arrow2_dim = 0
            else:
                self.arrow1_dim = np.sign(self.winit) * self.length
                self.arrow2_dim = np.sign(self.wfin) * self.length * np.abs(self.wfin/self.winit)
        elif np.abs(self.wfin) > np.abs(self.winit):
            self.arrow2_dim = np.sign(self.wfin) * self.length
            self.arrow1_dim = np.sign(self.winit) * self.length * np.abs(self.winit / self.wfin)

    def set_zorder(self, zorder):
        if self.arrow1_dim: self.flecha_i.set_zorder(zorder=zorder)
        if self.arrow2_dim: self.flecha_f.set_zorder(zorder=zorder)
        self.polygon.set_zorder(zorder=zorder)

    def set_visible(self, visible):
        if self.arrow1_dim: self.flecha_i.set_visible(visible)
        if self.arrow2_dim: self.flecha_f.set_visible(visible)
        self.polygon.set_visible(visible)

    def set_text_visible(self, visible):
        if self.arrow1_dim: self.flecha_i.set_text_visible(visible)
        if self.arrow2_dim: self.flecha_f.set_text_visible(visible)

    def remove(self):
        if self.arrow1_dim: self.flecha_i.remove()
        if self.arrow2_dim: self.flecha_f.remove()
        self.polygon.remove()

    def set_fontsize(self, fontsize):
        if self.arrow1_dim: self.flecha_i.set_fontsize(fontsize)
        if self.arrow2_dim: self.flecha_f.set_fontsize(fontsize)

    def update_polygon_size(self, scale):
        # Escalo el offset modificando p1 y p4
        p1 = self.pinit + (self.xyz[0][0] - self.pinit) * scale
        p4 = self.pfin + (self.xyz[0][3] - self.pfin) * scale
        # Modifico p2 y p3 segun las arrow_dim
        p2 = self.pinit + (self.xyz[0][1] - self.pinit) * scale
        p3 = self.pfin + (self.xyz[0][2] - self.pfin) * scale
        # Setear nuevos vértices
        self.xyz = [[tuple(p1), tuple(p2), tuple(p3), tuple(p4)]]
        self.polygon.set_verts(self.xyz)

    def update_length(self, length):
        # Actualizar tamaño de flechas
        scale = length / self.length
        self.length = length
        self.set_arrow_sizes()
        # Actualizar flechas
        if self.arrow1_dim:
            self.flecha_i.update_length(length)
        if self.arrow2_dim:
            self.flecha_f.update_length(length)
        # Actualizar polígono
        self.update_polygon_size(scale)


def plot_estructura(estructura, fuerzas=True, vinculos=True, cedimientos=True,
                    indiceN=False, ejes_loc=False, temp=True, indiceB=False,
                    origen=True, axes=[]):
    """
    Llama a todas las funciones necesarias para plotear la estructura en su
    conjunto, incluyendo condiciones de vinculo, articulaciones y si las hay
    fuerzas puntuales y distribuidas.
    Las funciones a las que llama son las siguientes:
        plot_barras
        plot_vinculos
        plot_releases
        plot_fuerzas_nodales
        plot_fuerzas_distribuidas

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo el
                            ploteo.
        Fuerzas [bool]: en caso de que haya fuerzas externas definidas por el
                        usuario el mismo es 'True', y se procede a plotear
                        dichas fuerzas. En caso de que no haya fuerzas el
                        booleano es 'False' y no se procede a llamar a las
                        funciones plot_fuerzas_nodales y
                        plot_fuerzas_distribuidas.

    """
    barras = estructura.barras
    nodos = estructura.nodos

    if not axes:  # Crear figura si no es input de la función
        axes = create_axes()

    # Plotear elementos de nodos
    for nodo in nodos.values():
        if not hasattr(nodo, 'artists'): nodo.artists = {}
        nodo.artists['nodo'] = plot_nodo(nodo, axes, color='blue', marker='s',
                                         markersize=5, zorder=1)
        nodo.artists['P'] = plot_fuerzas_nodales(nodo, axes, fontsize=8,
                                                 colors=['g', 'r'], visible=fuerzas,
                                                 zorder=10)
        nodo.artists['V'] = plot_vinculos(nodo, axes, fontsize=8, color='r',
                                          visible=vinculos, zorder=1)
        nodo.artists['ced'] = plot_cedimientos(nodo, axes, color='c', fontsize=8,
                                               visible=cedimientos, zorder=5)
        nodo.artists['indice'] = plot_indice_nodo(nodo, axes, fontsize=8, visible=indiceN)

    # Plotear elementos de barras
    for barra in barras.values():
        if not hasattr(barra, 'artists'): barra.artists = {}
        barra.artists['barra'] = plot_barra(barra, axes, color='black',
                                            lw=1, zorder=5)
        barra.artists['ejes'] = plot_ejes_locales(barra, axes, fontsize=8,
                                                  color='y', zorder=1,
                                                  visible=ejes_loc)
        barra.artists['temp'] = plot_temperatura(barra, axes, fontsize=8,
                                                 visible=temp)
        barra.artists['w'] = plot_fuerzas_distribuidas(barra, axes, fontsize=8,
                                                       zorder=5, visible=fuerzas)
        barra.artists['indice'] = plot_indice_barra(barra, axes, fontsize=8,
                                                    visible=indiceB)

    # Plotear flechas del origen
    axes.origin_arrows = plot_originarrows(axes, visible=origen)

    # Hacer zoom a la estructura y ajustar tamaño de flechas
    zoom2extent(nodos, axes, margin=0.25)
    update_ax_arrows(axes, nodos, barras, 0.15)

    return axes

def create_axes():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
    ax.set_proj_type('ortho')
    ax.view_init(azim=30, elev=15)
    # ax.set_aspect('equal', adjustable='box', anchor='C')
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")
    ax.axis([0,2,0,2])
    ax.set_xlim3d(0,2)
    ax.set_ylim3d(0,2)
    ax.set_zlim3d(0,2)
    ax.minorticks_on()
    ax.tick_params(axis='both', which = 'both', direction = 'out',
                    labelsize=4, pad=0, length=1,
                    bottom=True, top=True, left=True, right=True)
    return ax

def create_2d_axes():
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    ax.axis([0,2,0,2])
    ax.set_xlim(0,2)
    ax.set_ylim(0,2)
    ax.minorticks_on()
    ax.tick_params(axis='both', which = 'both', direction = 'out',
                    labelsize=8, pad=0, length=1,
                    bottom=True, top=True, left=True, right=True)
    return ax

def plot_nodo(nodo, axes, **kwargs):
    XYZ = nodo.coords
    xs = [XYZ[0]]
    ys = [XYZ[1]]
    zs = [XYZ[2]]
    artist = axes.plot3D(xs, ys, zs, **kwargs)
    return artist

def plot_fuerzas_nodales(nodo, axes, fontsize, length=1, colors=['g', 'b'],
                         visible=True, zorder=1):
    """
    Define para cada fuerza puntual del atributo P de la clase estructura, los
    argumentos necesarios para llamar a la función plot_flecha y poder plotear
    cada una de ellas.
    Para hacerlo itera en los DOF's de la estructura y los compara con los
    indices del vector estructura.P. En caso de que coincidan, se busca a que
    nodo pertenece el DOF en cuestion y segun sea el primero, segundo o tercer
    DOF asociado a dicho nodo se define  la coordenada en la que actua la
    fuerza. En funcion de las coordenadas del nodo se definen pos1 y pos2, y
    si se trata de una fuerza en las coordenadas X o Y se define la forma como
    'recta', en caso de que sea una fuerza en la coordenada Z se define la
    forma como 'curva'.
    Las fuerzas se dibujan todas del mismo tamaño.

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo la
                            defincion de argumentos mencionada previamente.
        axes [class]: figura donde se desean plotear las fuerzas
        scale [int]: escala con la que se desean plotear las fuerzas puntuales.
                     Esta se predefine en la funcion plot_estructura en funcion
                     del tamaño de la estructura.
    """
    color_fuerza = colors[0]
    color_momento = colors[1]
    offset = 0.1
    artists = []

    P = nodo.P
    # Fuerzas
    if P[0] != 0:
        dir = [P[0], 0, 0]
        flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=str(P[0]),
                                fontsize=fontsize, length=length, reverse=True,
                                offset=offset, lw=1, color=color_fuerza,
                                visible=visible, zorder=zorder)
        flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
        artists.append(flecha)
    if P[1] != 0:
        dir = [0, P[1], 0]
        flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=str(P[1]),
                                fontsize=fontsize, length=length, reverse=True,
                                offset=offset, lw=1, color=color_fuerza,
                                visible=visible, zorder=zorder)
        flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
        artists.append(flecha)
    if P[2] != 0:
        dir = [0, 0, P[2]]
        flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=str(P[2]),
                                fontsize=fontsize, length=length, reverse=True,
                                offset=offset, lw=1, color=color_fuerza,
                                visible=visible, zorder=zorder)
        flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
        artists.append(flecha)
    # Momentos
    if P[3] != 0:
        dir = [P[3], 0, 0]
        flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=str(P[3]),
                                tipo='doble', fontsize=fontsize, length=length,
                                reverse=False, offset=offset, lw=1,
                                color=color_momento, visible=visible,
                                zorder=zorder)
        flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
        artists.append(flecha)
    if P[4] != 0:
        dir = [0, P[4], 0]
        flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=str(P[4]),
                                tipo='doble', fontsize=fontsize, length=length,
                                reverse=False, offset=offset, lw=1,
                                color=color_momento, visible=visible,
                                zorder=zorder)
        flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
        artists.append(flecha)
    if P[5] != 0:
        dir = [0, 0, P[5]]
        flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=str(P[5]),
                                tipo='doble', fontsize=fontsize, length=length,
                                reverse=False, offset=offset, lw=1,
                                color=color_momento, visible=visible,
                                zorder=zorder)
        flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
        artists.append(flecha)
    return artists

def plot_vinculos(nodo, axes, fontsize, length=1, color='r', visible=True,
                  zorder=1, tipo='vinculo', precision=3):
    """
    Plotea las condiciones de vinculos segun cual sea naturaleza:
        Simplemente apoyado: restringe desplazamientos en las direcciones x e y
        Patin en x: restringe desplazamientos en la direccion y
        Patin en y: restringe desplazamientos en la direccion x
        Empotramiento: restringe desplazamientos en las 3 coordenadas
        Comegiro: restringe giros solamente
    Para el nodo dado la funcion toma su atributo restr y plotea la condicion
    de vinculo correspondiente.
    Simplemente apoyado: [1,1,0]
    Patin en X: restr = [0,1,0]
    Patin en Y: restr = [1,0,0]
    Empotramiento: restr = [1,1,1]
    Comegiro: restr = [0,0,1]

    Argumentos:
        nodo [class]: clase definida en el modulo barra2d, que reune toda la
              informacion necesaria para llevar acabo el ploteo
        axes [class]: figura donde se desean plotear los vinculos
        scale [int]: escala con la que se desean plotear los vinculos. Esta se
               predefine en la funcion plot_estructura en funcion del tamaño
               de la estructura.

    A agregar:
        Ploteo de vinculos que restringan el giro y el desplazamiento en una
        sola direccion X o Y.
    """
    offset = 0
    lw = 0.5
    artists = []
    restr = nodo.restr
    label = ''

    dir = [0,0,0]
    for i in range(3):
        # Fuerzas
        if restr[i] == 1:
            dir[i]=1
            if tipo == 'reaccion':
                label = str(to_precision(nodo.R[i][0], precision))
                dir[i] = np.sign(nodo.R[i])[0]
                if dir[i] == 0: dir[i] = 1
            flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=label,
                                    fontsize=fontsize, length=length,
                                    reverse=True, offset=offset, lw=lw,
                                    color=color, visible=visible,
                                    zorder=zorder)
            flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
            artists.append(flecha)
            dir = [0,0,0]
        # Momentos
        if len(restr)==6:
            if restr[i+3] == 1:
                dir[i] = 1
                if tipo == 'reaccion':
                    label = str(to_precision(nodo.R[i+3][0], precision))
                    dir[i] = np.sign(nodo.R[i+3])[0]
                    if dir[i] == 0: dir[i] = 1
                flecha = FlechaAtenea3d(axes, nodo.coords, dir, label=label,
                                        tipo='doble', fontsize=fontsize,
                                        color=color, length=length,
                                        offset=offset, lw=lw, visible=visible,
                                        zorder=zorder)
                flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
                artists.append(flecha)
                dir = [0,0,0]

    return artists

def plot_cedimientos(nodo, axes, fontsize=6, length=0.7, color = 'c',
                     visible=True, zorder=1):
    """
    """
    offset = 0.1*length
    artists = []

    ced = nodo.ced
    coords = nodo.coords
    dir = [0, 0, 0]
    for i in range(3):
        # Desplazamientos
        if ced[i] != 0:
            dir[i] = ced[i]
            flecha = FlechaAtenea3d(axes, coords, dir, label=str(ced[i]),
                                    fontsize=fontsize, length=length,
                                    reverse=False, offset=offset, lw=1,
                                    color=color, visible=visible,
                                    zorder=zorder)
            flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
            artists.append(flecha)
            dir[i] = 0
        # Giros
        if ced[i+3] != 0:
            dir[i] = ced[i+3]
            flecha = FlechaAtenea3d(axes, coords, dir, label=str(ced[i+3]),
                                    tipo='doble', fontsize=fontsize,
                                    length=length, reverse=False,
                                    offset=offset, lw=1, color=color,
                                    visible=visible, zorder=zorder)
            flecha.set_arrowstyle('fancy, head_length=5, head_width=3')
            artists.append(flecha)
            dir[i] = 0
    return artists

def plot_indice_nodo(nodo,axes,fontsize,visible=True):
    text = axes.text(nodo.coords[0], nodo.coords[1], nodo.coords[2],
                     '(' + str(nodo.indice) + ')', fontsize=fontsize,
                     zorder=7, visible=visible, color='b')
    return [text]

def plot_barra(barra, axes, *args, **kwargs):
    """
    Plotea barra por barra tomando las coordenadas de sus respectivos
    nodos iniciales y finales. En funcion de si se trata de un elemento 2DFrame
    o 2DTruss, se plotea con diferente color.
    2DFrame: rojo
    2DTruss: azul

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo el
                            ploteo
        axes [class]: figura donde se desean plotear las barras

    """
    nodoi = barra.nodos[0].coords
    nodof = barra.nodos[1].coords
    xs = [nodoi[0], nodof[0]]
    ys = [nodoi[1], nodof[1]]
    zs = [nodoi[2], nodof[2]]
    artist = FrameBar(axes, xs, ys, zs, rel=barra.rel, elemname=barra.ElemName,
                      *args, **kwargs)
    return [artist]

def plot_ejes_locales(barra, axes, fontsize, length=0.4, color='y', zorder=1, visible=True):
    """
    """
    artists = []
    # Propiedades de las flechas
    lw = 1

    # Coordenadas de la barra
    nodoi = barra.nodos[0].coords
    nodof = barra.nodos[1].coords
    xs = (nodoi[0], nodof[0])
    ys = (nodoi[1], nodof[1])
    zs = (nodoi[2], nodof[2])

    # Punto medio de la barra
    coords = [np.mean(xs), np.mean(ys), np.mean(zs)]

    # Plotear flecha axial (x)
    flecha = FlechaAtenea3d(axes, coords, barra.Mr[:, 0], label=str('x'),
                            fontsize=fontsize, length=length, lw=lw,
                            color=color, visible=visible, zorder=zorder)
    flecha.set_arrowstyle('->, head_length=5, head_width=3')
    artists.append(flecha)

    # Plotear flecha eje local y
    flecha = FlechaAtenea3d(axes, coords, barra.Mr[:, 1], label=str('y'),
                            fontsize=fontsize, length=length, lw=lw,
                            color=color, visible=visible, zorder=zorder)
    flecha.set_arrowstyle('->, head_length=5, head_width=3')
    artists.append(flecha)

    # Plotear flecha eje local z
    flecha = FlechaAtenea3d(axes, coords, barra.Mr[:, 2], label=str('z'),
                            fontsize=fontsize, length=length, lw=lw,
                            color=color, visible=visible, zorder=zorder)
    flecha.set_arrowstyle('->, head_length=5, head_width=3')
    artists.append(flecha)

    return artists

def plot_temperatura(barra, axes, fontsize, visible=True):
    e0 = barra.e0
    if not (e0[0] and e0[1] and e0[2]): pass

    # División de la barra en 7 puntos
    L = barra.long

    # Ubicacion de los simbolos
    offsetS = L*0.1
    X = np.linspace(L/8,L,7)
    Y = np.ones(7) * offsetS
    Z = np.ones(7) * offsetS
    posS = [np.row_stack((X, Y, np.zeros(7))),
            np.row_stack((X, -Y, np.zeros(7))),
            np.row_stack((X, np.zeros(7), Z)),
            np.row_stack((X, np.zeros(7), -Z))]
    for i in range(4):
        posS[i] = np.dot(barra.Mr, posS[i]) + np.transpose(np.tile(barra.coords[0],
                                                                    (7, 1)))
    # Ubicacion de los textos
    textoffset = L*0.4
    posT = np.array([[L/2,        L/2,         L/2,        L/2],
                     [textoffset, -textoffset, 0,          0],
                     [0,          0,           textoffset, -textoffset]])
    posT = np.dot(barra.Mr, posT) + np.transpose(np.tile(barra.coords[0], (4, 1)))

    # Diccionario con los datos sobre cada cara
    caras = {}

    if e0[1] != 0:
        caras['supy'] = {'e0': e0[1], 'simbols': posS[0], 'textos': posT[:, 0]}
        caras['infy'] = {'e0': -e0[1], 'simbols': posS[1], 'textos': posT[:, 1]}
    if e0[2] != 0:
        caras['supz'] = {'e0': e0[2], 'simbols': posS[2], 'textos': posT[:, 2]}
        caras['infz'] = {'e0': -e0[2], 'simbols': posS[3], 'textos': posT[:, 3]}
    if e0[0] != 0 and e0[1] == 0 and e0[2] != 0:
        caras['supy'] = {'e0': e0[0], 'simbols': posS[0], 'textos': posT[:, 2]}
        caras['infy'] = {'e0': e0[0], 'simbols': posS[1], 'textos': posT[:, 3]}
    if e0[0] != 0 and e0[1] != 0 and e0[2] == 0:
        caras['supz'] = {'e0': e0[0], 'simbols': posS[2], 'textos': posT[:, 2]}
        caras['infz'] = {'e0': e0[0], 'simbols': posS[3], 'textos': posT[:, 3]}
    if e0[0] != 0 and e0[2] == 0 and e0[2] == 0:
        caras['supz'] = {'e0': e0[0], 'simbols': posS[2], 'textos': posT[:, 2]}
        caras['infz'] = {'e0': e0[0], 'simbols': posS[3], 'textos': posT[:, 3]}

    artists = []
    for cara in caras.values():
        if not cara['e0']:
            continue
        if cara['e0']>0:
            marker = '+'
            color = 'r'
        if cara['e0']<0:
            marker = '_'
            color = 'b'
        sim = art3d.Line3D(cara['simbols'][0, :], cara['simbols'][1, :],
                           cara['simbols'][2, :], marker=marker, linestyle='',
                           color=color, visible=visible, zorder=7)
        axes.add_artist(sim)
        artists.append(sim)
        # text = axes.text(cara['textos'][0], cara['textos'][1], cara['textos'][2],
        #                  str(cara['e0']) + ' º', fontsize=fontsize, color=color,
        #                  visible=visible, zorder=7, horizontalalignment='center')
        # artists.append(text)
    return artists

def plot_fuerzas_distribuidas(barra, axes, fontsize, length=1, color='b',
                              visible=True, zorder=1):
    """
    Plotea fuerzas distribuidas. Utiliza el atributo w de fuerzas
    distribuidas de cada barra, y distingue entre terna 'global' y 'local'.
    Las fuerzas distribuidas en el eje y se representan a traves de un poligono
    y dos flechas, una al comienzo y otra al final del poligono.
    Para definir el poligono se interpola linealmente entre los valores
    inicial y final expresados en el atributo w de la barra. Para plotear las
    flechas se definen los argumentos necesarios para llamar la funcion
    plot_flecha

    Argumentos:
    barra [class]: clase definida en el modulo bar3d, que reune toda la
                   informacion necesaria para llevar acabo la defincion de
                   argumentos mencionada previamente.
    axes [class]: figura donde se desean plotear las fuerzas
    scale [int]: escala con la que se desean plotear las fuerzas puntuales.
                 Esta se predefine en la funcion plot_estructura en funcion del
                 tamaño de la estructura.
    """
    # Dimensiones
    offset = 0.1
    arrowstyle = 'fancy, head_length=5, head_width=3'

    # Inicializa listas vacias
    wx = []
    wy = []
    wz = []
    artists = []
    wx.append(barra.w['wxi'])
    wx.append(barra.w['wxf'])
    wy.append(barra.w['wyi'])
    wy.append(barra.w['wyf'])
    wz.append(barra.w['wzi'])
    wz.append(barra.w['wzf'])

    L = barra.long
    # Computar reacciones nodales equivalentes
    wxi = wx[0]
    wxf = wx[1]
    wyi = wy[0]
    wyf = wy[1]
    wzi = wz[0]
    wzf = wz[1]

    # Versor paralelo al eje de la barra
    versor_paral = np.dot(barra.Mr, np.array([1, 0, 0]))

    # Dibujar fuerzas distribuidas en 'y'
    if wyi == 0 and wyf == 0:
        pass
    elif wyi != 0 or wyf != 0:
        if barra.w['terna'] == 'global':
            dir = [0, 1, 0]
            wartist = cargadistribuidaAtenea(axes, barra.coords[0], wyi,
                                             barra.coords[1], wyf,
                                             dir, color, arrowstyle,
                                             fontsize, offset, length=length,
                                             visible=visible, zorder=zorder)
            artists.append(wartist)

        elif barra.w['terna'] == 'local':
            versor_perp = np.dot(barra.Mr, np.array([0, 1, 0]))
            wartist = cargadistribuidaAtenea(axes, barra.coords[0], wyi,
                                             barra.coords[1], wyf, versor_perp,
                                             color, arrowstyle, fontsize, offset,
                                             length=length, visible=visible,
                                             zorder=zorder)
            artists.append(wartist)

    # Dibujar fuerzas distribuidas en 'z'
    if wzi == 0 and wzf == 0:
        pass
    elif wzi != 0 or wzf != 0:
        if barra.w['terna'] == 'global':
            dir = [0, 0, 1]
            wartist = cargadistribuidaAtenea(axes, barra.coords[0], wzi,
                                             barra.coords[1], wzf, dir, color,
                                             arrowstyle, fontsize, offset,
                                             length=length, visible=visible,
                                             zorder=zorder)
            artists.append(wartist)

        elif barra.w['terna'] == 'local':
            versor_perp = np.dot(barra.Mr, np.array([0, 0, 1]))
            wartist = cargadistribuidaAtenea(axes, barra.coords[0], wzi,
                                             barra.coords[1], wzf, versor_perp,
                                             color, arrowstyle, fontsize, offset,
                                             length=length, visible=visible,
                                             zorder=zorder)
            artists.append(wartist)

    # Dibujar carga distribuida en 'x'
    if wxi == 0 and wxf == 0:
        pass
    elif wxi != 0 or wxf != 0:
        if barra.w['terna'] == 'global':
            dir = [1, 0, 0]
            wartist = cargadistribuidaAtenea(axes, barra.coords[0], wxi,
                                             barra.coords[1], wxf, dir, color,
                                             arrowstyle, fontsize, offset,
                                             length=length, visible=visible,
                                             zorder=zorder)
            artists.append(wartist)
        elif barra.w['terna'] == 'local':
            # Discretización a lo largo de la barra
            X = np.linspace(0.05 * L, L * 7 / 8, 6)
            XYZ_loc = np.row_stack((X, X * 0 + L * 0.02, X * 0 + L * 0.02))
            XYZ_glob = np.dot(barra.Mr, XYZ_loc) + np.tile(barra.coords[0],
                                                    (XYZ_loc.shape[1], 1)).T

            # Signo de w en cada punto para direccionar cada flecha
            sign_w = np.sign(wxi + (wxf-wxi)/L*X)

            length = (X[0]-X[1])*0.8
            flechas = []
            for i in range(XYZ_glob.shape[1]):
                # No plotear la flecha en el punto de cambio de signo
                if i:
                    if sign_w[i]*sign_w[i-1] < 0: continue
                Pi = np.array(XYZ_glob[:, i])
                dir = np.dot(barra.Mr, np.array([-sign_w[i], 0, 0]))
                flecha = Arrow3D(Pi, dir, length=length, offset=0, lw=1,
                                 color=color, visible=visible, zorder=zorder)
                flecha.set_arrowstyle(arrowstyle)
                axes.add_artist(flecha)
                flechas.append(flecha)

            pos_texti = barra.coords[0] + np.dot(barra.Mr,
                                                [0.05*L, 0.08*L, 0.08*L])
            pos_textf = barra.coords[1] + np.dot(barra.Mr,
                                                [-0.05 * L, 0.08 * L, 0.08 * L])
            texti = axes.text(pos_texti[0], pos_texti[1], pos_texti[2], str(wxi),
                              fontsize=fontsize, horizontalalignment='center',
                              visible=visible, zorder=7)
            textf = axes.text(pos_textf[0], pos_textf[1], pos_textf[2], str(wxf),
                              fontsize=fontsize, horizontalalignment='center',
                              visible=visible, zorder=7)
            artists.extend(flechas + [texti, textf])

        # elif barra.w['terna'] == 'local':
        #     # Discretización a lo largo de la barra
        #     X = np.linspace(0.05*L, 0.95*L, 7)
        #     XYZ_loc = np.row_stack((X, X * 0 + L * 0.02, X * 0 + L * 0.02))
        #     XYZ_glob = np.dot(barra.Mr, XYZ_loc) + np.tile(barra.coords[0], (XYZ_loc.shape[1], 1)).T
        #     pos_texti = barra.coords[0] + np.dot(barra.Mr, [0.05*L, 0.08*L, 0.08*L])
        #     pos_textf = barra.coords[1] + np.dot(barra.Mr, [-0.05 * L, 0.08 * L, 0.08 * L])
        #
        #     line = art3d.Line3D(XYZ_glob[0, :], XYZ_glob[1, :], XYZ_glob[2, :], marker=(3,0,30),
        #                         markevery=slice(1, 7), linestyle='-', color=color, lw=1,
        #                         ms=4, visible=visible, zorder=7)
        #     axes.add_artist(line)
        #     texti = axes.text(pos_texti[0], pos_texti[1], pos_texti[2], str(wxi), fontsize=fontsize,
        #                       horizontalalignment='center', visible=visible, zorder=7)
        #     axes.add_artist(texti)
        #     textf = axes.text(pos_textf[0], pos_textf[1], pos_textf[2], str(wxf), fontsize=fontsize,
        #                       horizontalalignment='center', visible=visible, zorder=7)
        #     axes.add_artist(textf)
        #     artists.extend([line, texti, textf])
    return artists

def plot_indice_barra(barra, axes, fontsize, visible=True):
    L = barra.long
    offset = 0.07

    # Coordenadas locales del texto
    xyz_l = np.array([L / 2, offset * L, offset * L])
    # Coordenadas globales del texto
    xyz_g = np.dot(barra.Mr, xyz_l) + barra.nodos[0].coords

    text = axes.text(xyz_g[0], xyz_g[1], xyz_g[2], '[' + str(barra.indice) + ']',
                     fontsize=fontsize, visible=visible)
    return [text]



def plot_originarrows(axes, fontsize=7, color='k', visible=True, zorder=1):
    flechax = FlechaAtenea3d(axes, [0,0,0], [1,0,0], label='X',
                             fontsize=fontsize, length=1, reverse=0,
                             offset=0, lw=0.5, color=color,
                             visible=visible, zorder=zorder)
    flechax.set_arrowstyle('fancy, head_length=5, head_width=3')
    flechay = FlechaAtenea3d(axes, [0,0,0], [0,1,0], label='Y',
                             fontsize=fontsize, length=1, reverse=0,
                             offset=0, lw=0.5, color=color,
                             visible=visible, zorder=zorder)
    flechay.set_arrowstyle('fancy, head_length=5, head_width=3')
    flechaz = FlechaAtenea3d(axes, [0,0,0], [0,0,1], label='Z',
                             fontsize=fontsize, length=1, reverse=0,
                             offset=0, lw=0.5, color=color,
                             visible=visible, zorder=zorder)
    flechaz.set_arrowstyle('fancy, head_length=5, head_width=3')
    return [flechax, flechay, flechaz]

def zoom2extent(nodos, ax, margin=0.05):
    tol = 0.01
    xmin, xmax, ymin, ymax, zmin, zmax = getNodesExtent(nodos)
    if (xmin == xmax) and (ymin == ymax) and (zmin == zmax):
        return
    xmargin = (xmax-xmin)*margin + tol
    ymargin = (ymax-ymin)*margin + tol
    zmargin = (zmax-zmin)*margin + tol
    max_margin = max(xmargin,ymargin,zmargin)
    axis_bounds = [xmin-xmargin, xmax+xmargin,
                    ymin-ymargin, ymax+ymargin,
                    zmin-zmargin, zmax+zmargin]
    max_bound = max(axis_bounds[1]-axis_bounds[0],
                    axis_bounds[3]-axis_bounds[2],
                    axis_bounds[5]-axis_bounds[4])
    ax.set_xlim3d(axis_bounds[0],axis_bounds[0]+max_bound)
    ax.set_ylim3d(axis_bounds[2],axis_bounds[2]+max_bound)
    ax.set_zlim3d(axis_bounds[4],axis_bounds[4]+max_bound)

def getNodesExtent(nodos):
    """
    Obtener límites de los nodos
    """
    nodos_xcoords = []
    nodos_ycoords = []
    nodos_zcoords = []
    for nodo in nodos.values():
        XYZ = nodo.coords
        nodos_xcoords.append(XYZ[0])
        nodos_ycoords.append(XYZ[1])
        nodos_zcoords.append(XYZ[2])
    xmin = min(nodos_xcoords)
    xmax = max(nodos_xcoords)
    ymin = min(nodos_ycoords)
    ymax = max(nodos_ycoords)
    zmin = min(nodos_zcoords)
    zmax = max(nodos_zcoords)
    return xmin, xmax, ymin, ymax, zmin, zmax

def update_ax_arrows(axes, nodos, barras, scale):
    ax = axes
    Lave = 1
    if barras:
        Lave = 0
        for barra in barras.values():
            L = barra.long
            Lave += L
        Lave = Lave / len(barras)
    length0 = Lave * scale

    for flecha in axes.origin_arrows:
        flecha.update_length(length0)

    for nodo in nodos.values():
        for group in nodo.artists.values():
            for artist in group:
                if (type(artist) is FlechaAtenea3d) or (type(artist) is curvedArrow3D):
                    artist.update_length(length0)

    for barra in barras.values():
        for group,list in barra.artists.items():
            if group == 'ejes': length = 0.7*length0
            else: length = 1*length0
            for artist in list:
                if (type(artist) is FlechaAtenea3d) or (type(artist) is cargadistribuidaAtenea):
                    artist.update_length(length)

    return length0

def get_max_estr(estructura):
    estructura.calc_dist_max()
    return estructura.dist_max

def get_max_axis(ax):
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    zmin, zmax = ax.get_zlim()
    dx = np.abs(xmax-xmin)
    dy = np.abs(ymax-ymin)
    dz = np.abs(zmax-zmin)
    return max(dx,dy,dz)
# ============================================================================================
def plot_resultados(estructura, axes=None, reacciones=True, scale_def=None,
                    deformada=True, diagrama=False):
    """
    Llama a las funciones plot_diagramas y plot_reacciones. Define la escala
    con la que se plotean las reacciones en funcion del tamaño de la
    estructura.

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo el
                            ploteo.
        diagrama [str]: define que diagrama se desea plotear, 'N' para
                        plotear diagrama de esfuerzo normal, 'Q' para plotear
                        diagrama de esfuerzo de corte y 'M' para plotear
                        diagrama de momentos flexores.
        scale_d [int]: argumento default, en caso de que el usuario desee
                       definir la escala de los diagramas lo puede hacer
                       definiendo su valor. Si el usuario no lo define, la
                       escala se calcula automaticamente en funcion del tamaño
                       de la estructura y de los valor maximos de los
                       diagramas.
        diagramas [bool]: si se desea plotear diagramas se define como 'True',
                          si no se desea plotear diagramas se define como
                          'False'.
        reacciones [bool]: si se deasea plotear las reacciones se define como
                           'True', si no se desea plotear las reacciones se
                           define como 'False'.
    """
    if not axes:  # Crear figura si no es input de la función
        axes = create_axes()

    # Plotear Deformada
    if deformada:
        if scale_def == None: scale_def = definir_escala_deformada(estructura)
        plot_deformada(estructura, axes, scale_def)
    # Plotear Reacciones
    for nodo in estructura.nodos.values():
        if not hasattr(nodo, 'artists'): nodo.artists = {}
        nodo.artists['reacciones'] = plot_vinculos(nodo, axes, fontsize=8,
                                        color='r', tipo='reaccion',
                                        visible=reacciones, zorder=1)
    plot_estructura(estructura, axes=axes, fuerzas=False, temp=False, cedimientos=False)
    # Plotear Diagramas
    if diagrama:
        scale_diag = definir_escala_diagramas(estructura, diagrama)
        plot_diagramas_global(estructura, axes, diagrama, scale_diag)
    plt.show()
    return axes

def definir_escala_deformada(estructura):
    # Busco el punto mas deformado
    barras = estructura.barras
    max_d = 0
    for barra in barras.values():
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X, (1000, 1))
        # Desplazamientos en coordenadas locales de la barra
        ux, uy, uz = barra.compute_displacements(X)
        delta_max = max((ux**2 + uy**2 + uz**2)**0.5)
        max_d = max(max_d, delta_max)
    scale_def = 0.5*get_max_estr(estructura)/max_d
    return scale_def

def plot_deformada(estructura, axes, scale_def, linewidth=1, color='g'):
    barras = estructura.barras
    # Definir escala
    scale = scale_def
    for barra in barras.values():  # ploteo elemento por elemento
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X, (1000, 1))
        # Desplazamientos en coordenadas locales de la barra
        ux, uy, uz = barra.compute_displacements(X)
        uloc = np.column_stack((ux, uy, uz))
        uloc = uloc * scale
        # Posición deformada de la barra en coordenadas locales
        Xloc = uloc + np.column_stack((X, 0*X, 0*X))
        # Transformación a coordenadas globales
        uglob = np.dot(Xloc, barra.Mr.T) + barra.nodos[0].coords

        Deformada = art3d.Line3D(uglob[:,0], uglob[:,1], uglob[:,2],
                                 linewidth=linewidth, color=color)
        axes.add_artist(Deformada)
        if not hasattr(barra, 'artists'): barra.artists = {}
        barra.artists['deformada'] = [Deformada]

def plot_reacciones(estructura, axes, visible=True, fontsize=8, color='r',
                    zorder=1, precision=3):
    """
    """
    # Plotear Reacciones
    for nodo in estructura.nodos.values():
        if not hasattr(nodo, 'artists'): nodo.artists = {}
        nodo.artists['reacciones'] = plot_vinculos(nodo, axes, fontsize=8,
                                          color = 'r',
                                          tipo='reaccion', visible=visible,
                                          zorder=zorder, precision=precision)
    return

def definir_escala_diagramas(estructura, diagrama):
    # Obtener valor maximo para definir escala
    Smax = 0
    for barra in estructura.barras.values():
        # Calcular solicitaciones en barra
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X, (1000, 1))
        N, Qy, Qz, Mt, My, Mz = barra.compute_internal_forces(X)

        # Calcular máximos
        if diagrama == 'N':
            Smax = np.max((Smax,np.max(np.abs(N))))
        elif diagrama == 'Qy':
            Smax = np.max((Smax,np.max(np.abs(Qy))))
        elif diagrama == 'Qz':
            Smax = np.max((Smax,np.max(np.abs(Qz))))
        elif diagrama == 'Mt':
            Smax = np.max((Smax,np.max(np.abs(Mt))))
        elif diagrama == 'My':
            Smax = np.max((Smax,np.max(np.abs(My))))
        elif diagrama == 'Mz':
            Smax = np.max((Smax,np.max(np.abs(Mz))))

    # Obtener dimensiones de estructura
    max_d = get_max_estr(estructura)/15
    scale = 1
    if Smax != 0:
        scale = max_d/Smax
    return scale

def plot_diagramas_global(estructura, axes, diagrama, scale_d=1, precision=3,
                          fontsize=8):

    for barra in estructura.barras.values():
        # Crear el poligono en coordenadas locales
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X, (1000, 1))
        N, Qy, Qz, Mt, My, Mz = barra.compute_internal_forces(X)

        # Generar vectores de traslacion
        n = len(X) + 2

        if diagrama == 'N':
            barra.artists['N_gl'] = []
            E_escalado = np.column_stack((X, N*scale_d))
            dir = [0,0,1]
            titulo = 'Esfuerzo Normal'
        if diagrama == 'Qy':
            barra.artists['Qy_gl'] = []
            E_escalado = np.column_stack((X, Qy*scale_d))
            dir = (0,1,0)
            titulo = 'Corte en "y"'
        if diagrama == 'Qz':
            barra.artists['Qz_gl'] = []
            E_escalado = np.column_stack((X, Qz*scale_d))
            dir = [0,0,1]
            titulo = 'Corte en "z"'
        if diagrama == 'My':
            barra.artists['My_gl'] = []
            E_escalado = np.column_stack((X, My*scale_d))

            dir = [0,0,1]
            titulo = 'Momento flector en "y"'
        if diagrama == 'Mz':
            barra.artists['Mz_gl'] = []
            E_escalado = np.column_stack((X, Mz*scale_d))

            dir = [0,-1,0]
            titulo = 'Momento flector en "z"'
        if diagrama == 'Mt':
            barra.artists['Mt_gl'] = []
            E_escalado = np.column_stack((X, Mt*scale_d))
            dir = [0,0,1]
            titulo = 'Momento torsor'

        # Extremos del diagrama
        E_ext_init = np.array([[0, 0]])
        E_ext_fin = np.array([[L, 0]])
        # Generar vector completo para definir poligono
        E_aux = np.row_stack((E_ext_init, E_escalado, E_ext_fin))

        E_totales = []  # Lista de matrices para definir los distintos poligonos
        for n in range(E_aux.shape[0]):  # Iterar en las filas de E_aux
            if n == 0:  # Primera iteracion
                E = E_aux[n, :]
                continue
            if E_aux[n, 1] * E_aux[n - 1, 1] >= 0:  # E del mismo signo
                E = np.vstack((E, E_aux[n, :]))
                if n == E_aux.shape[0] - 1:  # ultima iteracion
                    E_totales.append(E)
                continue
            if E_aux[n, 1] * E_aux[n - 1, 1] < 0:  # E distinto signo
                E_totales.append(E)
                E = E_aux[n, :]
                continue

        for E in E_totales:
            if any([n >= 0 for n in E[:, 1]]):  # Esfuerzo positivo
                color = 'r'
            if any([n < 0 for n in E[:, 1]]):  # Esfuerzo negativo
                color = 'b'

            # Asignar los valores en las columnas correspondientes
            XYZ_loc = np.empty((E.shape[0],3))
            XYZ_loc[:,0] = E[:,0]
            XYZ_loc[:, 1] = E[:, 1] * dir[1]
            XYZ_loc[:, 2] = E[:, 1] * dir[2]

            # Transformación a coordenadas globales
            XYZ_glob = np.dot(XYZ_loc, barra.Mr.T) + barra.nodos[0].coords

            # Adaptar los verts al argumento del Poly3DCollection
            verts = [[]]
            for vert in XYZ_glob: verts[0].append(tuple(vert))

            Esfuerzo = art3d.Poly3DCollection(verts, alpha=0.5, facecolor=color,
                                              lw=1, visible=True, zorder=7)
            axes.add_collection3d(Esfuerzo)

            barra.artists[str(diagrama)+'_gl'].append(Esfuerzo)

def plot_diagramas_local(barra, axes, diagrama, x, precision=3, fontsize=8):
    """
    Plotea para la barra el diagrama de caracteristicas que se le pida.
    Para calcular los esfuerzos caracteristicos, iterando barra por barra
    se llama al metodo del elemento barra compute_internal_forces y se lo
    evalúa en 1000 puntos equidistantes. A continuación se plotea el vector
    con los 1000 resultados. Luego se llama a la funcion hallar_maximo_diagramas
    para identificar los valores maximos de la solicitacion en la barra
    y plotear su valor donde corresponde.

    Argumentos:
        barra [class]: clase definida en el modulo bar3d, que reune toda
                       la informacion necesaria para llevar acabo el ploteo.
        axes [class]: figura donde se desean plotear los diagramas.
        diagrama [str]: string que define que diagrama se desea plotear,
                        'N' para plotear diagrama de esfuerzo normal, 'Qy'
                        para plotear diagrama de esfuerzo de corte en el eje
                        local y, 'Qz' para plotear diagrama de esfuerzo de
                        corte en el eje local z, 'My' para plotear diagrama
                        de momentos flexores en el eje local y, 'Mz' para
                        plotear diagrama de momentos flexores en el eje local
                        z y 'Mt' para plotear el diagrama de momento torsor.
        x [float]: valor de x en el cual queremos evaluar y colocar un marker
        precision [int]
        fontsize [int]
    """

    # Calcular solicitaciones en barra
    L = barra.long
    X = np.linspace(0, L, num=1000)
    X = np.reshape(X, (1000, 1))
    N, Qy, Qz, Mt, My, Mz = barra.compute_internal_forces(X)
    Nx, Qyx, Qzx, Mtx, Myx, Mzx = barra.compute_internal_forces(x)
    # text alignment
    if x < L/2:
        factor = L/40
        alignment = 'left'
    elif x >= L/2:
        factor = -L/40
        alignment = 'right'
    artists = []
    #   Generar vectores de traslacion
    n = len(X) + 2

    if diagrama == 'N':
        E_escalado = np.column_stack((X, N))
        # Valor para el marker
        y = Nx[0]
    if diagrama == 'Qy':
        E_escalado = np.column_stack((X, Qy))
        # Valor para el marker
        y = Qyx[0]
    if diagrama == 'Qz':
        E_escalado = np.column_stack((X, Qz))
        # Valor para el marker
        y = Qzx[0]
    if diagrama == 'My':
        E_escalado = np.column_stack((X, My))
        # Valor para el marker
        y = Myx[0]
    if diagrama == 'Mz':
        E_escalado = np.column_stack((X, Mz))
        # Valor para el marker
        y = Mzx[0]
    if diagrama == 'Mt':
        E_escalado = np.column_stack((X, Mt))
        # Valor para el marker
        y = Mtx[0]

    # Generar vector completo para definir poligono
    E_aux = E_escalado
    E_totales = []  # Lista de matrices para definir los distintos poligonos
    for n in range(E_aux.shape[0]):  # Iterar en las filas de E_aux
        if n == 0:  # Primera iteracion
            E = E_aux[n, :]
            continue
        if E_aux[n, 1]*E_aux[n-1,1] > 0: # E del mismo signo
            E = np.vstack((E, E_aux[n, :]))
            if n == E_aux.shape[0] - 1: # ultima iteracion
                E_totales.append(E)
            continue
        elif E_aux[n, 1]*E_aux[n-1, 1] < 0: # E distinto signo
            E_totales.append(E)
            E = E_aux[n, :]
            continue
        elif E_aux[n, 1]*E_aux[n-1,1] == 0 and E_aux[n-1, 1]==0 and E_aux[n, 1]!=0:
            E_totales.append(E)
            E = E_aux[n, :]
            continue
        elif E_aux[n, 1]*E_aux[n-1,1] == 0 and E_aux[n, 1]==0 and E_aux[n-1, 1]!=0:
            E = np.vstack((E, E_aux[n, :]))
            if n == E_aux.shape[0] - 1: # ultima iteracion
                E_totales.append(E)
            continue
        elif E_aux[n,1]==0 and E_aux[n-1, 1]==0:
            E = np.vstack((E, E_aux[n, :]))
            if n == E_aux.shape[0] - 1: # ultima iteracion
                E_totales.append(E)
            continue

    # Extremos del diagrama
    E_ext_init = np.array([[0, 0]])
    E_ext_fin = np.array([[L, 0]])
    # Completar con punto inicial y final de los pol'igoonos
    E_totales[0] = np.row_stack((E_ext_init, E_totales[0]))
    E_totales[-1] = np.row_stack((E_totales[-1], E_ext_fin))

    for E in E_totales:
        if any([n >= 0 for n in E[:, 1]]):  # Esfuerzo positivo
            color = 'r'
        if any([n < 0 for n in E[:, 1]]):  # Esfuerzo negativo
            color = 'b'
        # Graficar Normal
        Esfuerzo = Polygon(E, color=color, ls='solid', lw=1, alpha=0.5,
                           visible=True, zorder=7)
        axes.add_patch(Esfuerzo)
        artists.append(Esfuerzo)

    # Agregar marcador de posición x
    if y>=0: color='r'
    else: color='b'
    Marker = Line2D([x, x], [0, y], linewidth=2, color='y', zorder=1)
    axes.add_artist(Marker)
    Marker_text = Text(x+factor, y*0.8, to_precision(y, precision),
                       fontsize=fontsize,
                       horizontalalignment=alignment, zorder=8,
                       bbox=dict(facecolor='white', edgecolor=color,
                                 boxstyle='round', alpha=0.6))
    axes.add_artist(Marker_text)
    artists.append(Marker_text)
    axes.autoscale(axis='x', tight=True)
    axes.autoscale(axis='y', tight=None)

    return artists
