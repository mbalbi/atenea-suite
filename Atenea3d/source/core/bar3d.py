# módulo bar3d.py
"""
Módulo de clases Nodo y Barra.

creado por: equipo Atenea-UBATIC
"""

import numpy as np

class Nodo:
    """
    Clase que define objetos nodo, donde se plantean las ecuaciones de
    equilibrio. Dos nodos permiten formar un elemento.
    
    Argumentos:
        - coords [list]: Lista con las coordenadas del nodo [X,Y,Z]
        - indice [int]: Índice del nodo
    
    Atributos:
        - coords [list]: Lista con las coordenadas del nodo [X,Y,Z]
        - indice [int]: Índice del nodo
        - elemnames [list]: Lista con el tipo de cada elemento que llega al nodo
        - restr [list]: Lista con las restricciones de vínculo [Rx,Ry,Rz,Mx,My,Mz]
        - U [list]: Lista con los desplazamientos nodales calculados [dx,dy,dz,giro x,giro y,giro z]
        - P [list]: Lista con las cargas nodales exteriores [Px,Py,Pz,Mx,My,Mz]
        - Pw [list]: Lista con las cargas nodales equivalentes por cargas
                     distribuidas en los elementos
        -nodo_artists [dict]: Diccionario con todos los artistas asociados al nodo. 'self': el propio nodo,
                            'v': vinculos, 'p': fuerzas puntuales, 'c': cedimientos de vinculo,

    Métodos:
        - count_dofs(inicial): Cuenta y enumera (a partir de 'inicial') la
                               cantidad de grados de libertad del nodo en
                               función de los elementos que llegan a el. 

    """
    def __init__(self,coords,indice):
        """
        coords: list, que es el value del diccionario XYZ
        indice: keys del diccionario XYZ
        """
        # set attributes
        self.coords = coords
        self.indice = indice
        self.elemnames = [] # almacenar tipos de elementos que llegan al nodo
        # inicializar atributos vacios
        self.restr = [0,0,0,0,0,0] # 1 si hay vinculo restrictivo
        self.R = np.zeros((6,1)) # reacciones de vinculo
        self.U = [0,0,0,0,0,0] # desplazamientos nodales
        self.P = [0,0,0,0,0,0] # cargas nodales exteriores
        self.Pw = [0,0,0,0,0,0] # cargas nodales equivalentes por w
        self.ced = [0,0,0,0,0,0] # cedimientos de vinculo

    # def count_dofs(self,inicial):
    def count_dofs(self):
        """
        """
        # Cantidad de dofs
        if self.elemnames: 
            if all(i=='3DTruss' for i in self.elemnames):
                self.ndof = 3
            else:
                self.ndof = 6
        
        # # Cantidad de dofs
        # if self.elemnames: # Para que un nodo vacío no sea considerado como truss
        #     if all(i=='3DTruss' for i in self.elemnames):
        #         self.ndof = 3
        #         self.restr = self.restr[0:3]
        #         self.U = self.U[0:3]
        #         self.P = self.P[0:3]
        #         self.Pw = self.Pw[0:3]
        #         self.R = self.R[0:3]
        #         self.ced = self.ced[0:3]
        # self.ndof = len(self.U)
        # # Enumerar grados de libertad
        # self.DOF = [inicial + a for a in range(self.ndof)]


class Barra:
    """
    Clase que define objetos barra, donde se define la estática y cinemática
    de los objetos de viga y de reticulado. Guarda toda la información sobre
    propiedades del elemento, fuerzas básicas y deformaciones equivalentes.
    
    Argumentos:
        - indice [int]: Indice del elemento
        - nodos [list]: Lista de objetos nodos inicial y final [nodoi, nodof]
        - ElemName [str]: String que define el tipo de elemento; '3DFrame' o
                          '3DTruss'
        - rel [list]: Lista de releases para cada fuerza básica; 1: release
        - w [dict]: Diccionario de cargas distribuidas sobre el elemento del
                    tipo {'wxi':0, 'wxf':0, 'wyi':0, 'wyf':0, 'wzi':0, 'wzf':0, 'terna':'local'}
        - e0 [list]: Lista de deformaciones impuestas [dsupy, dinfy, hy, dsupz, dinfz, hz] donde
                     hy y hz son la mayor distancia entre dos puntos de la seccion segun los ejes
                     locales 'y' y 'z' respectivamente; y dsup y dinf son las temperaturas en las
                     caras superior e inferior respectivamente, de los ejes 'y' o 'z'.
                     Se denomina superior e inferior en correspondencia con el sentido positivo y
                     negativo del eje (ya sea 'y' o 'z') local, respectivamente.
                     Se consideran uniformes en todo el elemento.
        - data [dict]: Diccionario de propiedades del material y sección del
                       tipo {'mat':{'E':,'mu':,'l':},'seccion':{'A':,'Iy':,'Iz':,'Jt':}}
    
    Atributos:
        - indice [int]: Indice del elemento
        - nodos [list]: Lista de objetos nodos inicial y final [nodoi, nodof]
        - coords [numpy array]: Matriz de coordenadas de nodos (2x3)
        - ElemName [str]: String que define el tipo de elemento; '3DFrame' o
                          '3DTruss'
        - nq [int]: Cantidad de fuerzas básicas
        - seccion [dict]: Diccionario con valores geométricos de la sección
        - mat [dict]: Diccionario con parámetros constitutivos del material
        - w [dict]: Diccionario con cargas distribuidas; del tipo 
                    {'wxi':0, 'wxf':0, 'wyi':0, 'wyf':0, 'wzi':0, 'wzf':0, 'terna':'local'}
        - w_local [numpy array]: vector de cargas distribuidas
        - e0 [list]: Lista de deformaciones impuestas [dsupy, dinfy, hy, dsupz, dinfz, hz]
        - rel [list]: Lista de releases para cada fuerza básica; [Mxi, Myi, Mzi, Pxf, Myf, Mzf]; 1: release
        - long [num]: Longitud del elemento
        - dc [numpy array]: Vector de cosenos directores del elemento
        - Mr [numpy array]: Matriz de rotación de traslaciones únicamente
        - br [numpy array]: Matriz de rotación (traslaciones más giros)
        - b [numpy array]: Matriz estática en coordenadas locales
        - bg [numpy array]: Matriz estática en coordenadas globales
        - a [numpy array]: Matriz cinemática en coordenadas locales
        - ag [numpy array]: Matriz cinemática en coordenadas globales
        - k [numpy array]: Matriz de rigidez
        - f [numpy array]: Matriz de flexibilidad
        - barra_artists [dict]: Diccionario con todos los artistas de la carga de datos asociados a la barra
        - results_artists [dict]: Diccionario con todos los artistas de los resultados asociados a la barra
    Métodos:
        - assign_data(data): Inicializa y guarda los datos de material y sección
                             como atributos
        - assign_w(w): Guarda las cargas distribuidas como atributo y arma el
                       vector con las cargas distribuidas.
        - assign_e0(e0): Inicializa y guarda los datos de e0 como atributo
        - assign_rel(rel): Inicializa y guarda los valores de rel como atributo;
                           actualiza el vector de elemnames de cada nodo
        - calc_geom(): Calcula longitud, cosenos directores y Mr
        - define_br(): Define la matriz de rotación según el tipo de elemento
        - define_b(): Define la matriz estática en coordenadas locales
        - define_bg(): Define la matriz estática en coordenadas globales
        - define_a(): Define la matriz cinemática en coordenadas locales
        - define_ag(): Define la matriz cinemática en coordenadas globales
        - calc_f_k(): Define matriz de flexibilidad y de rigidez, descontando
                      los releases.
        - calc_pw(): Calcula el vector de cargas nodales equivalentes y
                     actualiza los nodos con las cargas
        - calc_element_loading(): Calcula las deformaciones y las fuerzas
                                  básicas equivalentes por cargas distribuidas
                                  y deformaciones impuestas
        - generate_dofs(): Define DOFs del elemento a partir de los DOFs de sus
                           nodos
        - compute_internal_forces(x): Computa los esfuerzos internos del elemento
                                      en la coordenada local x desde su nodo
                                      inicial
        
    """
    def __init__(self,indice,nodos,ElemName,rel=[],w={},e0=[],
                 data={'seccion':{},'mat':{}}):
        """
        nodos [int, int]: indice de nodo inicial y nodo final
        ElemName [str]
        """
        # Atributos
        self.indice = indice
        self.nodos = nodos
        self.coords = np.array([nodos[0].coords, nodos[1].coords])
        self.ElemName = ElemName

        # N de fuerzas básicas independientes
        if self.ElemName == '3DFrame': self.nq = 6
        elif self.ElemName == '3DTruss': self.nq = 1

        # Lista de fuerzas básicas
        self.qindex = []

        # Inicializar atributos para el metodo de las flexibilidades
        # Lista de releases agregados por el metodo de las flexibilidades
        self.new_rel = [0,0,0,0,0,0]

        # Asignar datos al elemento
        self.assign_data(data)

        # Calcular matrices estáticas y cinemáticas
        self.define_br() # Matriz de rotación (locales a globales)
        self.define_bg() # Matriz estática (locales y globales)
        self.define_ag() # Matriz cinemática (locales y globales)
        
        # Asignar datos al elemento
        self.assign_w(w)
        self.assign_e0(e0)
        self.assign_rel(rel)

        # Asignar datos al nodo
        self.nodes_elemnames()

    def update(self):
        """
        Actualizar el cálculo de matrices y vectores de carga
        """
        self.calc_geom()
        self.define_br()
        self.define_bg()
        self.define_ag()
        self.assign_w(self.w)
    
    def assign_data(self, data):
        """
        Asignar seccion y material. Computar matriz de flexibildiad y rigidez
        locales
        """
        self.mat = data['mat']
        self.seccion = data['seccion']
        if not self.seccion:
            self.seccion['alpha'] = 0
        self.calc_geom() # Cosenos directores y longitud
        # if not self.seccion:
        #     self.seccion['name'] = 'None'
        #     self.seccion['A'] = 1
        #     self.seccion['Iy'] = 1
        #     self.seccion['Iz'] = 1
        #     self.seccion['Jt'] = 1
        #     self.seccion['alpha'] = 0
        # if not self.mat:
        #     self.mat['name'] = 'None'
        #     self.mat['E'] = 1
        #     self.mat['mu'] = 1
        #     self.mat['l'] = 1
    
    def assign_w(self,w):
        """
        Asignar carga distribuida y computar cargas nodales equivalentes
        """
        # Inicializar
        self.w = {'wxi':0, 'wxf':0, 'wyi':0, 'wyf':0, 'wzi':0, 'wzf':0, 'terna':'local'}
        if w:
            self.w = w
        
        self.w_local = np.array([[self.w['wxi']],[self.w['wyi']],[self.w['wzi']],
                                 [self.w['wxf']],[self.w['wyf']],[self.w['wzf']]])
        # Girar en caso de terna global
        if self.w['terna'] == 'global':
            self.w_local = np.dot(np.linalg.inv(self.br[0:6, 0:6]),self.w_local)
    
    def assign_e0(self,e0):
        """
        Asignar deformaciones impuestas (temperatura por ej.) al elemento
        """
        self.e0 = [0, 0, 0] # [baricentrica, grad y, grad z]
        if e0:
            self.e0 = e0
    
    def assign_rel(self, rel):
        """
        Se cargan los releases a los elementos. En caso de que no haya ningun
        release cargado se predefinen las condiciones de borde para los
        elementos segun sean Frame o Truss. Las primera fila indica las
        condiciones del primer nodo y la segunda fila las condiciones del
        segundo nodo, siendo 1 restringido y 0 libre.
        """

        # Pasar ElemName a cada nodo
        self.nodos[0].elemnames.append(self.ElemName)
        self.nodos[1].elemnames.append(self.ElemName)
        
        # Incializar releases del elemento
        if self.ElemName == '3DFrame':
            self.rel = [0,0,0,0,0,0] # [Mt, Myi, Mzi, Axil, Myf, Mzf]
        elif self.ElemName == '3DTruss':
            self.rel = [0] # [Axil]
        
        # Agregar releases al elemento
        if rel:
            self.rel = rel

    def nodes_elemnames(self):
        # Pasar ElemName a cada nodo
        self.nodos[0].elemnames.append(self.ElemName)
        self.nodos[1].elemnames.append(self.ElemName)
    
    def calc_geom(self):
        """
        Defino parametros geometricos del elemento:
        Longitud; vector deltax, deltay, deltaz; cosenos directores
        """
        dif = self.coords[1] - self.coords[0]
        # Longitud
        self.long = np.sqrt(np.dot(dif,dif))
        # Cosenos directores
        self.dc = dif / self.long
        # Calculos preliminares
        dX = dif[0]
        dY = dif[1]
        dZ = dif[2]
        L = self.long
        proy_h = np.sqrt(dX**2+dY**2)
        alpha = np.deg2rad(self.seccion['alpha'])
        # Matriz que rota la seccion un angulo alpha en la direccion del eje local x
        Ma = np.array([[1,             0,              0],
                       [0, np.cos(alpha), -np.sin(alpha)],
                       [0, np.sin(alpha), np.cos(alpha)]])
        # Matriz de cambio de base de coordenadas locales a globales
        if proy_h == 0:
            #La barra es vertical
            self.angle = 90
            self.Mr0 = np.array([[0,    1,    0],
                                 [0,    0,    dZ/L],
                                 [dZ/L, 0,    0]])
        else:
            self.angle = np.rad2deg(np.arctan(dY / proy_h))
            self.Mr0 = np.array([[dX/L,  dY/proy_h, dX*dZ/proy_h/L],
                                 [dY/L, -dX/proy_h, dY*dZ/proy_h/L],
                                 [dZ/L,          0, -proy_h/L]])
        # Matriz de rotacion total
        self.Mr = np.dot(self.Mr0, Ma)
        
    def define_br(self):
        """
        Matriz de rotacion, relaciona las fuerzas elementales p de cada
        elemento en terna global, a terna local
        """
        if self.ElemName == '3DFrame':
            # La barra es vertical
            self.br = np.zeros((12, 12))
            self.br[0:3, 0:3] = self.Mr
            self.br[3:6, 3:6] = self.Mr
            self.br[6:9, 6:9] = self.Mr
            self.br[9:12, 9:12] = self.Mr
        elif self.ElemName == '3DTruss':
            self.br = np.zeros((6, 6))
            self.br[0:3, 0:3] = self.Mr
            self.br[3:6, 3:6] = self.Mr
            
    def define_b(self):
        """
        Matriz estática, relaciona las fuerzas elementales p de cada elemento 
        con las fuerzas básicas q del mismo en una terna local.
        """
        L = self.long
        if self.ElemName == '3DFrame':
            self.b = np.array([[0,    0,    0,    -1,   0,    0],
                               [0,    0,    1/L,  0,    0,    1/L],
                               [0,    -1/L, 0,    0,    -1/L, 0],
                               [1,    0,    0,    0,    0,    0],
                               [0,    1,    0,    0,    0,    0],
                               [0,    0,    1,    0,    0,    0],
                               [0,    0,    0,    1,    0,    0],
                               [0,    0,    -1/L, 0,    0,    -1/L],
                               [0,    1/L,  0,    0,    1/L,  0],
                               [-1,   0,    0,    0,    0,    0],
                               [0,    0,    0,    0,    1,    0],
                               [0,    0,    0,    0,    0,    1]])
        elif self.ElemName == '3DTruss':
            self.b = np.array([[-1],
                                [0],
                                [0],
                                [1],
                                [0],
                                [0]])
        
    def define_bg(self):
        """
        Defino la matriz bg, que relaciona las fuerzas elementales p en terna
        global, con las fuerzas basicas q en terna local.
        """
        self.define_b()
        self.bg = np.dot(self.br,self.b)
    
    def define_a(self):
        """
        Matriz cinemática...
        """
        self.a = np.transpose(self.b)
    
    def define_ag(self):
        """
        Defino la matriz ag
        """
        self.ag = np.transpose(self.bg)
    
    def calc_f_k(self):
        """
        """
        A = self.seccion['A']
        Iy = self.seccion['Iy']
        Iz = self.seccion['Iz']
        Jt = self.seccion['Jt']
        E = self.mat['E']
        mu = self.mat['mu']
        G = E/(2*(1 + mu))
        L = self.long
        if self.ElemName == '3DFrame':
            # Matriz de flexibilidad
            self.f0 = np.array([[L/(Jt*G), 0, 0, 0, 0, 0],
                                [0, L/(3*E*Iy), 0, 0, -L/(6*E*Iy), 0],
                                [0, 0, L/(3*E*Iz), 0, 0, -L/(6*E*Iz)],
                                [0, 0, 0, L/(E*A), 0, 0],
                                [0, -L/(6*E*Iy), 0, 0, L/(3*E*Iy), 0],
                                [0, 0, -L/(6*E*Iz), 0, 0, L/(3*E*Iz)]])
            # Remover filas y columnas por releases
            q_elim = [i for i in range(len(self.rel)) if self.rel[i]==1]
            self.f = np.delete(self.f0, q_elim, 1)
            self.f = np.delete(self.f, q_elim, 0)
            # Matriz de rigidez
            self.k = np.linalg.inv(self.f)
        if self.ElemName == '3DTruss':
            # Matriz de flexibilidad
            self.f0 = np.array([[L/(E*A)]])
            self.f = self.f0
            # Matriz de rigidez
            self.k = np.array([[(E*A)/L]])
    
    def calc_pw(self):
        """
        """

        # Variables
        L = self.long
        wxi = self.w_local[0,0]
        wyi = self.w_local[1,0]
        wzi = self.w_local[2,0]
        wxf = self.w_local[3,0]
        wyf = self.w_local[4,0]
        wzf = self.w_local[5,0]
        # Computar reacciones nodales equivalentes
        pw1 = -L/2*(wxi+wxf)
        pw2 = -L/6*(2*wyi+wyf)
        pw3 = -L/6*(2*wzi+wzf)
        pw8 = -L/6*(wyi+2*wyf)
        pw9 = -L/6*(wzi+2*wzf)
        # Guardar en elemento y en nodos
        if self.ElemName == '3DFrame':
            self.pw_local = np.array([[pw1], [pw2], [pw3], [0], [0], [0], [0],
                                      [pw8], [pw9], [0], [0], [0]])
            self.pw_global = np.dot(self.br,self.pw_local)
            # Completar nodos con cargas equivalentes globales
            self.nodos[0].Pw[0:3] += self.pw_global[0:3,0]
            self.nodos[1].Pw[0:3] += self.pw_global[6:9,0]
        elif self.ElemName == '3DTruss':
            self.pw_local = np.array([[pw1], [0], [0], [0], [0], [0]])
            self.pw_global = np.dot(self.br[0:6, 0:6],self.pw_local)
            # Completar nodos con cargas equivalentes globales
            self.nodos[0].Pw[0:3] += self.pw_global[0:3,0]
            self.nodos[1].Pw[0:3] += self.pw_global[3:6,0]
    
    def calc_element_loading(self):
        """
        Fuerzas básicas y deformaciones en el elemento por cargas distribuidas
        y deformaciones impuestas e0.
        """
        # Variables
        A = self.seccion['A']
        Iy = self.seccion['Iy']
        Iz = self.seccion['Iz']
        E = self.mat['E']
        l = self.mat['l']
        L = self.long
        wxi = self.w_local[0, 0]
        wyi = self.w_local[1, 0]
        wzi = self.w_local[2, 0]
        wxf = self.w_local[3, 0]
        wyf = self.w_local[4, 0]
        wzf = self.w_local[5, 0]
        # dsupy = self.e0[0]
        # dinfy = self.e0[1]
        # hy = self.e0[2]
        # dsupz = self.e0[3]
        # dinfz = self.e0[4]
        # hz = self.e0[5]
        e0 = l*self.e0[0]
        ky0 = l*self.e0[1]
        kz0 = l*self.e0[2]
        if self.ElemName == '3DFrame':
            # Deformaciones del elemento por condiciones impuestas
            self.ve00 = np.array([[0],
                                  [-kz0*L/2],
                                  [ky0*L/2],
                                  [e0*L],
                                  [kz0*L/2],
                                  [-ky0*L/2]])
            # Deformaciones del elemento por cargas distribuidas
            self.vw0 = np.array([[0],
                                 [-L**3/(360*E*Iy)*(7*wzf+8*wzi)],
                                 [L**3/(360*E*Iz)*(7*wyf+8*wyi)],
                                 [L**2/(6*E*A)*(2*wxf+wxi)],
                                 [L**3/(360*E*Iy)*(8*wzf+7*wzi)],
                                 [-L**3/(360*E*Iz)*(8*wyf+7*wyi)]])
            # Remover filas con releases
            q_elim = [i for i in range(len(self.rel)) if self.rel[i]==1]
            self.vw = np.delete(self.vw0,q_elim,0)
            self.ve0 = np.delete(self.ve00,q_elim,0)
        elif self.ElemName == '3DTruss':
            # Deformaciones del elemento por condiciones impuestas
            self.ve00 = np.array([e0*L])
            self.ve0 = self.ve00
            # Deformaciones del elemento por cargas distribuidas
            self.vw0 = np.array([[L**2/(6*E*A)*(2*wxf+wxi)]])
            self.vw = self.vw0
        # Fuerzas básicas equivalentes
        self.qw = np.dot(-self.k,self.vw)
        self.qe0 = np.dot(-self.k,self.ve0)

    def generate_dofs(self):
        """
        Arma una lista con los DOFs del nodo_i y nodo_f
        """
        # DOFs de cada nodo del elemento
        DOFi = self.nodos[0].DOF
        DOFf = self.nodos[1].DOF
        if self.ElemName == '3DFrame':
            self.DOF = DOFi + DOFf
        elif self.ElemName == '3DTruss':
            self.DOF = DOFi[0:3] + DOFf[0:3]

    def compute_internal_forces(self,x):
        """
        Calcular esfuerzos internos N, M y Q en la coordenada local x.
        x puede ser un vector
        """
        L = self.long
        wxi = self.w_local[0, 0]
        wyi = self.w_local[1, 0]
        wzi = self.w_local[2, 0]
        wxf = self.w_local[3, 0]
        wyf = self.w_local[4, 0]
        wzf = self.w_local[5, 0]
        # Esfuerzos internos
        if self.ElemName == '3DFrame':
            N = -self.end_forces[0] - x * wxi + ((wxi - wxf) / (2 * L)) * (x ** 2)
            Qy = -self.end_forces[1] - x * wyi + ((wyi - wyf) / (2 * L)) * (x ** 2)
            Qz = -self.end_forces[2] - x * wzi + ((wzi - wzf) / (2 * L)) * (x ** 2)
            Mt = -self.end_forces[3] + x * 0
            My = -self.end_forces[4] - self.end_forces[2] * x - (wzi * (x ** 2)) / 2 + ((wzi - wzf) * (x ** 3)) / (6 * L)
            Mz = -self.end_forces[5] + self.end_forces[1] * x + (wyi * (x ** 2)) / 2 - ((wyi - wyf) * (x ** 3)) / (6 * L)
        elif self.ElemName == '3DTruss':
            N = -self.end_forces[0] - x * wxi + ((wxi - wxf) / (2 * L)) * (x ** 2)
            Qy = -self.end_forces[1] - x * wyi + ((wyi - wyf) / (2 * L)) * (x ** 2)
            Qz = -self.end_forces[2] - x * wzi + ((wzi - wzf) / (2 * L)) * (x ** 2)
            Mt = 0 * x
            My = - self.end_forces[2] * x - (wzi * (x ** 2)) / 2 + ((wzi - wzf) * (x ** 3)) / (6 * L)
            Mz = self.end_forces[1] * x + (wyi * (x ** 2)) / 2 - ((wyi - wyf) * (x ** 3)) / (6 * L)
        return N,Qy,Qz,Mt,My,Mz
        
    def compute_displacements(self,x):
        """
        Calcular desplazamientos en la coordenada local x de la barra según
        las direcciones locales x (axial) e y (perpendicular)
        """
        # Datos de la barra
        A = self.seccion['A']
        Iy = self.seccion['Iy']
        Iz = self.seccion['Iz']
        E = self.mat['E']
        L = self.long
        # Cargas distribuidas en la barra
        wxi = self.w_local[0, 0]
        wyi = self.w_local[1, 0]
        wzi = self.w_local[2, 0]
        wxf = self.w_local[3, 0]
        wyf = self.w_local[4, 0]
        wzf = self.w_local[5, 0]
        # Obtener desplazamientos nodales en coordenadas globales
        # U = np.concatenate((self.nodos[0].U,self.nodos[1].U))
        # Obtener desplazamientos nodales en coordenadas locales
        Uloc = np.dot(np.transpose(self.br),self.U)
        if self.ElemName == '3DFrame':
            betay = (Uloc[2] - Uloc[8])/L
            betaz = (Uloc[7] - Uloc[1])/L
        elif self.ElemName == '3DTruss':
            betay = (Uloc[2] - Uloc[5]) / L
            betaz = (Uloc[4] - Uloc[1]) / L
        # Deformaciones internas
        if self.ElemName == '3DFrame':
            v2 = self.v[1]
            v3 = self.v[2]
            v4 = self.v[3]
            v5 = self.v[4]
            v6 = self.v[5]
        if self.ElemName == '3DTruss':
            v2 = 0
            v3 = 0
            v4 = self.v[0]
            v5 = 0
            v6 = 0
        # Desplazamientos internos en coordenadas locales para el giro de los
        # extremos y el alargamiento
        A1 = -(L*(wxf+2*wxi)-6*E*A*v4/L)/(6*E*A)
        A2 = 0
        A3 = -(3*L**3*wyf+7*L**3*wyi-120*E*Iz*(v3+v6))/(120*L**2*E*Iz)
        A4 = (2*L**3*wyf+3*L**3*wyi-120*E*Iz*(2*v3+v6))/(120*L*E*Iz)
        A5 = v3
        A6 = 0
        A7 = -(3*L**3*wzf+7*L**3*wzi+120*E*Iy*(v2+v5))/(120*L**2*E*Iy)
        A8 = (2*L**3*wzf+3*L**3*wzi+120*E*Iy*(2*v2+v5))/(120*L*E*Iy)
        A9 = -v2
        A10 = 0

        ux = A1*x + A2 + 1/E/A*(wxi*pow(x,2)/2+(wxf-wxi)*pow(x,3)/6/L)
        uy = A3*pow(x,3) + A4*pow(x,2) + A5*x + A6 + 1/E/Iz*(wyi/24*pow(x,4)+(wyf-wyi)/120/L*pow(x,5))
        uz = A7*pow(x,3) + A8*pow(x,2) + A9*x + A10 + 1/E/Iy*(wzi/24*pow(x,4)+(wzf-wzi)/120/L*pow(x,5))
        # Desplazamientos en coordenadas locales incluyendo giro rígido de la barra
        ux2 = ux
        uy2 = uy + betaz*x
        uz2 = uz - betay*x
        # Desplazamientos en coordenadas locales incluyendo traslación rígida de la barra
        ux3 = ux2 + Uloc[0]
        uy3 = uy2 + Uloc[1]
        uz3 = uz2 + Uloc[2]

        return ux3, uy3, uz3

    def find_maxmin_internal_forces(self,solicitacion,extreme):
        L = self.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X,(1000,1))
        N,Qy,Qz,Mt,My,Mz = self.compute_internal_forces(X)
        if solicitacion == 'N':
            values = N
        elif solicitacion == 'Qy':
            values = Qy
        elif solicitacion == 'Qz':
            values = Qz
        elif solicitacion == 'Mt':
            values = Mt
        elif solicitacion == 'My':
            values = My
        elif solicitacion == 'Mz':
            values = Mz
        if extreme == 'max':
            ex_val = values.max()
            values_list = values.tolist()
            position = values_list.index(ex_val)
        elif extreme == 'min':
            ex_val = values.min()
            values_list = values.tolist()
            position = values_list.index(ex_val)
        position = (L*position)/1000
        return position, ex_val
