# modulo atenea3dgui.py
"""
Este modulo define la clases de nodos barras y estructura con interfaz gráfica

creado por: equipo Atenea-UBATIC
"""

from ..core.bar3d import Nodo, Barra
from ..core.model3d import Estructura
from ..core.graph3d import *

class NodoGUI(Nodo):
    """
    """
    def __init__(self, coords, indice):

        # Inicializar superclase
        Nodo.__init__(self, coords, indice)

        # Propiedad para contener artistas de matplotlib
        self.artists = {'self': [], 'v': [], 'p': [], 'c': [], 'index': [],
                        'reacciones': []}

    def plot(self, axes, fontsize, indice_visible, keys=['self','index'], **kwargs):
        self.artists[keys[0]] = plot_nodo(self, axes, **kwargs)
        self.artists[keys[1]] = plot_indice_nodo(self, axes, fontsize,
                                                 indice_visible)
        self.set_picker(key=keys[0])

    def plot_vinculos(self, axes, fontsize, tipo, length, key='v', **kwargs):
        self.artists[key] = plot_vinculos(self, axes, fontsize, length,
                                          tipo='vinculo', **kwargs)

    def plot_cedimientos(self, axes, fontsize, length, key='c', **kwargs):
        self.artists[key] = plot_cedimientos(self, axes, length=length,
                                             fontsize=fontsize, **kwargs)

    def set_picker(self, radius=5, key='self'):
        for artist in self.artists[key]:
            artist.set_picker(radius)

    def plot_fuerzas(self, axes, fontsize, colors, length, key='p',**kwargs):
        self.artists[key] = plot_fuerzas_nodales(self, axes, fontsize, length,
                                                 colors = colors,
                                                 visible=True, zorder=1)

    def clean(self, types):
        """
        Eliminar los artistas que correspondan del nodo.
        mpde: string que indica si se quieren eliminar artistas de la carga de
              estructura, o de los resultados
        dicts: lista de todos los artists y textos asociados que se quieran borrar
        """
        for type in types:
            for artist in self.artists[type]:
                artist.remove()
            self.artists[type] = []

class BarraGUI(Barra):
    """
    """
    def __init__(self, indice, nodos, ElemName, rel=[], w={}, e0=[],
                 data={'seccion':{},'mat':{}}):

        # Inicializar superclase
        Barra.__init__(self,indice,nodos,ElemName,rel,w,e0,data)

        # Propiedad para contener artistas de matplotlib
        self.artists = {'self':[],'w': [], 'e':[], 'ejes': [],
                        'index':[], 'Mt':[], 'My':[], 'Mz':[],
                        'Qy':[], 'Qz':[], 'N':[],
                        'deformada':[]}

    def plot(self, axes, fontsize, length, indice_visible, ejes_visible,
             ejes_color, keys=['self','ejes','index'],*args, **kwargs):
        self.artists[keys[0]] = plot_barra(self, axes, *args, **kwargs)
        self.artists[keys[1]] = plot_ejes_locales(self, axes,
                        fontsize, length=length, color=ejes_color,
                        visible=ejes_visible, zorder=kwargs['zorder']+1)
        self.artists[keys[2]] = plot_indice_barra(self, axes, fontsize,
                                                    indice_visible)
        self.set_picker(key=keys[0])

    def set_picker(self, radius=5, key='self'):
        for artist in self.artists[key]:
            artist.set_picker(radius)

    def plot_fuerzas(self, axes, fontsize, length, key='w',**kwargs):
        self.artists[key] = plot_fuerzas_distribuidas(self, axes, fontsize,
                                                      length=length, **kwargs)

    def plot_temperatura(self, axes, fontsize, visible, key='e'):
        self.artists[key] = plot_temperatura(self, axes, fontsize, visible=True)

    def plot_diagramas(self, axes, diagrama, x, precision=3, fontsize=8):
        self.artists[diagrama+'_loc'] = plot_diagramas_local(self, axes,
                                            diagrama, x, precision, fontsize)

    def plot_ejes_locales(self, axes, fontsize, length, **kwargs):
        self.artists['ejes'] = plot_ejes_locales(self, axes, fontsize,
                                                 length=length, **kwargs)

    def clean(self, types):
        """
        Eliminar los artistas que correspondan de la barra.
        """
        for type in types:
            for artist in self.artists[type]:
                artist.remove()
            self.artists[type]=[]

class EstructuraGUI(Estructura):
    """
    """
    def __init__(self, nodos, barras):

        # Inicializar superclase
        Estructura.__init__(self, nodos, barras)
        self.artists = {}

    def plot_deformada(self, axes, scale_def, linewidth=1, color='g'):
        plot_deformada(self, axes, scale_def, linewidth=linewidth, color=color)

    def plot_diagramas(self, axes, precision=3, fontsize=8):
        solicitaciones = ['N','Mt','Mz','My','Qy','Qz']
        for sol in solicitaciones:
            scale = definir_escala_diagramas(self, sol)
            plot_diagramas_global(self, axes, sol, scale_d=scale,
                                  precision=precision, fontsize=fontsize)

    # def set_diagramas_visible(self, diagrama):
    #     solicitaciones = ['N','Mt','Mz','My','Qy','Qz']
    #     for barra in self.barras.values():
    #         for sol in solicitaciones:
    #             for artist in barra.artists[sol+'_gl']:
    #                 artist.set_visible(False)
    #         if diagrama:
    #             for artist in barra.artists[diagrama+'_gl']:
    #                 artist.set_visible(True)

    def set_diagramas_visible(self, diagrama, visible=True):
        for barra in self.barras.values():
            for sol in diagrama:
                for artist in barra.artists[sol+'_gl']:
                    artist.set_visible(visible)

    def set_barras_visible(self, key, visible=True):
        for barra in self.barras.values():
            for artist in barra.artists[key]:
                artist.set_visible(visible)

    def set_nodos_visible(self, key, visible=True):
        for nodo in self.nodos.values():
            for artist in nodo.artists[key]:
                artist.set_visible(visible)

    def plot_reacciones(self, axes, visible=True, fontisze=8, color='r',
                        zorder=1, precision=3):
        plot_reacciones(self, axes, visible=visible, fontsize=fontisze,
                        color=color, zorder=zorder, precision=precision)

    def SetArtistsVisible(self, artists, text=True):
        """
        """
        if artists == 'reacciones':
            for nodo in self.nodos.values():
                for dict in nodo.results_artists[artists]:
                    if dict == 'artists':
                        for artist in nodo.results_artists[artists][dict]:
                            artist.set_visible(True)
                    if dict == 'texts':
                        if text:
                            for artist in nodo.results_artists[artists][dict]:
                                artist.set_visible(True)

        else:
            for barra in self.barras.values():
                for dict in barra.results_artists[artists]:
                    if dict == 'artists':
                        for artist in barra.results_artists[artists][dict]:
                            artist.set_visible(True)
                    if dict == 'texts':
                        if text:
                            for artist in barra.results_artists[artists][dict]:
                                artist.set_visible(True)

    def remove_barra_artists(self, keys):
        """
        """
        for barra in self.barras.values():
            barra.clean(keys)

    def remove_nodo_artists(self, keys):
        for nodo in self.nodos.values():
            nodo.clean(keys)
















