# modulo interactiveCanvas.py
"""
Este modulo define la clase "AteneaCanvasQt" que es un widget de Pyqt para el
builder Qt. 

creado por: equipo Atenea-UBATIC
"""
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from PyQt5.QtCore import Qt, QSize

from .canvasWidgets import Zoom3D, Pan3D, Picker
from ..core import graph3d

class AteneaCanvasQt(FigureCanvas):
    """
    Matplotlib qt backend canvas qidget con clases para la interacción con el
    usuario para insertar dentro de un Qt GUI.
    
    Argumentos:
        - parent [QWidget]: QWidget que está una jerarquía arriba en el GUI
        - layout [QLayout widget]: layout widget donde estará inserto el canvas
        - grid [False / list]: False para crear el canvas sin grilla de puntos.
                               Lista [dx,dy,Nx,Ny] para crear una grilla de 
                               Nx x Ny puntos espaciados dx y dy respectivamente
        - QSizePolicy [QWidget.QsizePolicy]: QsizePolicy object
        - coordsLabel [QLabel]: QLabel donde actualizar la posición del cursor
                                dentro del canvas
        - zoom_base_scale [float]: multplicador para definir el zoom interactivo
        
    Atributos:
        - fig [matplotlib.figure.Figure]: Matplotlib figure object
        - coordsLabel [QLabel]: QLabel donde actualizar la posición del cursor
                                dentro del canvas
        - ax [fig.axes]: objeto axes de matplotlib figure
        - grid [False or list]: False para crear el canvas sin grilla de puntos.
                                Lista [dx,dy,Nx,Ny] para crear una grilla de 
                                Nx x Ny puntos espaciados dx y dy respectivamente
        - gridon [bool]: Define si la grilla está visible o no
        - grid_flatten [dict]: {'xgrid':xgrid,'ygrid':ygrid} donde xgrid e ygrid
                               son np.arrays con las coordenadas de los puntos
                               de la grilla
        - interactions [dict]: Diccionario con todos los objetos que agregan 
                               interactividad al objeto axes (self.ax).
                               'nodedrawer': dibuja nodos con el mouse
                               'bardrawer': dibuja barras entre nodos existentes
                               'pan': panea clickeando el mouse y arrastrando
                               'zoom': hace zoom usando el scroll
                               'pickall': selecciona objetos del axes
                               'picknodes': selecciona objetos del axes, solo nodos
                               'pickbars': selecciona objetos del axes, solo barras
        - nodos [list]: Lista de patch.Rectanfgle de todos los nodos dibujados 
                        en el canvas
        - barras [list]: Lista de mlines.Line2d de todas las barras dibujadas
                         en el canvas 
         
    Metodos:
        - onMotion(event): Define el evento de escribir las coordenadas del mouse
                           durante el movimiento del mouse
        - enablePan(event): Define el evento de habilitar el modo 'pan' cuando
                            se oprime la tecla 'control'
        - disablePan(event): Define el evento de deshabilitar el 'pan' y volver
                             al modo previo cuando se suelta la tecla 'control'
        - setMode(mode, QCursor): conecta el modo indicado en el string 'mode'
                                  y le asigna el cursor QCursor
        - disconnectAll(): Desconecta todas las funcionalidades
        - setBounds(bounds): Setea los límites del canvas
        - drawGrid(grid): Dibuja la grilla de puntos definida por el list grid
        - setGridOn(gridon): Prende y apaga la visibilidad de la grilla según
                             el boolean 'gridon'
        - clearCanvas(): Elimina todos las barras y nodos
        
    """
    def __init__(self, parent=None, layout=None, grid=False, QSizePolicy=None,
                 coordsLabel=None, zoom_base_scale=1.15, nodos={}, barras={},
                 nodecolor='b', barracolor='k', selectcolor='r', key_self='self',
                 originArrows=True):
        
        
        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5        
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        if parent:
            self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        if QSizePolicy:
            FigureCanvas.setSizePolicy(self,QSizePolicy,QSizePolicy)
        # Detect key press in canvas
        FigureCanvas.setFocusPolicy(self, Qt.ClickFocus)
        FigureCanvas.setFocus(self)
        # Detect mouse movement in canvas
        self.setMouseTracking(True)
        
        # Label widget para escribir coordenadas del mouse en statusbar
        if coordsLabel:
            self.coordsLabel = coordsLabel

        # Initialize and format axes properties
        self.ax = self.fig.add_subplot(111, projection='3d')
        self.ax.set_proj_type('ortho')
        self.ax.axis([0,2,0,2])
        self.ax.set_xlim3d(0,6)
        self.ax.set_ylim3d(0,6)
        self.ax.set_zlim3d(0,6)
        # self.ax.axis('scaled')
        self.ax.set_aspect('equal', adjustable='box', anchor='C')
        # self.ax.set_axis_on()
        self.setGridOn(False)
        
        # Crear flechas del origen
        self.ax.origin_arrows = graph3d.plot_originarrows(self.ax, color='r')
        
        # Initialize barras and nodos lists
        self.nodos = nodos
        self.barras = barras

        # Create interactive capabilities
        self.interactions = {}
        
        # self.interactions['pan'] = Pan(self.ax)
        self.interactions['zoom'] = Zoom3D(self.ax,zoom_base_scale)
        self.interactions['picker'] = Picker(self.ax, nodos_list=self.nodos, 
                                             barras_list=self.barras,
                                             nodecolor=nodecolor,
                                             barracolor=barracolor,
                                             key_self=key_self,
                                             selectcolor=selectcolor)
        self.interactions['pan'] = Pan3D(self.ax)
        self.setMode('rotate',Qt.OpenHandCursor)
        
        # # Set pan mode with control key
        self.fig.canvas.mpl_connect('key_press_event',self.enablePan)
        self.fig.canvas.mpl_connect('key_release_event',self.disablePan)
        self.fig.canvas.mpl_connect('motion_notify_event',self.onMotion)

    def onMotion(self,event):
        prev_button = self.ax.button_pressed
        if event.inaxes != self.ax: 
            if self.coordsLabel:
                self.coordsLabel.setText('x=  ,y=  ,z=  ') 
                self.coordsLabel.update()
            return
        mousex = event.xdata
        mousey = event.ydata
        # Update del label
        if self.coordsLabel:
            if not prev_button: # Si no está haciendo rotate, imprimir coordenadas xyz
                self.ax.button_pressed = -1
            self.coordsLabel.setText(self.ax.format_coord(mousex,mousey)) 
            self.coordsLabel.update()
            self.ax.button_pressed = prev_button

    def enablePan(self,event):
        """
        Habilitar paneo cuando se apreta la tecla control
        """
        if event.inaxes != self.ax: return
        if not event.key == 'control': return
        prev_mode = self.current_mode
        prev_cursor = self.current_cursor
        self.setMode('pan', Qt.ClosedHandCursor)
        self.current_mode = prev_mode
        self.current_cursor = prev_cursor
        
    def disablePan(self,event):
        """
        Habilitar paneo cuando se apreta la tecla control
        """
        if event.inaxes != self.ax: return
        if not event.key == 'control': return
        self.setMode(self.current_mode, self.current_cursor)
    
    def setView(self, view):
        if view == 'xy':
            self.ax.view_init(azim=-90, elev=90)
        elif view == 'xz':
            self.ax.view_init(azim=-90, elev=0)
        elif view == 'yz':
            self.ax.view_init(azim=0, elev=0)
        elif view == 'iso':
            self.ax.view_init(azim=-45, elev=20)
        self.fig.canvas.draw()
    
    def setMode(self, mode, QCursor=None, **kwargs):
        self.current_mode = mode
        self.current_cursor = QCursor
        # Setear cursor
        if QCursor:
            self.setCursor(QCursor)
        # Reiniciar modos
        self.disconnectAll()
        # Setear modo
        if mode == 'pan':
            self.ax.mouse_init(rotate_btn=None, zoom_btn=None)
            self.interactions['pan'].connect()
            self.interactions['zoom'].connect()
        elif mode == 'rotate':
            self.ax.mouse_init(rotate_btn=1, zoom_btn=None)
            self.interactions['zoom'].connect()
        elif mode == 'pickall':
            self.interactions['picker'].connect('all')
            self.interactions['zoom'].connect()
        elif mode == 'picknodes':
            self.interactions['picker'].connect('nodos')
            self.interactions['zoom'].connect()
        elif mode == 'pickbars':
            self.interactions['picker'].connect('barras')
            self.interactions['zoom'].connect()
    
    def disconnectAll(self):
        for interaction in self.interactions.values():
            interaction.disconnect()

    def setBounds(self, axis_bounds):
        # self.ax.axis(axis_bounds)
        self.ax.set_xlim3d(axis_bounds[0],axis_bounds[1])
        self.ax.set_ylim3d(axis_bounds[2],axis_bounds[3])
        self.ax.set_zlim3d(axis_bounds[4],axis_bounds[5])

    def setGridOn(self, status=True):
        self.gridon = status
        if status:
            self.ax.set_axis_on()
        elif not status:
            self.ax.set_axis_off()
        self.fig.canvas.draw()
    
    def clearCanvas(self, nodos={}, barras={}, gridon=False):
        # eliminar todos los artistas del ax
        self.ax.clear()
        self.ax.set_proj_type('ortho')
        self.ax.axis([0,2,0,2])
        self.ax.set_xlim3d(0,6)
        self.ax.set_ylim3d(0,6)
        self.ax.set_zlim3d(0,6)
        # self.ax.set_aspect('equal', adjustable='box', anchor='C')
        # self.ax.set_axis_on()
        self.setGridOn(gridon)

        # Create origin arrows
        if self.ax.origin_arrows:
            self.ax.origin_arrows = graph3d.plot_originarrows(self.ax, color='r')
            
        # reset nodes and bars list
        self.nodos = nodos
        self.barras = barras
        self.interactions['picker'].nodos_list = self.nodos
        self.interactions['picker'].barras_list = self.barras

    def getNodosCoords(self, nodos):
        nodos_xcoords = []
        nodos_ycoords = []
        nodos_zcoords = []
        for nodo in nodos.values():
            nodos_xcoords.append(nodo.coords[0])
            nodos_ycoords.append(nodo.coords[1])
            nodos_zcoords.append(nodo.coords[2])
        return nodos_xcoords, nodos_ycoords, nodos_zcoords
        
    def findNodebyCoords(self, coords):
        # Actualizar lista de nodos
        nodos_xcoords, nodos_ycoords, nodos_zcoords =  self.getNodosCoords(self.nodos)
        # Buscar nodo 
        for i in range(len(nodos_xcoords)):
            nodo = (nodos_xcoords[i],nodos_ycoords[i],nodos_zcoords)
            if nodo == coords:
                return i 
        return []

    def getNodosExtent(self, nodos):
        """
        Obtener límites de los nodos
        """
        nodos_xcoords, nodos_ycoords, nodos_zcoords = self.getNodosCoords(nodos)
        xmin = min(nodos_xcoords)
        xmax = max(nodos_xcoords)
        ymin = min(nodos_ycoords)
        ymax = max(nodos_ycoords)
        zmin = min(nodos_zcoords)
        zmax = max(nodos_zcoords)
        return xmin, xmax, ymin, ymax, zmin, zmax

    def zoom2extent(self, margin=0.05):
        tol = 0.01
        xmin, xmax, ymin, ymax, zmin, zmax = self.getNodosExtent(self.nodos)
        if (xmin == xmax) and (ymin == ymax) and (zmin == zmax):
            return
        dx = xmax-xmin
        dy = ymax-ymin
        dz = zmax-zmin
        d = max(dx,dy,dz)
        amargin = d*margin + tol
        xmargin = dx*margin + tol
        ymargin = dy*margin + tol
        zmargin = dz*margin + tol
        self.setBounds([xmin-amargin, xmin+d+amargin,
                        ymin-amargin, ymin+d+amargin,
                        zmin-amargin, zmin+d+amargin])
        self.fig.canvas.draw_idle()

class BarrasDisplayCanvas(FigureCanvas):
    """
    """
    def __init__(self, parent=None, layout=None, QSizePolicy=None):
        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        FigureCanvas.setSizePolicy(self, QSizePolicy, QSizePolicy)
        FigureCanvas.setMaximumHeight(self, 250)
        FigureCanvas.setMinimumHeight(self, 1)

        # Initialize and format axes properties
        self.ax = self.fig.add_subplot(111)
        self.ax.set_frame_on(False)
        self.ax.get_xaxis().set_visible(False)
        self.ax.get_yaxis().set_visible(False)
        self.ax.axis('off')
        
        # Permitir modo paneo
        # self.pan = Pan(self.ax)
        # self.pan.connect()
