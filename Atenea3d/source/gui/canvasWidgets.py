# modulo canvasWidgets.py
"""
Este modulo define las clases que agregan interactividad al pyqt canvas definido
en el módulo interactiveCanvas.py

creado por: equipo Atenea-UBATIC
"""
import numpy as np

import matplotlib.lines as mlines
from mpl_toolkits.mplot3d import proj3d, art3d

from ..core.graph3d import FrameBar
from .selectorWidgets import RectangleSelector

from PyQt5.QtCore import QObject, pyqtSignal, Qt

from matplotlib.patches import FancyArrowPatch, FancyArrow


def findObject(objectsList, targetObject):
    pos = 0
    for object in objectsList:
        if object == targetObject:
            return True, pos
        pos += 1
    return False, None

class Picker(QObject):
    """
    Clase que agrega la capacidad de seleccionar objetos (childs) del axes.
    Sólo permite seleccionar objetos nodo (patch.Rectangle) y objetos barra
    (mlines.Line2D)
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - nodos_list [list]: lista de nodos en el canvas
        - barras_list [list]: lista de barras en el canvas
        
    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - objectType: modo de funcionamiento
        - rs: RectangleSelector widget definido en módulo SelectorWidgets.py.
              Define la capacidad de dibujar un rectángulo cuando se arrastra
              el mouse
        - list_picks [list]: Lista con objeto clickeados por el mouse. El
                             matplotlib, por defecto, selecciona todo los objetos
                             que están en las coordenadas del click. Para un click
                             debería quedarme solo con un objeto.
        - selected_objects [list]: Lista de objetos seleccionados. 
        - prev_objects [list]: Lista de objetos previamente seleccionados
        - consecutive [bool]: Bool que define si el siguiente objeto seleccionado
                              se agrega al anterior o lo reemplaza
        - ignore_release [bool]: Bool para ignorar el callback cuando se suelta
                                 el click del mouse
    
    Métodos:
        - onPick(): Callback del 'pick_event'. Define la lista list_picks
        - onRelease(): Callback del evento 'mouse release'. Actualiza lista de 
                     objetos seleccionados y los colorea. Vuelve al color original
                     la lista de objetos previamente seleccionados. Emite pyqt
                     signal 'picked'
        - colorSelected(): Pinta de color rojo los objetos en selected_objects y
                         vuelve al color original los objetos en prev_objects
        - onKeyPress(): Callback de 'key press'. Define la funcionalida de
                      seleccionar objetos consecutivos usando 'shift' y de
                      eliminar objetos usando 'delete'
        - onKeyRelease(): Callback de 'key release'. Deja de seleccionar objetos
                        consecutivos
        - object_select_callback(): callback del RectangleSelector. Agrega a 
                                  selected_objects todos los objetos que estan
                                  completamente adentro del rectangulo y los
                                  colorea.
        - connect(objectType): Conecta los callbacks con el modo objectType
        - disconnect(): Desconecta los callbacks

    Signals:
        - picked: Se emite al final del callback 'onRelease'
        
    """
    picked = pyqtSignal(list,list)
    delete = pyqtSignal()
    cleared = pyqtSignal()
    
    def __init__(self, ax, nodos_list=[], barras_list=[], key_self='self',
                 nodecolor='b', barracolor='k',selectcolor='r'):
        """
        
        objectType: 'all', 'nodos', 'barras'
        """
        super().__init__()
        
        self.ax = ax
        self.fig = ax.get_figure()
        
        # self.rs = RectangleSelector(ax, self.object_select_callback,
        #                    drawtype='box', useblit=True, button=[1], 
        #                    minspanx=5, minspany=5, spancoords='pixels', 
        #                    interactive=False)
        
        self.connect('all')
        self.disconnect()
        
        self.list_picks = []
        self.selected_objects = []
        self.prev_obj = []
        self.nodos_list = nodos_list
        self.barras_list = barras_list
        # Flag que dice si se seleccionan objetos seguidos
        self.consecutive = False 
        self.ignore_onrelease = False
        self.nodecolor = nodecolor
        self.barracolor = barracolor
        self.selectcolor = selectcolor
        self.key_self = key_self
    
    def onPick(self, event):
        """
        Guarda todos los artistas pickeados y los guarda para posterior uso
        """
        if not event.mouseevent.button == 1: return
        self.list_picks.append(event.artist)
        
    def onRelease(self, event):
        """
        Este event handler se necesita ya que en los nodos, el pick event
        se ejecuta dos veces agarrando la barra y el nodo.
        EL programa requiere que en esos casos, se seleccione solo el nodo.
        """
        # De la lista de objetos pickeados me quedo con el nodo o con el primero
        # (si son varias lineas)
        if self.ignore_onrelease:
            self.ignore_onrelease = False
            return
        obj = None
        for pick in self.list_picks:
            if self.objectType == 'all':
                found_barra, pos_barra = self.findLineinBar(pick)
                if found_barra:
                    obj = self.barras_list[pos_barra].artists[self.key_self][0]
                artists_list = [a.artists[self.key_self][0] for a in \
                                self.nodos_list.values()]
                if findObject(artists_list, pick)[0]:
                    obj = pick
                    break
            elif self.objectType == 'nodos':
                artists_list = [a.artists[self.key_self][0] for a in \
                                self.nodos_list.values()]
                if findObject(artists_list, pick)[0]:
                    obj = pick
                    break
            elif self.objectType == 'barras':
                found_barra, pos_barra = self.findLineinBar(pick)
                if found_barra:
                    obj = self.barras_list[pos_barra].artists[self.key_self][0]
                    break
        # Actualizo lista de objetos seleccionados
        if self.consecutive:
            if obj:
                self.selected_objects += [obj]
        elif not self.consecutive:
            self.prev_obj = self.selected_objects
            if obj:
                self.selected_objects = [obj]
            else:
                self.selected_objects = []
        # Coloreo listo de objetos seleccionados y vuelvo al color original
        # lista de objetos previos
        self.colorSelected()
        # Reseteo lista de objetos pickeados
        self.list_picks = []
        
        # Envío senal con objetos seleccionados
        nodos_indices, barras_indices = self.findIndices()
        self.picked.emit(nodos_indices, barras_indices)

    def colorSelected(self):
        """
        Función para pintar de rojo todos los objetos seleccionados
        """
        # Volver al color original objetos previos
        for prev_obj in self.prev_obj:
            if type(prev_obj) is FrameBar:
                prev_obj.set_color(self.barracolor)
            elif (type(prev_obj) is FancyArrowPatch) or (type(prev_obj) \
                is FancyArrow):
                prev_obj.set_color(self.nodecolor)
            elif type(prev_obj) is art3d.Line3D:
                prev_obj.set_markeredgecolor(self.nodecolor)
                if self.objectType == 'releases':
                    prev_obj.set_markerfacecolor('white')
                else:
                    prev_obj.set_markerfacecolor(self.nodecolor)

        self.fig.canvas.draw()
        # Colorear objetos seleccionados
        for obj in self.selected_objects:
            if type(obj) is FrameBar:
                obj.set_color(self.selectcolor)
            elif (type(obj) is FancyArrowPatch) or (type(obj) is FancyArrow):
                obj.set_color(self.selectcolor)
            elif type(obj) is art3d.Line3D:
                obj.set_markeredgecolor(self.selectcolor)
                obj.set_markerfacecolor(self.selectcolor)

        self.fig.canvas.draw()

    def clearSelected(self):
        self.prev_obj = self.selected_objects
        self.selected_objects = []
        self.colorSelected()
        self.cleared.emit()

    def onKeyPress(self, event, override=False):
        """
        Si la tecla shift está apretada, entonces se seleccionan varios
        seguidos
        """
        barras_deleted = []
        nodos_deleted = []
        if not override:
            if event.inaxes != self.ax: return
        if event.key=='shift':
            self.consecutive = True
        elif event.key=='delete':
            self.delete.emit()
        else:
            return

    def onKeyRelease(self, event):
        """
        Si la tecla shift está apretada, entonces se seleccionan varios
        seguidos
        """
        if event.inaxes != self.ax:
            return
        if event.key == 'shift':
            self.consecutive = False
        elif event.key=='delete':
            # self.delete.emit()
            return
        else:
            return

    def findIndices(self):
        nodos_indices = []
        barras_indices = []
        for object in self.selected_objects:
            artists_list = [a.artists[self.key_self][0] for a in \
                            self.barras_list.values()]
            keys_list = [a for a in self.barras_list.keys()]
            found_barra, pos_barra = findObject(artists_list, object)
            if found_barra:
                pos_barra = keys_list[pos_barra]
                barras_indices.append(pos_barra)
            
            artists_list = [a.artists[self.key_self][0] for a in \
                            self.nodos_list.values()]
            keys_list = [a for a in self.nodos_list.keys()]
            found_nodo, pos_nodo = findObject(artists_list, object)
            if found_nodo:
                pos_nodo = keys_list[pos_nodo]
                nodos_indices.append(pos_nodo)
        return nodos_indices, barras_indices

    def findLineinBar(self, object):
        k = 0
        found_barra = False
        for key, barra in self.barras_list.items():
            axis_list = [a.baraxis for a in barra.artists[self.key_self]]
            is_barra = findObject(axis_list, object)
            if is_barra[0]:
                found_barra = True
                pos_barra = key
                return found_barra, pos_barra
            k += 1
        return found_barra, None

    def findBars(self, node):
        """
        Encontrar barras que concurren al nodo 'node'
        """
        node_center = node.get_xydata()[0]
        barras_found = []
        for barra in self.barras_list:
            bar_data = barra.get_xydata()
            nodei_center = bar_data[0,:]
            nodef_center = bar_data[1,:]
            if np.array_equal(node_center, nodei_center) or np.array_equal(
                    node_center, nodef_center):
                barras_found.append(barra)
        return barras_found

    def connect(self, objectType):
        self.objectType = objectType
        self._cidpick = self.fig.canvas.mpl_connect(
                            'pick_event', self.onPick)
        self._cidrelease = self.fig.canvas.mpl_connect(
                            'button_release_event', self.onRelease)
        self._cidkeypress = self.fig.canvas.mpl_connect(
                            'key_press_event', self.onKeyPress)
        self._cidkeyrelease = self.fig.canvas.mpl_connect(
                            'key_release_event', self.onKeyRelease)
        # self.rs.connect()
       
    def disconnect(self):
        self.fig.canvas.mpl_disconnect(self._cidpick)
        self.fig.canvas.mpl_disconnect(self._cidrelease)
        self.fig.canvas.mpl_disconnect(self._cidkeypress)
        self.fig.canvas.mpl_disconnect(self._cidkeyrelease)
        # self.rs.disconnect()
        
class Zoom3D(QObject):
    """
    Clase que agrega la capacidad de hacer zoom in y out con el scroll del mouse
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - base_scale [float]: Define la escala para agrandar o achicar el zoom
        
    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - base_scale: mismo del argumento
        - cur_xlim: axes xlim
        - cur_ylim: axes ylim
        
        
    Métodos:
        - zoom(): Callback del 'scroll event'. Agranda y achica los limites del
                  canvas
        - connect(): Conecta los callbacks
        - disconnect(): Desconecta los callbacks
        
    Signals:
        - 

    """
    zoomed = pyqtSignal()
    
    def __init__(self, ax, base_scale = 1.3):
        
        super().__init__()
        
        self.cur_xlim = None
        self.cur_ylim = None
        self.ax = ax
        self.base_scale = base_scale
        
        self.fig = ax.get_figure() # get the figure of interest
        
        self.connect()
        self.disconnect()
    
    def _line2d_seg_dist(self, p1, p2, p0):
        """distance(s) from line defined by p1 - p2 to point(s) p0
    
        p0[0] = x(s)
        p0[1] = y(s)
    
        intersection point p = p1 + u*(p2-p1)
        and intersection point lies within segment if u is between 0 and 1
        """
    
        x21 = p2[0] - p1[0]
        y21 = p2[1] - p1[1]
        x01 = np.asarray(p0[0]) - p1[0]
        y01 = np.asarray(p0[1]) - p1[1]
    
        u = (x01*x21 + y01*y21) / (x21**2 + y21**2)
        u = np.clip(u, 0, 1)
        d = np.hypot(x01 - u*x21, y01 - u*y21)
    
        return d
    
    def get_xyz(self, xd, yd):
        """
        Given the 2D view coordinates attempt to guess a 3D coordinate.
        Looks for the nearest edge to the point and then assumes that
        the point is at the same z location as the nearest point on the edge.
        """

        if self.ax.M is None:
            return ''

        # if self.ax.button_pressed in self.ax._rotate_btn:
        #     return 'azimuth=%d deg, elevation=%d deg ' % (self.ax.azim, self.ax.elev)
        #     # ignore xd and yd and display angles instead

        p = (xd, yd)
        edges = self.ax.tunit_edges()
        #lines = [proj3d.line2d(p0,p1) for (p0,p1) in edges]
        ldists = [(self._line2d_seg_dist(p0, p1, p), i) for \
                  i, (p0, p1) in enumerate(edges)]
        ldists.sort()
        # nearest edge
        edgei = ldists[0][1]

        p0, p1 = edges[edgei]

        # scale the z value to match
        x0, y0, z0 = p0
        x1, y1, z1 = p1
        d0 = np.hypot(x0-xd, y0-yd)
        d1 = np.hypot(x1-xd, y1-yd)
        dt = d0+d1
        z = d1/dt * z0 + d0/dt * z1

        x, y, z = proj3d.inv_transform(xd, yd, z, self.ax.M)

        xs = self.ax.format_xdata(x)
        ys = self.ax.format_ydata(y)
        zs = self.ax.format_zdata(z)
        return x, y, z

    def zoom(self, event):

        cur_xlim = self.ax.get_xlim()
        cur_ylim = self.ax.get_ylim()
        cur_zlim = self.ax.get_zlim()
        xdata, ydata, zdata = self.get_xyz(event.xdata,event.ydata)
        
        if event.button == 'down':
            # deal with zoom in
            scale_factor = self.base_scale
        elif event.button == 'up':
            # deal with zoom out
            scale_factor = 1 / self.base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print (event.button)

        new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
        new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor
        new_deep = (cur_zlim[1] - cur_zlim[0]) * scale_factor

        relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
        rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])
        relz = (cur_zlim[1] - zdata)/(cur_zlim[1] - cur_zlim[0])

        self.ax.set_xlim([xdata-new_width*(1-relx), xdata+new_width*(relx)])
        self.ax.set_ylim([ydata-new_height*(1-rely), ydata+new_height*(rely)])
        self.ax.set_zlim([zdata-new_deep*(1-relz), zdata+new_deep*(relz)])
        
        self.zoomed.emit()

        self.ax.figure.canvas.draw()
        
    def connect(self):
        self._cidscroll = self.fig.canvas.mpl_connect('scroll_event', self.zoom)
        
    def disconnect(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidscroll)
        
class Pan3D:
    """
    Clase que agrega la capacidad de panear en el canvas arrastrando el click
    del mouse
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - press [set]: set con x0, y0, event.xdata, event.ydata
        - xpress [float]: Coordenada x cuando se clickea
        - ypress [float]: Coordenada y cuando se clickea
        - keypress [False o str]: nombre de la tecla que activa el pan. Si
                                  keypress=False, entonces el pan está siempre
                                  activado

    Métodos:
        - onKeyPress(): Callback del 'key press event'. Llama a connect_pan()
        - onKeyRelease(): Callback del 'key release event'. Llama a
                          disconnect_pan()
        - onPress(): Lee límites del gráfico y punto donde se presionó el mouse
        - onRelease(): Actualiza el canvas
        - onMotion(): Redefine límites del gráfico
        - connect_pan(): Conecta los callback de mouse press,release y motion
        - disconnect_pan(): Desconecta los callback de mouse press,release y motion
        - connect(keypress): Conecta los callbacks
        - disconnect(): Desconecta los callbacks
        
    Signals:
        - 

    """
    def __init__(self,ax):
        self.ax = ax
        self.press = None
        self.xpress = None
        self.ypress = None
        self.x0 = None
        self.y0 = None
        
        self.fig = self.ax.get_figure() # get the figure of interest
        
        # El Objeto se inicializa desconectado
        self.connect()
        self.disconnect()
    
    def _line2d_seg_dist(self, p1, p2, p0):
        """distance(s) from line defined by p1 - p2 to point(s) p0
    
        p0[0] = x(s)
        p0[1] = y(s)
    
        intersection point p = p1 + u*(p2-p1)
        and intersection point lies within segment if u is between 0 and 1
        """
    
        x21 = p2[0] - p1[0]
        y21 = p2[1] - p1[1]
        x01 = np.asarray(p0[0]) - p1[0]
        y01 = np.asarray(p0[1]) - p1[1]
    
        u = (x01*x21 + y01*y21) / (x21**2 + y21**2)
        u = np.clip(u, 0, 1)
        d = np.hypot(x01 - u*x21, y01 - u*y21)
    
        return d
    
    def get_xyz(self, xd, yd):
        """
        Given the 2D view coordinates attempt to guess a 3D coordinate.
        Looks for the nearest edge to the point and then assumes that
        the point is at the same z location as the nearest point on the edge.
        """

        if self.ax.M is None:
            return ''

        if self.ax.button_pressed in self.ax._rotate_btn:
            return 'azimuth=%d deg, elevation=%d deg ' % (self.ax.azim, self.ax.elev)
            # ignore xd and yd and display angles instead

        p = (xd, yd)
        edges = self.ax.tunit_edges()
        #lines = [proj3d.line2d(p0,p1) for (p0,p1) in edges]
        ldists = [(self._line2d_seg_dist(p0, p1, p), i) for \
                i, (p0, p1) in enumerate(edges)]
        ldists.sort()
        # nearest edge
        edgei = ldists[0][1]
        p0, p1 = edges[edgei]

        # scale the z value to match
        x0, y0, z0 = p0
        x1, y1, z1 = p1
        d0 = np.hypot(x0-xd, y0-yd)
        d1 = np.hypot(x1-xd, y1-yd)
        dt = d0+d1
        z = d1/dt * z0 + d0/dt * z1

        x, y, z = proj3d.inv_transform(xd, yd, z, self.ax.M)

        xs = self.ax.format_xdata(x)
        ys = self.ax.format_ydata(y)
        zs = self.ax.format_zdata(z)
        return x, y, z

    def onKeyPress(self,event):
        if event.inaxes != self.ax: return
        if not event.key == self.keypress: return
        self.connect_pan()
        
    def onKeyRelease(self,event):
        if not event.key == self.keypress: return
        self.disconnect_pan()
    
    def onPress(self,event):
        if event.inaxes != self.ax: return
        if not event.button == 1: return
        self.cur_xlim = self.ax.get_xlim()
        self.cur_ylim = self.ax.get_ylim()
        self.cur_zlim =self.ax.get_zlim()
        # self.press = event.xdata, event.ydata
        # self.xpress, self.ypress = self.press
        self.xpress, self.ypress, self.zpress = self.get_xyz(event.xdata,event.ydata)
        self.press = self.xpress, self.ypress, self.zpress

    def onRelease(self,event):
        self.press = None
        self.fig.canvas.draw_idle()

    def onMotion(self,event):
        if self.press is None: return
        if event.inaxes != self.ax: return
        xf, yf, zf = self.get_xyz(event.xdata, event.ydata)
        dx = xf - self.xpress
        dy = yf - self.ypress
        dz = zf - self.zpress
        self.cur_xlim -= dx
        self.cur_ylim -= dy
        self.cur_zlim -= dz
        self.ax.set_xlim(self.cur_xlim)
        self.ax.set_ylim(self.cur_ylim)
        self.ax.set_zlim(self.cur_zlim)
        self.fig.canvas.draw_idle()
        
    def connect(self, keypress=False):
        """connect events"""
        self.keypress = keypress
        if not self.keypress:
            self.connect_pan()
            return
        self._cidkeypress = self.fig.canvas.mpl_connect('key_press_event',
                                                        self.onKeyPress)
        self._cidkeyrelease = self.fig.canvas.mpl_connect('key_release_event',
                                                        self.onKeyRelease)
    
    def disconnect(self):
        if not self.keypress:
            self.disconnect_pan()
            return
        self.fig.canvas.mpl_disconnect(self._cidkeypress)
        self.fig.canvas.mpl_disconnect(self._cidkeyrelease)
        
    def connect_pan(self):
        self._cidmotion = self.fig.canvas.mpl_connect('motion_notify_event',
                                                  self.onMotion)
        self._cidpress = self.fig.canvas.mpl_connect('button_press_event',
                                                 self.onPress)
        self._cidrelease = self.fig.canvas.mpl_connect('button_release_event',
                                                   self.onRelease)
                                                   
    def disconnect_pan(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidmotion)
        self.fig.canvas.mpl_disconnect(self._cidpress)
        self.fig.canvas.mpl_disconnect(self._cidrelease)



