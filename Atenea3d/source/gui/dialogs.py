# módulo dialogs.py
"""
Popus durante el GUI

creado por: equipo Atenea-UBATIC
"""
import sys, os
from PyQt5.QtGui import QDoubleValidator
from PyQt5 import QtCore, QtGui, QtWidgets

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

class saveDialog(QtWidgets.QMessageBox):

    def __init__(self, parent=None):
        super(saveDialog, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Question)
        self.setStandardButtons(QtWidgets.QMessageBox.Yes | \
                                QtWidgets.QMessageBox.No| \
                                QtWidgets.QMessageBox.Cancel)
        self.setWindowTitle('Guardar archivo')
        self.setText('Quiere guardar el archvo?')

    def exec_(self):
        ret = self.exec()
        if ret == QtWidgets.QMessageBox.Yes:
            ok = 1
        elif ret == QtWidgets.QMessageBox.No:
            ok = 0
        elif ret == QtWidgets.QMessageBox.Cancel:
            ok = 2
        return ok

class errorDialog(QtWidgets.QMessageBox):

    def __init__(self, parent=None):
        super(errorDialog, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Critical)
        self.setStandardButtons(QtWidgets.QMessageBox.Ok)
        self.setWindowTitle('Error')

    def error_catch(self, info, ex=None):
        """

        :return:
        """
        template = "Ocurrió un error de tipo {0}. Argumentos:\n{1!r}"
        if ex:
            message = template.format(type(ex).__name__, ex.args)
            self.setInformativeText(message)
        self.setText(info)
        self.exec_()
        return

class customDialog(QtWidgets.QDialog):

    def __init__(self, labels, title, description=None, parent=None):

        super(customDialog, self).__init__(parent)

        # Widgets
        self.qlabels = [QtWidgets.QLabel(label+":") for label in labels]
        self.edits = [QtWidgets.QLineEdit() for label in labels]
        buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok |
                                     QtWidgets.QDialogButtonBox.Cancel)
        if description:
            self.descr_label = QtWidgets.QLabel(description)

        # Layout
        layout = QtWidgets.QGridLayout()
        row = 0
        for label in self.qlabels:
            layout.addWidget(label, row, 0)
            layout.addWidget(self.edits[row], row, 1)
            row += 1

        if description:
            font = QtGui.QFont()
            font.setPointSize(6)
            self.descr_label.setFont(font)
            layout.addWidget(self.descr_label, row, 0)
            row += 1

        layout.addWidget(buttonBox, row, 0, 1, 2)
        self.setLayout(layout)

        # Functionality
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle(title)

        # INput validation
        onlyDouble = QDoubleValidator()
        for edit in self.edits[1:]:
            edit.setValidator(onlyDouble)

    def accept(self):
        self.ok = True
        QtWidgets.QDialog.accept(self)

    def reject(self):
        self.ok = False
        QtWidgets.QDialog.reject(self)

    def exec_(self, values=None):
        # Add user input values if input
        if values:
            count = 0
            for edit in self.edits:
                edit.setText(values[count])
                count += 1
        else:
            for edit in self.edits:
                edit.setText('')

        # Ejecutar dialog
        super(customDialog, self).exec_()

        # Leer valores
        returnvalues = []
        for edit in self.edits:
            returnvalues.append(edit.displayText())

        return returnvalues, self.ok


class runDialog(QtWidgets.QDialog):

    def __init__(self, estructura, parent=None):

        super(runDialog, self).__init__(parent)
        # Widgets
        est_label = QtWidgets.QLabel("Grado de estaticidad: " + \
                           str(estructura.est['grado'])+ "("+\
                           str(estructura.est['tipo']) + ")")
        hline = QtWidgets.QFrame()
        hline.setFrameShape(QtWidgets.QFrame.HLine)
        hline.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.isostaticoButton = QtWidgets.QRadioButton()
        self.isostaticoButton.setText("Equilibrio de fuerzas")
        self.rigidecesButton = QtWidgets.QRadioButton()
        self.rigidecesButton.setText("Método de rigideces")
        self.flexibilidadesButton = QtWidgets.QRadioButton()
        self.flexibilidadesButton.setText("Método de flexibilidades")
        buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok |
                                     QtWidgets.QDialogButtonBox.Cancel)
        # Disable or enable buttons
        if estructura.est['grado'] > 0:
            self.isostaticoButton.setEnabled(False)
            self.rigidecesButton.setEnabled(True)
            self.flexibilidadesButton.setEnabled(True)
            self.rigidecesButton.setChecked(True)
        elif estructura.est['grado'] < 0:
            self.isostaticoButton.setEnabled(False)
            self.rigidecesButton.setEnabled(False)
            self.flexibilidadesButton.setEnabled(False)
        elif estructura.est['grado'] == 0:
            self.isostaticoButton.setEnabled(True)
            self.rigidecesButton.setEnabled(True)
            self.flexibilidadesButton.setEnabled(True)
            self.rigidecesButton.setChecked(True)

        # Layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(est_label)
        layout.addWidget(hline)
        layout.addWidget(self.isostaticoButton, 0)
        layout.addWidget(self.rigidecesButton, 1)
        layout.addWidget(self.flexibilidadesButton, 2)
        layout.addWidget(buttonBox, 3)
        self.setLayout(layout)

        # Functionality
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle('Correr estructura')

    def accept(self):
        self.ok = True
        QtWidgets.QDialog.accept(self)

    def reject(self):
        self.ok = False
        QtWidgets.QDialog.reject(self)

    def exec_(self):
        super(runDialog, self).exec_()
        if self.isostaticoButton.isChecked():
            return 0, self.ok
        if self.rigidecesButton.isChecked():
            return 1, self.ok
        if self.flexibilidadesButton.isChecked():
            return 2, self.ok
        return -1, False

def about_dialog():
    ui = aboutDialog()
    ui.exec_()

class aboutDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.ui = about_ui()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.close)

class about_ui(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(700, 500)
        Dialog.setModal(True)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Dialog)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_4 = QtWidgets.QLabel(self.frame_2)
        self.label_4.setText("")
        self.label_4.setPixmap(QtGui.QPixmap(resource_path("icons/Atenea3d_icon_large.png")))
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.horizontalLayout.addWidget(self.frame_2)
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout.setSpacing(15)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.textBrowser = QtWidgets.QTextBrowser(self.frame)
        self.textBrowser.setReadOnly(True)
        self.textBrowser.setTextInteractionFlags(
            QtCore.Qt.TextBrowserInteraction)
        self.textBrowser.setOpenExternalLinks(True)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        self.frame_3 = QtWidgets.QFrame(self.frame)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20,
                                           QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.pushButton = QtWidgets.QPushButton(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                           QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_3.addWidget(self.pushButton)
        self.verticalLayout.addWidget(self.frame_3)
        self.horizontalLayout.addWidget(self.frame)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Sobre Atenea3d"))
        self.label.setText(_translate("Dialog", "Atenea3d"))
        self.label_2.setText(_translate("Dialog", "Versión 1.7"))
        self.label_3.setText(_translate("Dialog", "Bajo licencia GNU-GPL-v3"))
        self.textBrowser.setHtml(_translate("Dialog",
                                            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.1pt; font-weight:400; font-style:normal;\">\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Desarrollado por el Departamento de Estabilidad de la Facultad de ingeniería de la UBA (Argentina)</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">para el proyecto UBATIC 2018-2019</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Director del proyecto: Raúl D. Bertero</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Equipo de desarrollo: Mariano Balbi, Agustín Bertero, Juan Mussat, Joaquín Ortiz, Felipe López Rivarola, Ariel Terlisky, Marcial Zapater, Felipe Medán y Santiago Bertero</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Web del proyecto: </span><a href=\"https://campusgrado.fi.uba.ar/course/view.php?id=258\"><span style=\" font-size:8pt; text-decoration: underline; color:#0000ff;\">Atenea Campus (FIUBA)</span></a></p></body></html>"))
        self.pushButton.setText(_translate("Dialog", "OK"))
