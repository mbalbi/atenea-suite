# Ejemplo: Pórtico de dos pisos
from Atenea3d.source.core import *

## Input del usuario

# Crear nodos
# Coordenadas de nodos
XYZ = {}
XYZ[1] = [0,0,0] # primer nodo
XYZ[2] = [0,0,3] # segundo nodo
XYZ[3] = [4,0,0]
XYZ[4] = [4,0,3]
XYZ[5] = [4,4,0]
XYZ[6] = [4,4,3]
XYZ[7] = [0,4,0]
XYZ[8] = [0,4,3]

# Crear barras
# conectividad de barras
CON = {}
CON[1] = [1,2] # Barra 1 del nodo 1 al nodo 2
CON[2] = [3,4]
CON[3] = [5,6]
CON[4] = [7,8]
CON[5] = [2,4]
CON[6] = [2,8]
CON[7] = [6,8]
CON[8] = [6,4]

# Condiciones de borde [restr x, restr y, restr z, restr giro x, restr giro y, restr giro z]
BOUN = {}
BOUN[1] = [1,1,1,1,1,1] # Nodo 1 empotrado
BOUN[3] = [1,1,1,1,1,1] # Nodo 3 empotrado
BOUN[5] = [1,1,1,1,1,1] # Nodo 5 empotrado
BOUN[7] = [1,1,1,1,1,1] # Nodo 7 empotrado

# Tipos de elemento
ElemName = {}
ElemName[1] = '3DFrame'
ElemName[2] = '3DFrame'
ElemName[3] = '3DFrame'
ElemName[4] = '3DFrame'
ElemName[5] = '3DFrame'
ElemName[6] = '3DFrame'
ElemName[7] = '3DFrame'
ElemName[8] = '3DFrame'

# Releases de elementos [Mxi, Myi, Mzi, Pxf, Myf, Mzf], 1: release
rel = {}
rel[5] = [0,1,1,0,0,0]

# Propiedades del elememto
# Carga de materiales
all = {'E':30000000, 'mu':0.2, 'l':1e-5}
# Carga de secciones
"""
Iy, Iz y Jt son los momentos de inercia en los ejes locales "y", "z" y "x", respectivamente.
Alpha es el angulo de rotación de la sección en la direccion del eje local x. Cuando alpha vale 0,
el eje z esta en la direccion que lo contiene en el plano formado por el eje de la barra y el "y" global
(o en el caso de barra vertical, ele eje y es coincidente con el z global); y crece en sentido antihorario.
"""
vigas = {'A': 0.09, 'Iy': 0.12, 'Iz': 0.03, 'Jt': 0.06, 'alpha': 0}
cols = {'A': 0.05, 'Iy': 0.08, 'Iz': 0.08, 'Jt': 0.05, 'alpha': 0}
# Asignación de material y sección a cada barra
ElemData = {}
ElemData[1] = {'mat':all,'seccion':cols}
ElemData[2] = {'mat':all,'seccion':cols}
ElemData[3] = {'mat':all,'seccion':cols}
ElemData[4] = {'mat':all,'seccion':cols}
ElemData[5] = {'mat':all,'seccion':vigas}
ElemData[6] = {'mat':all,'seccion':vigas}
ElemData[7] = {'mat':all,'seccion':vigas}
ElemData[8] = {'mat':all,'seccion':vigas}

# Acciones exteriores
# Cargas nodales [carga x, carga y, carga z, momento x, momento y, momento z]
P = {}
P[2] =[1000,0,0,0,0,0]
P[8] =[1000,0,0,0,0,0]
# Cargas distribuidas
w = {}
# w[3] = {'wxi':5, 'wxf':5, 'wyi':0, 'wyf':0, 'wzi':0, 'wzf':0, 'terna':'local'}
w[5] = {'wxi':1, 'wxf':1, 'wyi':0, 'wyf':0, 'wzi':-1, 'wzf':-1, 'terna':'local'}
# Deformaciones impuestas (uniformes a lo largo de la barra) [Tsupy, Tinfy, hy, Tsupz, Tinfz, hz]
# hy es la altura para calcular la curvatura segun el eje y
# hz es la altura para calcular la curvatura segun el eje z
e0 = {}
e0[7] = [-10,20,0.5,0,0,1]
#Cedimientos de vinculo [dx, dy, dz, giro_x, giro_y, giro_z]
ced = {}
ced[1] = [0,0.01,0,0,0,0]

## ============================================================================
# Crear estructura desde archivo
# filename = 'E:\\GDrive FIUBA\\Atenea3d_GUI\\portico3D.a3d'
# Estructura = crear_estructura_from_file(filename)
# Crear estructura
Estructura = crear_estructura(XYZ, CON, ElemName, BOUN, rel, P, w, e0, ElemData,
                              ced, plot=False)
# Plotear estructura
plot_estructura(Estructura, fuerzas=True, vinculos=True, cedimientos=True,
                indiceN=False, ejes_loc=False, temp=True, indiceB=False,
                origen=False)
# Resolver estructura
metodo_rigideces(Estructura)
# Plotear resultados
plot_resultados(Estructura, reacciones=False, diagrama='Mz', deformada=False)
