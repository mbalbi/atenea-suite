# Ejemplo: Pórtico de dos pisos
from Atenea3d.source.core import *

## Input del usuario

# Crear nodos
# Coordenadas de nodos
XYZ = {}
XYZ[1] = [0,0,0] # primer nodo
XYZ[2] = [0,0,3] # segundo nodo
XYZ[3] = [0,0,6]
XYZ[4] = [0,6,6]
XYZ[5] = [0,6,3]
XYZ[6] = [0,6,0]

# XYZ[1] = [0,0,0] # primer nodo
# XYZ[2] = [0,0,0.3] # segundo nodo
# XYZ[3] = [0,0,0.6]
# XYZ[4] = [0,0.6,0.6]
# XYZ[5] = [0,0.6,0.3]
# XYZ[6] = [0,0.6,0]

# XYZ[1] = [0,0,0] # primer nodo
# XYZ[2] = [0,0,300] # segundo nodo
# XYZ[3] = [0,0,600]
# XYZ[4] = [0,600,600]
# XYZ[5] = [0,600,300]
# XYZ[6] = [0,600,0]

# Crear barras
# conectividad de barras
CON = {}
CON[1] = [1,2] # Barra 1 del nodo 1 al nodo 2
CON[2] = [2,3]
CON[3] = [3,4]
CON[4] = [4,5]
CON[5] = [5,6]
CON[6] = [2,5]

# Condiciones de borde [restr x, restr y, restr z, restr giro x, restr giro y, restr giro z]
BOUN = {}
BOUN[1] = [1,1,1,1,1,1] # Nodo 1 empotrado
BOUN[6] = [1,1,1,1,1,1] # Nodo 2 empotrado

# Tipos de elemento
ElemName = {}
ElemName[1] = '3DFrame'
ElemName[2] = '3DFrame'
ElemName[3] = '3DFrame'
ElemName[4] = '3DFrame'
ElemName[5] = '3DFrame'
ElemName[6] = '3DFrame'

# Releases de elementos [Mxi, Myi, Mzi, Pxf, Myf, Mzf], 1: release
rel = {}
rel[6] = [0,1,1,0,0,0]

# Propiedades del elememto
# Carga de materiales
mat1 = {'E':1000, 'mu':0.3, 'l':1e-5}
# Carga de secciones
"""
Iy, Iz y Jt son los momentos de inercia en los ejes locales "y", "z" y "x", respectivamente.
Alpha es el angulo de rotación de la sección en la direccion del eje local x. Cuando alpha vale 0,
el eje z esta en la direccion que lo contiene en el plano formado por el eje de la barra y el "y" global
(o en el caso de barra vertical, ele eje y es coincidente con el z global); y crece en sentido antihorario.
"""
seccion1 = {'A': 1e6, 'Iy': 50, 'Iz': 2.67e-4, 'Jt': 1.6e-3, 'alpha': 0}
# Asignación de material y sección a cada barra
ElemData = {}
ElemData[1] = {'mat':mat1,'seccion':seccion1}
ElemData[2] = {'mat':mat1,'seccion':seccion1}
ElemData[3] = {'mat':mat1,'seccion':seccion1}
ElemData[4] = {'mat':mat1,'seccion':seccion1}
ElemData[5] = {'mat':mat1,'seccion':seccion1}
ElemData[6] = {'mat':mat1,'seccion':seccion1}

# Acciones exteriores
# Cargas nodales [carga x, carga y, carga z, momento x, momento y, momento z]
P = {}
# P[2] =[0,5,0,0,0,0]
# Cargas distribuidas
w = {}
# w[3] = {'wxi':5, 'wxf':5, 'wyi':0, 'wyf':0, 'wzi':0, 'wzf':0, 'terna':'local'}
w[6] = {'wxi':0, 'wxf':0, 'wyi':0, 'wyf':0, 'wzi':-10, 'wzf':-10, 'terna':'global'}
# Deformaciones impuestas (uniformes a lo largo de la barra) [Tsupy, Tinfy, hy, Tsupz, Tinfz, hz]
# hy es la altura para calcular la curvatura segun el eje y
# hz es la altura para calcular la curvatura segun el eje z
e0 = {}
#Cedimientos de vinculo [dx, dy, dz, giro_x, giro_y, giro_z]
ced = {}

## ============================================================================
# Crear estructura desde archivo
# filename = 'E:\\GDrive FIUBA\\Atenea3d_GUI\\portico3D.a3d'
# Estructura = crear_estructura_from_file(filename)
# Crear estructura
Estructura = crear_estructura(XYZ, CON, ElemName, BOUN, rel, P, w, e0, ElemData,
                              ced, plot=False)
# Plotear estructura
plot_estructura(Estructura, fuerzas=True, vinculos=True, cedimientos=True,
                indiceN=True, ejes_loc=True, temp=True, indiceB=True,
                origen=True)
# Resolver estructura
metodo_rigideces(Estructura)
# Plotear resultados
plot_resultados(Estructura, reacciones=True, diagrama='My', deformada=True)
