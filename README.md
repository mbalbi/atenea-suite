# Atenea Suite

Atenea es un software educativo de análisis estructural, de código abierto y licencia libre, desarrollado por el Departamento de Estabilidad en el marco del proyecto UBATIC 2018/2019 y financiado por el CITEP-UBA.

**Director del proyecto** Raúl D. Bertero

**Mantenimiento** Mariano Balbi (mabalbi@fi.uba.ar), Felipe Medán

## Visión y objetivo

En la práctica de la ingeniería Civil y Mecánica, como en otras disciplinas, la tecnología ha modificado sustancialmente la manera de trabajar, de solucionar los problemas y de tomar decisiones. Se ha generado una brecha entre la práctica de la profesión con la utilización de software comerciales de análisis estructura que funcionan como “cajas negras”, y la enseñanza académica de los modelos analíticos que dan fundamento a estas herramientas informáticas. El presente proyecto tiene como objetivo principal acortar esta brecha, mediante el desarrollo de un software de análisis estructural que permita aprovechar la inmensa capacidad de cálculo de las computadoras modernas, pero que permita a los alumnos observar, interpelar e intervenir en el proceso de obtención de resultados que el programa realiza. Es decir, el software que se desarrollará busca romper con el esquema de “caja negra” de los softwares comerciales de cálculo, y ofrecer un entorno didáctico e interactivo donde los alumnos y docentes de las cátedras puedan complementar y potenciar la enseñanza de los conceptos fundamentales del análisis estructural con la capacidad de cálculo de una computadora. De esta manera, se busca empoderar a las nuevas generaciones de ingenieros con las herramientas adecuadas para la eficiente y rigurosa manipulación de las herramientas computacionales utilizadas en el ámbito profesional.

## Instalación

En este repositorio, encontrará el instalador de Windows (64-bit) para la última versión (y anteriores) del programa en la sección de *Releases*. El instalador contiene todo lo necesario para correr los programas de Atenea Suite, sin necesidad de instalar o descargar otras cosas.

## Correr localmente desde Python

Si la instalación no es una opción (para usuarios que no tengan Windows 64 bit), los programas pueden correrse con Python localmente en la computadora. 

Los requerimientos de Python para poder correr los programas con su interfaz gráfica están en el archivo 'requirements.txt' en este repositorio. Se pueden instalar de manera automática los paquetes necesarios con el siguiente comando desde el command prompt de windows:

```
pip install -r requirements.txt
```
