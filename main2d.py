# -*- coding: utf-8 -*-

import os, sys

from datetime import datetime
import numpy as np
import matplotlib as mpl

# os.system("pyuic5 -o mainwindow2.py mainwindow2.ui")
# os.system("C:\\Users\\Mariano\\AppData\\Local\\Programs\\Python\\Python37\\Scripts\\pyuic5
# -o source\\gui\\mainwindow.py source\\gui\\mainwindow.ui")

from Atenea2d.source.gui.mainwindow import *

from Atenea2d.source.gui.statusBar import createStatusBar
from Atenea2d.source.gui.agruparBotones import agrupar_botones

import Atenea2d.source.gui.rigideces_paso_a_paso as rigideces_paso_a_paso
import Atenea2d.source.gui.flexibilidades_paso_a_paso as flexibilidades_paso_a_paso

from Atenea2d.source.gui.interactiveCanvas import AteneaCanvasQt, BarrasDisplayCanvas

from Atenea2d.source.gui.atenea2dgui import NodoGUI, BarraGUI, EstructuraGUI

import Atenea2d.source.core.graph2d as graph
from Atenea2d.source.core.postprocess import read_txt, save_txt
from Atenea2d.source.core.analysis1el import metodo_rigideces, resolver_isostatico
from Atenea2d.source.core.analysis1el import metodo_flexibilidades
from Atenea2d.source.core.auxiliar import to_precision

from Atenea2d.source.gui.dialogs import customDialog, runDialog, about_dialog
from Atenea2d.source.gui.dialogs import errorDialog, saveDialog, matrices_dialog
from Atenea2d.source.gui.dialogs import flexibilidadesDialog

from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtWidgets import QFileDialog, QStyleFactory
from PyQt5.QtCore import Qt, QObject

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

class AteneaForm(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        ## Definir variables globales del programa
        # Variables de operacion
        self.nodos = {}
        self.barras = {}
        self.selected_nodos = {}
        self.selected_barras = {}
        self.Estructura = None
        self.materiales = {}
        self.secciones = {}
        self.filename = None
        self.saved = False
        self.DoubleValidation()
        # Variables de ploteo
        self.length_flechas_rel = {'p':1, 'w':0.6, 'c':0.7, 'ejes':0.5,
                                   'v':0.4, 'reacciones': 0.7}
        self.length_flechas = 1
        self.nodo_props = {'marker':'s', 'markersize':6, 'color':'b', 'zorder':2}
        self.barra_props = {'framewidth':1, 'color':'k', 'ejes_color':'y',
                            'zorder':1}
        self.colors = {'p':'m', 'w':'b', 'c':'c', 'e':['b','r'], 'reacciones':'g',
                       'diag':['b','r'], 'v':'maroon', 'def':'b'}
        self.zorder = {'p':3, 'w':3, 'c':3, 'e':3, 'reacciones':3,
                       'def':4, 'diag':3}
        self.fontsize = 8
        self.precision = 3
        self.default_grid = [1, 1, 20, 20]
        self.scale_def = 1

        ## Custom iconos
        # Custom png")),
        self.lock_icon = QtGui.QIcon()
        self.lock_icon.addPixmap(QtGui.QPixmap(
                            resource_path('icons/lock_icon.png')),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.unlock_icon = QtGui.QIcon()
        self.unlock_icon.addPixmap(QtGui.QPixmap(
                            resource_path("icons/unlock_icon.png")),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)

        ## Preparar layout
        # Setear comportamiento de docks
        self.resizeDocks([self.ui.arbolDock,self.ui.estructuraDock,
                          self.ui.cargasDock, self.ui.resultadosDock],
                          [1,1,1,1], Qt.Horizontal)
        self.openDocks()
        # Crear status bar
        createStatusBar(self)
        # Crear dialogs genéricos
        self.error_dialog = errorDialog()
        self.save_dialog = saveDialog()
        mat_dialog_labels = ['Nombre (sin espacios)',
                             'Módulo de elasticidad [E]',
                             'Coeficiente de expansion termica [l]']
        self.mat_dialog = customDialog(mat_dialog_labels,
                                       'Crear nuevo material',
                description='Ej. valores numéricos: 2.45, 24500, 24.5e3')
        sec_dialog_labels = ['Nombre (sin espacios)', 'Área [A]',
                             'Momento de inercia [I]']
        self.seccion_dialog = customDialog(sec_dialog_labels,
                                           'Crear nueva sección',
                    description='Ej. valores numéricos: 2.45, 24500, 24.5e3')
        # Crear subwindows y canvas
        #  Matplotlib canvas for results
        self.ui.resultadosSW = self.ui.mdiArea.addSubWindow(
                                            self.ui.resultadosSubwindow)
        # self.ui.resultadosSW.setWindowIcon(
        #         QtGui.QIcon(resource_path('icons/Atenea2d_icon.png')))
        self.ui.resultscanvas = AteneaCanvasQt(parent=self.ui.resultadosSubwindow,
                                    layout=self.ui.horizontalLayout_resultados,
                                    grid=False,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    coordsLabel=self.ui.coordsLabel,
                                    nodos=self.nodos, barras=self.barras,
                                    barprops={'framewidth':1, 'color':'silver',
                                              'ejes_color':'y'},
                                    nodeprops={'marker':'s', 'markersize':6,
                                               'color':'silver'},
                                    selectcolor='g', originArrows=False,
                                    key_self='self_res'
                                    )
        # Matplotlib canvas to create structure
        self.ui.cargaSW = self.ui.mdiArea.addSubWindow(self.ui.cargaSubwindow)
        # self.ui.cargaSW.setWindowIcon(QtGui.QIcon(
        #         resource_path('icons\Atenea2d_icon.png')))
        self.ui.loadcanvas = AteneaCanvasQt(parent=self.ui.cargaSubwindow,
                                    layout=self.ui.horizontalLayout_carga,
                                    grid=self.default_grid,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    coordsLabel=self.ui.coordsLabel,
                                    nodos=self.nodos, barras=self.barras,
                                    nodeprops=self.nodo_props,
                                    barprops=self.barra_props,
                                    selectcolor='r', key_self='self')
        # Canvas para solicitaciones de barras individuales
        self.ui.barrascanvas = BarrasDisplayCanvas(
                                parent=self.ui.frameCanvasContainer,
                                layout=self.ui.horizontalLayout_solicitaciones,
                                QSizePolicy=QtWidgets.QSizePolicy.Expanding)
        # Agrupar botones
        agrupar_botones(self)
        # Setear modos de los docks iniciales
        self.setEstructuraMode()
        self.setAccionesMode()

        ## Signals y slots de actions y botones
        # Set subwindows view actions
        self.ui.actionTabbed_View.triggered.connect(self.Tabbed_View)
        self.ui.actionCascade_View.triggered.connect(self.cascadeArrange)
        self.ui.actionTile_View.triggered.connect(self.tileArrange)

        # Set text action
        self.ui.actionFuerzas.triggered.connect(self.showFuerzas)
        self.ui.actionIndices.triggered.connect(self.showIndices)
        self.ui.actionEjes_Locales.triggered.connect(self.showEjesLocales)

        # Seleccionar modo de seleccion de objetos
        self.ui.actionPickAll.triggered.connect(self.setCanvasMode)
        self.ui.actionPickNodos.triggered.connect(self.setCanvasMode)
        self.ui.actionPickBarras.triggered.connect(self.setCanvasMode)
        self.ui.actionPan.triggered.connect(self.setCanvasMode)
        self.ui.actionDrawNodo.triggered.connect(self.setCanvasMode)
        self.ui.actionDrawBarra.triggered.connect(self.setCanvasMode)
        self.ui.actionZoom2Rect.triggered.connect(self.setCanvasMode)
        self.ui.actionPan.setChecked(True)
        self.setCanvasMode()

        # Conectar actions de docks
        self.ui.actionArbol.triggered.connect(self.openDocks)
        self.ui.actionEstructura.triggered.connect(self.openDocks)
        self.ui.actionAcciones.triggered.connect(self.openDocks)
        self.ui.actionResultados.triggered.connect(self.openDocks)
        self.ui.actionArbol.setChecked(True)
        self.ui.actionEstructura.setChecked(True)
        self.ui.actionAcciones.setChecked(True)
        self.ui.actionResultados.setChecked(False)
        self.splitDockWidget(self.ui.estructuraDock, self.ui.cargasDock,
                             Qt.Vertical)
        self.ui.estructuraDock.raise_()

        # Zoom to extent
        self.ui.actionZoom2Extent.triggered.connect(
                                        lambda: self.zoom2extent(0.25))

        # Action buttons de ayuda
        self.ui.actionSobre_el_software.triggered.connect(about_dialog)

        ## Botones del árbol dock
        # Seleccionar todos los objetos
        self.ui.selectAllNodosButton.clicked.connect(self.selectAllNodos)
        self.ui.selectAllBarrasButton.clicked.connect(self.selectAllBarras)
        # Group button del tree view outline
        self.ui.selectedToolButton.clicked.connect(self.updateTreeSelected)
        self.ui.nodosToolButton.clicked.connect(self.updateTreeNodos)
        self.ui.barrasToolButton.clicked.connect(self.updateTreeBarras)
        self.ui.propiedadesToolButton.clicked.connect(self.updateTreePropiedades)
        self.ui.deleteActionsToolButton.clicked.connect(self.cleanAcciones)
        self.ui.deleteToolButton.clicked.connect(self.deleteAll)

        ## Interacción del canvas con el GUI
        # Crear nodo interactivamente
        self.ui.loadcanvas.interactions['nodedrawer'].created.connect(
                                                        self.createNodo)
        # Crear barra interactivamente
        self.ui.loadcanvas.interactions['bardrawer'].created.connect(
                                                        self.createBarra)
        # Completar text browsers cuando se selecciona un nodo
        self.ui.loadcanvas.interactions['picker'].picked.connect(
                                                    self.addSelectedObjects)
        # Resetear objetos selccionados
        self.ui.loadcanvas.interactions['picker'].cleared.connect(
                                                    self.clearSelectedObjects)
        # Eliminar objetos seleccionados
        self.ui.loadcanvas.interactions["picker"].delete.connect(
                                            self.deleteSelectedObjects)

        ## Botones de carga de estructura
        # Seleccionar modo dibujar nodos
        self.ui.nodosButtonDraw.clicked.connect(self.setEstructuraMode)
        # Seleccionar modo agregar vinculos
        self.ui.nodosButtonVinculo.clicked.connect(self.setEstructuraMode)
        # Seleccionar modo dibujar barras
        self.ui.barrasButtonDraw.clicked.connect(self.setEstructuraMode)
        self.ui.VigaRadioButton.toggled.connect(self.setEstructuraMode)
        self.ui.ReticuladoRadioButton.toggled.connect(self.setEstructuraMode)
        # Seleccionar modo cargar secciones y materiales
        self.ui.barrasButtonProps.clicked.connect(self.setEstructuraMode)

        # Botón de Crear nodo
        self.ui.pushButtonCreateNode.clicked.connect(self.addNodo)
        # Botón de agregar barras
        self.ui.pushButtonCreateBar.clicked.connect(self.addBarra)
        # Botón de agregar vinculos
        self.ui.pushButtonVinculos.clicked.connect(self.addVinculos)
        # Botón de agregar articulaciones
        self.ui.pushButtonArticulaciones.clicked.connect(self.addReleases)
        # Leer Material
        self.ui.addMatButton.clicked.connect(self.addMaterial)
        self.ui.modMatButton.clicked.connect(self.modifyMaterial)
        # # Leer Sección
        self.ui.addSecButton.clicked.connect(self.addSeccion)
        self.ui.modSecButton.clicked.connect(self.modifySeccion)
        # # Botón de agregar propiedades
        self.ui.pushButtonPropiedades.clicked.connect(self.addProperties)


        ## Botones de acciones dock
        # Seleccionar modo cargas cargas puntuales
        self.ui.nodosButtonCarga.clicked.connect(self.setAccionesMode)
        # Seleccionar modo cargas cargas distribuidas
        self.ui.barrasButtonRel.clicked.connect(self.setEstructuraMode)
        self.ui.radioButtonTernaLocal.toggled.connect(self.changeWlabels)
        self.ui.radioButtonTernaGlobal.toggled.connect(self.changeWlabels)
        # Seleccionar modo cargas cargas distribuidas
        self.ui.barrasButtonCarga.clicked.connect(self.setAccionesMode)
        # Seleccionar modo cargas de temperatura
        self.ui.barrasButtonTemp.clicked.connect(self.setAccionesMode)
        # Seleccionar modo cargas de cedimiento de vinculo
        self.ui.nodosButtonCed.clicked.connect(self.setAccionesMode)

        # Botón de cargar cargas puntuales
        self.ui.pushButtonCargasP.clicked.connect(self.addCargasP)
        # Botón de cargar cargas distribuidas
        self.ui.pushButtonCargasW.clicked.connect(self.addCargasW)
        # Botón de cargar temperatura
        self.ui.pushButtonTemp.clicked.connect(self.addCargase0)
        # Botón de cargar cedimientos de vínculo
        self.ui.pushButtonCed.clicked.connect(self.addCedimientos)

        ## Botones de cargar, guardar y limpiar archivo
        # Cargar estructura
        self.ui.actionOpen.triggered.connect(self.openFileDialog)
        # Guardar estructura
        self.ui.actionSave.triggered.connect(self.saveFileDialog)
        # Guardar estructura como
        self.ui.actionSaveAs.triggered.connect(self.saveAsFileDialog)
        # Nuevo archivo
        self.ui.actionNew.triggered.connect(lambda: self.newCanvas(dialog=True))

        ## Botón de correr y bloquear ventanas
        # Correr estructura
        self.ui.actionRun.triggered.connect(self.runEstructura)
        # Desbloquear carga de estructura
        self.ui.actionUnlock.triggered.connect(self.unlockCargaMode)

        ## Botones de resultados
        self.ui.deformadaToolButton.clicked.connect(self.setResultsMode)
        self.ui.reaccionesToolButton.clicked.connect(self.setResultsMode)
        self.ui.momentoToolButton.clicked.connect(self.setResultsMode)
        self.ui.corteToolButton.clicked.connect(self.setResultsMode)
        self.ui.normalToolButton.clicked.connect(self.setResultsMode)
        self.ui.estructuraSiToolButton.clicked.connect(self.setResultsMode)
        self.ui.comboBox_rig.currentIndexChanged.connect(self.setResultsMode)
        # Interacción entre canvas de resultados y GUI
        self.ui.resultscanvas.interactions['picker'].picked.connect(
                                                    self.addSelectedObjects)
        self.ui.resultscanvas.interactions['picker'].cleared.connect(
                                                    self.clearSelectedObjects)
        # Slider de cambiar escala de deformada
        self.ui.scaleSlider.sliderMoved.connect(self.changeScale)
        self.ui.textEditDeformadaScale.editingFinished.connect(
                                                self.changeScale_position)
        # Mover slider de posición de barra
        self.ui.barpositionSlider.valueChanged.connect(self.plot_barra_selected)
        # action button de resultados de barras individuales
        self.ui.pushButton_max.clicked.connect(
                        lambda: self.show_max_barra_selected('max'))
        self.ui.pushButton_min.clicked.connect(
                        lambda: self.show_max_barra_selected('min'))
        # Cambiar de barra cuando se modifica el combo box
        self.ui.barrasComboBox.currentIndexChanged.connect(
                                                self.plot_barra_selected)
        # Botón de popear matriz de rigideces y términos de causa
        self.ui.matrizKToolButton.clicked.connect(
                    lambda: matrices_dialog(self, self.Estructura, self.met))

    def lockCargaMode(self):
        """
        Bloquea los botones de carga de estructura
        """
        # Marcar como activo el action de lock
        self.ui.actionUnlock.setEnabled(True)
        self.ui.actionUnlock.setIcon(self.lock_icon)
        # Habilitar docj de resultados y deshabilitar docks de estructura
        self.ui.cargasDock.setEnabled(False)
        self.ui.estructuraDock.setEnabled(False)
        self.ui.resultadosDock.setEnabled(True)
        self.ui.actionEstructura.setEnabled(False)
        self.ui.actionEstructura.setChecked(False)
        self.ui.actionAcciones.setEnabled(False)
        self.ui.actionAcciones.setChecked(False)
        self.ui.actionResultados.setEnabled(True)
        self.ui.actionResultados.setChecked(True)
        self.ui.deleteActionsToolButton.setEnabled(False)
        self.ui.deleteToolButton.setEnabled(False)
        self.openDocks()
        # Deshabilitar botones de guardado, abrir, nuevo y correr
        self.ui.actionSave.setEnabled(False)
        self.ui.actionSaveAs.setEnabled(False)
        self.ui.actionOpen.setEnabled(False)
        self.ui.actionNew.setEnabled(False)
        self.ui.actionRun.setEnabled(False)
        # Deshabilitar modos del canvas de dibujar
        self.ui.actionDrawNodo.setEnabled(False)
        self.ui.actionDrawBarra.setEnabled(False)
        # Set deformada mode por default
        self.ui.deformadaToolButton.setChecked(True)
        self.ui.reaccionesToolButton.setChecked(False)
        self.ui.momentoToolButton.setChecked(False)
        self.ui.corteToolButton.setChecked(False)
        self.ui.normalToolButton.setChecked(False)
        self.ui.estructuraSiToolButton.setChecked(False)
        # Set pick nodos por default
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()
        # Blanqeuar seleccionados
        self.ui.resultscanvas.interactions['picker'].clearSelected()
        # Si se corre en modo 'Ecuaciones de equilibrio', deshabilitar deformada
        if self.met==0:
            self.ui.deformadaToolButton.setVisible(False)
            self.ui.deformadaToolButton.setChecked(False)
            self.ui.frame_9.setVisible(False)
        else:
            self.ui.deformadaToolButton.setVisible(True)
            self.ui.deformadaToolButton.setChecked(True)
            self.ui.frame_9.setVisible(True)
        # Si se corre en modo 'Sistemas fundamentales', habilitar frames
        if self.paso_a_paso_mode:
            self.ui.frame_paso_a_paso.setVisible(True)
            self.ui.estructuraSiToolButton.setVisible(True)
        else:
            self.ui.frame_paso_a_paso.setVisible(False)
            self.ui.estructuraSiToolButton.setVisible(False)
        # Deshabilitar función de eliminar nodos
        self.ui.loadcanvas.interactions["picker"].delete.disconnect()

    def unlockCargaMode(self):
        """
        Resetea los resultados y la estructura y habilita los botones
        de carga de estructura
        """
        # File Dialog pop-up
        warning = QtWidgets.QMessageBox.warning(self, 'Desbloquear modo de carga',
                    'Al debloquear el modo de carga se perderán los resultados',
                    QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if warning == QtWidgets.QMessageBox.Ok:
            # Marcar como activo el action de lock
            self.ui.actionUnlock.setEnabled(False)
            self.ui.actionUnlock.setIcon(self.unlock_icon)
            # Habilitar botones de carga y deshabilitar botones de resultados
            self.ui.cargasDock.setEnabled(True)
            self.ui.estructuraDock.setEnabled(True)
            self.ui.resultadosDock.setEnabled(False)
            self.ui.actionSave.setEnabled(True)
            self.ui.actionSaveAs.setEnabled(True)
            self.ui.actionOpen.setEnabled(True)
            self.ui.actionNew.setEnabled(True)
            self.ui.actionRun.setEnabled(True)
            self.ui.actionDrawNodo.setEnabled(True)
            self.ui.actionDrawBarra.setEnabled(True)
            self.ui.actionEstructura.setEnabled(True)
            self.ui.actionAcciones.setEnabled(True)
            self.ui.actionResultados.setEnabled(False)
            self.ui.deleteActionsToolButton.setEnabled(True)
            self.ui.deleteToolButton.setEnabled(True)
            # Prender canvas de carga de estructura
            self.ui.mdiArea.setActiveSubWindow(self.ui.cargaSW)
            # self.ui.actionPan.setChecked(True)
            self.setCanvasMode()
            # self.zoom2extent()
            self.ui.actionResultados.setChecked(False)
            self.ui.actionEstructura.setChecked(True)
            self.ui.actionAcciones.setChecked(True)
            self.openDocks()
            # Limpiar canvas de resultados
            self.ui.barrasComboBox.clear()
            self.ui.comboBox_rig.clear()
            # self.ui.tableWidget_TerminosdeCausa.clear()
            # self.ui.tableWidget_Rigideces.clear()
            self.ui.tableWidget.clear()
            self.ui.resultscanvas.clearCanvas(nodos=self.nodos,
                                              barras=self.barras)
            self.ui.loadcanvas.interactions['picker'].clearSelected()
            # Borrar estructura
            self.Estructura = None
            # Borrar artists de los dicts de nodos y barras
            for key in ['self_res','index_res','v_res','p_res','c_res']:
                for nodo in self.nodos.values():
                    del nodo.artists[key]
            for key in ['self_res','ejes_res','index_res','w_res','e_res']:
                for barra in self.barras.values():
                    del barra.artists[key]
            # Conectar función de eliminar nodos
            self.ui.loadcanvas.interactions["picker"].delete.connect(
                                    self.deleteSelectedObjects)

    def runEstructura(self):
        # Crear estructura
        try:
            self.Estructura = EstructuraGUI(self.nodos, self.barras)
        except Exception as ex:
            self.error_dialog.error_catch('Error al armar la estructura', ex)
            return
        # Diálogo de modo de corrida
        dialog = runDialog(self.Estructura)
        met, pap, ok = dialog.exec_()
        if ok:
            self.met = met
            self.paso_a_paso_mode = False
            # Correr estructura con el método elegido
            if met == 0: # Isostático
                try:
                    resolver_isostatico(self.Estructura)
                except Exception as ex:
                    self.Estructura = None
                    self.error_dialog.error_catch(
                    'Error corriendo el método de equilibrio de fuerzas', ex)
                    return
            if met == 1:
                # Chequear que todas las barras tengan mat y sec
                for barra in self.barras.values():
                    if (not barra.mat) or (not barra.seccion):
                        self.Estructura = None
                        self.error_dialog.error_catch(
                        'Faltan propiedades en la barra {}'.format(barra.indice))
                        return
                try:
                    # Correr método de rigideces
                    metodo_rigideces(self.Estructura)
                    if pap: # Calcular sistemas fundamentales
                        self.paso_a_paso_mode = True
                        # Correr metodo de las rigideces paso a paso
                        self.rigideces_paso_a_paso()
                        # Plotear sistemas fundamentales: estructura del S0
                        self.sistemas_pap[0].plot_estructura(
                                self.ui.resultscanvas.ax, fontsize=self.fontsize,
                                keys={'nodo':['self','index'],
                                    'v':'v', 'barra':['self','ejes','index']},
                length={'v':self.length_flechas_rel['v']*self.length_flechas},
                                indice_visible=False, ejes_visible=False,
                                colors={'nodo':'silver', 'barra':'silver',
                                        'p':'silver', 'c':'silver', 'v':'silver',
                                        'w':'silver', 'e':['silver','silver'],
                                        'ejes':'y'},
                                zorder=self.zorder, vinculos=True,
                                acciones=False, picker=True)
                        # Plotear sistemas fundamentales: acciones y resultados
                        for sistema in self.sistemas_pap.values():
                            sistema.plot_acciones(self.ui.resultscanvas.ax,
                                        fontsize=self.fontsize,
                                        keys={'p':'p', 'c':'c',
                                              'w':'w', 'e':'e'},
                length={'p':self.length_flechas_rel['p']*self.length_flechas,
                        'c':self.length_flechas_rel['c']*self.length_flechas,
                        'w':self.length_flechas_rel['w']*self.length_flechas},
                                        colors=self.colors,
                                        zorder=self.zorder)
                            sistema.plot_results(self.ui.resultscanvas.ax,
                                                 colors=self.colors,
                                                 scale_def=self.scale_def,
                                                 precision=self.precision,
                                                 fontsize=self.fontsize,
                                                 zorder=self.zorder,
                length=self.length_flechas_rel['reacciones']*self.length_flechas)

                except Exception as ex:
                    self.Estructura = None
                    self.error_dialog.error_catch(
                                'Error corriendo el método de rigideces', ex)
                    return
            if met == 2:
                # Chequear que todas las barras tengan mat y sec
                for barra in self.barras.values():
                    if (not barra.mat) or (not barra.seccion):
                        self.Estructura = None
                        self.error_dialog.error_catch(
                        'Faltan propiedades en la barra {}'.format(barra.indice))
                        return
                try:
                    metodo_flexibilidades(self.Estructura)
                    if pap: # Calcular sistemas fundamentales
                        self.flexui = flexibilidadesDialog(self.Estructura)
                        ok, exts, ints = self.flexui.exec_()
                        if ok:
                            # Correr flexibilidades con incognitas seleccionadas
                            metodo_flexibilidades(self.Estructura, qinc=ints,
                                                  ext=exts)
                            self.paso_a_paso_mode = True
                            # Correr flexibilidades paso a paso
                            self.flexibilidades_paso_a_paso(exts, ints)
                            # Visibilizar boton de dibujar estructura de cada
                            # sistema
                            self.ui.estructuraSiToolButton.setVisible(True)
                            # Plotear sistemas fundamentales: estructura del S0
                            self.sistemas_pap[0].plot_estructura(
                                    self.ui.resultscanvas.ax,
                                    fontsize=self.fontsize,
                                    keys={'nodo':['self','index'], 'v':'v',
                                          'barra':['self','ejes','index']},
                length={'v':self.length_flechas_rel['v']*self.length_flechas},
                                    indice_visible=False, ejes_visible=False,
                                    colors={'nodo':'silver', 'barra':'silver',
                                            'p':'silver', 'c':'silver',
                                            'v':'silver', 'w':'silver',
                                            'e':['silver','silver'],
                                            'ejes':'y'},
                                    zorder=self.zorder, vinculos=True,
                                    acciones=False, picker=True)
                            # Plotear sistemas fundamentales: acciones y
                            # resultados
                            for sistema in self.sistemas_pap.values():
                                sistema.plot_acciones(self.ui.resultscanvas.ax,
                                            fontsize=self.fontsize,
                                            keys={'p':'p', 'c':'c',
                                                    'w':'w', 'e':'e'},
                    length={'p':self.length_flechas_rel['p']*self.length_flechas,
                        'c':self.length_flechas_rel['c']*self.length_flechas,
                        'w':self.length_flechas_rel['w']*self.length_flechas},
                                            colors=self.colors,
                                            zorder=self.zorder)
                                if sistema.Q_ini:
                                    sistema.plot_Q_ini(self.ui.resultscanvas.ax,
                                                    key='q_ini',
                                                    fontsize=self.fontsize,
                        length=self.length_flechas_rel['c']*self.length_flechas,
                                                    color=self.colors['p'],
                                                    zorder=self.zorder['p'],
                                                    linewidth=1)
                                sistema.plot_results(self.ui.resultscanvas.ax,
                                                    colors=self.colors,
                                                    scale_def=self.scale_def,
                                                    precision=self.precision,
                                                    fontsize=self.fontsize,
                                                    zorder=self.zorder,
            length=self.length_flechas_rel['reacciones']*self.length_flechas)
                        else:
                            self.Estructura = None
                            return
                except Exception as ex:
                    self.Estructura = None
                    self.error_dialog.error_catch(
                            'Error corriendo el método de flexibilidades', ex)
                    return
            # Plotear estructura en canvas de resultados
            self.res_keys = {'nodo':['self_res','index_res'], 'v':'v_res',
                            'p':'p_res', 'c':'c_res',
                            'barra':['self_res','ejes_res','index_res'],
                            'w':'w_res', 'e':'e_res'}
            self.Estructura.plot_estructura(self.ui.resultscanvas.ax,
                keys=self.res_keys, fontsize=self.fontsize,
                length={'p':self.length_flechas_rel['p']*self.length_flechas,
                        'c':self.length_flechas_rel['c']*self.length_flechas,
                        'w':self.length_flechas_rel['w']*self.length_flechas,
                        'v':self.length_flechas_rel['v']*self.length_flechas},
                indice_visible=self.ui.actionIndices.isChecked(),
                ejes_visible=self.ui.actionEjes_Locales.isChecked(),
                colors={'nodo':'silver', 'barra':'silver', 'p':'silver',
                       'c':'silver', 'v':'silver', 'w':'silver',
                       'e':['silver','silver'], 'ejes':'y'},
                zorder=self.zorder, vinculos=True, acciones=True, picker=True)
            # Generar ploteos diagramas, reacciones y deformada de la estructura
            if met==0: deformada=False
            else: deformada=True
            self.Estructura.plot_results(self.ui.resultscanvas.ax,
                                         colors=self.colors,
                                         scale_def=self.scale_def,
                                         precision=self.precision,
                                         fontsize=self.fontsize,
                                         zorder=self.zorder,
            length=self.length_flechas_rel['reacciones']*self.length_flechas,
                    deformada=deformada)
            # Bloquear modo de carga de estructura y habilitar dock de resultados
            self.lockCargaMode()
            # Mostrar resultados en canvas
            self.setResultsMode()
            # Focus y Zoom a la estructura resultados
            self.ui.mdiArea.setActiveSubWindow(self.ui.resultadosSW)
            self.zoom2extent(margin=0.2, canvas=self.ui.resultscanvas)
            if pap:
                # Completar combobox de sistemas
                self.ui.comboBox_rig.clear()
                self.ui.comboBox_rig.addItem('Total')
                for key in self.sistemas_pap.keys():
                    self.ui.comboBox_rig.addItem(str(key))
        else:
            self.Estructura = None

    def rigideces_paso_a_paso(self):
        """
        Construye y resuelve los N sistemas para el metodo de las rigideces
        paso a paso
        :return:
        """
        #Inicializar diccionario de sistemas para el metodo de las rigideces
        self.sistemas_pap = {}
        #Generar todos los sistemas, resolverlos, y guardarlos en un diccionario
        for indice_sist in range(len(self.Estructura.DOFf) + 1):
            if indice_sist == 0:
                DOFf = 0
                # self.ui.tableWidget_Rigideces.clearSelection()
            else:
                # self.ui.tableWidget_Rigideces.selectRow(indice_sist-1)
                DOFf = self.Estructura.DOFf[indice_sist-1]
            #Generar estructura
            sistema = rigideces_paso_a_paso.rigideces_paso_a_paso(
                                                    self.Estructura, DOFf)
            #Resolver estructura
            metodo_rigideces(sistema)
            #Guardar estructura en el diccionario
            self.sistemas_pap[indice_sist] = sistema

    def flexibilidades_paso_a_paso(self, ext=[], qinc=[]):
        """
        Construye y resuelve los N sistemas para el metodo de las flexibilidades
        paso a paso
        :param ext: Lista con los DOF's en los que se liberan fuerzas externas
        :param xcols: Lista con los q's en los que se liberan fuerzas internas
        :return:
        """
        self.sistemas_pap = flexibilidades_paso_a_paso.sistemas_flexibilidades(
                                self.Estructura, ext=ext, qinc=qinc)
        for sistema in self.sistemas_pap.values():
            if sistema.Q_ini: #Sistemas con q internos liberados
                resolver_isostatico(sistema, sistema.Q_ini, v=True)
            else:
                resolver_isostatico(sistema, v=True)

    def setResultsMode(self):
        # Cursor
        cursor = QtCore.Qt.PointingHandCursor
        # Invisibilizar todo en estructura total
        self.Estructura.set_diagramas_visible(['M','Q','N'], False)
        self.Estructura.set_barras_visible(
                    ['self_res', 'deformada', 'w_res', 'e_res'], False)
        self.Estructura.set_nodos_visible(
                    ['self_res', 'reacciones', 'p_res', 'v_res', 'c_res'], False)
        self.Estructura.set_nodos_picker(False, 'self_res')
        self.Estructura.set_barras_picker(False, 'self_res')
        # Obtener número del sistema del combobox
        if self.paso_a_paso_mode:
            # Invisibilizar todo en sistemas fundamentales
            for sistema in self.sistemas_pap.values():
                # Setear todo como invisible
                sistema.set_diagramas_visible(['M','Q','N'], False)
                # self.plot_estructura_flex(self, sistema, indice)
                sistema.set_nodos_visible(['self','v','p','c','reacciones'],
                                            False)
                sistema.set_barras_visible(['self','w','e','deformada','q_ini'],
                                            False)
                sistema.set_nodos_picker(False, 'self')
                sistema.set_barras_picker(False, 'self')
            # Obtener numero de sistema del combobox
            sist = self.ui.comboBox_rig.currentText()
            if sist != '':
                if sist == 'Total':
                    sistema = self.Estructura
                    sistema.set_nodos_visible(['self_res'], True)
                    sistema.set_barras_visible(['self_res'], True)
                    # Actualizar lista de nodos y barras del canvasQt de pap
                    self.ui.resultscanvas.updateNodosyBarras(key_self='self_res',
                                nodos=self.Estructura.nodos,
                                barras=self.Estructura.barras)
                else:
                    indice_sist = int(sist)
                    sistema = self.sistemas_pap[indice_sist]
                    self.sistemas_pap[0].set_nodos_visible(['self'], True)
                    self.sistemas_pap[0].set_barras_visible(['self'], True)
                    # Actualizar lista de nodos y barras del canvasQt de pap
                    self.ui.resultscanvas.updateNodosyBarras(key_self='self',
                                nodos=self.sistemas_pap[0].nodos,
                                barras=self.sistemas_pap[0].barras)
        else:
            sistema = self.Estructura
            sistema.set_nodos_visible(['self_res'], True)
            sistema.set_barras_visible(['self_res'], True)
            # Actualizar lista de nodos y barras del canvasQt de pap
            self.ui.resultscanvas.updateNodosyBarras(key_self='self_res',
                        nodos=sistema.nodos,
                        barras=sistema.barras)
        # Mostrar diagrama correspondiente
        if self.ui.momentoToolButton.isChecked():
            sistema.set_diagramas_visible(['M'])
        if self.ui.corteToolButton.isChecked():
            sistema.set_diagramas_visible(['Q'])
        if self.ui.normalToolButton.isChecked():
            sistema.set_diagramas_visible(['N'])
        if self.ui.deformadaToolButton.isChecked():
            sistema.set_barras_visible(['deformada'],True)
            self.changeScale()
        if self.ui.reaccionesToolButton.isChecked():
            cursor = QtCore.Qt.OpenHandCursor
            sistema.set_nodos_visible(['reacciones'],True)
        if self.ui.estructuraSiToolButton.isChecked():
            cursor = QtCore.Qt.OpenHandCursor
            if self.paso_a_paso_mode and sist == 'Total':
                self.Estructura.set_nodos_visible(['v_res'], True)
                sistema.set_nodos_visible(['c_res','p_res'],True)
                sistema.set_barras_visible(['w_res','e_res'],True)
            else:
                self.sistemas_pap[0].set_nodos_visible(['v'], True)
                sistema.set_nodos_visible(['c','p'],True)
                sistema.set_barras_visible(['w','e','q_ini'],True)
        self.ui.resultscanvas.draw_idle()
        #
        self.setCanvasMode()
        # Completar tablas y grafico de barra individual
        self.completeDisplacementTable()
        self.completeBarrasCombobox()

    def newCanvas(self, dialog=True):
        # Save dialog
        if dialog:
            ok = self.save_dialog.exec_()
            if ok==1: # Yes
                self.saveFileDialog()
            elif ok==0: # No
                pass
            elif ok==2: # Cancel
                return
        # Inicializar elementos de la estructura
        self.nodos = {}
        self.barras = {}
        self.Estructura = None
        # Eliminar todos los artists del canvas
        self.ui.loadcanvas.clearCanvas(grid=True, nodos=self.nodos,
                                       barras=self.barras)
        self.ui.resultscanvas.clearCanvas(nodos=self.nodos, barras=self.barras)
        # self.ui.p_a_pcanvas.clearCanvas(nodos=self.nodos, barras=self.barras)
        self.ui.actionPan.setChecked(True)
        self.setCanvasMode()
        # Borrar los Combo Box de createBarra
        self.ui.comboBoxNodo1.clear()
        self.ui.comboBoxNodo2.clear()
        # Deshabilitar botones de resultados
        self.ui.resultadosDock.setEnabled(False)
        # Eliminar secciones y materiales creados
        self.materiales = {}
        self.ui.matlistWidget.clear()
        self.secciones = {}
        self.ui.seccionlistWidget.clear()
        # Resetear guardado y nombre del file
        self.filename = None
        self.saved = False
        self.setWindowTitle('Atenea2d')
        # Vaciar los trees
        self.updateTreeNodos()
        self.updateTreeBarras()
        self.updateTreePropiedades()

    def openFileDialog(self, filename=None):
        if filename:
            self.filename = filename
        if not filename:
            filename = QFileDialog.getOpenFileName(self, 'Open file',
                                                   filter='Atenea2d(*.a2d)')
            # Chequear si no canceló
            if not filename[0]:
                return
            # Guardar nombre de archivo
            self.filename = filename[0]
        self.setWindowTitle('Atenea2d - ' + os.path.basename(self.filename) +\
                            ' [' + str(datetime.now().strftime('%H:%M')) + ']')
        if os.path.exists(self.filename):
            # Inicializar elementos de la estructura
            self.nodos = {}
            self.barras = {}
            self.materiales = {}
            self.secciones = {}
            self.Estructura = None
            self.ui.seccionlistWidget.clear()
            self.ui.matlistWidget.clear()
            # Eliminar todos los artists del canvas
            self.ui.loadcanvas.clearCanvas(grid=True, nodos=self.nodos,
                                           barras=self.barras)
            self.ui.resultscanvas.clearCanvas(nodos=self.nodos, barras=self.barras)
            # self.ui.p_a_pcanvas.clearCanvas(nodos=self.nodos, barras=self.barras)
            # Leer metadata de estructura desde el archivo
            metadata = read_txt(self.filename)
            # Cargar estructura
            self.loadEstructura(metadata)
            # Estructura guardada
            self.saved = True

    def loadEstructura(self, metadata):
        """
        Cargar estructura desde metadata
        """
        # Crear nodos
        for coords in metadata['XYZ'].values():
            self.createNodo(coords)
        # Crear barras
        for i, nodos in metadata['CON'].items():
            ElemName = metadata['ElemName'][i]
            self.createBarra((self.nodos[nodos[0]].coords,
                              self.nodos[nodos[1]].coords, ElemName))
        # Agregar releases
        for i, rel in metadata['rel'].items():
            self.createReleases({i:self.barras[i]}, rel)
        # Agregar restricciones de vínculo
        for i, boun in metadata['BOUN'].items():
            self.createVinculos({i:self.nodos[i]}, boun)
        # Agregar cargas nodales
        for i, P in metadata['P'].items():
            self.createCargasP({i:self.nodos[i]}, P)
        # Agregar cargas distribuidas
        for i, w in metadata['w'].items():
            self.createCargasW({i:self.barras[i]}, w)
        # Agregar deformaciones distribuidas
        for i, e0 in metadata['e0'].items():
            self.createCargase0({i:self.barras[i]}, e0)
        # Agregar cedimientos de vinculo
        for i, ced in metadata['ced'].items():
            self.createCedimientos({i:self.nodos[i]}, ced)
        # Crear secciones
        for i in metadata['secciones'].keys():
            seccion = metadata['secciones'][i]
            self.secciones[i] = seccion
            self.ui.seccionlistWidget.addItem(str(i))
        # Crear materiales
        for i in metadata['materiales'].keys():
            material = metadata['materiales'][i]
            self.materiales[i] = material
            self.ui.matlistWidget.addItem(str(i))
        # Agregar propiedades a las barras
        for i, data in metadata['ElemData'].items():
            sec = None
            mat = None
            if data['seccion']:
                sec = self.secciones[data['seccion']]
            if data['mat']:
                mat = self.materiales[data['mat']]
            self.barras[i].assign_data({'seccion':sec,'mat':mat})
        self.zoom2extent(margin=0.2, canvas=self.ui.loadcanvas)
        # Update tree nodos
        self.updateTreeNodos()

    def saveFileDialog(self):
        # Obtener nombre y directorio de archivo nuevo
        if not self.saved:
            filename = QFileDialog.getSaveFileName(self, 'Save file',
                                            filter='Atenea2d(*.a2d)')
            self.filename = filename[0]
            self.saved = True
        if self.filename:
            # Modificar window title
            self.setWindowTitle('Atenea2d - ' + os.path.basename(
                    self.filename) + \
                    ' [' + str(datetime.now().strftime('%H:%M')) + ']')
            # Guardarlo en el archivo
            save_txt(self.filename, self.nodos, self.barras, self.materiales,
                     self.secciones)

    def saveAsFileDialog(self):
        # Obtener nombre y directorio de archivo nuevo
        self.saved = False
        self.saveFileDialog()

    def openDocks(self):
        # Dock de acciones
        if self.ui.actionAcciones.isChecked():
            self.ui.cargasDock.setVisible(True)
        else:
            self.ui.cargasDock.setVisible(False)
        # Dock de estructura
        if self.ui.actionEstructura.isChecked():
            self.ui.estructuraDock.setVisible(True)
        else:
            self.ui.estructuraDock.setVisible(False)
        # Dock del arbol
        if self.ui.actionArbol.isChecked():
            self.ui.arbolDock.setVisible(True)
        else:
            self.ui.arbolDock.setVisible(False)
        # Dock de resultados
        if self.ui.actionResultados.isChecked():
            self.ui.resultadosDock.setVisible(True)
        else:
            self.ui.resultadosDock.setVisible(False)

    def setGridOn(self):
        self.ui.loadcanvas.setGridOn()

    def updateGrid(self):
        # Define new grid
        sx = self.ui.dxSpinBox.value()
        sy = self.ui.dySpinBox.value()
        nx = self.ui.nxSpinBox.value()
        ny = self.ui.nySpinBox.value()
        grid = [sx, sy, nx, ny]
        # Update grid in canvas
        self.ui.loadcanvas.updateGrid(grid,
                                      show=self.ui.snapOnToolButton.isChecked())

    def changeWlabels(self):
        # Leer terna
        if self.ui.radioButtonTernaGlobal.isChecked():
            self.ui.label_18.setText('wyi')
            self.ui.label_19.setText('wyf')
        elif self.ui.radioButtonTernaLocal.isChecked():
            self.ui.label_18.setText('wzi')
            self.ui.label_19.setText('wzf')

    def setEstructuraMode(self):
        # Clear selections
        # self.ui.loadcanvas.interactions['picker'].clearSelected()
        # Check button clicked
        if self.ui.nodosButtonDraw.isChecked():
            self.setNodedrawerMode()
        elif self.ui.barrasButtonDraw.isChecked():
            self.setBardrawerMode()
        elif self.ui.barrasButtonRel.isChecked():
            self.setReleasesMode()
        elif self.ui.nodosButtonVinculo.isChecked():
            self.setVinculosMode()
        elif self.ui.barrasButtonProps.isChecked():
            self.setSeccionMode()

    def setNodedrawerMode(self):
        self.ui.frameNodoDraw.setVisible(True)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeNodos()
        self.ui.actionDrawNodo.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Crear nodos'
        self.ui.estructuraDock.setWindowTitle(title)

    def setBardrawerMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(True)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionDrawBarra.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Crear barras'
        self.ui.estructuraDock.setWindowTitle(title)

    def setVinculosMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(True)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeNodos()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Condiciones de vínculo'
        self.ui.estructuraDock.setWindowTitle(title)

    def setReleasesMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(True)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Articulaciones de barras'
        self.ui.estructuraDock.setWindowTitle(title)

    def setSeccionMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(True)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Materiales y secciones'
        self.ui.estructuraDock.setWindowTitle(title)

    def setAccionesMode(self):
        # Clear selections
        # self.ui.loadcanvas.interactions['picker'].clearSelected()
        # Check button clicked
        if self.ui.nodosButtonCarga.isChecked():
            self.setCargasPMode()
        elif self.ui.barrasButtonCarga.isChecked():
            self.setCargasWMode()
        elif self.ui.barrasButtonTemp.isChecked():
            self.setTempMode()
        elif self.ui.nodosButtonCed.isChecked():
            self.setCedMode()

    def setCargasPMode(self):
        self.ui.frameNodoCarga.setVisible(True)
        self.ui.frameBarraCarga.setVisible(False)
        self.ui.frameBarraTemp.setVisible(False)
        self.ui.frameNodoCed.setVisible(False)
        # self.updateTreeNodos()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Cargas puntuales'
        self.ui.cargasDock.setWindowTitle(title)

    def setCargasWMode(self):
        self.ui.frameNodoCarga.setVisible(False)
        self.ui.frameBarraCarga.setVisible(True)
        self.ui.frameBarraTemp.setVisible(False)
        self.ui.frameNodoCed.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Cargas distribuidas'
        self.ui.cargasDock.setWindowTitle(title)

    def setTempMode(self):
        self.ui.frameNodoCarga.setVisible(False)
        self.ui.frameBarraCarga.setVisible(False)
        self.ui.frameBarraTemp.setVisible(True)
        self.ui.frameNodoCed.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Temperatura'
        self.ui.cargasDock.setWindowTitle(title)

    def setCedMode(self):
        self.ui.frameNodoCarga.setVisible(False)
        self.ui.frameBarraCarga.setVisible(False)
        self.ui.frameBarraTemp.setVisible(False)
        self.ui.frameNodoCed.setVisible(True)
        # self.updateTreeNodos()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()
        title = 'Acciones - Cedimientos de vínculo'
        self.ui.cargasDock.setWindowTitle(title)

    def setCanvasMode(self):
        # Clear selections
        # self.ui.loadcanvas.interactions['picker'].clearSelected()
        # self.ui.resultscanvas.interactions['picker'].clearSelected()
        # self.ui.p_a_pcanvas.interactions['picker'].clearSelected()
        # Check button clicked
        if self.ui.actionPan.isChecked():
            cursor = QtCore.Qt.ClosedHandCursor
            self.ui.loadcanvas.setMode("pan", QtGui.QCursor(cursor))
            if self.Estructura:
                self.ui.resultscanvas.setMode("pan", QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("pan", QtGui.QCursor(cursor))
        elif self.ui.actionPickAll.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode('pickall', QtGui.QCursor(cursor))
            if self.Estructura:
                self.ui.resultscanvas.setMode('pickall', QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("pickall", QtGui.QCursor(cursor))
        elif self.ui.actionPickNodos.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("picknodes", QtGui.QCursor(cursor))
            if self.Estructura:
                self.ui.resultscanvas.setMode("picknodes", QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("picknodes", QtGui.QCursor(cursor))
        elif self.ui.actionPickBarras.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("pickbars", QtGui.QCursor(cursor))
            if self.Estructura:
                self.ui.resultscanvas.setMode("pickbars", QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("pickbars", QtGui.QCursor(cursor))
        elif self.ui.actionZoom2Rect.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("zoom2rect", QtGui.QCursor(cursor))
            if self.Estructura:
                self.ui.resultscanvas.setMode("zoom2rect", QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("zoom2rect", QtGui.QCursor(cursor))
        elif self.ui.actionDrawNodo.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("nodedrawer", QtGui.QCursor(cursor))
            if self.Estructura:
                self.ui.resultscanvas.setMode("picknodes", QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("picknodes", QtGui.QCursor(cursor))
        elif self.ui.actionDrawBarra.isChecked():
            if self.ui.VigaRadioButton.isChecked():
                elemname = '2DFrame'
            if self.ui.ReticuladoRadioButton.isChecked():
                elemname = '2DTruss'
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("bardrawer", QtGui.QCursor(cursor),
                                                    elemname=elemname)
            if self.Estructura:
                self.ui.resultscanvas.setMode("pickbars", QtGui.QCursor(cursor))
                # self.ui.p_a_pcanvas.setMode("pickbars", QtGui.QCursor(cursor))

    def Tabbed_View(self):
        self.ui.mdiArea.setViewMode(1)

    def cascadeArrange(self):
        self.ui.mdiArea.setViewMode(0)
        self.ui.mdiArea.cascadeSubWindows()

    def tileArrange(self):
        self.ui.mdiArea.setViewMode(0)
        self.ui.mdiArea.tileSubWindows()

    def clearSelectedObjects(self):
        self.addSelectedObjects({},{})

    def getSelectedObjects(self, nodos_indices, barras_indices):
        selected_nodos = {}
        selected_barras = {}
        for index in nodos_indices:
            nodo = self.nodos[index]
            selected_nodos[index] = nodo
        for index in barras_indices:
            barra = self.barras[index]
            selected_barras[index] = barra
        self.selected_nodos = selected_nodos
        self.selected_barras = selected_barras

    def deleteSelectedObjects(self):
        # Des-seleccionar a todas las barras que ya van a eliminar los nodos
        for nodo in self.selected_nodos.values():
            barras2del = self.barrasennodo(nodo)
            for index in barras2del.keys():
                if index in self.selected_barras.keys():
                    del self.selected_barras[index]

        for nodo in self.selected_nodos.values():
            self.deleteNodo(nodo)
        for barra in self.selected_barras.values():
            self.deleteBarra(barra)


    def deleteAll(self):
        # Llamar al key press delete event del canvas
        event = mpl.backend_bases.KeyEvent('delete',
                                            self.ui.loadcanvas.fig.canvas,
                                           'delete', x=0, y=0)
        self.ui.loadcanvas.interactions['picker'].onKeyPress(event,
                                                            override=True)
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def selectAllNodos(self):
        if not self.Estructura:
            self.ui.loadcanvas.interactions['picker'].clearSelected()
        if self.Estructura:
            self.ui.resultscanvas.interactions['picker'].clearSelected()
            # self.ui.p_a_pcanvas.interactions['picker'].clearSelected()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()
        nodos_indices = [i for i in self.nodos.keys()]
        if not self.Estructura:
            selected_artists = [nodo.artists['self'][0] for nodo in \
                                self.nodos.values()]
            self.ui.loadcanvas.interactions['picker'].selected_objects = selected_artists
            self.ui.loadcanvas.interactions['picker'].list_picks = []
            self.ui.loadcanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects(nodos_indices, [])
        if self.Estructura:
            selected_artists = [nodo.artists['self_res'][0] for nodo in \
                                self.nodos.values()]
            self.ui.resultscanvas.interactions['picker'].selected_objects = selected_artists
            self.ui.resultscanvas.interactions['picker'].list_picks = []
            self.ui.resultscanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects2Results(nodos_indices, [])

    def selectAllBarras(self):
        if not self.Estructura:
            self.ui.loadcanvas.interactions['picker'].clearSelected()
        if self.Estructura:
            self.ui.resultscanvas.interactions['picker'].clearSelected()
            # self.ui.p_a_pcanvas.interactions['picker'].clearSelected()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()
        barras_indices = [i for i in self.barras.keys()]
        if not self.Estructura:
            selected_artists = [barra.artists['self'][0] for barra in \
                                self.barras.values()]
            self.ui.loadcanvas.interactions['picker'].selected_objects = selected_artists
            self.ui.loadcanvas.interactions['picker'].list_picks = []
            self.ui.loadcanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects([], barras_indices)
        if self.Estructura:
            selected_artists = [barra.artists['self_res'][0] for barra in \
                                self.barras.values()]
            self.ui.resultscanvas.interactions['picker'].selected_objects = selected_artists
            self.ui.resultscanvas.interactions['picker'].list_picks = []
            self.ui.resultscanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects2Results([], barras_indices)

    def addSelectedObjects2Edits(self):
        # nodos seleccionados
        nodos_text = ''
        for index in self.selected_nodos.keys():
            nodos_text += str(index)
            nodos_text += ', '
        # barras seleccionadas
        barras_text = ''
        for index in self.selected_barras.keys():
            barras_text += str(index)
            barras_text += ', '
        # Textos de nodos seleccionados
        self.ui.textEditSelectedNodos.setText(nodos_text)
        # Textos de barras seleccionadas
        self.ui.textEditSelectedBarras.setText(barras_text)

    def addSelectedObjects(self, nodos_indices, barras_indices):
        self.getSelectedObjects(nodos_indices, barras_indices)
        # Completar text edits y tablas
        self.addSelectedObjects2Edits()
        # Updatear arbol
        self.updateTreeSelected(expand=True)
        self.ui.selectedToolButton.setChecked(True)
        if not self.Estructura:
            # Habilitar edits de cedimientos de vinculo
            # Chequear restricciones de vínculo en cada nodo
            restrux = [0]
            restruy = [0]
            restrgiro = [0]
            if self.selected_nodos:
                restrux = [a.restr[0] for a in self.selected_nodos.values()]
                restruy = [a.restr[1] for a in self.selected_nodos.values()]
                restrgiro = [a.restr[2] for a in self.selected_nodos.values() if \
                                                        len(a.restr)==3]
            self.ui.textEditCedux.setEnabled(all(restrux))
            self.ui.textEditCeduy.setEnabled(all(restruy))
            self.ui.textEditCedgiro.setEnabled(all(restrgiro))
        if self.Estructura:
            # Completar tabla de desplazamientos nodales
            self.completeDisplacementTable()
            #Completar combobox de barras
            self.completeBarrasCombobox()

    def completeBarrasCombobox(self,):
        # Limpiar combo box
        combo_list = []
        self.ui.barrasComboBox.clear()
        # Completar combo box
        for index in self.selected_barras.keys():
            item = str(index)
            combo_list.append(item)
            self.ui.barrasComboBox.addItem(item)
        # Elegir primero de la lista
        self.ui.barrasComboBox.setCurrentIndex(0)
        self.plot_barra_selected()

    def plot_barra_selected(self):
        # Clear axes
        self.ui.barrascanvas.ax.clear()
        # Leer posición del slider
        value = self.ui.barpositionSlider.value()
        # Plot
        index = self.ui.barrasComboBox.currentText()
        if index:
            if self.paso_a_paso_mode:
                indice_txt = self.ui.comboBox_rig.currentText()
                if indice_txt != '':
                    if indice_txt == 'Total':
                        sistema = self.Estructura
                    else:
                        indice_sist = int(indice_txt)
                        sistema = self.sistemas_pap[indice_sist]
                    barra = sistema.barras[int(index)]
                else: return
            else:
                barra = self.barras[int(index)]
            solicitaciones = []
            if self.ui.momentoToolButton.isChecked():
                solicitaciones.append('M')
            if self.ui.corteToolButton.isChecked():
                solicitaciones.append('Q')
            if self.ui.normalToolButton.isChecked():
                solicitaciones.append('N')
            x_pos = ((value-1) * barra.long/100)
            for solicitacion in solicitaciones:
                barra.plot_diagramas(self.ui.barrascanvas.ax,
                                     solicitacion, x_pos,
                                     precision=self.precision,
                                     fontsize=self.fontsize,
                                     colors=['r','b'])
            # Actualizar line edit
            self.ui.textEditBarPosition.setText(to_precision(x_pos,3))
        self.ui.barrascanvas.ax.invert_yaxis()
        self.ui.barrascanvas.draw_idle()

    def show_max_barra_selected(self, extreme='max'):
        index = self.ui.barrasComboBox.currentText()
        if index:
            barra = self.barras[int(index)]
            solicitaciones = []
            if self.ui.momentoToolButton.isChecked():
                solicitaciones.append('M')
            if self.ui.corteToolButton.isChecked():
                solicitaciones.append('Q')
            if self.ui.normalToolButton.isChecked():
                solicitaciones.append('N')
            values = []
            positions = []
            for solicitacion in solicitaciones:
                pos, val = barra.find_maxmin_internal_forces(solicitacion,extreme)
                values.append(val)
                positions.append(pos)
            if values:
                if extreme == 'max': index = values.index(max(values))
                elif extreme == 'min': index = values.index(min(values))
                position = positions[index]
                value = round((position*100/barra.long)+1)
                self.ui.barpositionSlider.setSliderPosition(value)

    def completeDisplacementTable(self):
        """
        Completar tabla con desplazamientos nodales al seleccionar nodos

        """
        # Limpiar tabla
        self.ui.tableWidget.setRowCount(0)
        # Agregar a tabla
        for index, nodo in self.selected_nodos.items():
            if self.paso_a_paso_mode: # Metodo Paso a Paso
                indice_txt = self.ui.comboBox_rig.currentText()
                if not indice_txt == '':
                    if indice_txt == 'Total':
                        sistema = self.Estructura
                    else:
                        indice_sist = int(indice_txt)
                        sistema = self.sistemas_pap[indice_sist]
                    nodo = sistema.nodos[int(index)]
            # Create row
            numRows = self.ui.tableWidget.rowCount()
            self.ui.tableWidget.insertRow(numRows)
            # Add items to row
            headers = ["Nodo"]
            q = []
            self.ui.label_6.setText("Resultados nodales:")
            if self.ui.deformadaToolButton.isChecked():
                headers.extend(["dx", "dy", "giro"])
                q.extend(nodo.U[:,0])
                # if len(nodo.U)==2:
                if nodo.ndof == 2:
                    q.append(0)
            if self.ui.reaccionesToolButton.isChecked():
                headers.extend(["Rx", "Ry", "M"])
                q.extend(nodo.R[:,0])
                # if len(nodo.R)==2:
                if nodo.ndof == 2:
                    q.append(0)
            # Escribir headers
            self.ui.tableWidget.setColumnCount(len(headers))
            self.ui.tableWidget.setHorizontalHeaderLabels(headers)
            # Llenar columnas
            i = str(index)
            item = QtWidgets.QTableWidgetItem(i)
            item.setTextAlignment(Qt.AlignHCenter|Qt.AlignVCenter)
            self.ui.tableWidget.setItem(numRows, 0, item)
            for i in range(len(q)):
                d = to_precision(q[i], self.precision)
                item = QtWidgets.QTableWidgetItem(d)
                item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                self.ui.tableWidget.setItem(numRows, i+1, item)
        # Resize table to contents
        header = self.ui.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        for i in range(self.ui.tableWidget.columnCount()-1):
            header.setSectionResizeMode(i+1,
                QtWidgets.QHeaderView.ResizeToContents)

    def changeScale(self, value=None):
        # Obtener escala del slider
        if not value:
            value = self.ui.scaleSlider.value()
        self.scale_def = 10 ** (value / 100)
        self.ui.textEditDeformadaScale.setText(to_precision(self.scale_def,3))
        # Replotear deformada en canvas de resultados
        if not self.paso_a_paso_mode:
            self.Estructura.remove_barra_artists(['deformada'])
            self.Estructura.plot_deformada(self.ui.resultscanvas.ax,
                                self.scale_def, zorder=self.zorder['def'],
                                visible=self.ui.deformadaToolButton.isChecked())
        # Actualizar deformadas de paso a paso también
        if self.paso_a_paso_mode:
            sist = self.ui.comboBox_rig.currentText()
            if sist != '':
                if sist == 'Total':
                    sistema = self.Estructura
                else:
                    indice_sist = int(sist)
                    sistema = self.sistemas_pap[indice_sist]
                sistema.remove_barra_artists(['deformada'])
                sistema.plot_deformada(self.ui.resultscanvas.ax,
                                self.scale_def, zorder=self.zorder['def'],
                                visible=self.ui.deformadaToolButton.isChecked())
        self.ui.resultscanvas.fig.canvas.draw_idle()

    def changeScale_position(self, scale=None):
        if not scale:
            # Leer valor
            scale = float(self.ui.textEditDeformadaScale.text())
        # Obtener posicion en slider
        value = np.log10(scale)*100
        # Mover slider
        self.ui.scaleSlider.setValue(value)
        # Cambiar escala
        self.changeScale(value)

    def showFuerzas(self):
        state = self.ui.actionFuerzas.isChecked()
        # Nodos
        artist_list = ['p','c']
        for nodo in self.nodos.values():
            for list in artist_list:
                for artist in nodo.artists[list]:
                    artist.set_visible(state)
        # Barras
        artist_list = ['w', 'e']
        for barra in self.barras.values():
            for list in artist_list:
                for artist in barra.artists[list]:
                    artist.set_visible(state)
        self.ui.loadcanvas.draw_idle()

    def showEjesLocales(self):
        state = self.ui.actionEjes_Locales.isChecked()
        # Barras
        for barra in self.barras.values():
            for artist in barra.artists['ejes']:
                artist.set_visible(state)
        self.ui.loadcanvas.draw_idle()
        # En canvas de resultados
        if self.Estructura:
            # Barras
            for barra in self.barras.values():
                for text in barra.artists['ejes_res']:
                    text.set_visible(state)
            self.ui.resultscanvas.draw_idle()

    def showIndices(self):
        state = self.ui.actionIndices.isChecked()
        # Barras
        for barra in self.barras.values():
            for text in barra.artists['index']:
                text.set_visible(state)
        # Nodos
        for nodo in self.nodos.values():
            for text in nodo.artists['index']:
                text.set_visible(state)
        self.ui.loadcanvas.draw_idle()
        # En canvas de resultados
        if self.Estructura:
            # Barras
            for barra in self.barras.values():
                for text in barra.artists['index_res']:
                    text.set_visible(state)
            # Nodos
            for nodo in self.nodos.values():
                for text in nodo.artists['index_res']:
                    text.set_visible(state)
            self.ui.resultscanvas.draw_idle()

    def zoom2extent(self, margin=0.2, canvas=None):
        if canvas:
            canvas.zoom2extent(margin)
        else:
            if self.ui.cargaSW is self.ui.mdiArea.activeSubWindow():
                self.ui.loadcanvas.zoom2extent(margin)
            elif self.ui.resultadosSW is self.ui.mdiArea.activeSubWindow():
                self.ui.resultscanvas.zoom2extent(margin)

    def addNodo(self):
        node_center = [0,0]
        # Leer coordenadas de widgets
        node_center_x = self.ui.textEditCoordx.displayText()
        node_center_y = self.ui.textEditCoordy.displayText()
        try:
            if str(node_center_x) != '':
                node_center[0] = float(node_center_x)
            if str(node_center_y) != '':
                node_center[1] = float(node_center_y)
        except Exception as ex:
            self.error_dialog.error_catch(
                    'Error en ingreso de información', ex)
            return
        self.createNodo(tuple(node_center))
        # Zoom to extent
        self.zoom2extent(margin=0.2)

    def createNodo(self, node_center):
        # Crear objeto nodo de atenea
        k = len(self.nodos)
        nodo = NodoGUI(node_center, k + 1)
        self.nodos[k + 1] = nodo
        # Plotear nodo
        nodo.plot(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                  indice_visible=self.ui.actionIndices.isChecked(),
                  **self.nodo_props)
        # Update barras combobox
        self.ui.comboBoxNodo1.addItem(str(k + 1))
        self.ui.comboBoxNodo2.addItem(str(k + 1))
        # Update tree
        self.updateTreeNodos()
        # Draw
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def deleteNodo(self, nodo):
        # Encontrar cuales son las barras Atenea que se deben eliminar
        barras2del = self.barrasennodo(nodo)
        for barra in barras2del.values():
            self.deleteBarra(barra)
        # Limpiar artistas de nodo
        nodo.clean(['self', 'v', 'p', 'c', 'index'])
        index = nodo.indice
        # Eliminar objeto nodo
        del self.nodos[index]
        # Modificar
        for i in range(index+1, len(self.nodos)+2):
            self.nodos[i-1] = self.nodos.pop(i)
            self.nodos[i-1].indice = i-1
            self.nodos[i-1].artists['index'][0].set_text('['+str(i-1)+']')
        # Actualizar ComboBox
        self.ui.comboBoxNodo1.clear()
        self.ui.comboBoxNodo2.clear()
        for nodo in self.nodos.values():
            self.ui.comboBoxNodo1.addItem(str(nodo.indice))
            self.ui.comboBoxNodo2.addItem(str(nodo.indice))
        self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol
        self.updateTreeNodos()

    def barrasennodo(self, nodo):
        barras2del = {}
        for key, barra in self.barras.items():
            if nodo in barra.nodos:
                barras2del[key] = barra
        return barras2del

    def addBarra(self):
        # Leer nodo inicial y final de los widgets
        nodei_number = int(self.ui.comboBoxNodo1.currentText())
        nodei_center = self.nodos[nodei_number].coords
        nodef_number = int(self.ui.comboBoxNodo2.currentText())
        nodef_center = self.nodos[nodef_number].coords
        ElemName = None
        if nodei_number != nodef_number:
            # Coordenadas de los nodos y tipo de barra
            if self.ui.VigaRadioButton.isChecked():
                ElemName = '2DFrame'
            if self.ui.ReticuladoRadioButton.isChecked():
                ElemName = '2DTruss'
        self.createBarra((nodei_center, nodef_center, ElemName))
        # Draw
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def createBarra(self, barra):
        nodei_center = barra[0]
        nodef_center = barra[1]
        ElemName = barra[2]
        for nodo in self.nodos.values():
            if nodei_center == nodo.coords:
                nodoi = nodo
            if nodef_center == nodo.coords:
                nodof = nodo
        if nodei_center != nodef_center:
            # Crear objeto barra de atenea
            k = len(self.barras)
            barra = BarraGUI(k+1, [nodoi, nodof], ElemName)
            self.barras[k+1] = barra
            # Plotear barra
            barra.plot(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                       indice_visible=self.ui.actionIndices.isChecked(),
                       ejes_visible=self.ui.actionEjes_Locales.isChecked(),
                       barra_color=self.barra_props['color'],
                       ejes_color=self.barra_props['ejes_color'],
                       zorder=self.barra_props['zorder'], picker=True,
                       linewidth=self.barra_props['framewidth'])
            # Update tamaño de flechas
            self.length_flechas = graph.update_ax_arrows(self.ui.loadcanvas.ax,
                                                    self.nodos, self.barras,
                                                    self.length_flechas_rel,
                                                    0.25)
            # UPDATE TREE
            self.updateTreeBarras()

    def deleteBarra(self, barra):
        # Encontrar cuales son las barras Atenea que se deben eliminar
        index = barra.indice  # Lista con los indices de las barras a eliminar
        barra.clean(['self', 'w', 'e', 'ejes', 'index'])
        # Eliminar elemnames de los nodos extremos de la barra
        for nodo in barra.nodos:
            nodo.elemnames.remove(barra.ElemName)
        # Eliminar objeto barra
        del self.barras[index]
        # Modificar
        for i in range(index+1, len(self.barras)+2):
            self.barras[i-1] = self.barras.pop(i)
            self.barras[i-1].indice = i-1
            self.barras[i-1].artists['index'][0].set_text('['+str(i-1)+']')
        # Acutalizar arbol
        self.updateTreeBarras()
        # Draw
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def addVinculos(self):
        # Leer restricciones
        BOUN = [0, 0, 0]
        if self.ui.checkBoxRx.isChecked():
            BOUN[0] = 1
        if self.ui.checkBoxRy.isChecked():
            BOUN[1] = 1
        if self.ui.checkBoxM.isChecked():
            BOUN[2] = 1
        nodos = self.selected_nodos
        self.createVinculos(nodos, BOUN)

    def createVinculos(self, nodos, BOUN):
        # agregar restricciones a los nodos seleccionados
        # table = self.ui.tableVinculos
        for nodo in nodos.values():
            nodo.clean(['v'])
            nodo.restr = BOUN
            nodo.plot_vinculos(self.ui.loadcanvas.ax, visible=True,
                        length=self.length_flechas_rel['v']*self.length_flechas,
                        color=self.colors['v'])
        self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def addReleases(self):
        # Leer restricciones
        rel = [0, 0, 0]
        if self.ui.checkBoxIzq.isChecked():
            rel[1] = 1
        if self.ui.checkBoxDer.isChecked():
            rel[2] = 1
        self.createReleases(self.selected_barras, rel)

    def createReleases(self, barras, rel):
        for i, barra in barras.items():
            if barra.ElemName == '2DTruss':
                break
            barra.assign_rel(rel)
            # Release code
            if rel == [0, 0, 0]:
                rel_code = 0
            elif rel == [0, 1, 0]:
                rel_code = 1
            elif rel == [0, 0, 1]:
                rel_code = 2
            elif rel == [0, 1, 1]:
                rel_code = 3
            self.barras[i].artists['self'][0].add_releases(rel_code)
            self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def addMaterial(self):
        """
        Leer los inputs del material: módulo de elasticidad y tensión de
        fluencia.
        Se agregan a las opciones del combo box.
        """
        # comboMat_list = []
        ret, ok = self.mat_dialog.exec_()
        if ok:
            try:
                item = str(ret[0])
                self.materiales[ret[0]] = {'E': float(ret[1]),
                                           'l': float(ret[2])}
                self.ui.matlistWidget.addItem(item)
            except Exception as ex:
                self.Estructura = None
                self.error_dialog.error_catch(
                    'Chequear que esté bien cargada la información', ex)
                return
            self.updateTreePropiedades()

    def modifyMaterial(self):
        """

        :return:
        """
        # Leer material seleccionado
        if not self.ui.matlistWidget.currentItem():
            self.error_dialog.setText('No hay material seleccionado')
            self.error_dialog.exec_()
            return
        item = self.ui.matlistWidget.currentItem()
        itemMat = self.ui.matlistWidget.currentItem().text()
        old_mat = self.materiales[itemMat]
        # Abrir dialog de carga de material con input del material seleccionado
        ret, ok = self.mat_dialog.exec_(
                    values=[itemMat,str(old_mat['E']), str(old_mat['l'])])
        # Reescribir material seleccionado
        if ok:
            # Borrar material seleccionado
            del self.materiales[itemMat]
            self.materiales[ret[0]] = {'E': float(ret[1]), 'l': float(ret[2])}
            item.setText(str(ret[0]))
        #Update barras
        for barra in self.barras.values():
            if barra.mat:
                if barra.mat['name']==itemMat:
                    barra.mat['name'] = ret[0]
                    barra.mat['E'] = self.materiales[ret[0]]['E']
                    barra.mat['l'] = self.materiales[ret[0]]['l']
        self.updateTreePropiedades()

    def addSeccion(self):
        """
        Lee los inputs de la sección: área y momento de inercia.
        Se agregan a las opciones del combo box.
        """
        # comboSec_list = []
        ret, ok = self.seccion_dialog.exec_()
        if ok:
            try:
                item = str(ret[0])
                # comboSec_list.append(item)
                self.secciones[ret[0]] = {'A': float(ret[1]),
                                          'I': float(ret[2])}
                self.ui.seccionlistWidget.addItem(item)
            except Exception as ex:
                self.Estructura = None
                self.error_dialog.error_catch(
                    'Chequear que esté bien cargada la información', ex)
                return
            self.updateTreePropiedades()

    def modifySeccion(self):
        """

        :return:
        """
        # Leer sección seleccionada
        if not self.ui.seccionlistWidget.currentItem():
            self.error_dialog.setText('No hay sección seleccionada')
            self.error_dialog.exec_()
            return
        item = self.ui.seccionlistWidget.currentItem()
        itemSec = self.ui.seccionlistWidget.currentItem().text()
        old_sec = self.secciones[itemSec]
        # Abrir dialog de carga de material con input de la seccion seleccionada
        ret, ok = self.seccion_dialog.exec_(
            values=[itemSec, str(old_sec['A']), str(old_sec['I'])])
        # Reescribir seccion seleccionada
        if ok:
            # Borrar seccion seleccionada
            del self.secciones[itemSec]
            self.secciones[ret[0]] = {'A': float(ret[1]), 'I': float(ret[2])}
            item.setText(str(ret[0]))
            sec = self.secciones[ret[0]]
        #Update barras
        for barra in self.barras.values():
            if barra.seccion:
                if barra.seccion['name']==itemSec:
                    barra.seccion['name'] = ret[0]
                    barra.seccion['A'] = self.secciones[ret[0]]['A']
                    barra.seccion['I'] = self.secciones[ret[0]]['I']
        self.updateTreePropiedades()

    def addProperties(self):
        """
        Se le asignan las propiedades seleccionadas en los combobox a las
        barras seleccionadas.
        """
        barras = self.selected_barras
        data = {}
        if not self.ui.matlistWidget.currentItem():
            self.error_dialog.setText('No hay material seleccionado')
            self.error_dialog.exec_()
            return
        itemMat = self.ui.matlistWidget.currentItem().text()
        if not self.ui.seccionlistWidget.currentItem():
            self.error_dialog.setText('No hay sección seleccionada')
            self.error_dialog.exec_()
            return
        itemSec = self.ui.seccionlistWidget.currentItem().text()

        mat = self.materiales[itemMat]
        sec = self.secciones[itemSec]
        data['mat'] = {'name': itemMat,'E': mat['E'], 'l': mat['l']}
        data['seccion'] = {'name': itemSec,'A': sec['A'], 'I': sec['I']}
        for barra in barras.values():
            barra.assign_data(data)
        # Update del tree
        self.updateTreeSelected(expand=True)

    def addCargasP(self):
        # Mirar cuáles son los nodos seleccionados
        nodos = self.selected_nodos
        # Leer cargas
        P = [0, 0, 0]
        try:
            Px = self.ui.textEditCargasPx.displayText()
            if str(Px) != '':
                P[0] = float(Px)
            Py = self.ui.textEditCargasPy.displayText()
            if str(Py) != '':
                P[1] = float(Py)
            M = self.ui.textEditCargasM.displayText()
            if str(M) != '':
                P[2] = float(M)
        except Exception as ex:
            self.error_dialog.error_catch(
                    'Error en ingreso de información', ex)
            return
        self.createCargasP(nodos, P)

    def createCargasP(self, nodos, P):
        # agregar restricciones a los nodos seleccionados
        # table = self.ui.tableCargasP
        for nodo in nodos.values():
            nodo.clean(['p'])
            nodo.P = P
            nodo.plot_fuerzas(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                        visible=self.ui.actionFuerzas.isChecked(),
                        color=self.colors['p'],
                        length=self.length_flechas_rel['p']*self.length_flechas,
                        zorder=self.zorder['p'])
        self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def addCargasW(self):
        # Mirar cuáles son las barras seleccionados
        barras = self.selected_barras
        # Leer cargas
        w = {'wxi': 0, 'wyi': 0, 'wxf': 0, 'wyf': 0, 'terna': 'local'}
        try:
            wxi = self.ui.textEditWxi.displayText()
            if str(wxi) != '':
                w['wxi'] = float(wxi)
            wyi = self.ui.textEditWyi.displayText()
            if str(wyi) != '':
                w['wyi'] = float(wyi)
            wxf = self.ui.textEditWxf.displayText()
            if str(wxf) != '':
                w['wxf'] = float(wxf)
            wyf = self.ui.textEditWyf.displayText()
            if str(wyf) != '':
                w['wyf'] = float(wyf)
            # Leer terna
            if self.ui.radioButtonTernaGlobal.isChecked():
                w['terna'] = 'global'
            elif self.ui.radioButtonTernaLocal.isChecked():
                w['terna'] = 'local'
        except Exception as ex:
            self.error_dialog.error_catch(
                    'Error en ingreso de información', ex)
            return
        self.createCargasW(barras, w)

    def createCargasW(self, barras, w):
        for barra in barras.values():
            barra.clean(['w'])
            barra.assign_w(w)
            barra.plot_fuerzas(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                        length=self.length_flechas_rel['w']*self.length_flechas,
                        visible=self.ui.actionFuerzas.isChecked(),
                        color=self.colors['w'], zorder=self.zorder['w'])
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)
        # Draw
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def addCargase0(self):
        # Leer deformaciones
        e0 = [0, 0]
        try:
            # delta T baricentrico
            e00 = self.ui.textEdite01.displayText()
            if str(e00) != '':
                e0[0] = float(e00)
            # gradiente de temperatura
            e01 = self.ui.textEdite02.displayText()
            if str(e01) != '':
                e0[1] = float(e01)
        except Exception as ex:
            self.error_dialog.error_catch(
                    'Error en ingreso de información', ex)
            return
        barras = self.selected_barras
        self.createCargase0(barras, e0)

    def createCargase0(self, barras, e0):
        for barra in barras.values():
            barra.clean(['e'])
            barra.assign_e0(e0)
            barra.plot_temperatura(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                                   colors=self.colors['e'],
                                   visible=self.ui.actionFuerzas.isChecked(),
                                   zorder=self.zorder['e'])
        self.ui.loadcanvas.fig.canvas.draw_idle()

        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def addCedimientos(self):
        # Leer cedimientos
        ced = [0, 0, 0]
        try:
            if self.ui.textEditCedux.isEnabled():
                cedx = self.ui.textEditCedux.text()
                if str(cedx) != '':
                    ced[0] = float(cedx)
            if self.ui.textEditCeduy.isEnabled():
                cedy = self.ui.textEditCeduy.text()
                if str(cedy) != '':
                    ced[1] = float(cedy)
            if self.ui.textEditCedgiro.isEnabled():
                giro = self.ui.textEditCedgiro.text()
                if str(giro) != '':
                    ced[2] = float(giro)
        except Exception as ex:
            self.error_dialog.error_catch(
                    'Error en ingreso de información', ex)
            return
        nodos = self.selected_nodos
        self.createCedimientos(nodos, ced)

    def createCedimientos(self, nodos, ced):
        # agregar cedimientos de vínculo a los nodos seleccionados
        for nodo in nodos.values():
            nodo.clean(['c'])
            nodo.ced = ced
            nodo.plot_cedimientos(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                    color=self.colors['c'],
                    visible=self.ui.actionFuerzas.isChecked(),
                    length=self.length_flechas_rel['c']*self.length_flechas,
                    zorder=self.zorder['c'])
            self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def cleanAcciones(self):
        # Borrar cargas puntuales
        self.createCargasP(self.selected_nodos, [0,0,0])
        # Borrar cedimientos de vinculo
        self.createCedimientos(self.selected_nodos, [0,0,0])
        # Borrar cargas distribuidas
        self.createCargasW(self.selected_barras,
                {'wxi':0,'wyi':0,'wxf':0,'wyf':0,'terna':'local'})
        # Borrar deformaciones no-mecánicas
        self.createCargase0(self.selected_barras,[0,0])

    def writeNodoBranch(self, parent, nodo):
        parent.setText(1, str(nodo.coords))
        # crear coordenadas child
        self.addTreeBranch(parent, 'Coords.', ['x', 'y'], nodo.coords)
        # crear Vinculos child
        self.addTreeBranch(parent, 'Vínculos', ['Rx', 'Ry', 'M'], nodo.restr)
        # crear Cargas child
        self.addTreeBranch(parent, 'Cargas', ['Px', 'Py', 'M'], nodo.P)
        # crear Cedimientos child
        self.addTreeBranch(parent, 'Cedimientos', ['ux', 'uy', 'giro'], nodo.ced)

    def updateTreeNodos(self):
        self.ui.nodosToolButton.setChecked(True)
        self.ui.treeWidget.clear()
        for i, nodo in self.nodos.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Nodo {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                        resource_path('icons/arbolnodo_icon.png')))
            self.writeNodoBranch(parent, nodo)

    def writeBarraBranch(self, parent, barra):
        parent.setText(1, barra.ElemName)
        # crear nodos child
        self.addTreeBranch(parent, 'Nodos', ['Inicial', 'Final','Longitud'],
                           [barra.nodos[0].indice, barra.nodos[1].indice,
                            barra.long])
        # crear releases child
        if barra.ElemName == '2DFrame':
            self.addTreeBranch(parent, 'Releases', ['inicial', 'final'],
                               [barra.rel[1], barra.rel[2]])
        elif barra.ElemName == '2DTruss':
            self.addTreeBranch(parent, 'Releases', ['inicial', 'final'],
                               [1, 1])
        # crear Cargas child
        self.addTreeBranch(parent, 'Carga',
                           ['wxi', 'wyi', 'wxf', 'wyf', 'Terna'],
                           [v for v in barra.w.values()])
        # crear deformaciones child
        self.addTreeBranch(parent, 'Temperatura', ['baricentrica', 'gradiente'],
                           barra.e0)
        # crear material child
        self.addTreeBranch(parent, 'Material', [a for a in barra.mat.keys()],
                           [a for a in barra.mat.values()])
        # crear seccion child
        self.addTreeBranch(parent, 'Seccion', [a for a in barra.seccion.keys()],
                           [a for a in barra.seccion.values()])


    def updateTreeBarras(self):
        self.ui.barrasToolButton.setChecked(True)
        self.ui.treeWidget.clear()

        for i, barra in self.barras.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Barra {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                            resource_path('icons/arbolbarra_icon.png')))
            self.writeBarraBranch(parent, barra)

    def writePropsBranch(self, parent, name, props):
        keys = [v for v in props.keys() if v!='name']
        self.addTreeBranch(parent, name, keys,
                           [props[key] for key in keys])

    def updateTreePropiedades(self):
        self.ui.propiedadesToolButton.setChecked(True)
        self.ui.treeWidget.clear()
        self.Material_item = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
        self.Material_item.setIcon(0,
                    QtGui.QIcon(resource_path('icons\seccion_icon.png')))
        self.Material_item.setText(0, 'Materiales')
        for nombre, material in self.materiales.items():
            parent = self.Material_item
            self.writePropsBranch(parent, nombre, material)

        self.Seccion_item = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
        self.Seccion_item.setIcon(0,
                    QtGui.QIcon(resource_path('icons\seccion_icon.png')))
        self.Seccion_item.setText(0, 'Secciones')
        for nombre, seccion in self.secciones.items():
            parent = self.Seccion_item
            self.writePropsBranch(parent, nombre, seccion)

    def updateTreeSelected(self, expand=False):
        self.ui.selectedToolButton.setChecked(True)
        self.ui.treeWidget.clear()
        for i, nodo in self.selected_nodos.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Nodo {}'.format(i))
            parent.setIcon(0,
                    QtGui.QIcon(resource_path('icons/arbolnodo_icon.png')))
            self.writeNodoBranch(parent, nodo)
            self.expandAll(parent, expand)
        for i, barra in self.selected_barras.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Barra {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                        resource_path('icons/arbolbarra_icon.png')))
            self.writeBarraBranch(parent, barra)
            self.expandAll(parent, expand)

    def addTreeBranch(self, parent, child_title, text, data):
        child = QtWidgets.QTreeWidgetItem(parent)
        child.setText(0, child_title)
        for i in range(len(data)):
            item = QtWidgets.QTreeWidgetItem(child)
            item.setText(0, text[i])
            item.setText(1, str(data[i]))
            # item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)

    def expandAll(self, treeItem, expand):
        """
        Expande o colapsa todos los children de un tree widget item
        :return:
        """
        treeItem.setExpanded(expand)
        childCount = treeItem.childCount()
        for childNo in range(0, childCount):
            child = treeItem.child(childNo)
            if expand:  # if expanding, do that first (wonky animation otherwise)
                child.setExpanded(expand)
            subChildCount = child.childCount()
            if subChildCount > 0:
                self.expandAll(child, expand)
            if not expand:  # if collapsing, do it last (wonky animation otherwise)
                child.setExpanded(expand)

    def modifyTextsSize(self, step):
        self.fontsize = self.fontsize + step
        graph.modifyTextsSize(self.nodos, self.barras, self.fontsize)
        self.ui.loadcanvas.fig.canvas.draw_idle()
        self.ui.resultscanvas.fig.canvas.draw_idle()
        self.ui.barrascanvas.fig.canvas.draw_idle()

    def modifyPrecision(self, step):
        if self.Estructura:
            self.precision += step
            # Modificar diagramas
            self.completeDisplacementTable()
            # Modificar diagrama barra seleccionada
            if (self.ui.momentoToolButton.isChecked()) or \
               (self.ui.corteToolButton.isChecked()) or \
               (self.ui.normalToolButton.isChecked()) or \
               (self.ui.reaccionesToolButton.isChecked()):
                   self.plot_barra_selected()

    def DoubleValidation(self):
        onlyDouble = QtGui.QDoubleValidator()
        self.ui.textEditCoordx.setValidator(onlyDouble)
        self.ui.textEditCoordy.setValidator(onlyDouble)
        self.ui.textEditCedux.setValidator(onlyDouble)
        self.ui.textEditCeduy.setValidator(onlyDouble)
        self.ui.textEditCedgiro.setValidator(onlyDouble)
        self.ui.textEditCargasPx.setValidator(onlyDouble)
        self.ui.textEditCargasPy.setValidator(onlyDouble)
        self.ui.textEditCargasM.setValidator(onlyDouble)
        self.ui.textEditWxi.setValidator(onlyDouble)
        self.ui.textEditWxf.setValidator(onlyDouble)
        self.ui.textEditWyi.setValidator(onlyDouble)
        self.ui.textEditWyf.setValidator(onlyDouble)
        self.ui.textEdite01.setValidator(onlyDouble)
        self.ui.textEdite02.setValidator(onlyDouble)
        self.ui.textEditDeformadaScale.setValidator(onlyDouble)
        self.ui.textEditBarPosition.setValidator(onlyDouble)


if __name__ == "__main__":
    SCREEN_BORDER = 100
    app = 0
    app = QApplication(sys.argv)
    app.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps)
    QApplication.setStyle(QStyleFactory.create('Plastique'))
    w = AteneaForm()
    # Windows size
    rect = QApplication.desktop().availableGeometry()
    w.setGeometry(rect.x() + SCREEN_BORDER,
                  rect.y() + SCREEN_BORDER,
                  rect.width() - 2 * SCREEN_BORDER,
                  rect.height() - 2 * SCREEN_BORDER)
    w.setMinimumSize(900, 600)
    # Window title and icon
    w.setWindowTitle('Atenea2d')
    w.setWindowIcon(QtGui.QIcon(resource_path('icons/Atenea2d_icon.png')))
    # Abrir archivo
    if len(sys.argv)==2:
        w.openFileDialog(sys.argv[1])
    w.show()
    app.setWindowIcon(QtGui.QIcon(resource_path('icons/Atenea2d_icon.png')))
    sys.exit(app.exec_())
