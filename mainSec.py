# -*- coding: utf-8 -*-
import os, sys
from datetime import datetime

import numpy as np
import matplotlib as mpl

from AteneaSec.source.core.sectionlib import Figura,Seccion
from AteneaSec.source.core.leerescribir import save_txt, read_txt
from AteneaSec.source.core import graphsec
from AteneaSec.source.core.auxiliar import to_precision

from PyQt5.QtWidgets import QMainWindow, QApplication, QActionGroup
from PyQt5.QtWidgets import QFileDialog, QStyleFactory, QMessageBox
from PyQt5.QtCore import Qt

# os.system("C:\\Users\\Mariano\\AppData\\Local\\Programs\\Python\\Python37\\Scripts\\pyuic5 -o source\\gui\\mainUI.py source\\gui\\mainUI.ui")
# os.system("C:\\Users\\Mariano\\AppData\\Local\\Programs\\Python\\Python37\\Scripts\\pyuic5 -o source\\gui\\figurasUI.py source\\gui\\figurasUI.ui")

from AteneaSec.source.gui.mainUI import *

from AteneaSec.source.gui.dialogs import figuraDialog, about_dialog, saveDialog
from AteneaSec.source.gui.interactiveCanvas import seccionesCanvas, curvasCanvas
from AteneaSec.source.gui.interactiveCanvas import Tensiones3dCanvas
from AteneaSec.source.gui.statusBar import createStatusBar

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

class AteneaForm(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.splitter_3.setSizes([1000,3500])

        ## Custom iconos
        self.lock_icon = QtGui.QIcon()
        self.lock_icon.addPixmap(QtGui.QPixmap(
                            resource_path('icons/lock_icon.png')),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.unlock_icon = QtGui.QIcon()
        self.unlock_icon.addPixmap(QtGui.QPixmap(
                            resource_path("icons/unlock_icon.png")),
                            QtGui.QIcon.Normal, QtGui.QIcon.Off)

        # Variables globales
        self.figuras = {}
        self.seccion = None
        self.saved = False
        self.facecol = (0.6,0.6,0.6,0.6)
        self.precision = 4
        self.fontsize = 6
        self.tensiones = None
        self.tensionpoint = None

        # Create save dialog
        self.savedialog = saveDialog()

        # Crear status bar
        createStatusBar(self)

        # Validate numerical values in lineedits
        self.DoubleValidation()

        # Deshabilitar tabs
        self.ui.tabWidget.setTabEnabled(1,False)
        self.ui.tabWidget.setTabEnabled(2,False)

        # Crear canvas para ploteo de figuras
        self.ui.figurasCanvas = seccionesCanvas(
                                    parent=self.ui.frameSeccionCanvas,
                                    layout=self.ui.horizontalLayout_2,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    coordsLabel = self.coordsLabel,
                                    originArrows=True)
        # Crear canvas para ploteo de seccion
        self.ui.seccionCanvas = seccionesCanvas(
                                    parent=self.ui.frameSeccionCanvas,
                                    layout=self.ui.horizontalLayout_2,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    coordsLabel = self.coordsLabel,
                                    originArrows=True)
        # Crear canvas para ploteo de tensiones
        self.ui.tensionesCanvas = seccionesCanvas(
                                    parent=self.ui.frameSeccionCanvas,
                                    layout=self.ui.horizontalLayout_2,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    coordsLabel = self.coordsLabel)
        # Crear canvas para ploteo de tensiones en 3D
        self.ui.tensiones3dCanvas = Tensiones3dCanvas(
                                    parent=self.ui.frameSeccionCanvas,
                                    layout=self.ui.horizontalLayout_2,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    coordsLabel = self.coordsLabel,
                                    zoom_base_scale=0.5)
        # Crear canavs para ploteo de graficos de J
        self.ui.curvasCanvas = curvasCanvas(
                                    parent=self.ui.framePlotsCanvas,
                                    layout=self.ui.horizontalLayout_3,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding)

        # Action Open
        self.ui.actionOpen.triggered.connect(self.cargarArchivo)
        # Action Save
        self.ui.actionSave.triggered.connect(self.guardarArchivo)
        # Archivo nuevo
        self.ui.actionNew.triggered.connect(self.archivoNuevo)
        # Action buttons de ayuda
        self.ui.actionAbout.triggered.connect(about_dialog)
        # Desbloquear carga de estructura
        self.ui.actionUnlock.triggered.connect(self.unlockFiguras)

        # Grupo de botones de figuras
        self.ui.tabWidget.currentChanged.connect(self.prenderEjes)
        self.prenderEjes()
        # Mostrar o ocultar graficos de curvas
        self.ui.curvasToolButton.clicked.connect(self.prenderCurvasCanvas)
        self.prenderCurvasCanvas()

        # Boton de crear figura
        self.ui.cargarFiguraButton.clicked.connect(self.crearFigura)
        # Boton de eliminar figura
        self.ui.eliminarFiguraButton.clicked.connect(self.eliminarFigura)
        # Seleccionar figura callback
        self.ui.treeWidget.itemPressed.connect(self.colorFigura)
        # Boton de crear seccion
        self.ui.crearSeccionButton.clicked.connect(self.crearSeccion)
        # Calcular ejes uv de secicon
        self.ui.alphaLineEdit.editingFinished.connect(self.calcularUV)
        # Boton de calcular tensiones
        self.ui.cargarTensionesButton.clicked.connect(self.crearTensiones)
        # Boton de calcular tension en un punto
        self.ui.tensionyzButton.clicked.connect(self.calcularTensionyz)
        self.ui.tensionyzButton.setEnabled(False)

        # Mostrar o ocultar ejes principales
        self.ui.princToolButton.clicked.connect(self.prenderEjesPrinc)

        # Mostrar o ocultar ejes uv
        self.ui.uvToolButton.clicked.connect(self.prenderEjesuv)

        # Agrupar botones de axes de tensiones
        self.tensiones_grupo = QtWidgets.QButtonGroup(self)
        self.tensiones_grupo.addButton(self.ui.contourToolButton)
        self.tensiones_grupo.addButton(self.ui.tension3dToolButton)
        self.ui.contourToolButton.setChecked(True)
        self.ui.contourToolButton.clicked.connect(self.prenderEjes)
        self.ui.tension3dToolButton.clicked.connect(self.prenderEjes)

        # Botones de apagar tensiones
        self.ui.vectorMToolButton.clicked.connect(self.prenderTensiones)
        self.ui.sigmaparToolButton.clicked.connect(self.prenderTensiones)
        self.ui.sigmatotToolButton.clicked.connect(self.prenderTensiones)
        self.ui.sigmaprincToolButton.clicked.connect(self.prenderTensiones)
        self.ui.sigmalfToolButton.clicked.connect(self.prenderTensiones)
        self.ui.sigmaenToolButton.clicked.connect(self.prenderTensiones)
        self.ui.sigmacpToolButton.clicked.connect(self.prenderTensiones)

    def lockFiguras(self):
        # Marcar como activo el action de lock
        self.ui.actionUnlock.setEnabled(True)
        self.ui.actionUnlock.setIcon(self.lock_icon)
        # Deshabilitar botones de guardado, abrir, nuevo y correr
        self.ui.actionSave.setEnabled(False)
        self.ui.actionOpen.setEnabled(False)
        self.ui.actionNew.setEnabled(False)
        # Deshabilitar solapa de figuras
        self.ui.tabWidget.setTabEnabled(0,False)

    def unlockFiguras(self):
        # File Dialog pop-up
        warning = QtWidgets.QMessageBox.warning(self, 'Desbloquear carga de figuras',
                    'Al debloquear el modo de carga se perderán los resultados',
                    QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if warning == QtWidgets.QMessageBox.Ok:
            # Marcar como activo el action de lock
            self.ui.actionUnlock.setEnabled(False)
            self.ui.actionUnlock.setIcon(self.unlock_icon)
            # Habilitar botones de guardado, abrir, nuevo y correr
            self.ui.actionSave.setEnabled(True)
            self.ui.actionOpen.setEnabled(True)
            self.ui.actionNew.setEnabled(True)
            # Resetear resultados
            self.resetearResultados()
            self.ui.tabWidget.setTabEnabled(0,True)
            self.ui.framePlotsCanvas.setVisible(False)

    def prenderEjes(self):
        i = self.ui.tabWidget.currentIndex()
        if i == 0:
            self.ui.figurasCanvas.setVisible(True)
            self.ui.seccionCanvas.setVisible(False)
            self.ui.tensionesCanvas.setVisible(False)
            self.ui.tensiones3dCanvas.setVisible(False)
            self.ui.framePlotsCanvas.setVisible(False)
        elif i == 1:
            self.ui.figurasCanvas.setVisible(False)
            self.ui.seccionCanvas.setVisible(True)
            self.ui.tensionesCanvas.setVisible(False)
            self.ui.tensiones3dCanvas.setVisible(False)
            self.prenderCurvasCanvas()
        elif i == 2:
            self.ui.figurasCanvas.setVisible(False)
            self.ui.seccionCanvas.setVisible(False)
            self.ui.framePlotsCanvas.setVisible(False)
            if self.ui.contourToolButton.isChecked():
                self.ui.tensionesCanvas.setVisible(True)
                self.ui.tensiones3dCanvas.setVisible(False)
            elif self.ui.tension3dToolButton.isChecked():
                self.ui.tensionesCanvas.setVisible(False)
                self.ui.tensiones3dCanvas.setVisible(True)
            # Prender ejes
            if self.seccion:
                if self.seccion.sigma_artists_dict:
                    self.prenderTensiones()

    def prenderTensiones(self):
        # Apagar todos
        keys = ['mom','par','tot','princ','lf','en','cp']
        for key in keys:
            group = self.seccion.sigma_artists_dict[key]
            for artist in group:
                artist.set_visible(False)
        # Prender seleccionados
        if self.ui.vectorMToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['mom']:
                artist.set_visible(True)
        if self.ui.sigmaparToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['par']:
                artist.set_visible(True)
        if self.ui.sigmatotToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['tot']:
                artist.set_visible(True)
        if self.ui.sigmaprincToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['princ']:
                artist.set_visible(True)
        if self.ui.sigmalfToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['lf']:
                artist.set_visible(True)
        if self.ui.sigmaenToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['en']:
                artist.set_visible(True)
        if self.ui.sigmacpToolButton.isChecked():
            for artist in self.seccion.sigma_artists_dict['cp']:
                artist.set_visible(True)

        self.ui.tensionesCanvas.fig.canvas.draw_idle()

    def prenderCurvasCanvas(self):
        if self.ui.curvasToolButton.isChecked():
            self.ui.framePlotsCanvas.setVisible(True)
        else:
            self.ui.framePlotsCanvas.setVisible(False)

    def prenderEjesPrinc(self):
        if self.ui.princToolButton.isChecked():
            self.ui.frameEjesPrinc.setVisible(True)
            for artist in self.seccion.princ_artists:
                artist.set_visible(True)
            for artist in self.seccion.mrktheta:
                artist.set_visible(True)
        else:
            self.ui.frameEjesPrinc.setVisible(False)
            for artist in self.seccion.princ_artists:
                artist.set_visible(False)
            for artist in self.seccion.mrktheta:
                artist.set_visible(False)
        self.ui.seccionCanvas.fig.canvas.draw_idle()
        self.ui.curvasCanvas.fig.canvas.draw_idle()

    def prenderEjesuv(self):
        if self.ui.uvToolButton.isChecked():
            self.ui.frameEjesuv.setVisible(True)
            for artist in self.seccion.uv_artists:
                artist.set_visible(True)
            for artist in self.seccion.mrkbeta:
                artist.set_visible(True)
        else:
            self.ui.frameEjesuv.setVisible(False)
            for artist in self.seccion.uv_artists:
                artist.set_visible(False)
            for artist in self.seccion.mrkbeta:
                artist.set_visible(False)
        self.ui.seccionCanvas.fig.canvas.draw_idle()
        self.ui.curvasCanvas.fig.canvas.draw_idle()

    def crearFigura(self):
        self.figuraui = figuraDialog()
        ok, tipo, geometria = self.figuraui.exec_()
        if ok:
            # índice
            i = len(self.figuras) + 1
            self.figuras[i] = Figura(tipo, geometria)
            self.plotearFigura(i)
            self.figuras[i].plot
            # Actualizar arbol
            self.updateTree()
            # Habilitar boton de crear seccion
            if self.figuras:
                self.ui.crearSeccionButton.setEnabled(True)
            # Deshabilitar tabs de resultados
            self.ui.tabWidget.setTabEnabled(1,False)
            self.ui.tabWidget.setTabEnabled(2,False)
            # Resetear resultados
            # self.resetearResultados()

    def eliminarFigura(self):
        if self.figuras: # Si hay figuras
            index = self.ui.treeWidget.currentIndex().row()
            if index >= 0: # Si hay alguna seleccionada
                figura = self.figuras[index+1]
                # Actualizar indices de figuras
                del self.figuras[index+1]
                for i in range(index+2,len(self.figuras)+2):
                    self.figuras[i-1] = self.figuras.pop(i)
                # remover figura
                for artist in figura.artists:
                    artist.remove()
                figura.gartist.remove()
                self.ui.figurasCanvas.fig.canvas.draw()
                # Actualizar arbol
                self.updateTree()
                # Habilitar boton de crear seccion
                if not self.figuras:
                    self.ui.crearSeccionButton.setEnabled(False)
                # Deshabilitar tabs de resultados
                self.ui.tabWidget.setTabEnabled(1,False)
                self.ui.tabWidget.setTabEnabled(2,False)
                # Resetear resultados
                # self.resetearResultados()

    def plotearFigura(self,i):
        self.figuras[i].plot(ax=self.ui.figurasCanvas.ax, facecol=self.facecol)
        self.ui.figurasCanvas.fig.canvas.draw()
        self.ui.figurasCanvas.zoom2extent(margin=0.1)

    def crearSeccion(self):
        # Crear objeto
        self.seccion = Seccion(self.figuras)
        self.plotearSeccion()
        self.ui.seccionCanvas.zoom2extent(margin=0.1)
        # Completar tablas de resultados
        self.completarResultados()
        # Habilitar tabs de resultados
        self.ui.tabWidget.setTabEnabled(1,True)
        self.ui.tabWidget.setTabEnabled(2,True)
        # Deshabilitar
        self.lockFiguras()

    def completarResultados(self):
        # Resultados de seccion
        self.ui.xbarLabel.setText('x='+to_precision(
                            self.seccion.coords['g'][0],self.precision))
        self.ui.ybarLabel.setText('y='+to_precision(
                            self.seccion.coords['g'][1],self.precision))
        self.ui.Alabel.setText(to_precision(
                            self.seccion.props['a'],self.precision))
        self.ui.Jylabel.setText(to_precision(
                            self.seccion.props['jy'],self.precision))
        self.ui.Jzlabel.setText(to_precision(
                            self.seccion.props['jz'],self.precision))
        self.ui.Jyzlabel.setText(to_precision(
                            self.seccion.props['jyz'],self.precision))
        jp = self.seccion.props['jy'] + self.seccion.props['jz']
        self.ui.Jplabel.setText(to_precision(jp,self.precision))
        # Resultados de ejes principales
        self.ui.J1label.setText(to_precision(
                    self.seccion.princ['j1'],self.precision))
        self.ui.J2label.setText(to_precision(
                    self.seccion.princ['j2'],self.precision))
        self.ui.alpha12label.setText(to_precision(
                    self.seccion.princ['theta'],self.precision))
        # Resultados de ejes uv
        self.ui.Julabel.setText(to_precision(
                    self.seccion.uv['ju'],self.precision))
        self.ui.Jvlabel.setText(to_precision(
                    self.seccion.uv['jv'],self.precision))
        self.ui.Juvlabel.setText(to_precision(
                    self.seccion.uv['juv'],self.precision))
        self.ui.alphaLineEdit.setText(to_precision(
                    self.seccion.uv['beta'],self.precision))

    def plotearSeccion(self):
        # Plotear
        self.seccion.plot_seccion(ax=self.ui.seccionCanvas.ax, princ=True,
                                uv=True, length_scale=0.1, fontsize=self.fontsize,
                                colors=['k','b','g','r'], textoffset=0.2)
        # Plotear ejes de curvas
        self.seccion.plot_juv(ax=self.ui.curvasCanvas.ax, juv=True, j12=True,
                              colors=['b','g'])
        self.prenderEjes()
        self.prenderEjesPrinc()
        self.prenderEjesuv()

    def calcularUV(self):
        # Leer line edit
        alpha = float(self.ui.alphaLineEdit.displayText())
        # Calcular ejes
        self.seccion.RotAxis(alpha)
        # Resultados de ejes uv
        self.ui.Julabel.setText(to_precision(
                    self.seccion.uv['ju'],self.precision))
        self.ui.Jvlabel.setText(to_precision(
                    self.seccion.uv['jv'],self.precision))
        self.ui.Juvlabel.setText(to_precision(
                    self.seccion.uv['juv'],self.precision))
        self.ui.alphaLineEdit.setText(to_precision(
                    self.seccion.uv['beta'],self.precision))
        # Plotear ejes
        self.seccion.remove_artists(self.seccion.uv_artists)
        self.seccion.plot_uv(ax=self.ui.seccionCanvas.ax, length_scale=0.1,
                             fontsize=self.fontsize, color='g', textoffset=0.2)
        self.ui.seccionCanvas.fig.canvas.draw_idle()
        for ax in self.ui.curvasCanvas.ax:
            ax.clear()
        self.seccion.plot_juv(ax=self.ui.curvasCanvas.ax, juv=True,
                              j12=True, colors=['b','g'])
        self.ui.curvasCanvas.fig.canvas.draw_idle()

    def crearTensiones(self):
        # Clear canvas
        self.ui.tensionesCanvas.ax.clear()
        if self.seccion.cbar:
            self.seccion.cbar.remove()
        self.ui.tensiones3dCanvas.ax.clear()
        # Leer solicitaciones
        N = float(self.ui.NEdit.displayText())
        My = float(self.ui.MyEdit.displayText())
        Mz = float(self.ui.MzEdit.displayText())
        # solicitaciones = {'N':N, 'My':My, 'Mz':Mz}
        self.seccion.computeSigma(My=My, Mz=Mz, N=N)
        self.seccion.plot_sigmas(ax=self.ui.tensionesCanvas.ax, princ=True,
                                 mom=True, en=True, lf=True, cp=True,
                                 par=True, tot=True, rscale=0.1, length_scale=0.2,
                                 fontsize=self.fontsize, size=20,
                                 colors=['k','b','g'], textoffset=0.2)
        self.ui.tensionesCanvas.fig.canvas.draw()
        # grafico 3d
        self.seccion.plot_sigma3d(ax=self.ui.tensiones3dCanvas.ax,
                                  length_scale=0.1, fontsize=self.fontsize,
                                  colors=['k','b','k'])
        self.ui.tensionesCanvas.fig.canvas.draw()
        # Completar labels
        self.ui.m1Label.setText('M1='+to_precision(
                    self.seccion.Mprinc['M1'],self.precision))
        self.ui.m2Label.setText('M2='+to_precision(
                    self.seccion.Mprinc['M2'],self.precision))

        if self.seccion.aux2['CPyz'] is not np.nan:
            self.ui.cpycoordsLabel.setText('y='+to_precision(
                    self.seccion.aux2['CPyz'][0],self.precision))
            self.ui.cpzcoordsLabel.setText('z='+to_precision(
                    self.seccion.aux2['CPyz'][1],self.precision))
        else:
            self.ui.cpycoordsLabel.setText('-')
            self.ui.cpzcoordsLabel.setText('-')

        if not np.isnan(self.seccion.aux2['EN']):
            self.ui.aenLabel.setText('alpha='+to_precision(
                            self.seccion.aux2['EN'],self.precision))
        else: self.ui.aenLabel.setText('alpha=-')

        if not np.isnan(self.seccion.aux2['Dist']):
            self.ui.aedLabel.setText('d='+to_precision(
                            self.seccion.aux2['Dist'],self.precision))
        else: self.ui.aedLabel.setText('d=infty')

        if not np.isnan(self.seccion.aux2['LF']):
            self.ui.alfLabel.setText('alpha='+to_precision(
                            self.seccion.aux2['LF'],self.precision))
        else: self.ui.alfLabel.setText('alpha=-')
        # Resetear labels de tension yz
        self.ui.tensionM1Label.setText('-')
        self.ui.tensionM2Label.setText('-')
        self.ui.tensionNLabel.setText('-')
        self.ui.tensionLabel.setText('-')
        # Prender boton de tension en un punto
        self.ui.tensionyzButton.setEnabled(True)
        self.tensiones = True

    def calcularTensionyz(self):
        if self.tensionpoint:
            self.tensionpoint.remove()
        # Leer lineedits
        if self.ui.ytensionEdit.displayText() and \
           self.ui.ytensionEdit.displayText():
            y = float(self.ui.ytensionEdit.displayText())
            z = float(self.ui.ztensionEdit.displayText())
        else: return
        # Calcular tension
        tensiones = self.seccion.computeXYSigma(np.array([y,z]))
        # Completar tensiones
        self.ui.tensionM1Label.setText(to_precision(tensiones['sigmaM1'],
                                                    self.precision))
        self.ui.tensionM2Label.setText(to_precision(tensiones['sigmaM2'],
                                                    self.precision))
        self.ui.tensionNLabel.setText(to_precision(tensiones['sigmaN'],
                                                    self.precision))
        self.ui.tensionLabel.setText(to_precision(tensiones['sigma'],
                                                    self.precision))
        # Plotear punto en canvas
        ax = self.ui.tensionesCanvas.ax
        yplot = self.seccion.coords['g'][0] - y
        zplot = self.seccion.coords['g'][1] - z
        self.tensionpoint = ax.plot(yplot,zplot,'rx')[0]
        self.ui.tensionesCanvas.fig.canvas.draw_idle()

    def cargarArchivo(self, filename=None):
        # Cargar
        if filename:
            self.filename = filename
        if not filename:
            filename = QFileDialog.getOpenFileName(self, 'Open file',
                                                   filter='Atenea(*.asec)')
            filename = filename[0]
            # Chequear si no canceló
            if not filename:
                return
        # Limpiar todo
        self.clear()
        # Guardar nombre
        self.filename = filename
        self.setWindowTitle('AteneaSec (beta) - ' + \
                            os.path.basename(self.filename) +\
                            ' [' + str(datetime.now().strftime('%H:%M')) + ']')
        if os.path.exists(self.filename):
            aux, self.figuras = read_txt(self.filename)
            if self.figuras:
                self.ui.crearSeccionButton.setEnabled(True)
            for key in self.figuras.keys():
                self.plotearFigura(key)
            # Actualizar arbol
            self.updateTree()

    def guardarArchivo(self):
        # Obtener nombre y directorio de archivo nuevo
        if not self.saved:
            filename = QFileDialog.getSaveFileName(self, 'Save file',
                                        filter='Atenea(*.asec)')
            if not filename[0]: return
            self.filename = filename[0]
            self.setWindowTitle('AteneaSec (beta) - ' + \
                                os.path.basename(self.filename[0]) +\
                                ' [' + str(datetime.now().strftime('%H:%M')) + ']')
            self.saved = True
        if self.filename:
            # Guardarlo en el archivo
            save_txt(self.filename, self.figuras)

    def archivoNuevo(self):
        if self.figuras:
            ok = self.savedialog.exec_()
            if ok==1:
                self.guardarArchivo()
            elif ok==2:
                return
            elif ok==0:
                pass
        self.clear()

    def clear(self):
        # Clear axes
        self.ui.figurasCanvas.ax.clear()
        self.ui.figurasCanvas.originArrows()
        self.ui.figurasCanvas.ax.axis([-1,5,-1,4])
        self.ui.seccionCanvas.ax.clear()
        self.ui.seccionCanvas.originArrows()
        self.ui.tensionesCanvas.ax.clear()
        if self.seccion:
            if self.seccion.cbar:
                self.seccion.cbar.remove()
        self.ui.tensiones3dCanvas.ax.clear()
        for ax in self.ui.curvasCanvas.ax:
            ax.clear()
        self.ui.figurasCanvas.fig.canvas.draw()
        self.ui.seccionCanvas.fig.canvas.draw()
        self.ui.tensionesCanvas.fig.canvas.draw()
        self.ui.curvasCanvas.fig.canvas.draw()
        # Resetear variables
        self.figuras = {}
        self.seccion = None
        self.saved = False
        self.filename = None
        self.setWindowTitle('AteneaSec (beta)')
        self.precision = 4
        self.fontsize = 6
        self.tensiones = None
        self.tensionpoint = None
        # Deshabilitar boton de crear seccion
        self.ui.crearSeccionButton.setEnabled(False)
        # Resetear treewidget
        self.ui.treeWidget.clear()
        # Deshabilitar tabs de resultados
        self.ui.tabWidget.setTabEnabled(1,False)
        self.ui.tabWidget.setTabEnabled(2,False)
        # Deshabilitar boton de tension en un punto
        self.ui.tensionyzButton.setEnabled(False)
        # Vaciar lineedits
        self.ui.alphaLineEdit.setText('0')
        self.ui.MyEdit.setText('')
        self.ui.MzEdit.setText('')
        self.ui.NEdit.setText('')
        self.ui.ytensionEdit.setText('')
        self.ui.ztensionEdit.setText('')
        # Apagar curvas Canvas
        self.ui.curvasToolButton.setChecked(False)
        self.ui.uvToolButton.setChecked(False)
        self.ui.princToolButton.setChecked(False)
        # self.prenderCurvasCanvas()

    def resetearResultados(self):
        # Clear axes
        self.ui.seccionCanvas.ax.clear()
        self.ui.seccionCanvas.originArrows()
        self.ui.tensionesCanvas.ax.clear()
        if self.seccion:
            if self.seccion.cbar:
                self.seccion.cbar.remove()
        self.ui.tensiones3dCanvas.ax.clear()
        for ax in self.ui.curvasCanvas.ax:
            ax.clear()
        self.ui.seccionCanvas.fig.canvas.draw()
        self.ui.tensionesCanvas.fig.canvas.draw()
        self.ui.curvasCanvas.fig.canvas.draw()
        # Resetear seccion
        self.seccion = None
        self.tensiones = None
        self.tensionpoint = None
        # Deshabilitar tabs de resultados
        self.ui.tabWidget.setTabEnabled(1,False)
        self.ui.tabWidget.setTabEnabled(2,False)
        # Deshabilitar boton de tension en un punto
        self.ui.tensionyzButton.setEnabled(False)
        # Vaciar lineedits
        self.ui.alphaLineEdit.setText('0')
        self.ui.MyEdit.setText('')
        self.ui.MzEdit.setText('')
        self.ui.NEdit.setText('')
        self.ui.ytensionEdit.setText('')
        self.ui.ztensionEdit.setText('')
        # Apagar curvas Canvas
        self.ui.curvasToolButton.setChecked(False)
        self.ui.uvToolButton.setChecked(False)
        self.ui.princToolButton.setChecked(False)

    def updateTree(self):
        self.ui.treeWidget.clear()
        count = 0
        for index, figura in self.figuras.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Figura {}'.format(index))
            if figura.tipo['Tipo'] == 'Rect':
                icon = QtGui.QIcon(resource_path('icons\\fig_rect_icon.png'))
                self.writeFiguraBranch(parent, figura)
            if figura.tipo['Tipo'] == 'Tri':
                icon = QtGui.QIcon(resource_path('icons\\fig_tri_icon.png'))
                self.writeFiguraBranch(parent, figura)
            if figura.tipo['Tipo'] == 'Circ':
                icon = QtGui.QIcon(resource_path('icons\\fig_circ_icon.png'))
                self.writeFiguraBranch(parent, figura)
            if figura.tipo['Tipo'] == 'Poly':
                icon = QtGui.QIcon(resource_path('icons\\fig_poly_icon.png'))
                self.writeFiguraBranch(parent, figura)
            parent.setIcon(0, icon)

    def writeFiguraBranch(self, parent, figura):
        for key, val in figura.tipo.items():
            item = QtWidgets.QTreeWidgetItem(parent)
            item.setText(0, key)
            item.setText(1, str(val))
        for key, val in figura.coords.items():
            item = QtWidgets.QTreeWidgetItem(parent)
            item.setText(0, key)
            item.setText(1, str(val))
        for key, val in figura.props.items():
            item = QtWidgets.QTreeWidgetItem(parent)
            item.setText(0, key)
            item.setText(1, to_precision(val, self.precision))

    def colorFigura(self):
        # Setear todas las figuras en color original
        for figura in self.figuras.values():
            for artist in figura.artists:
                if figura.tipo['Prop']=='Lleno':
                    artist.set_facecolor(self.facecol)
                elif figura.tipo['Prop']=='Hueco':
                    artist.set_facecolor((1,1,1,1))
        # Colorear figura seleccionada
        item = self.ui.treeWidget.currentItem()
        # Si selecciono un child
        if item.parent():
            parent = item.parent()
            parent.setSelected(True)
            self.ui.treeWidget.setCurrentItem(parent)
            index = self.ui.treeWidget.currentIndex().row()
            self.ui.treeWidget.setCurrentItem(item)
            figura = self.figuras[index+1]
            for artist in figura.artists:
                artist.set_facecolor((0,0,1,0.6))
        # Si selecciono el parent
        if not item.parent():
            index = self.ui.treeWidget.currentIndex().row()
            figura = self.figuras[index+1]
            for artist in figura.artists:
                artist.set_facecolor((0,0,1,0.6))
        self.ui.figurasCanvas.fig.canvas.draw()

    def modifyPrecision(self, d):
        self.precision += d
        if self.seccion:
            # Modificar diagramas
            self.completarResultados()
            if self.tensiones:
                # Completar labels de tensiones
                self.ui.m1Label.setText('M1='+to_precision(
                            self.seccion.Mprinc['M1'],self.precision))
                self.ui.m2Label.setText('M2='+to_precision(
                            self.seccion.Mprinc['M2'],self.precision))

                if self.seccion.aux2['CPyz'] is not np.nan:
                    self.ui.cpycoordsLabel.setText('y='+to_precision(
                            self.seccion.aux2['CPyz'][0],self.precision))
                    self.ui.cpzcoordsLabel.setText('z='+to_precision(
                            self.seccion.aux2['CPyz'][1],self.precision))
                else:
                    self.ui.cpycoordsLabel.setText('-')
                    self.ui.cpzcoordsLabel.setText('-')

                if not np.isnan(self.seccion.aux2['EN']):
                    self.ui.aenLabel.setText('alpha='+to_precision(
                                    self.seccion.aux2['EN'],self.precision))
                else: self.ui.aenLabel.setText('alpha=-')

                if not np.isnan(self.seccion.aux2['Dist']):
                    self.ui.aedLabel.setText('d='+to_precision(
                                    self.seccion.aux2['Dist'],self.precision))
                else: self.ui.aedLabel.setText('d=infty')

                if not np.isnan(self.seccion.aux2['LF']):
                    self.ui.alfLabel.setText('alpha='+to_precision(
                                    self.seccion.aux2['LF'],self.precision))
                else: self.ui.alfLabel.setText('alpha=-')
                # Recalcular tension en punto
                self.calcularTensionyz()

    def modifyTextsSize(self, d):
        self.fontsize += d
        # Ejes origen
        self.ui.figurasCanvas.flechax.texto.set_fontsize(self.fontsize)
        self.ui.figurasCanvas.flechay.texto.set_fontsize(self.fontsize)
        # Artistas de la figura
        for figura in self.figuras.values():
            for artist in figura.artists:
                if type(artist) is mpl.text.Text:
                    artist.set_fontsize(self.fontsize)
                if type(artist) is graphsec.flecha:
                    artist.texto.set_fontsize(self.fontsize)
        # Artistas de la seccion
        if self.seccion:
            for artist in self.seccion.yz_artists:
                if type(artist) is mpl.text.Text:
                    artist.set_fontsize(self.fontsize)
                if type(artist) is graphsec.flecha:
                    artist.texto.set_fontsize(self.fontsize)
            for artist in self.seccion.princ_artists:
                if type(artist) is mpl.text.Text:
                    artist.set_fontsize(self.fontsize)
                if type(artist) is graphsec.flecha:
                    artist.texto.set_fontsize(self.fontsize)
            for artist in self.seccion.uv_artists:
                if type(artist) is mpl.text.Text:
                    artist.set_fontsize(self.fontsize)
                if type(artist) is graphsec.flecha:
                    artist.texto.set_fontsize(self.fontsize)
            for artist in self.seccion.sigma_artists_dict.values():
                if type(artist) is mpl.text.Text:
                    artist.set_fontsize(self.fontsize)
                if type(artist) is graphsec.flecha:
                    artist.texto.set_fontsize(self.fontsize)
            for artist in self.seccion.sigma_artists:
                if type(artist) is mpl.text.Text:
                    artist.set_fontsize(self.fontsize)
                if type(artist) is graphsec.flecha:
                    artist.texto.set_fontsize(self.fontsize)
            self.ui.seccionCanvas.flechax.texto.set_fontsize(self.fontsize)
            self.ui.seccionCanvas.flechay.texto.set_fontsize(self.fontsize)
        self.ui.figurasCanvas.fig.canvas.draw()
        self.ui.seccionCanvas.fig.canvas.draw()
        self.ui.tensionesCanvas.fig.canvas.draw()
        self.ui.tensiones3dCanvas.fig.canvas.draw()

    def DoubleValidation(self):
        onlyDouble = QtGui.QDoubleValidator()
        self.ui.alphaLineEdit.setValidator(onlyDouble)
        self.ui.MyEdit.setValidator(onlyDouble)
        self.ui.MzEdit.setValidator(onlyDouble)
        self.ui.NEdit.setValidator(onlyDouble)
        self.ui.ytensionEdit.setValidator(onlyDouble)
        self.ui.ztensionEdit.setValidator(onlyDouble)


if __name__ == "__main__":
    SCREEN_BORDER = 50
    app = 0
    app = QApplication(sys.argv)
    QApplication.setStyle(QStyleFactory.create('Plastique'))
    w = AteneaForm()
    # Windows size
    rect = QApplication.desktop().availableGeometry()
    w.setGeometry(rect.x() + SCREEN_BORDER,
                  rect.y() + SCREEN_BORDER,
                  rect.width() - 2 * SCREEN_BORDER,
                  rect.height() - 2 * SCREEN_BORDER)
    w.setMinimumSize(900, 500)
    # Window title and icon
    w.setWindowTitle('AteneaSec (beta)')
    w.setWindowIcon(QtGui.QIcon(resource_path('icons\AteneaSec_icon.png')))
    # Abrir archivo
    if len(sys.argv)==2:
        w.cargarArchivo(sys.argv[1])
    # w.cargarArchivo('AteneaSec/tests/Ejemplo2.asec')
    w.show()
    app.setWindowIcon(QtGui.QIcon(resource_path('icons\\AteneaSec_icon.png')))
    sys.exit(app.exec_())
