# Ejemplo: Pórtico de dos pisos
from Atenea2d.source.core import *

## Input del usuario

# Crear nodos
# Coordenadas de nodos
XYZ = {}
XYZ[1] = [0,0] # primer nodo
XYZ[2] = [0,3] # segundo nodo
XYZ[3] = [0,6]
XYZ[4] = [6,6]
XYZ[5] = [6,3]
XYZ[6] = [6,0]

# Crear barras
# conectividad de barras
CON = {}
CON[1] = [1,2] # Barra 1 del nodo 1 al nodo 2
CON[2] = [2,3]
CON[3] = [3,4]
CON[4] = [4,5]
CON[5] = [5,6]
CON[6] = [2,5]

# Condiciones de borde [restr hor, restr ver, restr giro]
BOUN = {}
BOUN[1] = [1,1,1] # Barra 1 con restricción horizontal, vertical y de giro
BOUN[6] = [1,1,1]

# Tipos de elemento
ElemName = {}
ElemName[1] = '2DFrame'
ElemName[2] = '2DFrame'
ElemName[3] = '2DFrame'
ElemName[4] = '2DFrame'
ElemName[5] = '2DFrame'
ElemName[6] = '2DFrame'

# Releases de elementos [axial,momento inicial, momento final]
rel = {}
rel[6] = [0,1,1]

# Propiedades del elememto
# Carga de materiales
mat1 = {'E':1000, 'l':0.001}
# Carga de secciones
seccion1 = {'A':1e6, 'I':50}
# Asignación de material y sección a cada barra
ElemData = {}
ElemData[1] = {'mat':mat1,'seccion':seccion1}
ElemData[2] = {'mat':mat1,'seccion':seccion1}
ElemData[3] = {'mat':mat1,'seccion':seccion1}
ElemData[4] = {'mat':mat1,'seccion':seccion1}
ElemData[5] = {'mat':mat1,'seccion':seccion1}
ElemData[6] = {'mat':mat1,'seccion':seccion1}

# Acciones exteriores
# Cargas nodales [carga hor., carga vert.]
P = {}
P[2] =[5,0,0]
# Cargas distribuidas
w = {}
w[3] = {'wyi':-5, 'wyf':-5, 'wxi':0, 'wxf':0, 'terna':'local'}
w[6] = {'wyi':-5, 'wyf':-5, 'wxi':0, 'wxf':0, 'terna':'local'}
# Deformaciones impuestas (uniformes a lo largo de la barra) [elongacion, curvatura]
e0 = {}


## ============================================================================
import numpy as np
import matplotlib.pyplot as plt
# Generación aleatoria de cargas
Nsim = 1000
media = (5,5)
cov = [[2,1],[1,2]]
wrand = np.random.multivariate_normal(media,cov, Nsim)
# Simular estructuras
Rout = []
Estructura = crear_estructura(XYZ,CON,ElemName,BOUN,rel,P,w,e0,ElemData,plot=True)
Estructura2 = crear_estructura_from_file('Atenea2d\\tests\\2storyframe.a2d')
for i in range(Nsim):
    # Actualizar carga distribuida
    w[3] = {'wyi':-wrand[i,0], 'wyf':-wrand[i,0], 'wxi':0, 'wxf':0, 'terna':'local'}
    w[6] = {'wyi':-wrand[i,1], 'wyf':-wrand[i,1], 'wxi':0, 'wxf':0, 'terna':'local'}
    # Crear estructura
    Estructura = crear_estructura(XYZ, CON, ElemName, BOUN, rel, P, w, e0,
                                  ElemData, plot=False)
    # Resolver estructura
    metodo_rigideces(Estructura)
    # Guardar salida de interés
    Rout += [Estructura.R[2,0]]
    # Resetear estructura
    Estructure = None

# Plotear resultados
# plot_reacciones(Estructura, BOUN, CON, ElemName)
# plot_diagramas(Estructura, 'N',scale=0.1,size=15) # 'N','M' o 'Q'
fig, axs = plt.subplots(1,2, tight_layout=True)
axs[0].hist2d(wrand[:,0], wrand[:,1], bins=Nsim/15)
axs[1].hist(Rout, density=True, edgecolor='r', facecolor='b')
plt.show()
