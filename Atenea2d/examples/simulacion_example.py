from Atenea2d.source.core import *
import numpy as np
import matplotlib.pyplot as plt

# Crear estructura
Estructura = crear_estructura_from_file('Atenea2d\\tests\\2storyframe.a2d')
plot_estructura(Estructura)

# Generación aleatoria de cargas
Nsim = 1000
media = (5,5)
cov = [[2,1],[1,2]]
wrand = np.random.multivariate_normal(media,cov, Nsim)

# Simular estructuras
Rout = []
for i in range(Nsim):
    # Actualizar cargas puntuales
    Estructura.nodos[2].P = wrand[i,0]
    Estructura.nodos[3].P = wrand[i,1]
    # Actualizar estructura
    Estructura.assemble_P()
    # Resolver estructura
    metodo_rigideces(Estructura)
    # Guardar salida de interés
    Rout += [Estructura.R[2,0]]

# Plotear resultados
fig, axs = plt.subplots(1,2, tight_layout=True)
axs[0].hist2d(wrand[:,0], wrand[:,1], bins=Nsim/15)
axs[1].hist(Rout, density=True, edgecolor='r', facecolor='b')
plt.show()
