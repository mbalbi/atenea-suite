# Ejemplo: Pórtico de dos pisos
# from Atenea2d import *
from Atenea2d.source.core import *

## Input del usuario

# Crear nodos
# Coordenadas de nodos
XYZ = {}
# XYZ[1] = [0,0] # primer nodo
# XYZ[2] = [0,3] # segundo nodo
# XYZ[3] = [0,6]
# XYZ[4] = [6,6]
# XYZ[5] = [6,3]
# XYZ[6] = [6,0]

# XYZ[1] = [0,0] # primer nodo
# XYZ[2] = [0,0.03] # segundo nodo
# XYZ[3] = [0,0.06]
# XYZ[4] = [0.06,0.06]
# XYZ[5] = [0.06,0.03]
# XYZ[6] = [0.06,0]

XYZ[1] = [0,0] # primer nodo
XYZ[2] = [0,300] # segundo nodo
XYZ[3] = [0,600]
XYZ[4] = [600,600]
XYZ[5] = [600,300]
XYZ[6] = [600,0]

# Crear barras
# conectividad de barras
CON = {}
CON[1] = [1,2] # Barra 1 del nodo 1 al nodo 2
CON[2] = [2,3]
CON[3] = [3,4]
CON[4] = [4,5]
CON[5] = [5,6]
CON[6] = [2,5]

# Condiciones de borde [restr hor, restr ver, restr giro]
BOUN = {}
BOUN[1] = [1,1,1] # Barra 1 con restricción horizontal, vertical y de giro
BOUN[6] = [1,1,0]

# Tipos de elemento
ElemName = {}
ElemName[1] = '2DFrame'
ElemName[2] = '2DFrame'
ElemName[3] = '2DFrame'
ElemName[4] = '2DFrame'
ElemName[5] = '2DFrame'
ElemName[6] = '2DFrame'

# Releases de elementos [axial,momento inicial, momento final]
rel = {}
rel[3] = [0,0,1]
rel[6] = [0,1,0]

# Propiedades del elememto
# Carga de materiales
mat1 = {'E':1000, 'l':1e-5}
# Carga de secciones
seccion1 = {'A':1e6, 'I':50}
# Asignación de material y sección a cada barra
ElemData = {}
ElemData[1] = {'mat':mat1,'seccion':seccion1}
ElemData[2] = {'mat':mat1,'seccion':seccion1}
ElemData[3] = {'mat':mat1,'seccion':seccion1}
ElemData[4] = {'mat':mat1,'seccion':seccion1}
ElemData[5] = {'mat':mat1,'seccion':seccion1}
ElemData[6] = {'mat':mat1,'seccion':seccion1}
# Acciones exteriores
# Cargas nodales [carga hor., carga vert., momento]
P = {}
# P[2] =[5,0,0]
# Cargas distribuidas
w = {}
# w[3] = {'wyi':5, 'wyf':-10, 'wxi':0, 'wxf':0, 'terna':'local'}
w[6] = {'wyi':-10, 'wyf':-10, 'wxi':0, 'wxf':0, 'terna':'global'}
# Deformaciones impuestas (uniformes a lo largo de la barra)
e0 = {}
# e0[1] = [-10, 20, 0.5]
# Cedimientos de vinculo [dx, dy, giro]
ced = {}
# ced[1] = [2,0,0]


## ===================================================================================
# Crear estructura desde archivo
# filename = 'E:\\GDrive FIUBA\\Atenea2d_last\\reticulado.a2d'
# Estructura = preprocess.crear_estructura_from_file(filename)
# Crear estructura
Estructura = preprocess.crear_estructura(
                    XYZ, CON, ElemName, BOUN, rel, P, w, e0, ElemData, ced)
# Plotear estructura
graph2d.plot_estructura(Estructura, fuerzas=True, vinculos=True,
                        cedimientos=True, indiceN=True, ejes_loc=True,
                        temp=True, indiceB=True, origen=True)
# Resolver estructura
analysis1el.metodo_rigideces(Estructura)
# Plotear resultados
graph2d.plot_resultados(Estructura, reacciones=False, diagrama=False, deformada=True)
