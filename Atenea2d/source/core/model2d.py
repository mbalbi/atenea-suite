# modulo model2D.py
"""
Este modulo define la clase "Estructura" definida como un conjunto de
objetos "nodo" y objetos "barra". A partir de la informacion guardada en los
objetos "barra" que la conforman, se ensamblan las matrices que definen a la
estructura y los vectores de vargas. Este objeto sera llamado por las funciones
de analisis estructural.

creado por: equipo Atenea-UBATIC
"""

import numpy as np
 
class Estructura:
    """
    Clase que define objetos nodo, donde se plantean las ecuaciones de
    equilibrio. Dos nodos permiten formar un elemento.
    
    Argumentos:
        - nodos [dict]: Diccionario de objetos nodos; los keys son el índice
                        del nodo
        - barras [dict]: Diccionario de objetos barra; los keys son el índice
                         del nodo
    
    Atributos:
        - DOF [list]: Lista de los grados de libertad numerados
        - DOFf [list]: Lista de grados de libertad libres (sin vinculos)
        - DOFd [list]: Lista de grados de libertad restringidos (con vinculos)
        - ndof [int]: Cantidad de grados de libertad nodales (length(DOF))
        - nf [int]: Cantidad de grados de libertad libres (length(DOFf))
        - nq [int]: Cantidad de fuerzas básicas totales (independ. de releases)
        - qid [list]: Lista de numeración de fuerzas básicas por elemento
        - qrelid [list]: Lista de índices de q con releases
        - cid [list]: Lista de índices de q sin releases (qid - qrelid)
        - nrel [int]: Cantidad de releases (length(qrelid))
        - B [numpy array]: Matriz estática (ndof x nq)
        - Bf [numpy array]: Matriz estática en dofs libres (ndoff x nq)
        - Bd [numpy array]: Matriz estática en dofs restringidos (ndofd x nq)
        - A [numpy array]: Matriz cinemática A=B.T (nq x ndof)
        - Af [numpy array]: Matriz cinemática en dofs libres (nq x doff)
        - Ad [numpy array]: Matriz cinemática en dofs restringidos (nq x ndofd)
        - P [numpy array]: Vector de cargas nodales exteriores (ndof x 1)
        - Pf [numpy array]: Vector de cargas P en dofs libres (ndoff x 1)
        - Pd [numpy array]: Vector de cargas P en dofs restringidos (ndofd x 1)
        - Pw [numpy array]: Vector de cargas nodales equivalentes por w (ndof x 1)
        - Pwf [numpy array]: Vector de cargas Pw en dofs libres (ndoff x 1)
        - Pwd [numpy array]: Vector de cargas Pw en dofs restringidos (ndofd x 1)
        - est [dict]: {'tipo': [string],'grado': [int]} indica grado de
                      estaticidad de la estructura; string: 'hipostático',
                      'isostáticos' o 'hiperestático'
        - Fs [numpy array]: Matriz de bloques diagonal con matrices de
                            flexibilidad de cada elemento en la diagonal
        - Ks [numpy array]: Matriz de bloques diagonal con matrices de
                            rigidez de cada elemento en la diagonal
        - Qw [numpy array]: Vector de fuerzas básicas equivalentes por carga w
        - Vw [numpy array]: Vector de deformaciones elementales por carga w
        - Qe0 [numpy array]: Vector de fuerzas básicas equivalentes por
                             deformaciones impuestas e0
        - Ve0 [numpy array]: Vector de deformaciones elemntales por
                             deformaciones impuestas e0
        - Kf [numpy array]: Matriz de rigideces en los grados de libertad libres

    Metodos:
        - numerate(): Enumera DOF de la estructura, actualizando elementos y
                      nodos; enumera fuerzas básicas de cada elemento y
                      numera fuerzas básicas con releases
        - assemble_B(): Ensambla matriz estática de la estructura
        - assemble_A(): Ensambla matriz cinemática de la estructura
        - assemble_Fs_Ks(): Ensambla matriz diagonal de flexibilidades y de 
                            rigideces
        - assemble_Q0_V0(): Ensambla vectores de fuerzas básicas y deformaciones
                            elementales por cargas distribuidas w y deformaciones
                            impuestas e0
        - check_statics(): Chequea el grado de estaticidad de la estructura
        - SetArtistsVisible(artists): Visibiliza los artistas que se le pidan
        - SetArtistsInvisible(): Invisibiliza todos los artistas
    """
    def __init__(self,nodos,barras):
        """
        """
        self.barras = barras
        self.nodos = nodos

        # Numerar grados de libertad nodales
        self.numerate()
        # Crear matriz estatica B, y dividir en grados de libertad libres.
        self.assemble_B()
        # Crear matriz cinemática A, y dividir en grados de libertad libres.
        self.assemble_A()
        # crear vector de cargas nodales P y cargas nodales equivalentes PW
        self.assemble_P()
        
        # # Armar matriz diagonal de matrices de rigidez y flexiblidades
        # self.assemble_Fs_Ks()
        # # Armar vector de fuerzas básicas y deformaciones equivalentes por w y e0
        # self.assemble_Q0_V0()
        # Chequear ecuaciones de equilibrio nulas e indeterminación estática
        self.check_statics()
        # Calcular distancia máxima de la estructura
        self.calc_dist_max()

        # Inicializar vectores de respuesta
        self.U = np.zeros([self.ndof, 1]) # Desplazamientos nodales
        self.R = np.zeros([self.ndof, 1])  # Reacciones nodales
        self.Q = np.zeros([self.nq, 1]) # Fuerzas básicas
        self.V = np.zeros([self.nq, 1]) # Deformaciones
        self.Vh = np.zeros([self.nq, 1]) # Deformaciones en releases

        # Inicializar atributos para el metodo de las flexibilidades
        self.int = None
        self.ext = None
        # Lista de fuerzas basicas redundantes
        self.qinc=[]
        # Lista con q unitario impuesto
        self.Q_ini=[]

    def calc_dist_max(self):
        nodos = self.nodos

        # Coordenadas
        x_coords = [x.coords[0] for x in nodos.values()]
        y_coords = [x.coords[1] for x in nodos.values()]
        x_max = max(x_coords)
        x_min = min(x_coords)
        y_max = max(y_coords)
        y_min = min(y_coords)
        self.dist_max = ((x_max - x_min) ** 2 + (y_max - y_min) ** 2) ** (.5)

    def numerate(self):
        """
        Se le asigna a cada nodo sus DOFs correspondientes
        Si alguno de sus elementos es Frame entonces tiene 3 DOFS.
        Luego genero una lista con todos los DOFs de la estructura
        """
        # Enumerar DOFs en nodos
        dof_count = 1
        self.ndof = 0
        self.DOF = []
        for nodo in self.nodos.values():
            # nodo.count_dofs(dof_count) # Enumerar dofs del nodo
            nodo.count_dofs()
            nodo.DOF = [dof_count+a for a in range(nodo.ndof)]
            dof_count = nodo.DOF[-1]+1
            self.ndof += nodo.ndof
            self.DOF += nodo.DOF
        
        # Enumerar DOFs en barras y definir fuerzas con releases
        self.nq = 0
        self.qid = []
        self.qrelid = []
        self.cid = []
        for barra in self.barras.values():
            barra.generate_dofs()
            barra.qid = list(range(self.nq+1,self.nq+barra.nq+1))
            self.qid += barra.qid
            self.qrelid += \
                    [barra.qid[i] for i in range(barra.nq) if barra.rel[i]==1]
            self.nq += barra.nq
        # Indices de q sin releases
        self.cid = list(set(self.qid)-set(self.qrelid)) 
        # Número de releases
        self.nrel = len(self.qrelid)
          
        # Obtener grados de libertad libres y restringidos
        self.DOFf = [] #DOFs libres
        self.DOFd = [] #DOFs restringidos
        self.DOFr = [] #DOFs con cedimientos de vinculo
        self.Xr = [] #Lista con cedimientos de vinculo
        for nodo in self.nodos.values():
            # for i in range(len(nodo.restr)):
            for i in range(nodo.ndof):
                if nodo.restr[i]==0:
                    self.DOFf.append(nodo.DOF[i])
                elif nodo.restr[i]==1:
                    self.DOFd.append(nodo.DOF[i])
                    if nodo.ced[i]!=0:
                        self.DOFr.append(nodo.DOF[i])
                        self.Xr.append(nodo.ced[i])
        # Cantidad de grados de libertad libres
        self.nf = len(self.DOFf)
        
    def assemble_B(self):
        """
        Definimos una matriz b de cada elemento que tiene dimensiones
        de DOFs x nq, luego se concatenan horizontalmente y se obtiene B global
        La fila k de la matriz bg del elemento n va a la fila id[k] de la 
        matriz b
      
        """
        # Ensamblar matriz estática
        self.B = np.zeros((self.ndof,self.nq))
        for barra in self.barras.values():
            dof = [d-1 for d in barra.DOF] # para poder indexar
            self.B[dof,barra.qid[0]-1:barra.qid[-1]] = barra.bg
         
        # Dividir B en Bf y Bd
        doff = [d-1 for d in self.DOFf] # restar 1 para indexar
        self.Bf = self.B[doff,]
        dofd = [d-1 for d in self.DOFd] # restar 1 para indexar
        self.Bd = self.B[dofd,]
        
    def assemble_A(self):
        """
           Matriz cinemática de la estructura   
        """
        # Ensamblar matriz cinemática
        self.A = np.transpose(self.B)
        self.Af = np.transpose(self.Bf)
        self.Ad = np.transpose(self.Bd)

    def assemble_P(self):
        """
        Funcion que asigna a cada DOF la carga externa que actúa en él.
        Devuelve un vector con n=DOF's filas.
        Primero se deben asignar las cargas a los nodos con el metodo
        asignar_Loads
        """
        for nodo in self.nodos.values():
            nodo.Pw[0:2] = [0,0]
        # Calcular fuerzas nodales equivalentes por barra
        for barra in self.barras.values():
            barra.calc_pw()
            
        # asignar cargas a cada nodo y completar vector de cargas nodales de la
        # estructura
        self.P = np.zeros((self.ndof,1))
        self.Pw = np.zeros((self.ndof,1))
        for nodo in self.nodos.values():
            dof = [d-1 for d in nodo.DOF] # para indexar
            self.P[dof,0] = nodo.P[:nodo.ndof]
            self.Pw[dof,0] = nodo.Pw[:nodo.ndof]
        
        # Dividir vectores de cargas nodales
        doff = [d-1 for d in self.DOFf] # para indexar
        dofd = [d-1 for d in self.DOFd] # para indexar
        self.Pf = self.P[doff,]
        self.Pd = self.P[dofd,]
        self.Pwf = self.Pw[doff,]
        self.Pwd = self.Pw[dofd,]

    def check_statics(self):
        """
        Chequear grado de estaticidad de la estructura
        """
        # Obtener grado de estaticidad
        # if np.size(self.Bf) != 0:
        # Cantidad de incógnitas q
        cid = [c-1 for c in self.cid]
        Bf_cid = self.Bf[:, cid]
        nqi = np.size(Bf_cid,1)
        # Rango de matriz estática
        if np.size(Bf_cid) != 0:
            aux = np.linalg.matrix_rank(Bf_cid)
            rank = np.linalg.matrix_rank(Bf_cid) # rango de la matriz Bf
        else:
            rank = 0
        B_min_dim = min(np.shape(Bf_cid)) # Mínima dimensión de Bf
        B_full_rank = (rank == B_min_dim) # chequear si Bf es de rango completo
        # Grado de estaticidad (nqs - nfs)
        grado_est = np.int(nqi-self.nf)
        if (grado_est==0 and B_full_rank):
            tipo = 'isostatico'
            grado = grado_est
        elif (grado_est > 0 and B_full_rank):
            tipo = 'hiperestatico'
            grado = grado_est
        else:
            tipo = 'inestable'
            grado = rank - self.nf
        self.est = {'tipo':tipo,'grado':grado}
           
    def assemble_Fs_Ks(self):
        """
        """
        self.Fs = np.empty((0,0))
        self.Ks = np.empty((0,0))
        for ind in self.barras.keys():
            self.barras[ind].calc_f_k()
            zeros = np.zeros((np.size(self.Fs,0),np.size(self.barras[ind].f,1)))
            self.Fs = np.block([[self.Fs, zeros],
                               [zeros.T, self.barras[ind].f]
                               ])
            zeros = np.zeros((np.size(self.Ks,0),np.size(self.barras[ind].k,1)))
            self.Ks = np.block([[self.Ks, zeros],
                               [zeros.T, self.barras[ind].k]
                               ])
    
    def assemble_Q0_V0(self):
        """
        """
        self.Qw = np.zeros((self.nq,1))
        self.Vw = np.zeros((self.nq,1))
        self.Qe0 = np.zeros((self.nq,1))
        self.Ve0 = np.zeros((self.nq,1))
        for barra in self.barras.values():
            barra.calc_element_loading()
            qwid = sorted(list(set(self.cid)&set(barra.qid)))
            qwid = [i-1 for i in qwid]
            self.Qw[qwid] = barra.qw
            self.Vw[qwid] = barra.vw
            self.Qe0[qwid] = barra.qe0
            self.Ve0[qwid] = barra.ve0

    def getNodesExtent(self):
        """

        """
        # Obtener coordenadas de los nodos
        nodos_xcoords = []
        nodos_ycoords = []
        for nodo in self.nodos.values():
            nodos_xcoords.append(nodo.coords[0])
            nodos_ycoords.append(nodo.coords[1])
        # Obtener máximos y mínimos
        xmin = min(nodos_xcoords)
        xmax = max(nodos_xcoords)
        ymin = min(nodos_ycoords)
        ymax = max(nodos_ycoords)
        return xmin, xmax, ymin, ymax









