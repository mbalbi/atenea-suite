# module postprocess.py
"""
Created on Sun Jul 22 12:57:31 2018

@author: Mariano
"""

def print_results(estructura):
    """
    Imprimir hoja con resultados de la estructura, formateados
    """

def save_txt(filename, nodos, barras, materiales, secciones):
    f = open(filename,"w+")

    XYZ = {}
    BOUN = {}
    P = {}
    CON = {}
    ElemName = {}
    rel = {}
    ElemData = {}
    w = {}
    e0 = {}
    ced = {}
    for i,nodo in nodos.items():
        XYZ[i] = nodo.coords
        if any(nodo.restr)==1:
            BOUN[i] = nodo.restr
        if any(nodo.P)==1:
            P[i] = nodo.P
        if any(nodo.ced)==1:
            ced[i] = nodo.ced
    for i,barra in barras.items():
        CON[i] = [barra.nodos[0].indice,barra.nodos[1].indice]
        ElemName[i] = barra.ElemName
        if (any(barra.rel) == 1 and barra.ElemName == '2DFrame'):
            rel[i] = barra.rel
        ElemData[i] = {'seccion':barra.seccion,'mat':barra.mat}
        if any(list(barra.w.values())[0:4])==1:
            w[i] = barra.w
        if any(barra.e0)==1:
            e0[i] = barra.e0

    #Guardar XYZ
    f.write("#"+"\t"+"XYZ"+"\n")
    for i in XYZ:
        f.write(str(i)+"\t"+str(XYZ[i][0])+"\t"+str(XYZ[i][1])+"\n")
    #Guardar CON
    f.write("#"+"\t"+"CON"+"\n")
    for i in CON:
        f.write(str(i)+"\t"+str(CON[i][0])+"\t"+str(CON[i][1])+"\n")
    f.write("#"+"\t"+"BOUN"+"\n")
    for i in BOUN:
        string = str(i)
        for b in BOUN[i]:
            string += "\t"
            string += str(b)
        string += "\n"
        f.write(string)
    f.write("#"+"\t"+"ElemName"+"\n")
    for i in ElemName:
        f.write(str(i)+"\t"+ElemName[i]+"\n")
    f.write("#"+"\t"+"rel"+"\n")
    for i in rel:
        f.write(str(i)+"\t"+str(rel[i][0])+"\t"+str(rel[i][1])+"\t"+str(rel[i][2])+"\n")
    f.write("#"+"\t"+"materiales"+"\n")
    for name, material in materiales.items():
        f.write(name+"\t"+str(material["E"])+"\t"+str(material['l'])+"\t"+"\n")
    f.write("#"+"\t"+"secciones"+"\n")
    for name, seccion in secciones.items():
        f.write(name+"\t"+str(seccion["A"])+"\t"+str(seccion["I"])+"\t"+"\n")
    f.write("#"+"\t"+"ElemData"+"\n")
    for i in ElemData:
        if (ElemData[i]['mat']) and (ElemData[i]['seccion']):
            f.write(str(i)+"\t"+str(ElemData[i]["mat"]["name"])+"\t" \
            +str(ElemData[i]["seccion"]["name"])+"\t"+"\n")
    f.write("#"+"\t"+"P"+"\n")
    for i in P:
        string = str(i)
        for b in P[i]:
            string += "\t"
            string += str(b)
        string += "\n"
        f.write(string)
    f.write("#"+"\t"+"w"+"\n")
    for i in w:
        f.write(str(i)+"\t"+str(w[i]["wyi"])+"\t"+str(w[i]["wyf"])+"\t"\
                +str(w[i]["wxi"])+"\t"+str(w[i]["wxf"])+"\t"+w[i]["terna"]+"\n")
    f.write("#"+"\t"+"e0"+"\n")
    for i in e0:
        f.write(str(i)+"\t"+str(e0[i][0])+"\t"+str(e0[i][1])+"\n")
    f.write("#"+"\t"+"Cedimientos de vinculo" + "\n")
    for i in ced:
        f.write(
            str(i) + "\t" + str(ced[i][0]) + "\t" + str(ced[i][1]) + "\t" + str(
                ced[i][2]) + "\n")
    f.close()

def read_txt(filename):
    f = open(filename,"r")
    metadata = {'XYZ':{}, 'CON':{}, 'BOUN':{}, 'ElemName':{}, 'counter':{},
                'ElemData':{}, 'P':{}, 'w':{}, 'e0':{}, 'secciones':{},
                'materiales':{}, 'rel':{}, 'ced':{}} # output
    #Definir los diccionarios vacios
    XYZ = {}
    CON = {}
    BOUN = {}
    ElemName = {}
    rel = {}
    materiales = {}
    secciones = {}
    ElemData = {}
    P = {}
    w = {}
    e0 = {}
    ced = {}
    counter = 0
    for line in f:
        l = line.rstrip("\n")
        l = l.split("\t")
        if l[0] == '#':
            counter += 1
            continue
        if counter == 1: #Definir XYZ
            XYZ[int(l[0])] = (float(l[1]),float(l[2]))
            metadata['XYZ'] = XYZ
        if counter == 2: #Definir CON
            CON[int(l[0])] = [int(l[1]),int(l[2])]
            metadata['CON'] = CON
        if counter == 3: #Definir BOUN
            boun = [0,0,0]
            for i in range(1,len(l)):
                boun[i-1] = int(l[i])
            BOUN[int(l[0])] = boun
            metadata['BOUN'] = BOUN
        if counter == 4: #Definir ElemName
            ElemName[int(l[0])] = l[1]
            metadata['ElemName'] = ElemName
        if counter == 5: #Definir rel
            rel[int(l[0])] = [int(l[1]),int(l[2]), int(l[3])]
            metadata['rel'] = rel
        if counter == 6: #Definir materiales
            materiales[l[0]] = {'name':l[0], 'E':float(l[1]), 'l':float(l[2])}
            metadata['materiales'] = materiales
        if counter == 7: #Definir secciones
            secciones[l[0]] = {'name':l[0], 'A':float(l[1]), 'I':float(l[2])}
            metadata['secciones'] = secciones
        if counter == 8: #Definir ElemData
            ElemData[int(l[0])] = {'mat':l[1], 'seccion':l[2]}
            metadata['ElemData'] = ElemData
        if counter == 9: #Definir P
            p = [0,0,0]
            for i in range(1,len(l)):
                p[i-1] = float(l[i])
            P[int(l[0])] = p
            metadata['P'] = P
        if counter == 10: #Definir w
            w[int(l[0])] = {'wyi':float(l[1]), 'wyf':float(l[2]), 'wxi':float(l[3]),\
              'wxf': float(l[4]),'terna': l[5]}
            metadata['w'] = w
        if counter == 11: #Definir e0
            e0[int(l[0])] = [float(l[1]),float(l[2])]
            metadata['e0'] = e0
        if counter == 12: #Definir cedimientos de vinculo
            c = [0,0,0]
            for i in range(1,len(l)):
                c[i-1] = float(l[i])
            ced[int(l[0])] = c
            metadata['ced'] = ced

    return metadata
