# módulo analysis1el.py
"""
Módulo de funciones de análisis estructural. Estas funciones reciben un objeto
de la clase Estructura como argumento, y obtienen reacciones de vínculo,
esfuerzos internos y deformaciones a partir de distintos método de análisis
estructural

creado por: equipo Atenea-UBATIC
"""
import numpy as np
# from scipy import linalg

def resolver_isostatico(estructura, Qini=[], v=False):
    """
    Resuelve una estructura isostática por medio de ecuaciones de equilibrio.
    
    Argumentos:
        - estructura: Objeto de la clase estructura. Debe ser isostática para
                      poder resolverse
        - Qini: Lista con valor de q unitario en coordenada correspondiente, para ser impuesto cuando se resuelven
                los sistemas para el metodo de las flexibilidades
        
    Salida:
        - Se completa el objeto estructura con los resultados del análisis;
          Desplazamientos nodales, esfuerzos internos y deformaciones.
        
    """
    # Salir si la estructura no es isostatica
    if estructura.est['tipo']=='hipostatico':
        print('La estructura no es estable')
        return()
    elif estructura.est['tipo']=='hiperestatico':
        print('La estructura es hiperestatica')
        return()
            
    # Obtener incognitas estaticas de los elementos
    A = estructura.A
    Af = estructura.Af
    Bf = estructura.Bf
    Bd = estructura.Bd
    Pf = estructura.Pf
    Pd = estructura.Pd
    Pwf = estructura.Pwf
    Pwd = estructura.Pwd
    
    # Incluir releases en matriz estatica y cinemática
    cid = [i-1 for i in estructura.cid]
    Bf_cid = Bf[:,cid]
    Af_cid = Af[cid,:]
    
    # Calcular fuerzas básicas por equilibrio
    estructura.Q[cid] = np.linalg.solve(Bf_cid,Pf-Pwf) #aca esta pasando algo muy raro
    if Qini:
        Qini = np.reshape(np.asarray(Qini),(estructura.nq,1))
        estructura.Q += Qini
    # Calcular reacciones de vínculo por equilibrio
    dofd = [d - 1 for d in estructura.DOFd]
    estructura.R[dofd] = np.dot(Bd,estructura.Q) - (Pd-Pwd)
    
    if v: # Calcular deformaciones
        # Armar matriz diagonal de matrices de rigidez y flexiblidades
        estructura.assemble_Fs_Ks()
        # Armar vector de fuerzas básicas y deformaciones equivalentes por w y e0
        estructura.assemble_Q0_V0()
        # Calcular deformaciones en los elementos a partir de Q
        estructura.V[cid] = np.dot(estructura.Fs,estructura.Q[cid]) + \
                            estructura.Vw[cid] + estructura.Ve0[cid]
        # Deformaciones por cedimientos de vínculo
        dofr = [d - 1 for d in estructura.DOFr]
        if dofr:
            Ar = A[:,dofr]
            Xr = np.array([estructura.Xr])
            Vr = -np.dot(Ar,Xr.T)
            estructura.U[dofr] = Xr.T
        else:
            Vr = np.zeros(np.shape(estructura.V))
        # Calcular desplazamientos nodales libres
        doff = [d-1 for d in estructura.DOFf]
        estructura.U[doff] = np.linalg.solve(Af_cid,estructura.V[cid]+Vr[cid])
        # Calcular deformaciones en releases
        estructura.Vh = np.dot(A,estructura.U) - estructura.V
    # Completar elementos y nodos
    complete_elements(estructura, v=v)
    
def metodo_rigideces(estructura):
    """
    Resuelve estructuras isostáticas e hiperestáticas utilizando el método
    de las rigideces
    
    Argumentos:
        - estructura: Objeto de la clase estructura.
        
    Salida:
        - Se completa el objeto estructura con los resultados del análisis;
          Desplazamientos nodales, esfuerzos internos y deformaciones.
        
    """
    # Armar matriz diagonal de matrices de rigidez y flexiblidades
    estructura.assemble_Fs_Ks()
    # Armar vector de fuerzas básicas y deformaciones equivalentes por w y e0
    estructura.assemble_Q0_V0()
    
    # Obtener incognitas estaticas de los elementos
    B = estructura.B
    Bf = estructura.Bf
    Bd = estructura.Bd
    Af = estructura.Af
    Pf = estructura.Pf
    Pd = estructura.Pd
    Pwf = estructura.Pwf
    Pwd = estructura.Pwd
    Ks = estructura.Ks
    
    # grados de libertad sin releases
    cid = [i-1 for i in estructura.cid]
    # grados de libertad libres sin releases
    DOFf = [i-1 for i in estructura.DOFf]
    # grados de libertad con cedimientos de vinculo
    DOFr = [i-1 for i in estructura.DOFr]

    # Cargas nodales equivalentes
    P0 = Pf - (np.dot(Bf[:,cid],estructura.Qw[cid,]) + \
               np.dot(Bf[:,cid],estructura.Qe0[cid,]) + Pwf)
    
    # Definir matriz Ar, asociada a los grados de libertad restringidos con
    # cedimientos de vínculo
    Ar = estructura.A[:,DOFr]
    #Calcular la matriz de rigideces global
    K = np.dot(np.dot(B[:,cid],Ks),B[:,cid].T)
    #Calcular la submatriz Kfr
    Kfr = K[DOFf][:,DOFr]
    #Generar el vector Xr
    Xr = np.array([estructura.Xr])
    #Calcular vector Pr asociado a los cedimientos de vinculo
    Pr = np.dot(Kfr,Xr.T)
    #Calcular P total, tiene cuenta fuerzas externas e internas debidas a
    # cedimientos de vinculo
    estructura.Ptot = P0-Pr
    # Matriz de rigidez de los grados de libertad libres de la estructura
    estructura.Kf = np.dot(np.dot(Bf[:,cid],Ks),Af[cid,:])
    # Resolver ecuaciones de equilibrio
    doff = [d-1 for d in estructura.DOFf]
    estructura.U[doff] = np.linalg.solve(estructura.Kf,estructura.Ptot)
    # Computar cedimientos de vinculos en los desplazamientos de nodos restringidos
    estructura.U[DOFr] = Xr.T
    # Deformaciones en los elementos
    if np.size(Xr) == 0: #Por cuestiones de dimensiones de Ar y Xr, cuando ambas son 0
        estructura.V[cid] = np.dot(Af[cid,:],estructura.U[doff])
    else: #Cuando hay cedimientos de vinculo
        estructura.V[cid] = np.dot(Af[cid,:],estructura.U[doff]) + np.dot(Ar[cid,],Xr.T)
    # Fuerzas basicas en los elementos
    estructura.Q[cid] = np.dot(estructura.Ks,estructura.V[cid,]) + \
                        estructura.Qw[cid,] + estructura.Qe0[cid,]
    # Calcular reacciones de vínculo por equilibrio
    dofd = [d-1 for d in estructura.DOFd]
    estructura.R[dofd] = np.dot(Bd,estructura.Q) - (Pd-Pwd)
    # Completar elementos y nodos
    complete_elements(estructura)

def metodo_flexibilidades(estructura, qinc = None, ext = None):
    """
    Resuelve estructuras isostáticas e hiperestáticas utilizando el método
    de las flexibilidades

    Argumentos:
        - estructura: Objeto de la clase estructura.
        - qinc: lista con las columnas de la matriz estática que el usuario desea liberar. Si el usuario no lo
        especifica, la matriz se triangula automaticamente.
        - ext: lista con las restricciones de vinculo que el usuario desea quitar para resolver el metodo de las
                flexibilidades

    Salida:
        - Se completa el objeto estructura con los resultados del análisis;
          Desplazamientos nodales, esfuerzos internos y deformaciones.

    """
    # Armar matriz diagonal de matrices de rigidez y flexiblidades
    estructura.assemble_Fs_Ks()
    # Armar vector de fuerzas básicas y deformaciones equivalentes por w y e0
    estructura.assemble_Q0_V0()
    
    # Obtener incognitas estaticas de los elementos
    A = estructura.A
    B = estructura.B
    Bf = estructura.Bf
    Bd = estructura.Bd
    Pf = estructura.Pf
    Pd = estructura.Pd
    Pwf = estructura.Pwf
    Pwd = estructura.Pwd
    Fs = estructura.Fs
    
    # indices q sin releases
    cid = [i - 1 for i in estructura.cid]
    # grados de libertad libres sin releases
    DOFf = [i - 1 for i in estructura.DOFf]
    # Grados de libertad con vinculos
    DOFd = [d - 1 for d in estructura.DOFd]
    # grados de libertad con cedimientos de vinculo
    DOFr = [i - 1 for i in estructura.DOFr]

    # Excluir releases en matriz estatica y cinemática
    Bf_cid = Bf[:, cid]
    B_cid = B[:,cid]

    NOS = estructura.est['grado']
    nf = np.size(Bf_cid, 0)
    nq = np.size(Bf_cid, 1)

    #Liberar fuerzas internas y externas si el usuario especifica qinc o ext
    if qinc or ext: #Liberar fuerzas internas y externas
        #Agregar filas de reacciones de vinculo redundantes
        ext = [x - 1 for x in ext][:] #Restar 1 para indexar
        # ext.sort()
        new_doff = DOFf + ext
        #new_doff = [x - 1 for x in new_doff][:] #Restar 1 para indexar
        new_doff.sort()
        #Mapear DOFf y ext en new_doff
        i_DOFf=[new_doff.index(i) for i in DOFf]
        i_ext=[new_doff.index(i) for i in ext]
        #Obtener Be
        Be = B_cid[new_doff,:]
        #Obtener bi y bx
        xcols = [cid.index(x-1) for x in qinc]
        # xcols.sort() #xcols columnas de fuerzas internas redundantes
        icols = [a for a in range(np.size(Bf_cid, 1)) if a not in xcols]
        # icols.sort() #icols columnas de fuerzas internas independientes

        bi = Be[:,icols]
        bx = Be[:,xcols]
        estructura.bx = bx
        bi_inv = np.linalg.inv(bi)
        #Obtener bii, bix y aix
        bii = bi_inv[:,i_DOFf]
        bix = bi_inv[:,i_ext]
        # aix = bix.T
        #Armar Bi y Bix
        Bi = np.zeros([nq,nf])
        Bi[icols,:] = bii
        Bix = np.zeros([nq,len(ext)])
        Bix[icols,:] = bix
        Bx = np.zeros([nq,len(xcols)])
        Bx[xcols,:] = np.eye(len(xcols))
        Bx[icols,:] = np.dot(-bi_inv,bx)
        #Calcular matriz de flexibilidades
        Fii = np.dot(Bx.T,np.dot(Fs,Bx))
        Fie = np.dot(Bx.T,np.dot(Fs,Bix))
        Fei = np.dot(Bix.T,np.dot(Fs,Bx))
        Fee = np.dot(Bix.T,np.dot(Fs,Bix))
        F = np.concatenate((np.concatenate((Fii,Fie),axis=1),
                            np.concatenate((Fei,Fee),axis=1)))
        estructura.F = F
        #Obtener termino de causa
        Ve0 = estructura.Ve0[cid]
        Vw = estructura.Vw[cid]
        #Desplazamientos en X por cedimientos de vínculo
        Xr = np.array([estructura.Xr])
        estructura.U[DOFr] = Xr.T
        Ar = A[:, DOFr]
        Ar = Ar[cid,:]
        Vr = -np.dot(Ar, Xr.T)
        e0i = np.dot(Bx.T,np.dot(Fs,np.dot(Bi,Pf-Pwf))+ Ve0 + Vw + Vr)
        e0e = np.dot(Bix.T,np.dot(Fs,np.dot(Bi,Pf-Pwf))+ Ve0 + Vw + Vr)
        e0 = np.concatenate((e0i,e0e))
        estructura.e0 = e0
        #Calcular fuerzas redundantes
        X = np.linalg.solve(F,-e0)
        Xint = X[:len(xcols)]
        Xext = X[len(xcols):]

        Qp = np.dot(Bi,Pf-Pwf) + np.dot(Bix,Xext)

    # Resolver automaticamente liberando fuerzas internas si el usuario no 
    # especifica xcols o ext
    else:
        # Get set of independent columns from Bf
        if nf > 0:
            # q, r, p = linalg.qr(Bf_cid, pivoting=True)
            dummy, li_index = LI_vecs(Bf_cid.T)
            # Indices de fuerzas básicas dentro del sistema fundamental
            # icols = list(p[0:np.linalg.matrix_rank(Bf_cid)])
            icols = li_index
            icols.sort()
        else:
            icols = []
        # Indices de fuerzas básicas redundantes
        xcols = [a for a in range(np.size(Bf_cid,1)) if a not in icols]
        # estructura.xcols = [x + 1 for x in xcols]
        estructura.qinc = [cid[x]+1 for x in xcols]
        # Matriz estática de la estructura del sistema fundamental
        bi = Bf_cid[:,icols]
        bi_inv = np.linalg.inv(bi)
        # Matriz estática de fuerzas redundantes
        estructura.bx = Bf_cid[:,xcols]

        # Matrices de influencia de fuerzas
        Bi = np.zeros((nq,nf))
        Bi[icols,:] = bi_inv

        Bx = np.zeros((nq,NOS))
        Bx[icols,:] = -np.dot(bi_inv,estructura.bx)
        Bx[xcols,:] = np.eye(NOS)

        # Fuerzas básicas del sistema fundamental ante cargas exteriores
        Qp = np.dot(Bi, Pf-Pwf)

        # Matriz de flexibilidad de fuerzas redundantes
        estructura.F = np.dot(Bx.T,np.dot(estructura.Fs,Bx))
        Fxx = estructura.F
        # Matriz de flexibilidad de fuerzas básicas del sistema fundamental
        Fxi = np.dot(Bx.T,np.dot(estructura.Fs,Bi))

        # Desplazamientos en X por cedimientos de vínculo
        Xr = np.array([estructura.Xr])
        estructura.U[DOFr] = Xr.T
        Ar = A[:, DOFr]
        Ar = Ar[cid,:]
        Vr = -np.dot(Ar, Xr.T)
        Urx = np.dot(Bx.T, Vr)

        # Calcular fuerzas redundantes
        Ve0 = estructura.Ve0[cid]
        Vw = estructura.Vw[cid]
        Ux = np.dot(Fxi,Pf-Pwf) + np.dot(Bx.T, Ve0+Vw) + Urx
        estructura.e0 = Ux
        Xint = np.linalg.solve(Fxx,-Ux)

    # Calcular fuerzas independientes
    estructura.Q[cid] = Qp + np.dot(Bx,Xint)

    # Obtener deformaciones de elementos
    estructura.V[cid] = np.dot(estructura.Fs, estructura.Q[cid]) + Ve0 + Vw
    # Obtener desplazamintos nodales
    if ext:
        # i = [new_doff.index(a) for a in DOFf]
        estructura.U[DOFf] = np.dot(Bi.T, estructura.V[cid] + Vr)
    else:
        estructura.U[DOFf] = np.dot(Bi.T, estructura.V[cid] + Vr)
    # Calcular reacciones de vínculo por equilibrio
    estructura.R[DOFd] = np.dot(Bd, estructura.Q) - (Pd - Pwd)

    # Completar elementos y nodos
    complete_elements(estructura)

def complete_elements(estructura, v=True):
    """
    Completa la información en cada elemento y nodo a partir de los resultados
    del análisis de la estructura completa.
    """
    # Completar elementos
    for barra in estructura.barras.values():
        qid = [i-1 for i in barra.qid]
        # Fuerzas básicas del elemento
        barra.q = estructura.Q[qid,]
        # Reacciones nodales
        barra.end_forces = (np.dot(barra.b,barra.q)) + barra.pw_local
        # Deformaciones
        if v:
            barra.v = np.dot(barra.f0,barra.q) + barra.vw0 + barra.ve00
        # Desplazamientos
        dof = [i - 1 for i in barra.DOF]
        barra.U = estructura.U[dof]
    # Completar nodos
    for nodo in estructura.nodos.values():
        # Desplazamientos nodales
        dof = [i-1 for i in nodo.DOF]
        nodo.U = estructura.U[dof]
        # Reacciones de vínculo
        nodo.R = estructura.R[dof]

def LI_vecs(mat):
    dim = mat.shape[0]
    # Start from row that is not all 0s
    counter = 0
    while all(mat[counter]==0):
        counter += 1
    LI=[mat[counter]]
    index = [counter]
    for i in range(dim):
        tmp = []
        # tmp2=[r for r in LI]
        for r in LI:
            tmp.append(r)
        # set tmp=LI+[M[i]]
        tmp.append(mat[i])
        # test if M[i] is linearly independent from all (row) vectors in LI
        if np.linalg.matrix_rank(tmp)>len(LI):
            # note that matrix_rank does not need to take in a square matrix
            LI.append(mat[i])
            index.append(i)
    return LI, index