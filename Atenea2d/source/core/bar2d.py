# módulo bar2d.py
"""
Módulo de clases Nodo y Barra.

creado por: equipo Atenea-UBATIC
"""

import numpy as np

class Nodo:
    """
    Clase que define objetos nodo, donde se plantean las ecuaciones de
    equilibrio. Dos nodos permiten formar un elemento.
    
    Argumentos:
        - coords [list]: Lista con las coordenadas del nodo [X,Y]
        - indice [int]: Índice del nodo
    
    Atributos:
        - coords [list]: Lista con las coordenadas del nodo [X,Y]
        - indice [int]: Índice del nodo
        - elemnames [list]: Lista con el tipo de cada elemento que llega al nodo
        - restr [list]: Lista con las restricciones de vínculo [Rx,Ry,M]
        - U [list]: Lista con los desplazamientos nodales calculados [dx,dy,giro]
        - P [list]: Lista con las cargas nodales exteriores [Px,Py,M]
        - Pw [list]: Lista con las cargas nodales equivalentes por cargas
                     distribuidas en los elementos
        -nodo_artists [dict]: Diccionario con todos los artistas asociados al nodo. 'self': el propio nodo,
                            'v': vinculos, 'p': fuerzas puntuales, 'c': cedimientos de vinculo,

    Métodos:
        - count_dofs(inicial): Cuenta y enumera (a partir de 'inicial') la
                               cantidad de grados de libertad del nodo en
                               función de los elementos que llegan a el. 

    """
    def __init__(self, coords, indice):
        """
        coords: list, que es el value del diccionario XYZ
        indice: keys del diccionario XYZ
        """
        # set attributes
        self.coords = coords
        self.indice = indice
        self.elemnames = [] # almacenar tipos de elementos que llegan al nodo
        # inicializar atributos vacios
        self.restr = [0,0,0] # 1 si hay vinculo restrictivo
        self.R = [0,0,0]
        self.U = [0,0,0] # desplazamientos nodales
        self.P = [0,0,0] # cargas nodales exteriores
        self.Pw = [0,0,0] # cargas nodales equivalentes por w
        self.ced = [0,0,0] # cedimientos de vinculo

    # def count_dofs(self, inicial):
    def count_dofs(self):
        """
        """
        # 
        if self.elemnames:
            if all(i=='2DTruss' for i in self.elemnames):
                self.ndof = 2
            else:
                self.ndof = 3
        
        ##
        # # Cantidad de dofs
        # if self.elemnames: # Para que un nodo vacío no sea considerado como truss
        #     if all(i=='2DTruss' for i in self.elemnames):
        #         self.ndof = 2
        #         self.restr = self.restr[0:2]
        #         self.U = self.U[0:2]
        #         self.P = self.P[0:2]
        #         self.Pw = self.Pw[0:2]
        #         self.R = self.R[0:2]
        #         self.ced = self.ced[0:2]
        # self.ndof = len(self.U)
        # # Enumerar grados de libertad
        # self.DOF = [inicial + a for a in range(self.ndof)]

class Barra:
    """
    Clase que define objetos barra, donde se define la estática y cinemática
    de los objetos de viga y de reticulado. Guarda toda la información sobre
    propiedades del elemento, fuerzas básicas y deformaciones equivalentes.
    
    Argumentos:
        - indice [int]: Indice del elemento
        - nodos [list]: Lista de objetos nodos inicial y final [nodoi, nodof]
        - ElemName [str]: String que define el tipo de elemento; '2DFrame' o 
                          '2DTruss'
        - rel [list]: Lista de releases para cada fuerza básica; 1: release
        - w [dict]: Diccionario de cargas distribuidas sobre el elemento del
                    tipo {'wxi':0,'wyi':0,'wxf':0,'wyf':0,'terna':'local'}
        - e0 [list]: Lista de deformaciones impuestas [e0,k0] donde e0 es un
                     alargamiento impuesto (sin unidades) y k0 es una
                     curvatura impuesta (1/m). Se consideran uniformes en todo
                     el elemento
        - data [dict]: Diccionario de propiedades del material y sección del
                       tipo {'mat':{'E':},'seccion':{'A':,'I':}}
    
    Atributos:
        - indice [int]: Indice del elemento
        - nodos [list]: Lista de objetos nodos inicial y final [nodoi, nodof]
        - coords [numpy array]: Matriz de coordenadas de nodos (2x2)
        - ElemName [str]: String que define el tipo de elemento; '2DFrame' o 
                          '2DTruss'
        - nq [int]: Cantidad de fuerzas básicas
        - seccion [dict]: Diccionario con valores geométricos de la sección
        - mat [dict]: Diccionario con parámetros constitutivos del material
        - w [dict]: Diccionario con cargas distribuidas; del tipo 
                    tipo {'wxi':0,'wyi':0,'wxf':0,'wyf':0,'terna':'local'}
        - w_local [numpy array]: vector de cargas distribuidas
        - e0 [list]: Lista de deformaciones impuestas [e0,k0]
        - rel [list]: Lista de releases para cada fuerza básica; 1: release
        - long [num]: Longitud del elemento
        - dc [numpy array]: Vector de cosenos directores del elemento
        - Mr [numpy array]: Matriz de rotación de traslaciones únicamente
        - br [numpy array]: Matriz de rotación (traslaciones más giros)
        - b [numpy array]: Matriz estática en coordenadas locales
        - bg [numpy array]: Matriz estática en coordenadas globales
        - a [numpy array]: Matriz cinemática en coordenadas locales
        - ag [numpy array]: Matriz cinemática en coordenadas globales
        - k [numpy array]: Matriz de rigidez
        - f [numpy array]: Matriz de flexibilidad
        - barra_artists [dict]: Diccionario con todos los artistas de la carga de datos asociados a la barra
        - results_artists [dict]: Diccionario con todos los artistas de los resultados asociados a la barra
    Métodos:
        - assign_data(data): Inicializa y guarda los datos de material y sección
                             como atributos
        - assign_w(w): Guarda las cargas distribuidas como atributo y arma el
                       vector con las cargas distribuidas.
        - assign_e0(e0): Inicializa y guarda los datos de e0 como atributo
        - assign_rel(rel): Inicializa y guarda los valores de rel como atributo;
                           actualiza el vector de elemnames de cada nodo
        - calc_geom(): Calcula longitud, cosenos directores y Mr
        - define_br(): Define la matriz de rotación según el tipo de elemento
        - define_b(): Define la matriz estática en coordenadas locales
        - define_bg(): Define la matriz estática en coordenadas globales
        - define_a(): Define la matriz cinemática en coordenadas locales
        - define_ag(): Define la matriz cinemática en coordenadas globales
        - calc_f_k(): Define matriz de flexibilidad y de rigidez, descontando
                      los releases.
        - calc_pw(): Calcula el vector de cargas nodales equivalentes y
                     actualiza los nodos con las cargas
        - calc_element_loading(): Calcula las deformaciones y las fuerzas
                                  básicas equivalentes por cargas distribuidas
                                  y deformaciones impuestas
        - generate_dofs(): Define DOFs del elemento a partir de los DOFs de sus
                           nodos
        - compute_internal_forces(x): Computa los esfuerzos internos del elemento
                                      en la coordenada local x desde su nodo
                                      inicial
        
    """
    def __init__(self, indice, nodos, ElemName, rel=[], w={}, e0=[],
                 data={'seccion':{},'mat':{}}):
        """
        nodos [int, int]: indice de nodo inicial y nodo final
        ElemName [str]
        """
        # Atributos
        self.indice = indice
        self.nodos = nodos
        self.coords = np.array([nodos[0].coords, nodos[1].coords])
        self.ElemName = ElemName

        # N de fuerzas básicas independientes
        if self.ElemName == '2DFrame': self.nq = 3
        elif self.ElemName == '2DTruss': self.nq = 1

        # Lista de fuerzas básicas
        self.qindex = []

        # Inicializar atributos para el metodo de las flexibilidades
        # Lista de releases agregados por el metodo de las flexibilidades
        self.new_rel = [0,0,0]

        # Calcular matrices estáticas y cinemáticas
        self.calc_geom() # Cosenos directores y longitud
        self.define_br() # Matriz de rotación (locales a globales)
        self.define_bg() # Matriz estática (locales y globales)
        self.define_ag() # Matriz cinemática (locales y globales)

        # Pasar elemname a nodo inicial y final
        self.nodes_elemnames()
        
        # Asignar datos al elemento
        self.assign_data(data)
        self.assign_w(w)
        self.assign_e0(e0)
        self.assign_rel(rel)

    def assign_data(self,data):
        """
        Asignar seccion y material. Computar matriz de flexibildiad y rigidez
        locales
        """
        self.mat = data['mat']
        self.seccion = data['seccion']
        # if not self.seccion:
        #     self.seccion['name'] = 'None'
        #     self.seccion['A'] = 1
        #     self.seccion['I'] = 1
        # if not self.mat:
        #     self.mat['name'] = 'None'
        #     self.mat['E'] = 1
        #     self.mat['l'] = 1

    def assign_w(self,w):
        """
        Asignar carga distribuida y computar cargas nodales equivalentes
        """
        # Inicializar
        self.w = {'wxi':0,'wyi':0,'wxf':0,'wyf':0,'terna':'local'}
        if w:
            self.w = w
        
        self.w_local = np.array([[self.w['wxi']],[self.w['wyi']],[0],
                                 [self.w['wxf']],[self.w['wyf']],[0]])
        # Girar en caso de terna global
        if self.w['terna'] == 'global':
            self.w_local = np.dot(self.br,self.w_local)
    
    def assign_e0(self,e0):
        """
        Asignar deformaciones impuestas (temperatura por ej.) al elemento
        """
        # self.e0 = [0,0,1] # Dsup, Dinf, espesor
        self.e0 = [0,0] # T baricentrica, T gradiente
        if e0:
            self.e0 = e0
    
    def assign_rel(self, rel):
        """
        Se cargan los releases a los elementos. En caso de que no haya ningun
        release cargado se predefinen las condiciones de borde para los
        elementos segun sean Frame o Truss. Las primera fila indica las
        condiciones del primer nodo, siendo 1 restringido y 0 libre, y la 
        segunda fila las condiciones del segundo nodo.
        """
        
        # Incializar releasesdel elemento
        if self.ElemName == '2DFrame':
            self.rel = [0,0,0]
        elif self.ElemName == '2DTruss':
            self.rel = [0]
        
        # Agregar releases al elemento
        if rel:
            self.rel = rel
    
    def calc_geom(self):
        """
        Defino parametros geometricos del elemento:
        Longitud; vector deltax,deltay; cosenos directores
        """
        dif = self.coords[1] - self.coords[0]
        # Longitud
        self.long = np.sqrt(np.dot(dif,dif))
        # Cosenos directores
        self.dc = dif / self.long
        # Matriz de rotación 
        dX = dif[0]
        dY = dif[1]
        L = self.long
        self.Mr = np.array([[dX/L, dY/L],
                            [dY/L, -dX/L]])
        
    def define_br(self):
        """
        Matriz de rotacion, relaciona las fuerzas elementales p de cada
        elemento en terna global, a terna local
        """
        dif = self.coords[1] - self.coords[0]
        dX = dif[0]
        dY = dif[1]
        L = self.long
        if dX == 0:
            self.angle = 90
        else:
            self.angle = np.rad2deg(np.arctan(dY/dX))

        if self.ElemName == '2DFrame':
            self.br = np.zeros((6,6))
            self.br[0:3,0:3]=([[dX/L, dY/L, 0],
                              [dY/L, -dX/L,0],
                              [0,0, 1]])
            self.br[3:6,3:6]=([[dX/L, dY/L, 0],
                              [dY/L, -dX/L,0],
                              [0,0,1]])
        elif self.ElemName == '2DTruss':
            self.br = np.zeros((4,4))
            self.br[0:2,0:2]=([[dX/L, dY/L],
                              [dY/L, -dX/L]])
            self.br[2:4,2:4]=([[dX/L, dY/L],
                              [dY/L, -dX/L]])
    
    def define_b(self):
        """
        Matriz estática, relaciona las fuerzas elementales p de cada elemento 
        con las fuerzas básicas q del mismo en una terna local.
        """
        L = self.long
        if self.ElemName == '2DFrame':
            # self.b = np.array([[-1, 0, 0], [0, -1/L, -1/L],
            #                    [0, 1, 0], [1, 0, 0],
            #                    [0, 1/L, 1/L], [0, 0, 1]])
            self.b = np.array([[-1, 0, 0], [0, 1/L, -1/L],
                               [0, -1, 0], [1, 0, 0],
                               [0, -1/L, 1/L], [0, 0, 1]])
        elif self.ElemName == '2DTruss':
            self.b = np.array([[-1], [0], [1], [0]])
        
    def define_bg(self):
        """
        Defino la matriz bg, que relaciona las fuerzas elementales p en terna
        global, con las fuerzas basicas q en terna local.
        """
        self.define_b()
        self.bg = np.dot(self.br,self.b)
    
    def define_a(self):
        """
        Matriz cinemática...
        """
        self.a = np.transpose(self.b)
    
    def define_ag(self):
        """
        Defino la matriz ag
        """
#        self.calc_a()
        self.ag = np.transpose(self.bg)
    
    def calc_f_k(self):
        """
        """
        A = self.seccion['A']
        I = self.seccion['I']
        E = self.mat['E']
        L = self.long
        if self.ElemName == '2DFrame':
            # Matriz de flexibilidad
            # self.f0 = np.array([[L/(E*A), 0, 0],
            #                   [0, L/(3*E*I), -L/(6*E*I)],
            #                   [0, -L/(6*E*I), L/(3*E*I)]])
            self.f0 = np.array([[L/(E*A), 0, 0],
                              [0, L/(3*E*I), L/(6*E*I)],
                              [0, L/(6*E*I), L/(3*E*I)]])
            # Remover filas y columnas por releases
            q_elim = [i for i in range(len(self.rel)) if self.rel[i]==1]
            self.f = np.delete(self.f0,q_elim,1)
            self.f = np.delete(self.f,q_elim,0)
            # Matriz de rigidez
            self.k = np.linalg.inv(self.f)
        if self.ElemName == '2DTruss':
            # Matriz de flexibilidad
            self.f0 = np.array([[L/(E*A)]])
            # Remover filas y columnas por releases
            q_elim = [i for i in range(len(self.rel)) if self.rel[i]==1]
            self.f = np.delete(self.f0,q_elim,1)
            self.f = np.delete(self.f,q_elim,0)
            # Matriz de rigidez
            self.k = np.array([[(E*A)/L]])
    
    def calc_pw(self):
        """
        """

        # Variables
        L = self.long
        wxi = self.w_local[0,0]
        wyi = self.w_local[1,0]
        wxf = self.w_local[3,0]
        wyf = self.w_local[4,0]
        # Computar reacciones nodales equivalentes
        pw1 = -L/2*(wxi+wxf)
        pw2 = -L/6*(2*wyi+wyf)
        pw5 = -L/6*(wyi+2*wyf)
        # Guardar en elemento y en nodos
        if self.ElemName == '2DFrame':
            self.pw_local = np.array([[pw1], [pw2], [0], [0], [pw5], [0]])
            self.pw_global = np.dot(self.br,self.pw_local)
            # Completar nodos con cargas equivalentes globales
            self.nodos[0].Pw[0:2] += self.pw_global[0:2,0]
            self.nodos[1].Pw[0:2] += self.pw_global[3:5,0]
        elif self.ElemName == '2DTruss':
            self.pw_local = np.array([[pw1], [pw2], [0], [pw5]])
            self.pw_global = np.dot(self.br,self.pw_local)
            # Completar nodos con cargas equivalentes globales
            self.nodos[0].Pw[0:2] += self.pw_global[0:2,0]
            self.nodos[1].Pw[0:2] += self.pw_global[2:4,0]
    
    def calc_element_loading(self):
        """
        Fuerzas básicas y deformaciones en el elemento por cargas distribuidas
        y deformaciones impuestas e0.
        """
        # Variables
        A = self.seccion['A']
        I = self.seccion['I']
        E = self.mat['E']
        L = self.long
        wxi = self.w_local[0,0]
        wyi = self.w_local[1,0]
        wxf = self.w_local[3,0]
        wyf = self.w_local[4,0]
        # dsup = self.e0[0]
        # dinf = self.e0[1]
        # h = self.e0[2]
        e0 = self.mat['l']*self.e0[0]
        k0 = self.mat['l']*self.e0[1]
        if self.ElemName == '2DFrame':
            # Deformaciones del elemento por condiciones impuestas
            # self.ve00 = np.array([[e0*L],
            #                       [-k0*L/2],
            #                       [k0*L/2]])
            self.ve00 = np.array([[e0*L],
                                  [k0*L/2],
                                  [k0*L/2]])
            # Deformaciones del elemento por cargas distribuidas
            # self.vw0 = np.array([[L**2/(6*E*A)*(2*wxf+wxi)],
            #                     [-L**3/(360*E*I)*(7*wyf+8*wyi)],
            #                     [L**3/(360*E*I)*(8*wyf+7*wyi)]])
            self.vw0 = np.array([[L**2/(6*E*A)*(2*wxf+wxi)],
                                [L**3/(360*E*I)*(7*wyf+8*wyi)],
                                [L**3/(360*E*I)*(8*wyf+7*wyi)]])
            # Remover filas con releases
            q_elim = [i for i in range(len(self.rel)) if self.rel[i]==1]
            self.vw = np.delete(self.vw0,q_elim,0)
            self.ve0 = np.delete(self.ve00,q_elim,0)
        elif self.ElemName == '2DTruss':
            # Deformaciones del elemento por condiciones impuestas
            self.ve00 = np.array([e0*L])
            self.ve0 = self.ve00
            # Deformaciones del elemento por cargas distribuidas
            self.vw0 = np.array([L**2/(6*E*A)*(2*wxf+wxi)])
            self.vw = self.vw0
        # Fuerzas básicas equivalentes
        self.qw = -np.dot(self.k, self.vw)
        self.qe0 = -np.dot(self.k, self.ve0)

    def generate_dofs(self):
        """
        Arma una lista con los DOFs del nodo_i y nodo_f
        """
        # DOFs de cada nodo del elemento
        DOFi = self.nodos[0].DOF
        DOFf = self.nodos[1].DOF
        if self.ElemName == '2DFrame':
            self.DOF = DOFi + DOFf
        elif self.ElemName == '2DTruss':
            self.DOF = DOFi[0:2] + DOFf[0:2]

    def nodes_elemnames(self):
        # Pasar ElemName a cada nodo
        self.nodos[0].elemnames.append(self.ElemName)
        self.nodos[1].elemnames.append(self.ElemName)

    def compute_internal_forces(self,x):
        """
        Calcular esfuerzos internos N,M y Q en la coordenada local x.
        x puede ser un vector
        """
        L = self.long
        wxi = self.w_local[0,0]
        wyi = self.w_local[1,0]
        wxf = self.w_local[3,0]
        wyf = self.w_local[4,0]
        # Esfuerzos internos
        if self.ElemName == '2DFrame':
            N = -self.end_forces[0] + ((wxi-wxf)/(2*L))*pow(x,2) - wxi*x
            M = -self.end_forces[2] - self.end_forces[1]*x - \
                ((wyf-wyi)/(6*L))*pow(x,3) - (wyi/2)*pow(x,2)
            Q = -self.end_forces[1] - ((wyf-wyi)/(2*L))*pow(x,2) - wyi*x

        elif self.ElemName == '2DTruss':
            N = -self.end_forces[0] + ((wxi-wxf)/2*L)*pow(x,2) - wxi*x
            M = -self.end_forces[1]*x - ((wyf-wyi)/(6*L))*pow(x,3) - (wyi/2)*pow(x,2)
            Q = -self.end_forces[1] - ((wyf-wyi)/(2*L))*pow(x,2) - wyi*x
        return N,M,Q
        
    def compute_displacements(self,x):
        """
        Calcular desplazamientos en la coordenada local x de la barra según
        las direcciones locales x (axial) e y (perpendicular)
        """
        # Datos de la barra
        L = self.long
        E = self.mat['E']
        J = self.seccion['I']
        A = self.seccion['A']
        # Cargas distribuidas en la barra
        wxi = self.w_local[0, 0]
        wyi = self.w_local[1, 0]
        wxf = self.w_local[3, 0]
        wyf = self.w_local[4, 0]
        # Obtener desplazamientos nodales en coordenadas globales
        # U = np.concatenate((self.nodos[0].U,self.nodos[1].U))
        # Obtener desplazamientos nodales en coordenadas locales
        Uloc = np.dot(np.transpose(self.br), self.U)
        if self.ElemName == '2DFrame':
            beta = (Uloc[4] - Uloc[1])/L
        elif self.ElemName == '2DTruss':
            beta = (Uloc[3] - Uloc[1]) / L
        # Deformaciones internas
        v1 = self.v[0]
        v2 = 0
        v3 = 0
        if self.ElemName == '2DFrame':
            v2 = self.v[1]
            v3 = self.v[2]
        # Desplazamientos internos en coordenadas locales para el giro de los
        # extremos y el alargamiento
        A1 = -(3*L**3*wyf+7*L**3*wyi+120*E*J*(-v2+v3))/(120*L**2*E*J)
        A2 = (2*L**3*wyf+3*L**3*wyi+120*E*J*(-2*v2+v3))/(120*L*E*J)
        A3 = v2
        A4 = 0
        A5 = -(L*(wxf+2*wxi)-6*E*A*v1/L)/(6*E*A)
        A6 = 0
        uy = A1*pow(x,3) + A2*pow(x,2) + A3*x + A4 + 1/E/J*(wyi/24*pow(x,4) +
             (wyf-wyi)/120/L*pow(x,5))
        ux = A5*x + A6 + 1/E/A*(wxi*pow(x,2)/2 + (wxf-wxi)*pow(x,3)/6/L)
        # Desplazamientos en coordenadas locales incluyendo giro rígido de la barra
        ux2 = ux
        uy2 = uy + beta*x
        # Desplazamientos en coordenadas locales incluyendo traslación rígida de la barra
        ux3 = ux2 + Uloc[0]
        uy3 = uy2 + Uloc[1]

        return ux3 ,uy3

    def find_maxmin_internal_forces(self,solicitacion, extreme):
        L = self.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X,(1000,1))
        N,M,Q = self.compute_internal_forces(X)
        if solicitacion == 'N':
            values = N
        elif solicitacion == 'Q':
            values = Q
        elif solicitacion == 'M':
            values = M
        if extreme == 'max':
            ex_val = values.max()
            values_list = values.tolist()
            position = values_list.index(ex_val)
        elif extreme == 'min':
            ex_val = values.min()
            values_list = values.tolist()
            position = values_list.index(ex_val)
        position = (L*position)/1000
        return position, ex_val
