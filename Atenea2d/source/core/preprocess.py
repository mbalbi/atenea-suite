# module preprocess.py
"""
Módulo de preprocesamiento de información provista por el usuario. Define la
función para crear un objeto estructura usando los datos provistos por el 
usuario.

creado por: equipo Atenea-UBATIC
"""
from .model2d import Estructura
from .bar2d import Nodo, Barra
from .graph2d import plot_estructura
from .postprocess import read_txt

def crear_estructura(XYZ, CON, ElemName, BOUN={}, rel={}, P={}, w={}, e0={},
                     ElemData={}, ced={}, plot=False):
    """
    Utilizando los datos a ingresar por el usuario, se crea la Estructura,
    definiendo las clases Nodo2D (a partir de las coordenadas XYZ y las 
    restricciones BOUN); Elem2D(ElemData, material, sección, conectividad CON,
    se le asignan las cargas distribuidas)
    """
    # crear nodos
    nodos = {x:Nodo(XYZ[x],x) for x in XYZ.keys()}
    
    # Agregar restricciones a los nodos: BOUN
    for i in BOUN.keys():
        nodos[i].restr = BOUN[i]
        
    # Agregar cedimientos de vinculos
    for i in ced.keys():
        nodos[i].ced = ced[i]
        
    # Agregar cargas nodales
    for i in P.keys():
        nodos[i].P = P[i]
    
    # crear elementos a partir de CON
    barras = {}
    for x in CON.keys():
        nodo_i = nodos[CON[x][0]]
        nodo_f = nodos[CON[x][1]]
        elemname = ElemName[x]
        if x in rel.keys(): relx = rel[x] 
        else: relx={}
        if x in w.keys(): wx = w[x] 
        else: wx={}
        if x in e0.keys(): e0x = e0[x] 
        else: e0x={}
        if x in ElemData.keys(): data = ElemData[x] 
        else: data={'mat':{},'seccion':{}}
        barras[x] = Barra(x,[nodo_i,nodo_f],elemname,relx,wx,e0x,data)
    
    # Crear objeto estructura
    estructura = Estructura(nodos,barras)
    
    if plot:
        # Plotear estructura
        plot_estructura(estructura)
    
    # Crear y devolver objeto estructura        
    return estructura
    
def crear_estructura_from_file(filename):
    """
    Cargar estructura desde metadata
    """
    metadata = read_txt(filename)
    XYZ = metadata['XYZ']
    CON = metadata['CON']
    ElemName = metadata['ElemName']
    BOUN = metadata['BOUN']
    rel = metadata['rel']
    P = metadata['P']
    w = metadata['w']
    e0 = metadata['e0']
    ElemData = metadata['ElemData']
    ced = metadata['ced']
    secciones = metadata['secciones']
    materiales = metadata['materiales']
    for key, elemdata in ElemData.items():
        ElemData[key] = {'mat':materiales[elemdata['mat']],
                         'seccion':secciones[elemdata['seccion']]}
    estructura = crear_estructura(XYZ, CON, ElemName, BOUN, rel, P, w, e0,
                                  ElemData, ced)
    return estructura