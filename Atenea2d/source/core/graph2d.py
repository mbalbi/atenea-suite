# module graph2d.py
"""
Módulo de funciones de ploteo.

creado por: equipo Atenea-UBATIC
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.transforms as transforms

from matplotlib.patches import Polygon, Circle
from matplotlib.lines import Line2D
from matplotlib.patches import FancyArrowPatch, FancyArrow
from matplotlib.text import Text
from .auxiliar import to_precision

class Apoyo:
    """
    Clase para generar artists de un apoyo
    """
    def __init__(self, axes, coords, id, dim=1, zorder=1, color='r'):

        self.axes = axes
        self.coords = coords
        self.id = id
        self.zorder = zorder
        self.dim = dim
        self.color = color
        self.artists = []

        if id == [1,0,0] or id == [1,0]:
            self.createTriangle(self.coords, -np.pi/2, 0.8*self.dim)
            patin_coords = [self.coords[0]-dim, self.coords[1]]
            self.createPatin(patin_coords, -np.pi/2, self.dim)
        if id == [0,1,0] or id == [0,1]:
            self.createTriangle(self.coords, 0, 0.8*self.dim)
            patin_coords = [self.coords[0], self.coords[1]-dim]
            self.createPatin(patin_coords, 0, self.dim)
        if id == [0,0,1]:
            self.createHatch(self.coords, 0, self.dim)
            self.createHatch(self.coords, np.pi, self.dim)
        if id == [1,1,0] or id == [1,1]:
            self.createTriangle(self.coords, 0, 0.8*self.dim)
        if id == [1,0,1]:
            # Apoyo movil superior
            tri1_coords = [self.coords[0], self.coords[1]+dim/2]
            self.createTriangle(tri1_coords, -np.pi/2, self.dim/1.5)
            patin1_coords = [tri1_coords[0]-0.8*dim, tri1_coords[1]]
            self.createPatin(patin1_coords, -np.pi/2, self.dim/1.5)
            # Apoyo movil inferior
            tri2_coords = [self.coords[0], self.coords[1]-dim/2]
            self.createTriangle(tri2_coords, -np.pi/2, self.dim/1.5)
            patin2_coords = [tri2_coords[0]-0.8*dim, tri2_coords[1]]
            self.createPatin(patin2_coords, -np.pi/2, self.dim/1.5)
            # Patin superior
            self.createPatin(self.coords, -np.pi/2, self.dim)
        if id == [0,1,1]:
            # Apoyo movil izquierdo
            tri1_coords = [self.coords[0]-dim/2, self.coords[1]]
            self.createTriangle(tri1_coords, 0, self.dim/1.5)
            patin1_coords = [tri1_coords[0], tri1_coords[1]-0.8*dim]
            self.createPatin(patin1_coords, 0, self.dim/1.5)
            # Apoyo movil derecho
            tri2_coords = [self.coords[0]+dim/2, self.coords[1]]
            self.createTriangle(tri2_coords, 0, self.dim/1.5)
            patin2_coords = [tri2_coords[0], tri2_coords[1]-0.8*dim]
            self.createPatin(patin2_coords, 0, self.dim/1.5)
            # Patin superior
            self.createPatin(self.coords, 0, self.dim)
        if id == [1,1,1]:
            self.createHatch(self.coords, 0, self.dim)
            self.createPatin(self.coords, 0, self.dim)

    def createTriangle(self, coords, rot, dim):
        # Definir vertices del triangulo en el origen
        vertices = np.array([[0, 0], [1.155/2, -1], [-1.155/2, -1]]) * dim
        # Rotar angulo
        Mr = np.array([[np.cos(rot), -np.sin(rot)],[-np.sin(rot), np.cos(rot)]])
        vertices = np.dot(Mr, vertices.T).T
        # trasladar al punto de aplicacion
        vertices += np.array(coords)
        # Definir poligono
        apoyo = Polygon(vertices, color=self.color, ls='solid', lw=1, alpha=0.5)
        self.axes.add_artist(apoyo)
        # Agregar artistas a una lista
        self.artists.append(apoyo)

    def createPatin(self, coords, rot, dim):
        # Definir centro del patin
        vertices = np.array([[-1.155/2, 0], [1.155/2, 0]]) * dim
        # Rotar angulo
        Mr = np.array([[np.cos(rot), -np.sin(rot)],[-np.sin(rot), np.cos(rot)]])
        vertices = np.dot(Mr, vertices.T).T
        # trasladar al punto de aplicacion
        vertices += np.array(coords)
        # Plotear patin
        patin = Line2D(vertices[:,0], vertices[:,1], color=self.color, lw=1,
                       zorder=0)
        self.axes.add_artist(patin)
        # Agregar artistas a una lista
        self.artists.append(patin)

    def createHatch(self, coords, rot, dim):
        # Definir vectores para representar el apoyo
        vertices = np.array([[-1.155/2, 0], [1.155/2, 0], [1.155/2, -0.5],
                            [-1.155/2, -0.5]])*dim
        # Rotar angulo
        Mr = np.array([[np.cos(rot), -np.sin(rot)],[-np.sin(rot), np.cos(rot)]])
        vertices = np.dot(Mr, vertices.T).T
        # trasladar al punto de aplicacion
        vertices += np.array(coords)
        # Definir los artists a partir de los vectores
        apoyo = Polygon(vertices, hatch='/' * 5, color=self.color, lw=0,
                        fill=False)
        self.axes.add_artist(apoyo)
        # Agregar artistas a una lista
        self.artists.append(apoyo)

    def update_dim(self, dim):
        # Actualizar dimension nueva
        scale = dim/self.dim
        self.dim = dim
        # Escalar alrededor del punto de apoyo
        for artist in self.artists:
            if type(artist) is Polygon:
                xy = artist.get_xy()
            if type(artist) is Line2D:
                xy = artist.get_xydata()
            dxy = xy - np.array(self.coords)
            dxy_sc = dxy*scale
            xy_sc = dxy_sc + np.array(self.coords)
            if type(artist) is Polygon:
                artist.set_xy(xy_sc)
            if type(artist) is Line2D:
                artist.set_xdata(xy_sc[:,0])
                artist.set_ydata(xy_sc[:,1])

    def remove(self):
        for artist in self.artists:
            artist.remove()

    def set_visible(self, visible):
        for artist in self.artists:
            artist.set_visible(visible)

class FrameBar:
    """
    Clase de artist que combina un Line2D y circular Patches para formar
    una barra con 0, 1 o 2 articulaciones en sus extremos
    """
    def __init__(self, axes, xdata, ydata, linewidth, color,
                 elemname='2DFrame', zorder=1):

        self.ax = axes
        self.artists = []
        self.release_radius = 6
        self.release_distance = 0.05
        self.color = color
        self.zorder = zorder

        self.elemname = elemname
        if self.elemname == '2DFrame':
            linestyle = '-'
            self.releases = 0
        elif self.elemname == '2DTruss':
            linestyle = '--'
            self.releases = 3

        # Create Line2D artist
        self.baraxis = Line2D(xdata, ydata,
                                     linewidth, linestyle, self.color)
        self.baraxis.set_zorder(self.zorder)
        self.rel = []
        self.ax.add_artist(self.baraxis)
        self.compute_circle_coords()
        self.add_releases(self.releases)

    def compute_circle_coords(self):
        xdata = self.baraxis.get_xdata()
        ydata = self.baraxis.get_ydata()

        dx = xdata[1] - xdata[0]
        dy = ydata[1] - ydata[0]
        # self.alpha = dy / dx
        self.L = np.sqrt(dx ** 2 + dy ** 2)
        if self.L == 0:
            self.L = 0.000001
        # Releases artists location
        self.rel_init_coords = (xdata[0] + self.release_distance*dx,
                                ydata[0] + self.release_distance*dy)
        self.rel_end_coords = (xdata[1] - self.release_distance*dx,
                               ydata[1] - self.release_distance*dy)
        # Release de normal
        self.rel_norm_coords = ((xdata[1]+xdata[0])/2,
                                (ydata[1]+ydata[0])/2)
    def add_releases(self, releases, new_rel=[]):
        self.clean_releases()
        #Articulaciones blancas por default
        color_n = 'w'
        color_init = 'w'
        color_end = 'w'
        #Cambiar el color de articulaciones agregadas por metodo de flexibilidades
        if new_rel:
            if new_rel[0]==1:
                color_n = 'r'
            if new_rel[1]==1:
                color_init = 'r'
            if new_rel[2]==1:
                color_end = 'r'

        if releases == 1:
            artist = \
            self.ax.plot(self.rel_init_coords[0], self.rel_init_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_init,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
        elif releases == 2:
            artist = self.ax.plot(self.rel_end_coords[0], self.rel_end_coords[1],
                                  'o',fillstyle='full',
                                  markersize=self.release_radius,
                                  markeredgecolor=self.color,
                                  markerfacecolor=color_end,
                                  zorder=self.zorder+1)[0]
            self.rel.append(artist)
        elif releases == 3:
            artist = \
            self.ax.plot(self.rel_init_coords[0], self.rel_init_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_init,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
            artist = \
            self.ax.plot(self.rel_end_coords[0], self.rel_end_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_end,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
        elif releases == 4:
            artist = self.ax.plot(self.rel_norm_coords[0],
                                  self.rel_norm_coords[1],
                                  '^', fillstyle='full',
                                  markersize=self.release_radius,
                                  markeredgecolor=self.color,
                                  markerfacecolor=color_n,
                                  zorder=self.zorder+1)[0]
            self.rel.append(artist)
        elif releases == 5:
            artist = self.ax.plot(self.rel_norm_coords[0],
                                  self.rel_norm_coords[1],
                                  '^', fillstyle='full',
                                  markersize=self.release_radius,
                                  markeredgecolor=self.color,
                                  markerfacecolor=color_n,
                                  zorder=self.zorder+1)[0]
            self.rel.append(artist)
            artist = \
            self.ax.plot(self.rel_init_coords[0], self.rel_init_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_init,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
        elif releases == 6:
            artist = self.ax.plot(self.rel_norm_coords[0],self.rel_norm_coords[1],
                                  '^',fillstyle='full',
                                  markersize=self.release_radius,
                                  markeredgecolor=self.color,
                                  markerfacecolor=color_n,
                                  zorder=self.zorder+1)[0]
            self.rel.append(artist)
            artist = \
            self.ax.plot(self.rel_end_coords[0], self.rel_end_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_end,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
        elif releases == 7:
            artist = self.ax.plot(self.rel_norm_coords[0],
                                  self.rel_norm_coords[1],
                                  '^', fillstyle='full',
                                  markersize=self.release_radius,
                                  markeredgecolor=self.color,
                                  markerfacecolor=color_n,
                                  zorder=self.zorder+1)[0]
            self.rel.append(artist)
            artist = \
            self.ax.plot(self.rel_init_coords[0], self.rel_init_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_init,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
            artist = \
            self.ax.plot(self.rel_end_coords[0], self.rel_end_coords[1],
                         'o', fillstyle='full', markersize=self.release_radius,
                         markeredgecolor=self.color, markerfacecolor=color_end,
                         zorder=self.zorder+1)[0]
            self.rel.append(artist)
        self.artists = [self.baraxis] + self.rel

    def clean_releases(self):
        self.artists = [self.baraxis]
        for rel in self.rel:
            rel.remove()
        self.rel = []

    def set_releases_data(self):
        self.compute_circle_coords()
        if self.releases == 1:
            # self.rel[0].center = self.rel_init_coords
            self.rel[0].set_data(self.rel_init_coords[0],
                                 self.rel_init_coords[1])
        elif self.releases == 2:
            # self.rel[0].center = self.rel_end_coords
            self.rel[0].set_data(self.rel_end_coords[0],
                                 self.rel_end_coords[1])
        elif self.releases == 3:
            # self.rel[0].center = self.rel_init_coords
            self.rel[0].set_data(self.rel_init_coords[0],
                                 self.rel_init_coords[1])
            # self.rel[1].center = self.rel_end_coords
            self.rel[1].set_data(self.rel_end_coords[0],
                                 self.rel_end_coords[1])

    def set_picker(self, picker):
        self.baraxis.set_picker(picker)

    def set_picker_releases(self, picker):
        for rel in self.rel:
            rel.set_picker(picker)

    def set_zorder(self, zorder):
        self.baraxis.set_zorder(zorder)
        for artist in self.rel:
            artist.set_zorder(zorder+1)

    def set_data(self, xdata, ydata):
        self.baraxis.set_data(xdata, ydata)
        self.set_releases_data()

    def set_animated(self, animated):
        for artist in self.artists:
            artist.set_animated(animated)

    def remove(self):
        for artist in self.artists:
            artist.remove()

    def get_data(self):
        return self.baraxis.get_data()

    def draw_artist(self):
        for artist in self.artists:
            self.ax.draw_artist(artist)

    def set_color(self, color):
        for artist in self.artists:
            artist.set_color(color)
            artist.set_markeredgecolor(color)

    def get_xydata(self):
        return self.baraxis.get_xydata()

    def get_color(self):
        return self.baraxis.get_color()

    def set_visible(self, visible):
        for artist in self.artists:
            artist.set_visible(visible)

class flechaAtenea:
    """
    ax: objeto axis de matplotlib
    x: [xi,xf]
    y: [yi,yf]
    length: largo de la flecha en unidades fisicas
    reverse: bool. 0:alejandose del nodos; 1:acercandose al nodo
    offset: separacion del nodo
    label: texto
    fontsize: tamaño del texto
    """

    def __init__(self, ax, coords, dir, length=1, reverse=0, offset=0, label='',
                 fontsize=8, forma='recta', visible=True, transform=False,
                 *args, **kwargs):
        self.ax = ax
        self.coords = coords
        dir = np.array(dir) / np.linalg.norm(dir)
        if reverse and forma=='recta': dir = [-i for i in dir]
        # Set transform
        if transform:
            self.trans = (self.ax.get_figure().dpi_scale_trans +
                     transforms.ScaledTranslation(self.coords[0], self.coords[1],
                                                  self.ax.transData))
            # Si la flecha es curva, el sentido esta indicado en la componente x
            # de dir.
            self.flecha = Arrow2D(coords, dir, length=length, reverse=reverse,
                                  forma=forma, offset=offset, visible=visible,
                                  transform=self.trans, *args, **kwargs)
            self.create_annotation(label, fontsize, visible=visible,
                                   transform=transform)
        else:
            self.flecha = Arrow2D(coords, dir, length=length, reverse=reverse,
                                forma=forma, offset=offset, visible=visible,
                                *args, **kwargs)
            self.create_annotation(label, fontsize, visible=visible)
        self.ax.add_artist(self.flecha)


    def create_annotation(self, label, fontsize, visible=True, transform=False):
        xs, ys = self.flecha._verts
        dx = (xs[1] - xs[0])
        dy = (ys[1] - ys[0])
        L = (dx**2+dy**2)**0.5
        if self.flecha.reverse: x, y = xs[0]-L/8, ys[0]+L/8
        if not self.flecha.reverse: x, y = xs[1]-L/8, ys[1]+L/8
        # Rotacion del texto
        if dx != 0: angle = np.arctan(dy/dx)
        else: angle = 90
        if transform:
            self.text = Text(x, y, label, fontsize=fontsize, rotation=angle,
                            visible=visible, horizontalalignment='center',
                            va='center', transform=self.trans)
        else:
            self.text = Text(x, y, label, fontsize=fontsize, rotation=angle,
                            visible=visible, horizontalalignment='center',
                            va='center')
        self.ax.add_artist(self.text)

    def set_arrowstyle(self, arrowstyle):
        self.flecha.set_arrowstyle(arrowstyle)

    def set_connectionstyle(self, connectionstyle):
        self.flecha.set_connectionstyle(connectionstyle)

    def update_length(self, length):
        l = self.flecha.length
        self.flecha.update_length(length/l, self.coords)
        xs, ys = self.flecha._verts
        if self.flecha.reverse: x, y = xs[0], ys[0]
        if not self.flecha.reverse: x, y = xs[1], ys[1]
        self.text.set_x(x)
        self.text.set_y(y)

    def set_zorder(self, zorder):
        self.flecha.set_zorder(zorder)
        self.text.set_zorder(zorder + 1)

    def set_visible(self, visible):
        self.flecha.set_visible(visible)
        self.text.set_visible(visible)

    def set_text_visible(self, visible):
        self.text.set_visible(visible)

    def set_arrow_visible(self, visible):
        self.flecha.set_visible(visible)

    def set_fontsize(self, fontsize):
        self.text.set_fontsize(fontsize)

    def set_picker(self, picker=5):
        self.flecha.set_picker(picker)

    def remove(self):
        self.flecha.remove()
        self.text.remove()

class Arrow2D(FancyArrowPatch):

    def __init__(self, coords, dir, length=1, reverse=0, offset=0,
                 forma='recta', *args, **kwargs):

        FancyArrowPatch.__init__(self, (coords), (dir), *args, **kwargs)
        self.reverse = reverse
        self.forma = forma
        if forma == 'recta':
            self._verts = ([coords[0], coords[0] + dir[0]],
                           [coords[1], coords[1] + dir[1]])
            if self.reverse: self.set_reverse()
            self.calc_length()
            scale = length/self.length
            self.update_length(scale, coords)
            self.add_offset(offset)
        elif forma == 'curva':
            self._verts = ([coords[0] - dir[0] / 2, coords[0] + dir[0] / 2],
                           [coords[1] - dir[1] / 2, coords[1] + dir[1] / 2])
            if self.reverse:
                self.connectionstyle = "arc3,rad=0.8"
            else:
                self.connectionstyle = "arc3,rad=-0.8"
            self.set_connectionstyle(self.connectionstyle)
            self.calc_length()
            scale = length/self.length
            self.update_length(scale, coords)

    def draw(self, renderer):
        xs, ys = self._verts
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

    def add_offset(self, offset):
        self.calc_length()
        xs, ys = self._verts
        dx = offset*(xs[1]-xs[0])/self.length
        dy = offset*(ys[1]-ys[0])/self.length
        if self.reverse:
            dx = -dx
            dy = -dy
        self._verts = ([xs[0]+dx, xs[1]+dx], [ys[0]+dy,ys[1]+dy])

    def calc_length(self):
        xs, ys = self._verts
        self.length = np.sqrt((xs[1]-xs[0])**2 + (ys[1]-ys[0])**2)

    def set_reverse(self):
        xs, ys = self._verts
        self._verts = ([xs[1],xs[0]], [ys[1],ys[0]])

    def update_length(self, scale, coords):
        xs, ys = self._verts
        # Distancias del nodo al inicio de la flecha
        dx1 = (xs[0] - coords[0]) * scale
        dy1 = (ys[0] - coords[1]) * scale
        # Distancias del nodo al final de la flecha
        dx2 = dx1 + (xs[1] - xs[0]) * scale
        dy2 = dy1 + (ys[1] - ys[0]) * scale
        self._verts = ([coords[0] + dx1, coords[0] + dx2],
                       [coords[1] + dy1, coords[1] + dy2])
        self.calc_length()
        return

class cargadistribuidaAtenea:

    def __init__(self, axes, pinit, winit, pfin, wfin, dir, color, arrowstyle,
                 fontsize, offset, visible=True, zorder=5, length=1):
        """
        La lista de coordenadas de los vértices del polígono están en el
        siguiente orden:
            1- Punto inicial de flecha en el nodo inicial
            2- Punto final de flecha en el nodo inicial
            3- Punto final de flecha en el nodo final
            4- Punto inicial de flecha en el nodo final
        :param axes:
        :param xy:
        :param color:
        :param arrowstyle:
        """

        # Parámetros
        self.winit = winit
        self.wfin = wfin
        self.pinit = np.array(pinit)
        self.pfin = np.array(pfin)
        self.length = length

        # Versor de direccion
        self.dir = np.array(dir)/np.linalg.norm(dir)

        # Tamaño de las flechas
        self.set_arrow_sizes()

        # Dibujar polígono de carga distribuida
        if self.arrow1_dim:
            p1 = pinit - np.sign(self.arrow1_dim) * self.dir * offset
            p4 = pfin - np.sign(self.arrow1_dim) * self.dir * offset
        else:
            p1 = pinit - np.sign(self.arrow2_dim) * self.dir * offset
            p4 = pfin - np.sign(self.arrow2_dim) * self.dir * offset
        p2 = p1 - self.dir * self.arrow1_dim
        p3 = p4 - self.dir * self.arrow2_dim
        self.xy = np.vstack((p1,p2,p3,p4))
        self.polygon = Polygon(self.xy, color=color, alpha=0.3, visible=visible,
                               zorder=zorder)
        axes.add_artist(self.polygon)

        # Flecha inicial
        if self.arrow1_dim:
            dir1 = self.dir*self.arrow1_dim
            self.flecha_i = flechaAtenea(axes, pinit, dir1, label=str(self.winit),
                                offset=offset, fontsize=fontsize,
                                length=abs(self.arrow1_dim), lw=1, color=color,
                                visible=visible, zorder=zorder, reverse=True)
            self.flecha_i.set_arrowstyle(arrowstyle)

        # Flecha final
        if self.arrow2_dim:
            dir2 = self.dir * self.arrow2_dim
            # Para cuando es cruzada, que el offset se aplique para el lado del
            # poligono
            signo_offset = 1
            if self.arrow1_dim:
                if np.dot(dir1,dir2) < 0: signo_offset=-1
            self.flecha_f = flechaAtenea(axes, pfin, dir2, label=str(self.wfin),
                    offset=signo_offset*offset, fontsize=fontsize,
                    length=abs(self.arrow2_dim), lw=1, color=color,
                    visible=visible, zorder=zorder, reverse=True)
            self.flecha_f.set_arrowstyle(arrowstyle)

    def set_arrow_sizes(self):
        '''
        Setea las longitudes de las flechas final e inicial, tal que la mayor tenga
        el valor de self.length y la menor se ajuste a la escala.
        '''
        if np.abs(self.winit) >= np.abs(self.wfin):
            if self.winit == 0: # Los dos son cero
                self.arrow1_dim = 0
                self.arrow2_dim = 0
            else:
                self.arrow1_dim = np.sign(self.winit) * self.length
                self.arrow2_dim = np.sign(self.wfin) * self.length * \
                                  np.abs(self.wfin/self.winit)
        elif np.abs(self.wfin) > np.abs(self.winit):
            self.arrow2_dim = np.sign(self.wfin) * self.length
            self.arrow1_dim = np.sign(self.winit) * self.length * \
                              np.abs(self.winit / self.wfin)

    def set_zorder(self, zorder):
        if self.arrow1_dim:
            self.flecha_i.set_zorder(zorder=zorder)
        if self.arrow2_dim:
            self.flecha_f.set_zorder(zorder=zorder)
        self.polygon.set_zorder(zorder=zorder)

    def set_visible(self, visible):
        if self.arrow1_dim:
            self.flecha_i.set_visible(visible)
        if self.arrow2_dim:
            self.flecha_f.set_visible(visible)
        self.polygon.set_visible(visible)

    def set_text_visible(self, visible):
        if self.arrow1_dim:
            self.flecha_i.set_text_visible(visible)
        if self.arrow2_dim:
            self.flecha_f.set_text_visible(visible)

    def remove(self):
        if self.arrow1_dim:
            self.flecha_i.remove()
        if self.arrow2_dim:
            self.flecha_f.remove()
        self.polygon.remove()

    def set_fontsize(self, fontsize):
        if self.arrow1_dim:
            self.flecha_i.set_fontsize(fontsize)
        if self.arrow2_dim:
            self.flecha_f.set_fontsize(fontsize)

    def update_polygon_size(self, scale):
        # Escalo el offset modificando p1 y p4
        self.xy[0, :] = self.pinit + (self.xy[0,:] - self.pinit) * scale
        self.xy[3, :] = self.pfin + (self.xy[3,:] - self.pfin) * scale
        # Modificar p2 y p3 segun los arrow_dim
        self.xy[1,:] = self.pinit + (self.xy[1,:] - self.pinit) * scale
        self.xy[2,:] = self.pfin + (self.xy[2,:] - self.pfin) * scale
        # Setear nuevos vértices
        self.polygon.set_xy(self.xy)

    def update_length(self, length):
        # Actualizar tamaño de flechas
        scale = length / self.length
        self.length = length
        self.set_arrow_sizes()
        # Actualizar flechas
        if self.arrow1_dim:
            self.flecha_i.update_length(length)
        if self.arrow2_dim:
            self.flecha_f.update_length(length)
        # Actualizar polígono
        self.update_polygon_size(scale)

def plot_estructura(estructura, fuerzas=True, vinculos=True, cedimientos=True,
                    indiceN=False, ejes_loc=False, temp=True, indiceB=False,
                    origen=True, axes=[]):
    """
    Llama a todas las funciones necesarias para plotear la estructura en su
    conjunto, incluyendo condiciones de vinculo, articulaciones y si las hay
    fuerzas puntuales y distribuidas.
    Las funciones a las que llama son las siguientes:
        plot_barras
        plot_vinculos
        plot_releases
        plot_fuerzas_nodales
        plot_fuerzas_distribuidas

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo el
                            ploteo.
        Fuerzas [bool]: en caso de que haya fuerzas externas definidas por el
                        usuario el mismo es 'True', y se procede a plotear
                        dichas fuerzas. En caso de que no haya fuerzas el
                        booleano es 'False' y no se procede a llamar a las
                        funciones plot_fuerzas_nodales y
                        plot_fuerzas_distribuidas.

    """
    barras = estructura.barras
    nodos = estructura.nodos

    if not axes:  # Crear figura si no es input de la función
        axes = create_axes()

    # Plotear elementos de nodos
    for nodo in nodos.values():
        if not hasattr(nodo, 'artists'): nodo.artists = {}
        nodo.artists['nodo'] = plot_nodo(nodo, axes, color='blue', marker='s',
                                         markersize=3, zorder=1)
        if fuerzas:
            nodo.artists['p'] = plot_fuerzas_nodales(nodo, axes, fontsize=8,
                                                 color='m', visible=fuerzas,
                                                 zorder=10)
        if vinculos:
            nodo.artists['v'] = plot_vinculos(nodo, axes, visible=vinculos)
        if cedimientos:
            nodo.artists['c'] = plot_cedimientos(nodo, axes, color='c',
                                               fontsize=8, visible=cedimientos,
                                               zorder=5)
        if indiceN:
            nodo.artists['indice'] = plot_indice_nodo(nodo, axes, fontsize=8,
                                                  visible=indiceN, zorder=7)

    # Plotear elementos de barras
    for barra in barras.values():
        if not hasattr(barra, 'artists'): barra.artists = {}
        barra.artists['barra'] = plot_barra(barra, axes, color='black',
                                                  linewidth=1, zorder=5)
        if ejes_loc:
            barra.artists['ejes'] = plot_ejes_locales(barra, axes, fontsize=8,
                                                        color='y', zorder=10,
                                                        visible=ejes_loc)
        if temp:
            barra.artists['e'] = plot_temperatura(barra, axes, fontsize=8,
                                                       visible=temp)
        if fuerzas:
            barra.artists['w'] = plot_fuerzas_distribuidas(barra, axes,
                                                    fontsize=8, zorder=5,
                                                    visible=fuerzas)
        if indiceB:
            barra.artists['indice'] = plot_indice_barra(barra, axes,
                                                    fontsize=8, visible=indiceB)

    # Plotear flechas del origen
    if not hasattr(estructura, 'estructura_artists'): estructura.estructura_artists = {}
    estructura.estructura_artists['origin'] = plot_originarrows(axes, visible=origen)

    # Hacer zoom a la estructura y ajustar tamaño de flechas
    zoom2extent(nodos, axes, margin=0.25)
    update_ax_arrows(axes, nodos, barras,
                     {'p':0.8, 'w':0.6, 'c':0.7, 'ejes':0.5, 'v':0.6,
                     'reacciones': 1.5},
                     0.25)

    plt.show()
    return axes

def create_axes():
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.subplots_adjust(left=0.05, right=0.95, top=0.95, bottom=0.05)
    ax.set_aspect('equal', adjustable='box', anchor='C')
    plt.title('Estructura')
    ax.set_frame_on(True)
    ax.axis([0,2,0,2])
    return ax

def plot_nodo(nodo, axes, **kwargs):
    artist = axes.plot(nodo.coords[0], nodo.coords[1], **kwargs)
    return artist

def plot_fuerzas_nodales(nodo, axes, fontsize, color='m', length=1,
                         visible=True, zorder=1):
    """
    Define para cada fuerza puntual del atributo P de la clase estructura, los
    argumentos necesarios para llamar a la función plot_flecha y poder plotear
    cada una de ellas.
    Para hacerlo itera en los DOF's de la estructura y los compara con los
    indices del vector estructura.P. En caso de que coincidan, se busca a que
    nodo pertenece el DOF en cuestion y segun sea el primero, segundo o tercer
    DOF asociado a dicho nodo se define  la coordenada en la que actua la
    fuerza. En funcion de las coordenadas del nodo se definen pos1 y pos2, y
    si se trata de una fuerza en las coordenadas X o Y se define la forma como
    'recta', en caso de que sea una fuerza en la coordenada Z se define la
    forma como 'curva'.
    Las fuerzas se dibujan todas del mismo tamaño.

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo la
                            defincion de argumentos mencionada previamente.
        axes [class]: figura donde se desean plotear las fuerzas
        scale [int]: escala con la que se desean plotear las fuerzas puntuales.
                     Esta se predefine en la funcion plot_estructura en funcion
                     del tamaño de la estructura.
    """
    off = 0.1*length
    artists = []
    arrowstyle = 'simple, head_length=10, head_width=6, tail_width=2'
    P = nodo.P
    # Fuerzas
    if P[0] != 0:
        dir = [P[0], 0]
        flecha = flechaAtenea(axes, nodo.coords, dir, label=str(P[0]),
                              fontsize=fontsize, length=length, reverse=True,
                              offset=off, lw=1, color=color,
                              visible=visible, zorder=zorder)
        flecha.set_arrowstyle(arrowstyle)
        artists.append(flecha)
    if P[1] != 0:
        dir = [0, P[1]]
        flecha = flechaAtenea(axes, nodo.coords, dir, label=str(P[1]),
                              fontsize=fontsize, length=length, reverse=True,
                              offset=off, lw=1, color=color,
                              visible=visible, zorder=zorder)
        flecha.set_arrowstyle(arrowstyle)
        artists.append(flecha)

    # Momento
    if len(P) == 3:
        if P[2] != 0:
            dir = [-P[2], 0]
            reverse = (P[2]>0)*True + (P[2]<0)*False
            flecha = flechaAtenea(axes, nodo.coords, dir, label=str(P[2]),
                                fontsize=fontsize, length=length, offset=off,
                                forma='curva', lw=1, color=color,
                                reverse=reverse, visible=visible, zorder=zorder)
            flecha.set_arrowstyle(arrowstyle)
            artists.append(flecha)
    return artists

def plot_vinculos(nodo, axes, visible=True, length=1, zorder=1, color='r'):
    """
    Plotea las condiciones de vinculos segun cual sea naturaleza:
        Simplemente apoyado: restringe desplazamientos en las direcciones x e y
        Patin en x: restringe desplazamientos en la direccion y
        Patin en y: restringe desplazamientos en la direccion x
        Empotramiento: restringe desplazamientos en las 3 coordenadas
        Comegiro: restringe giros solamente
    Para el nodo dado la funcion toma su atributo restr y plotea la condicion
    de vinculo correspondiente.
    Simplemente apoyado: [1,1,0]
    Patin en X: restr = [0,1,0]
    Patin en Y: restr = [1,0,0]
    Empotramiento: restr = [1,1,1]
    Comegiro: restr = [0,0,1]

    Argumentos:
        nodo [class]: clase definida en el modulo barra2d, que reune toda la
              informacion necesaria para llevar acabo el ploteo
        axes [class]: figura donde se desean plotear los vinculos
        scale [int]: escala con la que se desean plotear los vinculos. Esta se
               predefine en la funcion plot_estructura en funcion del tamaño
               de la estructura.

    A agregar:
        Ploteo de vinculos que restringan el giro y el desplazamiento en una
        sola direccion X o Y.
    """
    artists = []
    dim = length
    coords = nodo.coords
    apoyo = Apoyo(axes, coords, nodo.restr, dim=dim, zorder=zorder, color=color)
    return [apoyo]

def plot_cedimientos(nodo, axes, color = 'c', fontsize=8, length=1,
                     visible=True, zorder=1):
    """
    """
    offset = 0.1*length
    artists = []
    arrowstyle = 'fancy, head_length=5, head_width=5, tail_width=1.5'
    if nodo.ced[0] != 0:
        dir = [nodo.ced[0], 0]
        flecha = flechaAtenea(axes, nodo.coords, dir, label=str(nodo.ced[0]),
                              fontsize=fontsize, length=length, offset=offset,
                              lw=1, color=color, visible=visible, zorder=zorder)
        flecha.set_arrowstyle(arrowstyle)
        artists.append(flecha)
    if nodo.ced[1] != 0:
        dir = [0, nodo.ced[1]]
        flecha = flechaAtenea(axes, nodo.coords, dir, label=str(nodo.ced[1]),
                              fontsize=fontsize, length=length, offset=offset,
                              lw=1, color=color, visible=visible, zorder=zorder)
        flecha.set_arrowstyle(arrowstyle)
        artists.append(flecha)
    if len(nodo.ced) == 3:
        if nodo.ced[2] != 0:
            dir = [-nodo.ced[2], 0]
            reverse = (nodo.ced[2]>0)*True + (nodo.ced[2]<0)*False
            flecha = flechaAtenea(axes, nodo.coords, dir, label=str(nodo.ced[2]),
                                fontsize=fontsize, length=length,
                                reverse=reverse, offset=offset, forma='curva',
                                lw=1, color=color, visible=visible,
                                zorder=zorder)
            flecha.set_arrowstyle(arrowstyle)
            artists.append(flecha)
    return artists

def plot_indice_nodo(nodo, axes, fontsize=8, visible=True, zorder=1):
    text_pos = nodo.coords
    trans = (axes.get_figure().dpi_scale_trans +
             transforms.ScaledTranslation(text_pos[0], text_pos[1],
                            axes.transData))
    offset = [10/72,10/72]
    text = Text(offset[0], offset[1], '('+str(nodo.indice)+')',
                fontsize=fontsize, ha='center', va='center', zorder=zorder,
                visible=visible, transform=trans, color='b')
    axes.add_artist(text)
    return [text]

def plot_barra(barra, axes, linewidth=1, color='b', zorder=1):
    nodo_i = barra.nodos[0]
    nodo_f = barra.nodos[1]
    xplot = (nodo_i.coords[0], nodo_f.coords[0])
    yplot = (nodo_i.coords[1], nodo_f.coords[1])
    if barra.ElemName == '2DFrame':
        linestyle = '-'
    elif barra.ElemName == '2DTruss':
        linestyle = '--'
    # artist = FrameBar(xplot, yplot, *args, **kwargs)
    artist = FrameBar(axes, xplot, yplot, linewidth, color,
                      elemname=barra.ElemName, zorder=zorder)
    # Agregar releases
    if barra.rel == [0, 0, 0]:
        releases = 0
    if barra.rel == [0, 1, 0]:
        releases = 1
    if barra.rel == [0, 0, 1]:
        releases = 2
    if barra.rel == [0, 1, 1] or barra.rel == [0]: # O 2DTruss
        releases = 3
    if barra.rel == [1, 0, 0]:
        releases = 4
    if barra.rel == [1, 1, 0]:
        releases = 5
    if barra.rel == [1, 0, 1]:
        releases = 6
    if barra.rel == [1, 1, 1] or barra.rel == [1]:
        releases = 7
    artist.add_releases(releases)
    return [artist]

def plot_ejes_locales(barra, axes, fontsize = 8, color = 'y', visible=True,
                      zorder=1):
    """
    """
    off = 0
    length = 0.7
    artists=[]

    nodoi = barra.nodos[0].coords
    nodof = barra.nodos[1].coords
    xs = (nodoi[0], nodof[0])
    ys = (nodoi[1], nodof[1])

    #Eje Longitudinal x
    flecha_x = flechaAtenea(axes, [np.mean(xs), np.mean(ys)], barra.Mr[:, 0],
                            label='x', fontsize=fontsize, length=length, lw=1,
                            color=color, visible=visible, zorder=zorder)
    flecha_x.set_arrowstyle('fancy, head_length=3, head_width=3')
    artists.append(flecha_x)

    #Eje Transversal z
    flecha_z = flechaAtenea(axes, [np.mean(xs), np.mean(ys)], barra.Mr[:, 1],
                            label='z', fontsize=fontsize, length=length, lw=1,
                            color=color, visible=visible, zorder=zorder)
    flecha_z.set_arrowstyle('fancy, head_length=3, head_width=3')
    artists.append(flecha_z)
    return artists

def plot_temperatura(barra, axes, colors=['b','r'], zorder=1, fontsize=8,
                     visible=True):
    offset = 10/72
    L = barra.long
    X = np.arange(L/10, L, L/10)
    Y = np.ones(np.size(X))*0
    artists = []
    sup = np.row_stack((X, Y))
    #Posicion texto
    p1 = (barra.nodos[0].coords[0]+barra.nodos[1].coords[0])/2
    p2 = (barra.nodos[0].coords[1]+barra.nodos[1].coords[1])/2
    #Vector traslacion
    tras = np.tile(barra.nodos[0].coords, (np.size(X), 1))
    #Girar y rotar
    s_plot = np.dot(barra.Mr, sup) + tras.T
    versor_paral = np.dot(barra.Mr, np.array([1,0]))
    versor_perp = [versor_paral[1],-versor_paral[0]]
    trans_sup = axes.transData + \
            transforms.ScaledTranslation(versor_perp[0]*offset,
                                         versor_perp[1]*offset,
                                         axes.get_figure().dpi_scale_trans)
    trans_inf = axes.transData + \
                transforms.ScaledTranslation(-versor_perp[0] * offset,
                                             -versor_perp[1] * offset,
                                             axes.get_figure().dpi_scale_trans)
    # trans_text_sup = axes.transData + \
    #             transforms.ScaledTranslation(versor_perp[0] * offset *1.8,
    #                                          versor_perp[1] * offset*1.8,
    #                                          axes.get_figure().dpi_scale_trans)
    # trans_text_inf = axes.transData + \
    #             transforms.ScaledTranslation(-versor_perp[0] * offset*1.8,
    #                                          -versor_perp[1] * offset*1.8,
    #                                          axes.get_figure().dpi_scale_trans)
    # if barra.e0[0] > 0: #Estiramiento
    #     sup = Line2D(s_plot[0,:],s_plot[1,:], marker='+', linestyle='',
    #                  color = colors[1], transform=trans_sup, visible=visible)
    #     axes.add_artist(sup)
    #     text = Text(p1, p2, to_precision(barra.e0[0],3), fontsize=fontsize,
    #                 ha='center', va='center', zorder=zorder, color=colors[1],
    #                 rotation=barra.angle, transform=trans_text_sup,
    #                 visible=visible)
    #     axes.add_artist(text)
    #     artists.extend([sup, text])
    # if barra.e0[0] < 0: #Contraccion
    #     sup = Line2D(s_plot[0,:], s_plot[1,:], marker='_', linestyle='',
    #                  transform=trans_sup, color=colors[0], visible=visible)
    #     axes.add_artist(sup)
    #     text = Text(p1 ,p2, to_precision(barra.e0[0], 3), fontsize=fontsize,
    #                 ha='center', va='center', zorder=7, color=colors[0],
    #                 rotation=barra.angle, transform=trans_text_sup,
    #                 visible=visible)
    #     axes.add_artist(text)
    #     artists.extend([sup,text])
    # if barra.e0[1] > 0: #Curvatura positiva para terna derecha
    #     inf = Line2D(s_plot[0,:],s_plot[1,:], marker='+', linestyle='',
    #                  transform=trans_inf, color = colors[1], visible=visible)
    #     axes.add_artist(inf)
    #     text = Text(p1,p2,to_precision(barra.e0[1], 3), fontsize=fontsize,
    #                 ha='center', va='center', zorder=zorder, color=colors[1],
    #                 rotation=barra.angle, transform=trans_text_inf,
    #                 visible=visible)
    #     axes.add_artist(text)
    #     artists.extend([inf,text])
    # if barra.e0[1] < 0: #Curvatura negativa para terna izquierda
    #     inf = Line2D(s_plot[0,:],s_plot[1,:], marker='_', linestyle='',
    #                  transform=trans_inf, color = colors[0], visible=visible)
    #     axes.add_artist(inf)
    #     text = Text(p1, p2, to_precision(barra.e0[1], 3), fontsize=fontsize,
    #                 ha='center', va='center', zorder=zorder, color=colors[0],
    #                 rotation=barra.angle, transform=trans_text_inf,
    #                 visible=visible)
    #     axes.add_artist(text)
    #     artists.extend([inf,text])

    if barra.e0[0] > 0 and barra.e0[1] == 0: #Estiramiento
        sup = Line2D(s_plot[0,:],s_plot[1,:], marker='+', linestyle='',
                     color = colors[1], transform=trans_sup, visible=visible)
        axes.add_artist(sup)
        inf = Line2D(s_plot[0,:],s_plot[1,:], marker='+', linestyle='',
                     transform=trans_inf, color = colors[1], visible=visible)
        axes.add_artist(inf)
        artists.extend([sup,inf])
    if barra.e0[0] < 0 and barra.e0[1] == 0: #Acortamiento
        sup = Line2D(s_plot[0,:],s_plot[1,:], marker='_', linestyle='',
                     color = colors[0], transform=trans_sup, visible=visible)
        axes.add_artist(sup)
        inf = Line2D(s_plot[0,:],s_plot[1,:], marker='_', linestyle='',
                     transform=trans_inf, color = colors[0], visible=visible)
        axes.add_artist(inf)
        artists.extend([sup,inf])
    if barra.e0[1] > 0: #Curvatura negativa para terna derecha
        sup = Line2D(s_plot[0,:],s_plot[1,:], marker='+', linestyle='',
                     transform=trans_sup, color = colors[1], visible=visible)
        axes.add_artist(sup)
        inf = Line2D(s_plot[0,:],s_plot[1,:], marker='_', linestyle='',
                     transform=trans_inf, color = colors[0], visible=visible)
        axes.add_artist(inf)
        artists.extend([sup,inf])
    if barra.e0[1] < 0: #Curvatura positiva para terna derecha
        sup = Line2D(s_plot[0,:],s_plot[1,:], marker='_', linestyle='',
                     transform=trans_sup, color = colors[0], visible=visible)
        axes.add_artist(sup)
        inf = Line2D(s_plot[0,:],s_plot[1,:], marker='+', linestyle='',
                     transform=trans_inf, color = colors[1], visible=visible)
        axes.add_artist(inf)
        artists.extend([sup,inf])

    return artists

def plot_reacciones(nodo, axes, fontsize, color='m', length=1, visible=True,
                    zorder=1, precision=3, R=None):
    off = 0.1*length
    artists = []
    arrowstyle = 'simple, head_length=5, head_width=4, tail_width=1.5'
    if not R:
        R = nodo.R[:,0]
    # Fuerzas
    if R[0] != 0:
        dir = [R[0], 0]
        flecha = flechaAtenea(axes, nodo.coords, dir,
                              label=to_precision(R[0],precision),
                              fontsize=fontsize, length=length, reverse=True,
                              offset=off, lw=1, color=color,
                              visible=visible, zorder=zorder)
        flecha.set_arrowstyle(arrowstyle)
        artists.append(flecha)
    if R[1] != 0:
        dir = [0, R[1]]
        flecha = flechaAtenea(axes, nodo.coords, dir,
                              label=to_precision(R[1],precision),
                              fontsize=fontsize, length=length, reverse=True,
                              offset=off, lw=1, color=color,
                              visible=visible, zorder=zorder)
        flecha.set_arrowstyle(arrowstyle)
        artists.append(flecha)

    # Momento
    if len(R) == 3:
        if R[2] != 0:
            dir = [-R[2], 0]
            reverse = (R[2]>0)*True + (R[2]<0)*False
            flecha = flechaAtenea(axes, nodo.coords, dir,
                                label=to_precision(R[2],precision),
                                fontsize=fontsize, length=length, offset=off,
                                forma='curva', lw=1, color=color,
                                reverse=reverse, visible=visible, zorder=zorder)
            flecha.set_arrowstyle(arrowstyle)
            artists.append(flecha)
    return artists

def plot_fuerzas_distribuidas(barra, axes, fontsize=8, color='b', visible=True,
                              zorder=1, length=1):
    """
    Plotea fuerzas distribuidas. Utiliza el atributo w de fuerzas
    distribuidas de cada barra, y distingue entre terna 'global' y 'local'.
    Las fuerzas distribuidas en el eje y se representan a traves de un poligono
    y dos flechas, una al comienzo y otra al final del poligono.
    Para definir el poligono se interpola linealmente entre los valores
    inicial y final expresados en el atributo w de la barra. Para plotear las
    flechas se definen los argumentos necesarios para llamar la funcion
    plot_flecha

    Argumentos:
    estructura [class]: clase definida en el modulo model2d, que reune toda la
                        informacion necesaria para llevar acabo la defincion de
                        argumentos mencionada previamente.
    axes [class]: figura donde se desean plotear las fuerzas
    scale [int]: escala con la que se desean plotear las fuerzas puntuales.
                 Esta se predefine en la funcion plot_estructura en funcion del
                 tamaño de la estructura.

    A agregar:
        Ploteo de fuerzas distribuidas en X
    """

    # Dimensiones
    offset = 0.15*length
    arrowstyle = 'fancy, head_length=5, head_width=3'

    # Inicializa listas vacias
    wx = []
    wy = []
    artists = []
    wx.append(barra.w['wxi'])
    wx.append(barra.w['wxf'])
    wy.append(barra.w['wyi'])
    wy.append(barra.w['wyf'])

    L = barra.long
    # Computar reacciones nodales equivalentes
    wxi = barra.w['wxi']
    wyi = barra.w['wyi']
    wxf = barra.w['wxf']
    wyf = barra.w['wyf']

    # Transformación de coordenadas
    tras = barra.nodos[0].coords

    # Dibujar fuerzas distribuidas en 'y'
    if wyi == 0 and wyf == 0:
        pass
    elif wyi != 0 or wyf != 0:
        if barra.w['terna'] == 'global':
            dir = [0, 1]
            wartist = cargadistribuidaAtenea(axes, barra.nodos[0].coords, wyi,
                                             barra.nodos[1].coords, wyf, dir,
                                             color, arrowstyle, fontsize, offset,
                                             visible=visible, zorder=zorder,
                                             length=length)
            artists.append(wartist)
        elif barra.w['terna'] == 'local':
            versor_perp = np.dot(barra.Mr, np.array([0, 1]))
            wartist = cargadistribuidaAtenea(axes, barra.nodos[0].coords, wyi,
                                             barra.nodos[1].coords, wyf,
                                             versor_perp, color, arrowstyle,
                                             fontsize, offset, visible=visible,
                                             zorder=zorder, length=length)
            artists.append(wartist)

    # Dibujar carga distribuida en 'x'
    if wxi == 0 and wxf == 0:
        pass
    elif wxi != 0 or wxf != 0:
        if barra.w['terna'] == 'global':
            dir = [1, 0]
            wartist = cargadistribuidaAtenea(axes, barra.nodos[0].coords, wxi,
                                             barra.nodos[1].coords, wxf, dir,
                                             color, arrowstyle, fontsize, offset,
                                             visible=visible, zorder=zorder,
                                             length=length)
            artists.append(wartist)

        elif barra.w['terna'] == 'local':

            # Discretización a lo largo de la barra
            X = np.linspace(0.05 * L, L * 7 / 8, 6)
            XY_loc = np.row_stack((X, X * 0 + L * 0.02))
            XY_glob = np.dot(barra.Mr, XY_loc) + np.tile(barra.coords[0],
                                                    (XY_loc.shape[1], 1)).T

            # Signo de w en cada punto para direccionar cada flecha
            sign_w = np.sign(wxi + (wxf-wxi)/L*X)

            length = (X[0]-X[1])*0.8
            flechas = []
            for i in range(XY_glob.shape[1]):
                # No plotear la flecha en el punto de cambio de signo
                if i:
                    if sign_w[i]*sign_w[i-1] < 0: continue
                Pi = np.array(XY_glob[:, i])
                dir = np.dot(barra.Mr, np.array([-sign_w[i], 0]))
                flecha = Arrow2D(Pi, dir, length=length, offset=0, lw=1,
                                 color=color, visible=visible, zorder=zorder)
                flecha.set_arrowstyle(arrowstyle)
                axes.add_artist(flecha)
                flechas.append(flecha)

            # Texto
            pos_texti = barra.coords[0] + np.dot(barra.Mr, [0.05*L, 0.08*L])
            pos_textf = barra.coords[1] + np.dot(barra.Mr, [-0.05*L, 0.08*L])
            texti = axes.text(pos_texti[0], pos_texti[1], str(wxi),
                              fontsize=fontsize, horizontalalignment='center',
                              visible=visible, zorder=zorder)
            textf = axes.text(pos_textf[0], pos_textf[1], str(wxf),
                              fontsize=fontsize, horizontalalignment='center',
                              visible=visible, zorder=zorder)
            artists.extend(flechas + [texti, textf])

    return artists

def plot_indice_barra(barra, axes, fontsize, visible=True):
    L = barra.long
    text_pos = np.dot(barra.Mr, np.array([L / 2,0])) + barra.nodos[0].coords
    trans = (axes.get_figure().dpi_scale_trans +
             transforms.ScaledTranslation(text_pos[0], text_pos[1], axes.transData))
    offset = [12/72*np.cos(barra.angle+90),12/72*np.sin(barra.angle+90)]
    text = Text(offset[0], offset[1], '['+str(barra.indice)+']', fontsize=fontsize,
                ha='center', va='center', rotation=barra.angle, zorder=7,
                visible=visible, transform=trans)
    axes.add_artist(text)
    return [text]

def plot_originarrows(axes, fontsize=7, color='k', visible=True, zorder=1):
    flechax = flechaAtenea(axes, [0,0], [1,0], label='X', fontsize=fontsize,
                           length=0.5, reverse=0, offset=0, lw=1, color=color,
                           visible=visible, zorder=zorder)
    flechax.set_arrowstyle('simple, head_length=5, head_width=3')
    flechay = flechaAtenea(axes, [0,0], [0,1], label='Y', fontsize=fontsize,
                           length=0.5, reverse=0, offset=0, lw=1, color=color,
                           visible=visible, zorder=zorder)
    flechay.set_arrowstyle('simple, head_length=5, head_width=3')
    return [flechax, flechay]

def zoom2extent(nodos, ax, margin=0.05):
    tol = 0.01
    xmin, xmax, ymin, ymax= getNodesExtent(nodos)
    if (xmin == xmax) and (ymin == ymax):
        return
    xmargin = (xmax - xmin) * margin + tol
    ymargin = (ymax - ymin) * margin + tol
    max_margin = max(xmargin, ymargin)
    axis_bounds = [xmin - xmargin, xmax + xmargin,
                   ymin - ymargin, ymax + ymargin]
    max_bound = max(axis_bounds[1] - axis_bounds[0],
                    axis_bounds[3] - axis_bounds[2])
    ax.set_xlim(axis_bounds[0], axis_bounds[0] + max_bound)
    ax.set_ylim(axis_bounds[2], axis_bounds[2] + max_bound)

def getNodesExtent(nodos):
    """
    Obtener límites de los nodos
    """
    nodos_xcoords = []
    nodos_ycoords = []
    for nodo in nodos.values():
        XYZ = nodo.coords
        nodos_xcoords.append(XYZ[0])
        nodos_ycoords.append(XYZ[1])
    xmin = min(nodos_xcoords)
    xmax = max(nodos_xcoords)
    ymin = min(nodos_ycoords)
    ymax = max(nodos_ycoords)
    return xmin, xmax, ymin, ymax

def modifyTextsSize(nodos, barras, fontsize):

    # Update en nodos
    for nodo in nodos.values():
        for artist_list in nodo.artists.values():
            for artist in artist_list:
                if (type(artist) is Text) or \
                    (type(artist) is flechaAtenea):
                    artist.set_fontsize(fontsize)
    # Update en barras
    for barra in barras.values():
        for artist_list in barra.artists.values():
            for artist in artist_list:
                if (type(artist) is Text) or \
                    (type(artist) is flechaAtenea) or \
                    (type(artist) is cargadistribuidaAtenea):
                    artist.set_fontsize(fontsize)

def update_ax_arrows(axes, nodos, barras, length_flechas_rel, scale):
    ax = axes
    Lave = 1
    if barras:
        Lave = 0
        for barra in barras.values():
            L = barra.long
            Lave += L
        Lave = Lave / len(barras)
    length0 = Lave * scale

    for nodo in nodos.values():
        for key, group in nodo.artists.items():
            for artist in group:
                if (type(artist) is flechaAtenea) or (type(artist) is Arrow2D):
                    l0 = length0*length_flechas_rel[key]
                    artist.update_length(l0)
                if (type(artist) is Apoyo):
                    l0 = length0*length_flechas_rel[key]
                    artist.update_dim(l0)

    for barra in barras.values():
        for key, group in barra.artists.items():
            for artist in group:
                if (type(artist) is flechaAtenea) or (type(artist) is \
                    cargadistribuidaAtenea):
                    l0 = length0*length_flechas_rel[key]
                    artist.update_length(l0)

    return length0

def get_max_estr(estructura):
    estructura.calc_dist_max()
    return estructura.dist_max

def get_max_axis(ax):
    xmin, xmax = ax.get_xlim()
    ymin, ymax = ax.get_ylim()
    dx = np.abs(xmax-xmin)
    dy = np.abs(ymax-ymin)
    return max(dx,dy)

def hallar_maximo_diagramas(axes, Vector, Posicion, barra, bbox=None,
                            precision=3, fontsize=8):
    """
    Halla los valores maximos positivos y negativos de un esfuerzo
    caracteristico en particular para una barra. Luego, plotea el valor en el
    diagrama.

    Argumentos:
        Vector [numpy array]: El vector con los valores que toma el esfuerzo
                              caracteristico a lo largo de toda la barra.
        Posicion [numpy array: Es una matriz con las coordenadas de todos los
                               puntos que forman el diagrama de caracteristica.
    """
    # Inicializar lista de artistas texto
    text=[]

    # Direccion perpendicular de barra
    offset = 10/72
    perp = [-np.sin(barra.angle*np.pi/180), np.cos(barra.angle*np.pi/180)]

    valor_max_positivo = Vector.max()
    valor_max_negativo = Vector.min()
    list_valores = Vector.tolist()
    index_max_pos = list_valores.index(valor_max_positivo)
    index_max_neg = list_valores.index(valor_max_negativo)
    if valor_max_positivo != valor_max_negativo:
        xmax_pos = Posicion[index_max_pos+1,0]
        ymax_pos = Posicion[index_max_pos+1,1]
        xmax_neg = Posicion[index_max_neg+1,0]
        ymax_neg = Posicion[index_max_neg+1,1]
        trans_text1 = (axes.get_figure().dpi_scale_trans +
                      transforms.ScaledTranslation(xmax_pos, ymax_pos,
                                                   axes.transData))
        text1 = Text(-np.sign(valor_max_positivo)*offset*perp[0],
                     -np.sign(valor_max_positivo)*offset*perp[1],
                     (to_precision(valor_max_positivo,precision)),
                     fontsize=fontsize, visible=False, zorder=10, rotation=barra.angle,
                     bbox=bbox, transform=trans_text1, va='center')
        trans_text2 = (axes.get_figure().dpi_scale_trans +
                       transforms.ScaledTranslation(xmax_neg, ymax_neg,
                                                    axes.transData))
        text2 = Text(-np.sign(valor_max_negativo)*offset*perp[0],
                     -np.sign(valor_max_negativo)*offset*perp[1],
                     (to_precision(valor_max_negativo,precision)),
                     fontsize=fontsize,visible=False, zorder=10, rotation=barra.angle,
                     bbox=bbox, transform=trans_text2, va='center')
        text.extend([text1,text2])

    if valor_max_positivo == valor_max_negativo:
        if valor_max_positivo == 0:
            pass
        else:
            xmax_pos = Posicion[np.size(Vector)//2,0]
            ymax_pos = Posicion[np.size(Vector)//2,1]
            trans_text1 = (axes.get_figure().dpi_scale_trans +
                           transforms.ScaledTranslation(xmax_pos, ymax_pos,
                                                        axes.transData))
            text1 = Text(-np.sign(valor_max_positivo)*offset*perp[0],
                         -np.sign(valor_max_positivo)*offset*perp[1],
                         (to_precision(valor_max_positivo,precision)),
                         fontsize=fontsize,visible=False,zorder=10,
                         rotation=barra.angle,bbox=bbox, transform=trans_text1,
                         va='center')
            text.append(text1)
    for artist in text:
        artist.set_visible(True)
        axes.add_artist(artist)
    return text

def plot_deformada(estructura, axes, scale_def=1, color='b', visible=True,
                   linewidth=1, zorder=1):
    """
    """
    barras = estructura.barras
    # Definir escala
    scale = scale_def
    for barra in barras.values():    #ploteo elemento por elemento

        L = barra.long
        X = np.linspace(0, L, num=150)
        X = np.reshape(X,(150, 1))
        # Desplazamientos en coordenadas locales de la barra
        ux, uy = barra.compute_displacements(X)
        uloc = np.column_stack((ux, uy))
        uloc = uloc*scale
        # Posición deformada de la barra en coordenadas locales
        Xloc = uloc + np.column_stack((X, 0*X))
        # Transformación a coordenadas globales
        uglob = np.dot(Xloc, barra.Mr) + barra.nodos[0].coords
        Deformada = Line2D(uglob[:, 0], uglob[:, 1], color=color,
                           linewidth=linewidth, visible=visible, zorder=zorder)
        axes.add_artist(Deformada)
        if not hasattr(barra, 'artists'): barra.artists = {}
        # barra.artists['deformada'].append(Deformada)
        barra.artists['deformada'] = [Deformada]

def plot_diagramas_global(estructura, axes, diagrama, linewidth=2, zorder=1,
                          colors=['b','r'], scale_d=1, precision=3, fontsize=8):
    """
    Plotea el diagrama de caracteristica que se le pida. Primero, predefine
    las escalas de los diagramas de las tres solicitaciones en funcion del
    tamaño de la estructura y de los máximos esfuerzos caracteristicos. Para
    calcular los esfuerzos caracteristicos, iterando barra por barra se llama
    al metodo del elemento barra compute_internal_forces. Luego se define
    un poligono para representar la solicitacion de cada barra, se lo gira y
    traslada y luego se lo plotea. Luego se llama a la funcion
    hallar_maximo_diagramas para identificar los valores maximos de la
    solicitacion en la barra y plotear su valor donde corresponde.
    El ploteo de los diagramas se hace barra por barra.

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune
                            toda la informacion necesaria para llevar acabo el
                            ploteo.
        axes [class]: figura donde se desean plotear los diagramas.
        diagrama [str]: que define que diagrama se desea plotear, 'N' para
                        plotear diagrama de esfuerzo normal, 'Q' para plotear
                        diagrama de esfuerzo de corte y 'M' para plotear
                        diagrama de momentos flexores.
        scale [int]: argumento defualt, en caso de que el usuario desee definir
                     la escala de los diagramas lo puede hacer definiendo su
                     valor. Si el usuario no lo define, la escala se calcula
                     automaticamente en funcion del tamaño de la estructura y
                     de los valor maximos de los diagramas.
        barra [class]: clase definida en el modulo bar2d

    """
    barras = estructura.barras
    Nx = 200

    # Obtener valor maximo para definir escala
    Smax = 0
    for barra in barras.values():
        # Calcular solicitaciones en barra
        L = barra.long
        # Definir los X
        X = np.linspace(0, L, num=Nx)
        X = np.reshape(X, (Nx, 1))
        N, M, Q = barra.compute_internal_forces(X)
        # Recortar por tolerancia

        # Calcular máximos
        if diagrama == 'N':
            Smax = np.max((Smax,np.max(np.abs(N))))
        elif diagrama == 'Q':
            Smax = np.max((Smax,np.max(np.abs(Q))))
        elif diagrama == 'M':
            Smax = np.max((Smax,np.max(np.abs(M))))
    # Obtener dimensiones de estructura
    xmin, xmax, ymin, ymax = estructura.getNodesExtent()
    lave = ((xmax-xmin)+(ymax-ymin))/2/10
    scale = 1
    if Smax != 0:
        scale = lave/Smax * scale_d

    # Definir esfuerzos nodales de cada elemento para poder calcular sus esfuerzos
    # internos
    k = 0
    for barra in barras.values():
        # Calcular solicitaciones en barra
        L = barra.long
        # Definir los X
        X = np.linspace(0, L, num=Nx)
        X = np.reshape(X,(Nx,1))
        N,M,Q = barra.compute_internal_forces(X)

        # Generar vectores de traslacion
        n = len(X) + 2
        tras = np.tile(barra.nodos[0].coords,(n,1))

        if diagrama == 'Q':
            S = Q
        elif diagrama == 'M':
            S = M
        elif diagrama == 'N':
            S = N

        barra.artists[diagrama] = []
        if any(S) != 0:

            S_escalado = np.column_stack((X,S*scale))
            S_aux = S_escalado
            S_totales = [] #Lista de matrices para definir los distintos poligonos

            for n in range(S_aux.shape[0]):  # Iterar en las filas de S_aux
                if n == 0:  # Primera iteracion
                    Ss = S_aux[n, :]
                    continue
                if S_aux[n, 1]*S_aux[n-1,1] > 0: # S del mismo signo
                    Ss = np.vstack((Ss, S_aux[n, :]))
                    if n == S_aux.shape[0] - 1: # ultima iteracion
                        S_totales.append(Ss)
                    continue
                elif S_aux[n, 1]*S_aux[n-1, 1] < 0: # S distinto signo
                    S_totales.append(Ss)
                    Ss = S_aux[n, :]
                    continue
                elif S_aux[n, 1]*S_aux[n-1,1] == 0 and S_aux[n-1, 1]==0 and \
                     S_aux[n, 1]!=0:
                    S_totales.append(Ss)
                    Ss = S_aux[n, :]
                    continue
                elif S_aux[n, 1]*S_aux[n-1,1] == 0 and S_aux[n, 1]==0 and \
                     S_aux[n-1, 1]!=0:
                    Ss = np.vstack((Ss, S_aux[n, :]))
                    if n == S_aux.shape[0] - 1: # ultima iteracion
                        S_totales.append(Ss)
                    continue
                elif S_aux[n,1]==0 and S_aux[n-1, 1]==0:
                    Ss = np.vstack((Ss, S_aux[n, :]))
                    if n == S_aux.shape[0] - 1: # ultima iteracion
                        S_totales.append(Ss)
                    continue

            #Extremos del diagrama
            S_ext_init = np.array([[0, 0]])
            S_ext_fin = np.array([[L, 0]])
            # Completar con punto inicial y final de los polígonos
            S_totales[0] = np.row_stack((S_ext_init, S_totales[0]))
            S_totales[-1] = np.row_stack((S_totales[-1], S_ext_fin))
            #Generar vector completo para definir poligono
            S_aux = np.row_stack((S_ext_init, S_escalado, S_ext_fin))

            #Diagrama de corte total de la barra, girado y trasladado
            S_plot = np.dot(barra.Mr, S_aux.T) + tras.T
            #Hallar y plotear valores maximos y minimos
            texts = hallar_maximo_diagramas(axes, S, S_plot.T, barra,
                                            bbox=dict(facecolor='white',
                                                        edgecolor='black',
                                                        boxstyle='round',
                                                        alpha=0.6),
                                            precision=precision,
                                            fontsize=fontsize)
            for text in texts:
                barra.artists[diagrama].append(text)

            for S in S_totales:
                if any([s >= 0 for s in S[:,1]]): #Corte negativo
                    color = colors[1]
                if any([s < 0 for s in S[:,1]]): #Corte positivo
                    color = colors[0]
                tras = np.tile(barra.nodos[0].coords, (S.shape[0],1))
                #Girar y trasladar
                S_plot = np.dot(barra.Mr, S.T) + tras.T
                #Graficar Corte
                Pol = Polygon(S_plot.T, color = color, ls = 'solid',
                              lw = linewidth, alpha=0.5, visible=True,
                              zorder=zorder)
                barra.artists[diagrama].append(Pol)
                axes.add_artist(Pol)

def plot_diagramas_local(barra, axes, diagrama, x, precision=3,
                         fontsize=8, linewidth=1, colors=['b','r']):
    """
    Plotea el diagrama de caracteristica que se le pida. Primero, predefine
    las escalas de los diagramas de las tres solicitaciones en funcion del
    tamaño de la estructura y de los máximos esfuerzos caracteristicos. Para
    calcular los esfuerzos caracteristicos, iterando barra por barra se llama
    al metodo del elemento barra compute_internal_forces. Luego se define
    un poligono para representar la solicitacion de cada barra, se lo gira y
    traslada y luego se lo plotea. Luego se llama a la funcion
    hallar_maximo_diagramas para identificar los valores maximos de la
    solicitacion en la barra y plotear su valor donde corresponde.
    El ploteo de los diagramas se hace barra por barra.

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune
                            toda la informacion necesaria para llevar acabo el
                            ploteo.
        axes [class]: figura donde se desean plotear los diagramas.
        diagrama [str]: que define que diagrama se desea plotear, 'N' para
                        plotear diagrama de esfuerzo normal, 'Q' para plotear
                        diagrama de esfuerzo de corte y 'M' para plotear
                        diagrama de momentos flexores.
        x [num]:
        scale [int]: argumento defualt, en caso de que el usuario desee definir
                     la escala de los diagramas lo puede hacer definiendo su
                     valor. Si el usuario no lo define, la escala se calcula
                     automaticamente en funcion del tamaño de la estructura y
                     de los valor maximos de los diagramas.
        barra [class]: clase definida en el modulo bar2d

    """

    # Calcular solicitaciones en barra
    L = barra.long
    X = np.linspace(0, L, num=1000)
    X = np.reshape(X, (1000, 1))
    N, M, Q = barra.compute_internal_forces(X)
    Nx, Mx, Qx = barra.compute_internal_forces(x)
    # text alignment
    if x < L/2:
        factor = L/40
        alignment = 'left'
    elif x >= L/2:
        factor = -L/40
        alignment = 'right'
    artists = []
    #   Generar vectores de traslacion
    n = len(X) + 2

    if diagrama == 'N':
        E_escalado = np.column_stack((X, N))
        # Valor para el marker
        y = Nx[0]
    if diagrama == 'M':
        E_escalado = np.column_stack((X, M))
        # Valor para el marker
        y = Mx[0]
    if diagrama == 'Q':
        E_escalado = np.column_stack((X, Q))
        # Valor para el marker
        y = Qx[0]

    # Generar vector completo para definir poligono
    E_aux = E_escalado
    E_totales = []  # Lista de matrices para definir los distintos poligonos
    for n in range(E_aux.shape[0]):  # Iterar en las filas de E_aux
        if n == 0:  # Primera iteracion
            E = E_aux[n, :]
            continue
        if E_aux[n, 1]*E_aux[n-1,1] > 0: # E del mismo signo
            E = np.vstack((E, E_aux[n, :]))
            if n == E_aux.shape[0] - 1: # ultima iteracion
                E_totales.append(E)
            continue
        elif E_aux[n, 1]*E_aux[n-1, 1] < 0: # E distinto signo
            E_totales.append(E)
            E = E_aux[n, :]
            continue
        elif E_aux[n, 1]*E_aux[n-1,1] == 0 and E_aux[n-1, 1]==0 and E_aux[n, 1]!=0:
            E_totales.append(E)
            E = E_aux[n, :]
            continue
        elif E_aux[n, 1]*E_aux[n-1,1] == 0 and E_aux[n, 1]==0 and E_aux[n-1, 1]!=0:
            E = np.vstack((E, E_aux[n, :]))
            if n == E_aux.shape[0] - 1: # ultima iteracion
                E_totales.append(E)
            continue
        elif E_aux[n,1]==0 and E_aux[n-1, 1]==0:
            E = np.vstack((E, E_aux[n, :]))
            if n == E_aux.shape[0] - 1: # ultima iteracion
                E_totales.append(E)
            continue

    # Extremos del diagrama
    E_ext_init = np.array([[0, 0]])
    E_ext_fin = np.array([[L, 0]])
    # Completar con punto inicial y final de los poligoonos
    E_totales[0] = np.row_stack((E_ext_init, E_totales[0]))
    E_totales[-1] = np.row_stack((E_totales[-1], E_ext_fin))

    for E in E_totales:
        if any([n >= 0 for n in E[:, 1]]):  # Esfuerzo positivo
            color = colors[0]
        if any([n < 0 for n in E[:, 1]]):  # Esfuerzo negativo
            color = colors[1]
        # Graficar Normal
        Esfuerzo = Polygon(E, color=color, ls='solid', lw=linewidth, alpha=0.5,
                        visible=True, zorder=1)
        axes.add_patch(Esfuerzo)
        artists.append(Esfuerzo)

    # Agregar marcador de posición x
    if y>=0: color=colors[0]
    else: color=colors[1]
    Marker = Line2D([x, x], [0, y], linewidth=2, color='y', zorder=1)
    axes.add_artist(Marker)
    Marker_text = Text(x+factor, y*0.8, to_precision(y, precision),
                    fontsize=fontsize,
                    horizontalalignment=alignment, zorder=2,
                    bbox=dict(facecolor='white', edgecolor=color,
                              boxstyle='round', alpha=0.6))
    axes.add_artist(Marker_text)
    artists.append(Marker_text)
    axes.autoscale(axis='x', tight=True)
    axes.autoscale(axis='y', tight=None)

    return artists

def definir_escala_diagramas(estructura, diagrama):
    # Obtener valor maximo para definir escala
    Smax = 0
    for barra in estructura.barras.values():
        # Calcular solicitaciones en barra
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X, (1000, 1))
        N, M, Q = barra.compute_internal_forces(X)

        # Calcular máximos
        if diagrama == 'N':
            Smax = np.max((Smax,np.max(np.abs(N))))
        elif diagrama == 'Q':
            Smax = np.max((Smax,np.max(np.abs(Q))))
        elif diagrama == 'M':
            Smax = np.max((Smax,np.max(np.abs(M))))

    # Obtener dimensiones de estructura
    max_d = get_max_estr(estructura)/15
    scale = 1
    if Smax != 0:
        scale = max_d/Smax
    return scale

def calcular_delta_MAX_diagramas(diagrama, axes, estructura, scale):
    """
    Solo funciona cuando de entrada los diagramas estan por fuera de los
    limites del ax.

    """
    #Obtengo los limites de los ejes
    x_lim = axes.get_xlim()
    y_lim = axes.get_ylim()

    #Inicializo lista de valores maximos/minimos de coordenadas
    x_max = []
    y_max = []
    x_min = []
    y_min = []
    for barra in estructura.barras.values():
        #Definir X
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X,(1000,1))
        #Calcular solicitaciones
        N,M,Q = barra.compute_internal_forces(X)
        #Definir Y
        if diagrama == 'Q':
            Y = Q
        if diagrama == 'M':
            Y = -M
        if diagrama == 'N':
            Y = -N
        #Vector traslacion
        tras = np.tile(barra.nodos[0].coords,(len(X),1))
        #Matriz con las coordenadas X e Y del diagrama sin rotar ni escalar
        XY_aux = np.column_stack((X,Y*scale))
        XY = np.dot(barra.Mr,XY_aux.T) + tras.T
        x_max.append(max(XY[0,:])) #!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Hay algo raro con esto, no estoy llamando a toda la columna 0
        y_max.append(max(XY[1,:]))
        x_min.append(min(XY[0,:]))
        y_min.append(min(XY[1,:]))

    #Coordenadas maximas/minimas globales
    X_MAX = max(x_max)
    Y_MAX = max(y_max)
    X_MIN = min(x_min)
    Y_MIN = min(y_min)
    #Calculo los deltas con los limites de los ejes
    delta_X_MAX = np.absolute(x_lim[1]) - np.absolute(X_MAX)
    delta_Y_MAX = np.absolute(y_lim[1]) - np.absolute(Y_MAX)
    delta_X_MIN = np.absolute(x_lim[0]) - np.absolute(X_MIN)
    delta_Y_MIN = np.absolute(y_lim[0]) - np.absolute(Y_MIN)

    delta_MAX = min(delta_X_MAX,delta_Y_MAX,delta_X_MIN,delta_Y_MIN)

    return delta_MAX

def definir_escala_deformada(estructura):
    # Busco el punto mas deformado
    barras = estructura.barras
    max_d = 0
    for barra in barras.values():
        L = barra.long
        X = np.linspace(0, L, num=1000)
        X = np.reshape(X, (1000, 1))
        # Desplazamientos en coordenadas locales de la barra
        ux, uy = barra.compute_displacements(X)
        delta_max = max((ux**2 + uy**2)**0.5)
        max_d = max(max_d, delta_max)
    scale_def = 0.2*get_max_estr(estructura)/max_d
    return scale_def

def plot_resultados(estructura, axes=None, reacciones=True, scale_def=None,
                    deformada=True, diagrama=False):
    """
    Llama a las funciones plot_diagramas y plot_reacciones. Define la escala
    con la que se plotean las reacciones en funcion del tamaño de la
    estructura.

    Argumentos:
        estructura [class]: clase definida en el modulo model2d, que reune toda
                            la informacion necesaria para llevar acabo el
                            ploteo.
        diagrama [str]: define que diagrama se desea plotear, 'N' para
                        plotear diagrama de esfuerzo normal, 'Q' para plotear
                        diagrama de esfuerzo de corte y 'M' para plotear
                        diagrama de momentos flexores.
        scale_d [int]: argumento default, en caso de que el usuario desee
                       definir la escala de los diagramas lo puede hacer
                       definiendo su valor. Si el usuario no lo define, la
                       escala se calcula automaticamente en funcion del tamaño
                       de la estructura y de los valor maximos de los
                       diagramas.
        diagramas [bool]: si se desea plotear diagramas se define como 'True',
                          si no se desea plotear diagramas se define como
                          'False'.
        reacciones [bool]: si se deasea plotear las reacciones se define como
                           'True', si no se desea plotear las reacciones se
                           define como 'False'.
    """
    if not axes:  # Crear figura si no es input de la función
        axes = create_axes()

    # Plotear Deformada
    if deformada:
        if scale_def == None: scale_def = definir_escala_deformada(estructura)
        plot_deformada(estructura, axes, scale_def)
    # Plotear Reacciones
    if reacciones:
        for nodo in estructura.nodos.values():
            nodo.artists['reacciones'] = plot_reacciones(nodo, axes, fontsize=8,
                                                color='m', length=1,
                                                visible=True, zorder=1,
                                                precision=3)
    plot_estructura(estructura, axes=axes, fuerzas=False, temp=False,
                    cedimientos=False, vinculos=False)
    # Plotear Diagramas
    if diagrama:
        scale_diag = definir_escala_diagramas(estructura, diagrama)
        plot_diagramas_global(estructura, axes, diagrama, scale_diag)

    update_ax_arrows(axes, estructura.nodos, estructura.barras,
                     {'p':0.8, 'w':0.6, 'c':0.7, 'ejes':0.5, 'v':0.6,
                     'reacciones': 0.5},
                     0.25)
    plt.show()
    return axes

def plot_barra_q_ini(axes, barra, q, color='r', length=1, fontsize=8, zorder=1,
                     linewdith=1):

    off = 0.1*length
    artists = []
    arrowstyle = 'simple, head_length=9, head_width=6, tail_width=2'

    if q: #Fuerza unitaria relativa
        L = barra.long
        nodo_init = barra.nodos[0]
        nodo_end = barra.nodos[1]
        dx = nodo_end.coords[0] - nodo_init.coords[0]
        dy = nodo_end.coords[1] - nodo_init.coords[1]
        # coordenadas del centro de barra
        center = (nodo_init.coords + np.dot(np.array([L / 2, 0]), barra.Mr))
        if barra.ElemName == '2DTruss':
            if q[0] != 0:
                forma = 'recta'
                point_1=np.array([center[0]-0.05*dx,center[1]-0.05*dy])
                dir = list(np.dot(np.array([-1, 0]), barra.Mr))
                flecha_1 = flechaAtenea(axes, point_1, dir, label=str(q[0]),
                                        fontsize=fontsize, length=length,
                                        reverse=False, offset=off, lw=linewdith,
                                        color=color, visible=True, zorder=zorder,
                                        forma=forma)
                flecha_1.set_arrowstyle(arrowstyle)
                artists.append(flecha_1)
                point_2=np.array([center[0]+0.05*dx,center[1]+0.05*dy])
                dir = list(np.dot(np.array([1,0]),barra.Mr))
                flecha_2 = flechaAtenea(axes, point_2, dir, label=str(q[0]),
                                      fontsize=fontsize, length=length,
                                      reverse=False, offset=off, lw=linewdith,
                                      color=color, visible=True, zorder=zorder,
                                      forma=forma)
                flecha_2.set_arrowstyle(arrowstyle)
                artists.append(flecha_2)
        if barra.ElemName == '2DFrame':

            if q[0] != 0:
                forma = 'recta'
                point_1 = np.array([center[0] - 0.05*dx, center[1] - 0.05*dy])
                dir = list(np.dot(np.array([-1,0]),barra.Mr))
                flecha_1 = flechaAtenea(axes, point_1, dir, label=str(q[0]),
                                      fontsize=fontsize, length=length,
                                      reverse=False, offset=off, lw=linewdith,
                                      color=color, visible=True, zorder=zorder,
                                      forma=forma)
                flecha_1.set_arrowstyle(arrowstyle)
                artists.append(flecha_1)
                point_2 = np.array([center[0] + 0.05*dx, center[1] + 0.05*dy])
                dir = list(np.dot(np.array([1, 0]), barra.Mr))
                flecha_2 = flechaAtenea(axes, point_2, dir, label=str(q[0]),
                                      fontsize=fontsize, length=length,
                                      reverse=False, offset=off, lw=linewdith,
                                      color=color, visible=True, zorder=zorder,
                                      forma=forma)
                flecha_2.set_arrowstyle(arrowstyle)
                artists.append(flecha_2)
            if q[1] != 0: #Fuerza unitaria relativa momento inicial
                forma = 'curva'
                point_1 = np.array([nodo_init.coords[0], nodo_init.coords[1]])
                # dir = [-dx, dy]
                dir = [-dy, dx]
                flecha_1 = flechaAtenea(axes, point_1, dir, label=str(q[1]),
                                    fontsize=fontsize, length=length,
                                    reverse=False, offset=off, lw=linewdith,
                                    color=color, visible=True, zorder=zorder,
                                    forma=forma)
                flecha_1.set_arrowstyle(arrowstyle)
                artists.append(flecha_1)
                point_2 = np.array([nodo_init.coords[0]+0.1*dx,
                                    nodo_init.coords[1]+0.1*dy])
                # dir = [dx, dy]
                dir = [-dy, dx]
                flecha_2 = flechaAtenea(axes, point_2, dir, label=str(q[1]),
                                    fontsize=fontsize, length=length,
                                    reverse=True, offset=off, lw=linewdith,
                                    color=color, visible=False, zorder=zorder,
                                    forma=forma)
                flecha_2.set_arrowstyle(arrowstyle)
                artists.append(flecha_2)
            if q[2] != 0: #Fuerza unitaria relativa momento final
                forma = 'curva'
                point_1 = np.array([nodo_end.coords[0]-0.1*dx,
                                    nodo_end.coords[1]-0.1*dy])
                # dir = [-dx, dy]
                dir = [-dy, dx]
                flecha_1 = flechaAtenea(axes, point_1, dir, label=str(q[2]),
                                    fontsize=fontsize, length=length,
                                    reverse=False, offset=off, lw=linewdith,
                                    color=color, visible=True, zorder=zorder,
                                    forma=forma)
                flecha_1.set_arrowstyle(arrowstyle)
                artists.append(flecha_1)
                point_2 = np.array([nodo_end.coords[0], nodo_end.coords[1]])
                # dir = [dx, dy]
                dir = [-dy, dx]
                flecha_2 = flechaAtenea(axes, point_2, dir, label=str(q[2]),
                                      fontsize=fontsize, length=length,
                                      reverse=True, offset=off, lw=linewdith,
                                      color=color, visible=True, zorder=zorder,
                                      forma=forma)
                flecha_2.set_arrowstyle(arrowstyle)
                artists.append(flecha_2)
    return artists
