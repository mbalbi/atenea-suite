# módulo dialogs.py
"""
Popus durante el GUI

creado por: equipo Atenea-UBATIC
"""
import sys, os

from PyQt5.QtGui import QDoubleValidator
from PyQt5 import QtCore, QtGui, QtWidgets

from matplotlib.text import Text

import numpy as np

from .interactiveCanvas import AteneaCanvasQt
from ..core import graph2d as graph
from ..core import analysis1el
from .flexibilidades_paso_a_paso import check_statics_sistemas
from ..core import auxiliar

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def matrices_dialog(parent, Estructura, met):
    ui = matricesDialog(parent)
    ui.completarTablas(Estructura, met)
    ui.show()

class matricesDialog(QtWidgets.QDialog):

    def __init__(self, parent=None):

        super(matricesDialog, self).__init__(parent)
        self.setWindowTitle('Matrices')

        # Widgets
        # Splitter
        self.splitter = QtWidgets.QSplitter()
        self.splitter.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.splitter.setFrameShadow(QtWidgets.QFrame.Plain)
        self.splitter.setOrientation(QtCore.Qt.Horizontal)
        self.splitter.setOpaqueResize(True)
        self.splitter.setObjectName("splitter")
        # Tabla de matriz
        self.frameRigidez = QtWidgets.QFrame(self.splitter)
        self.verticalLayout_1 = QtWidgets.QVBoxLayout(self.frameRigidez)
        self.label_1 = QtWidgets.QLabel(self.frameRigidez)
        self.verticalLayout_1.addWidget(self.label_1)
        self.tableRigideces = QtWidgets.QTableWidget(self.frameRigidez)
        font = QtGui.QFont()
        font.setPointSize(8)
        self.tableRigideces.setFont(font)
        self.tableRigideces.setSizeAdjustPolicy(
                            QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tableRigideces.setAlternatingRowColors(True)
        self.tableRigideces.setSelectionMode(
                            QtWidgets.QAbstractItemView.SingleSelection)
        self.tableRigideces.setSelectionBehavior(
                            QtWidgets.QAbstractItemView.SelectRows)
        self.tableRigideces.setObjectName("tableRigideces")
        self.tableRigideces.setColumnCount(0)
        self.tableRigideces.setRowCount(0)
        self.verticalLayout_1.addWidget(self.tableRigideces)
        # Tabla de términos de causa
        self.frameCausa = QtWidgets.QFrame(self.splitter)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.frameCausa)
        self.label_2 = QtWidgets.QLabel(self.frameCausa)
        self.verticalLayout_2.addWidget(self.label_2)
        self.tableTerminosdeCausa = QtWidgets.QTableWidget(self.frameCausa)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
                self.tableTerminosdeCausa.sizePolicy().hasHeightForWidth())
        self.tableTerminosdeCausa.setSizePolicy(sizePolicy)
        self.tableTerminosdeCausa.setObjectName("tableTerminosdeCausa")
        self.tableTerminosdeCausa.setColumnCount(0)
        self.tableTerminosdeCausa.setRowCount(0)
        self.verticalLayout_2.addWidget(self.tableTerminosdeCausa)

        # Layout
        layout = QtWidgets.QVBoxLayout()
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(self.splitter)
        self.setLayout(layout)
        self.splitter.setSizes([3500,1000])

    def completarTablas(self, estructura, met):
        if met==1: # Método de rigideces
            # Términos de causa
            self.label_2.setText('Términos de causa')
            self.tableTerminosdeCausa.setVisible(True)
            self.tableTerminosdeCausa.setRowCount(
                                    len(estructura.Ptot))
            self.tableTerminosdeCausa.setColumnCount(
                                    len(estructura.Ptot[0]))
            for i, row in enumerate(estructura.Ptot):
                val = row[0]
                item = QtWidgets.QTableWidgetItem(
                                        str(auxiliar.to_precision(val, 5)))
                self.tableTerminosdeCausa.setItem(i, 0, item)
            # Matriz de rigidez
            self.label_1.setText('Matriz de rigidez')
            self.tableRigideces.setVisible(True)
            self.tableRigideces.setRowCount(
                                        len(estructura.Kf))
            self.tableRigideces.setColumnCount(
                                        len(estructura.Kf[0]))
            for i, row in enumerate(estructura.Kf):
                for j, val in enumerate(row):
                    item = QtWidgets.QTableWidgetItem(
                                        str(auxiliar.to_precision(val, 5)))
                    self.tableRigideces.setItem(i, j, item)
        if met==2: # Método de flex.
            # Chequear que no esté vacía (caso de isostáticos)
            if len(estructura.e0) > 0:
                # Términos de causa
                self.label_2.setText('Términos de causa')
                self.tableTerminosdeCausa.setVisible(True)
                self.tableTerminosdeCausa.setRowCount(
                                            len(estructura.e0))
                self.tableTerminosdeCausa.setColumnCount(
                                            len(estructura.e0[0]))
                for i, row in enumerate(estructura.e0):
                    val = row[0]
                    item = QtWidgets.QTableWidgetItem(
                                            str(auxiliar.to_precision(val, 5)))
                    self.tableTerminosdeCausa.setItem(i, 0, item)
                # Matriz de flexibilidad
                self.label_1.setText('Matriz de Flexibilidad')
                self.tableRigideces.setVisible(True)
                self.tableRigideces.setRowCount(
                                            len(estructura.F))
                self.tableRigideces.setColumnCount(
                                            len(estructura.F[0]))
                for i, row in enumerate(estructura.F):
                    for j, val in enumerate(row):
                        item = QtWidgets.QTableWidgetItem(
                                            str(auxiliar.to_precision(val, 5)))
                        self.tableRigideces.setItem(i, j, item)
        # Resize table to contents
        self.tableRigideces.resizeColumnsToContents()
        self.tableTerminosdeCausa.resizeColumnsToContents()

    def exec_(self, values=None):
        # Ejecutar dialog
        super(matricesDialog, self).exec_()
        return

class saveDialog(QtWidgets.QMessageBox):

    def __init__(self, parent=None):
        super(saveDialog, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Question)
        self.setStandardButtons(QtWidgets.QMessageBox.Yes | \
                                QtWidgets.QMessageBox.No| \
                                QtWidgets.QMessageBox.Cancel)
        self.setWindowTitle('Guardar archivo')
        self.setText('Quiere guardar el archvo?')

    def exec_(self):
        ret = self.exec()
        if ret == QtWidgets.QMessageBox.Yes:
            ok = 1
        elif ret == QtWidgets.QMessageBox.No:
            ok = 0
        elif ret == QtWidgets.QMessageBox.Cancel:
            ok = 2
        return ok

class errorDialog(QtWidgets.QMessageBox):

    def __init__(self, parent=None):
        super(errorDialog, self).__init__(parent)

        self.setIcon(QtWidgets.QMessageBox.Critical)
        self.setStandardButtons(QtWidgets.QMessageBox.Ok)
        self.setWindowTitle('Error')

    def error_catch(self, info, ex=None):
        """

        :return:
        """
        template = "Ocurrió un error de tipo {0}. Argumentos:\n{1!r}"
        if ex:
            message = template.format(type(ex).__name__, ex.args)
            self.setInformativeText(message)
        self.setText(info)
        self.exec_()
        return

class customDialog(QtWidgets.QDialog):

    def __init__(self, labels, title, description=None, parent=None):

        super(customDialog, self).__init__(parent)

        # Widgets
        self.qlabels = [QtWidgets.QLabel(label+":") for label in labels]
        self.edits = [QtWidgets.QLineEdit() for label in labels]
        buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok |
                                     QtWidgets.QDialogButtonBox.Cancel)
        if description:
            self.descr_label = QtWidgets.QLabel(description)

        # Layout
        layout = QtWidgets.QGridLayout()

        row = 0
        for label in self.qlabels:
            layout.addWidget(label, row, 0)
            layout.addWidget(self.edits[row], row, 1)
            row += 1

        if description:
            font = QtGui.QFont()
            font.setPointSize(6)
            self.descr_label.setFont(font)
            layout.addWidget(self.descr_label, row, 0)
            row += 1

        layout.addWidget(buttonBox, row, 0, 1, 2)
        self.setLayout(layout)

        # Functionality
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle(title)

        # INput validation
        onlyDouble = QDoubleValidator()
        for edit in self.edits[1:]:
            edit.setValidator(onlyDouble)

    def accept(self):
        self.ok = True
        QtWidgets.QDialog.accept(self)

    def reject(self):
        self.ok = False
        QtWidgets.QDialog.reject(self)

    def exec_(self, values=None):
        # Add user input values if input
        if values:
            count = 0
            for edit in self.edits:
                edit.setText(values[count])
                count += 1
        else:
            for edit in self.edits:
                edit.setText('')

        # Ejecutar dialog
        super(customDialog, self).exec_()

        # Leer valores
        returnvalues = []
        for edit in self.edits:
            returnvalues.append(edit.displayText())

        return returnvalues, self.ok


class runDialog(QtWidgets.QDialog):

    def __init__(self, estructura, parent=None):

        super(runDialog, self).__init__(parent)
        # Widgets
        est_label = QtWidgets.QLabel("Grado de estaticidad: " + \
                           str(estructura.est['grado'])+ "("+\
                           str(estructura.est['tipo']) + ")")
        hline = QtWidgets.QFrame()
        hline.setFrameShape(QtWidgets.QFrame.HLine)
        hline.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.isostaticoButton = QtWidgets.QRadioButton()
        self.isostaticoButton.setText("Equilibrio de fuerzas")
        self.rigidecesButton = QtWidgets.QRadioButton()
        self.rigidecesButton.setText("Método de rigideces")
        self.flexibilidadesButton = QtWidgets.QRadioButton()
        self.flexibilidadesButton.setText("Método de flexibilidades")
        hline_2 = QtWidgets.QFrame()
        hline_2.setFrameShape(QtWidgets.QFrame.HLine)
        hline_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.papButton = QtWidgets.QCheckBox()
        self.papButton.setText("Ver Sistemas Fundamentales")
        buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok |
                                     QtWidgets.QDialogButtonBox.Cancel)
        # Disable or enable buttons
        if estructura.est['grado'] > 0:
            self.isostaticoButton.setEnabled(False)
            self.rigidecesButton.setEnabled(True)
            self.flexibilidadesButton.setEnabled(True)
            self.papButton.setEnabled(True)
            self.rigidecesButton.setChecked(True)
        elif estructura.est['grado'] < 0:
            self.isostaticoButton.setEnabled(False)
            self.rigidecesButton.setEnabled(False)
            self.flexibilidadesButton.setEnabled(False)
            self.papButton.setEnabled(False)
        elif estructura.est['grado'] == 0:
            self.isostaticoButton.setEnabled(True)
            self.rigidecesButton.setEnabled(True)
            self.flexibilidadesButton.setEnabled(True)
            self.rigidecesButton.setChecked(True)
            self.papButton.setEnabled(True)

        # Layout
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(est_label)
        layout.addWidget(hline)
        layout.addWidget(self.isostaticoButton, 0)
        layout.addWidget(self.rigidecesButton, 1)
        layout.addWidget(self.flexibilidadesButton, 2)
        layout.addWidget(hline_2,3)
        layout.addWidget(self.papButton, 4)
        layout.addWidget(buttonBox, 6)
        self.setLayout(layout)

        # Functionality
        self.rigidecesButton.toggled.connect(self.setpapEnabled)
        self.flexibilidadesButton.toggled.connect(self.setpapEnabled)
        self.isostaticoButton.toggled.connect(self.setpapEnabled)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)
        self.setWindowTitle('Correr estructura')

        self.adjustSize()

    def setpapEnabled(self):
        if self.isostaticoButton.isChecked():
            self.papButton.setChecked(False)
            self.papButton.setEnabled(False)
        if self.rigidecesButton.isChecked():
            self.papButton.setEnabled(True)
        if self.flexibilidadesButton.isChecked():
            self.papButton.setEnabled(True)

    def accept(self):
        self.ok = True
        QtWidgets.QDialog.accept(self)

    def reject(self):
        self.ok = False
        QtWidgets.QDialog.reject(self)

    def exec_(self):
        super(runDialog, self).exec_()
        if self.papButton.isChecked():
            pap = True
        else: pap=False
        if self.isostaticoButton.isChecked():
            return 0, pap, self.ok
        if self.rigidecesButton.isChecked():
            return 1, pap, self.ok
        if self.flexibilidadesButton.isChecked():
            return 2, pap, self.ok
        return -1, -1, False

def flexibilidades_dialog():
    ui = flexibilidadesDialog()
    ui.setMinimumSize(1100, 700)
    ui.exec_()

class flexibilidadesDialog(QtWidgets.QDialog):

    def __init__(self, estructura):
        super(flexibilidadesDialog, self).__init__()

        self.Estructura = estructura
        self.ui = flexibilidadesUI()
        self.ui.setupUi(self)
        self.error_dialog = errorDialog()

        self.setWindowFlag(QtCore.Qt.WindowMaximizeButtonHint, True)

        buttonBox = QtWidgets.QDialogButtonBox(QtWidgets.QDialogButtonBox.Ok |
                                               QtWidgets.QDialogButtonBox.Cancel)
        self.ui.horizontalLayout.addWidget(buttonBox, 4)
        buttonBox.accepted.connect(self.accept)
        buttonBox.rejected.connect(self.reject)

        self.exts = []
        self.ints = []

        # Botón de selección automática de qs
        self.ui.autoButton.clicked.connect(self.automaticSelection)

        # self.ui.okButton.clicked.connect(self.close)
        # self.ui.cancelButton.clicked.connect(self.close)
        self.ui.label_1.setText("Grado de hiperestaticidad: {}".format(
                                                estructura.est['grado']))
        #Plotear la estructura para elegir sistema fundamental
        self.plot_estructura_flex()
        # #Zoom to extent
        self.ui.flex_canvas.zoom2extent(margin=0.15)
        # Setear modo interactivo
        self.ui.flex_canvas.setMode('pickreleases')
        # Apagar picker en releases originales
        self.set_picker_false_old_rel()
        # #Conectar acciones interactivas del canvas
        self.ui.flex_canvas.interactions['picker_rel'].picked.connect(
                                                    self.addIntsExtSelected)

    def accept(self):
        tipo, grado = check_statics_sistemas(
                                    self.Estructura, self.exts, self.ints)
        if tipo == 'isostatico':
            self.ok = True
            QtWidgets.QDialog.accept(self)
        elif tipo == 'hiperestatico':
            self.error_dialog.setText(
            'Sistema Fundamental es hiperestatico de grado ' + str(grado))
            self.error_dialog.exec()
            self.ok = False
            return
        else:
            self.error_dialog.setText('Sistema Fundamental es inestable')
            self.error_dialog.exec()
            self.ok = False
            return

    def reject(self):
        self.ok = False
        self.exts=[]
        self.ints=[]
        QtWidgets.QDialog.reject(self)

    def automaticSelection(self):
        # Obtener matriz estática de la estructura
        Bf = self.Estructura.Bf
        cid = [i - 1 for i in self.Estructura.cid]
        Bf_cid = Bf[:, cid]
        # Triangular matriz estática
        dummy, li_index = analysis1el.LI_vecs(Bf_cid.T)
        icols = li_index
        icols.sort()
        xcols = [a for a in range(np.size(Bf_cid,1)) if a not in icols]
        # Esfuerzos redundantes
        qinc = [cid[x]+1 for x in xcols]
        # Colorear esfuerzos internos elegidos
        sel_obj = []
        for qi in qinc:
            for barra in self.Estructura.barras.values():
                if qi in barra.qid:
                    rel_index = barra.qid.index(qi)
                    # agarrar artista del rel
                    sel_obj.append(barra.artists['self_fl'][0].rel[rel_index])
        # Setear objetos seleccionados en el picker
        self.ui.flex_canvas.interactions['picker_rel'].clearSelected()
        self.ui.flex_canvas.interactions['picker_rel'].selected_objects = sel_obj
        self.ui.flex_canvas.interactions['picker_rel'].colorSelected()
        self.ui.flex_canvas.interactions['picker_rel'].prev_obj = sel_obj
        # Agregar releases
        self.ints = qinc
        self.exts = []

    def addIntsExtSelected(self, int, ext):
        #Inicializar listas de esfuerzos internos y externos
        self.ints=[]
        self.exts=[]
        #Inicializar strings para incluirlos en los line edits
        if int:
            for bar_index in int.keys():
                for q in int[bar_index]:
                    self.ints.append(self.Estructura.barras[bar_index].qid[q])
        if ext:
            for node_index in ext.keys():
                for DOF in ext[node_index]:
                    self.exts.append(self.Estructura.nodos[node_index].DOF[DOF])

    def plot_estructura_flex(self):
        # Plotear
        self.Estructura.plot_estructura(self.ui.flex_canvas.ax,
            fontsize=8, length=1, zorder=1,
            keys={'nodo':['self_fl','index_fl'],
                  'barra':['self_fl','ejes_fl','index_fl']},
            indice_visible=False, ejes_visible=False,
            colors={'nodo':'silver', 'barra':'silver', 'v':'silver', 'ejes':'y'},
            vinculos=False, acciones=False, picker=True)
        # Plotear releases a elegir y setearlos como pickeables
        for barra in self.Estructura.barras.values():
            barra.artists['self_fl'][0].add_releases(7)
            barra.artists['self_fl'][0].set_picker(False)
            for rel in barra.artists['self_fl'][0].rel:
                rel.set_picker(5)
        # Actualizar key_self
        self.ui.flex_canvas.updateNodosyBarras(key_self='self_fl',
                    nodos=self.Estructura.nodos,
                    barras=self.Estructura.barras)
        # Plotear vinculos como flechas sin texto
        for nodo in self.Estructura.nodos.values():
            nodo.plot_reacciones(self.ui.flex_canvas.ax,
                                 fontsize=8, color='silver', length=1,
                                 visible=True, zorder=1, key='reacciones_fl',
                                 precision=3, R=nodo.restr)
            for artist in nodo.artists['reacciones_fl']:
                artist.set_text_visible(False)
                artist.set_picker(15)
                # if artist.flecha.forma == 'curva':
                #     artist.set_picker(10)
            for artist in nodo.artists['self_fl']:
                artist.set_picker(False)

    def set_picker_false_old_rel(self):
        for barra in self.Estructura.barras.values():
            releases = []
            i = barra.indice
            rel = barra.rel
            if rel == [0]: # para 2DTruss elements
                rel = [0,1,1]
            if rel[1]==1:
                releases.append(barra.artists['self_fl'][0].rel[1])
            if rel[2]==1:
                releases.append(barra.artists['self_fl'][0].rel[2])
            for release in releases:
                release.set_picker(False)

    def exec_(self):
        super(flexibilidadesDialog, self).exec_()
        self.Estructura.remove_nodo_artists(
                                        ['self_fl','index_fl','reacciones_fl'])
        self.Estructura.remove_barra_artists(['self_fl','ejes_fl','index_fl'])
        return self.ok, self.exts, self.ints

class flexibilidadesUI(object):

    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(700, 500)
        Dialog.setModal(True)

        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame_1")
        self.verticalLayout.addWidget(self.frame, 0)
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0,0,0,0)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_1 = QtWidgets.QLabel(self.frame)
        self.label_1.setText("Grado de hiperestaticidad")
        self.label_1.setObjectName("label_1")
        self.horizontalLayout.addWidget(self.label_1,0)
        self.autoButton = QtWidgets.QToolButton(self.frame)
        self.autoButton.setText('Auto')
        self.autoButton.setObjectName("autoButton")
        self.autoButton.setToolTip("Elegir incógnitas automáticamente")
        self.horizontalLayout.addWidget(self.autoButton,1)
        spacerItem = QtWidgets.QSpacerItem(40, 20,
                                           QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.flex_canvas = AteneaCanvasQt(parent=Dialog,
                                    layout=self.verticalLayout,
                                    grid=False, nodos=Dialog.Estructura.nodos,
                                    barras=Dialog.Estructura.barras,
                                    QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                    barprops={'framewidth':1, 'color':'silver',
                                              'ejes_color':'y'},
                                    nodeprops={'marker':'s', 'markersize':6,
                                               'color':'silver'},
                                    selectcolor='g', originArrows=False
                                    )

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog",
                                         "Seleccion de sistema fundamental"))
        # self.label_1.setText(_translate("Dialog", "Internos"))
        # self.label_2.setText(_translate("Dialog", "Externos"))

def about_dialog():
    ui = aboutDialog()
    ui.exec_()

class aboutDialog(QtWidgets.QDialog):
    def __init__(self):
        super().__init__()
        self.ui = about_ui()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.close)

class about_ui(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(700, 500)
        Dialog.setModal(True)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Dialog)
        self.horizontalLayout.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout.setSpacing(6)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame_2 = QtWidgets.QFrame(Dialog)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.frame_2)
        self.horizontalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.horizontalLayout_2.setSpacing(6)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_4 = QtWidgets.QLabel(self.frame_2)
        self.label_4.setText("")
        self.label_4.setPixmap(QtGui.QPixmap(
                    resource_path("icons/Atenea2d_icon_large.png")))
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        self.horizontalLayout.addWidget(self.frame_2)
        self.frame = QtWidgets.QFrame(Dialog)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout.setSpacing(15)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.frame)
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.label_2 = QtWidgets.QLabel(self.frame)
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.label_3 = QtWidgets.QLabel(self.frame)
        self.label_3.setObjectName("label_3")
        self.verticalLayout.addWidget(self.label_3)
        self.textBrowser = QtWidgets.QTextBrowser(self.frame)
        self.textBrowser.setReadOnly(True)
        self.textBrowser.setTextInteractionFlags(
            QtCore.Qt.TextBrowserInteraction)
        self.textBrowser.setOpenExternalLinks(True)
        self.textBrowser.setObjectName("textBrowser")
        self.verticalLayout.addWidget(self.textBrowser)
        self.frame_3 = QtWidgets.QFrame(self.frame)
        self.frame_3.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_3.setObjectName("frame_3")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout(self.frame_3)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20,
                                           QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.pushButton = QtWidgets.QPushButton(self.frame_3)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed,
                                           QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy)
        self.pushButton.setObjectName("pushButton")
        self.horizontalLayout_3.addWidget(self.pushButton)
        self.verticalLayout.addWidget(self.frame_3)
        self.horizontalLayout.addWidget(self.frame)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Sobre Atenea2d"))
        self.label.setText(_translate("Dialog", "Atenea2d"))
        self.label_2.setText(_translate("Dialog", "Versión 1.7"))
        self.label_3.setText(_translate("Dialog", "Bajo licencia GNU-GPL-v3"))
        self.textBrowser.setHtml(_translate("Dialog",
                                            "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                            "p, li { white-space: pre-wrap; }\n"
                                            "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.1pt; font-weight:400; font-style:normal;\">\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Desarrollado por el Departamento de Estabilidad de la Facultad de ingeniería de la UBA (Argentina)</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">para el proyecto UBATIC 2018-2019</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Director del proyecto: Raúl D. Bertero</span></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Equipo de desarrollo: Mariano Balbi, Agustín Bertero, Juan Mussat, Joaquín Ortiz, Felipe López Rivarola, Ariel Terlisky, Marcial Zapater, Felipe Medán y Santiago Bertero</span></p>\n"
                                            "<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
                                            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Web del proyecto: </span><a href=\"https://campusgrado.fi.uba.ar/course/view.php?id=258\"><span style=\" font-size:8pt; text-decoration: underline; color:#0000ff;\">Atenea Campus (FIUBA)</span></a></p></body></html>"))
        self.pushButton.setText(_translate("Dialog", "OK"))
