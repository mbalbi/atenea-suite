from PyQt5 import QtWidgets, QtGui, QtCore
import sys, os

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

def createStatusBar(self):
    # Add to the status bar
    # Tool Button de mejorar precision
    self.ui.increasePrecisionToolButton = QtWidgets.QToolButton()
    self.ui.increasePrecisionToolButton.setToolTip('Incrementar decimales de resultados')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/increase_precision_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.increasePrecisionToolButton.setIcon(icon)
    self.ui.increasePrecisionToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.increasePrecisionToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.increasePrecisionToolButton.setAutoRaise(True)
    self.ui.increasePrecisionToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.increasePrecisionToolButton.setObjectName(
        "increaseArrowsToolButton")
    self.ui.increasePrecisionToolButton.setChecked(True)
    self.ui.increasePrecisionToolButton.clicked.connect(
        lambda: self.modifyPrecision(1))
    # Tool Button de achicar precision
    self.ui.decreasePrecisionToolButton = QtWidgets.QToolButton()
    self.ui.decreasePrecisionToolButton.setToolTip('Reducir decimales de resultados')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/decrease_precision_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.decreasePrecisionToolButton.setIcon(icon)
    self.ui.decreasePrecisionToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.decreasePrecisionToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.decreasePrecisionToolButton.setAutoRaise(True)
    self.ui.decreasePrecisionToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.decreasePrecisionToolButton.setObjectName(
        "decreaseArrowsToolButton")
    self.ui.decreasePrecisionToolButton.setChecked(True)
    self.ui.decreasePrecisionToolButton.clicked.connect(
        lambda: self.modifyPrecision(-1))
    # Tool Button de agrandar textos
    self.ui.increaseTextsToolButton = QtWidgets.QToolButton()
    self.ui.increaseTextsToolButton.setToolTip('Aumentar tamaño de textos')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/increase_letter_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.increaseTextsToolButton.setIcon(icon)
    self.ui.increaseTextsToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.increaseTextsToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.increaseTextsToolButton.setAutoRaise(True)
    self.ui.increaseTextsToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.increaseTextsToolButton.setObjectName(
        "increaseArrowsToolButton")
    self.ui.increaseTextsToolButton.setChecked(True)
    self.ui.increaseTextsToolButton.clicked.connect(
        lambda: self.modifyTextsSize(1))
    # Tool Button de achicar textos
    self.ui.decreaseTextsToolButton = QtWidgets.QToolButton()
    self.ui.decreaseTextsToolButton.setToolTip('Reducir tamaño de textos')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/decrease_letter_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.decreaseTextsToolButton.setIcon(icon)
    self.ui.decreaseTextsToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.decreaseTextsToolButton.setPopupMode(
        QtWidgets.QToolButton.DelayedPopup)
    self.ui.decreaseTextsToolButton.setAutoRaise(True)
    self.ui.decreaseTextsToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.decreaseTextsToolButton.setObjectName(
        "decreaseArrowsToolButton")
    self.ui.decreaseTextsToolButton.setChecked(True)
    self.ui.decreaseTextsToolButton.clicked.connect(
        lambda: self.modifyTextsSize(-1))
    # Tool Button del grid snap
    self.ui.snapOnToolButton = QtWidgets.QToolButton()
    self.ui.snapOnToolButton.setToolTip('Prender/Apagar grilla del canvas')
    icon = QtGui.QIcon()
    icon.addPixmap(QtGui.QPixmap(
                    resource_path("icons/grid_icon.png")),
                    QtGui.QIcon.Normal, QtGui.QIcon.Off)
    self.ui.snapOnToolButton.setIcon(icon)
    self.ui.snapOnToolButton.setIconSize(QtCore.QSize(15, 15))
    self.ui.snapOnToolButton.setCheckable(True)
    self.ui.snapOnToolButton.setPopupMode(QtWidgets.QToolButton.DelayedPopup)
    self.ui.snapOnToolButton.setAutoRaise(True)
    self.ui.snapOnToolButton.setArrowType(QtCore.Qt.NoArrow)
    self.ui.snapOnToolButton.setObjectName("selectedToolButton")
    self.ui.snapOnToolButton.setChecked(True)
    self.ui.snapOnToolButton.clicked.connect(self.setGridOn)
    # Spin Box de grilla
    self.ui.dxSpinBox = QtWidgets.QDoubleSpinBox()
    self.ui.dxSpinBox.setSingleStep(0.1)
    self.ui.dxSpinBox.setValue(self.default_grid[0])
    self.ui.dxSpinBox.setMaximum(1000)
    self.ui.dxSpinBox.valueChanged.connect(self.updateGrid)
    self.ui.dySpinBox = QtWidgets.QDoubleSpinBox()
    self.ui.dySpinBox.setSingleStep(0.1)
    self.ui.dySpinBox.setValue(self.default_grid[1])
    self.ui.dySpinBox.setMaximum(1000)
    self.ui.dySpinBox.valueChanged.connect(self.updateGrid)
    self.ui.nxSpinBox = QtWidgets.QSpinBox()
    self.ui.nxSpinBox.setValue(self.default_grid[2])
    self.ui.nxSpinBox.setMaximum(1000)
    self.ui.nxSpinBox.valueChanged.connect(self.updateGrid)
    self.ui.nySpinBox = QtWidgets.QSpinBox()
    self.ui.nySpinBox.setValue(self.default_grid[3])
    self.ui.nySpinBox.setMaximum(1000)
    self.ui.nySpinBox.valueChanged.connect(self.updateGrid)

    # Add widgets
    # Frame for precision tools
    frame1 = QtWidgets.QFrame()
    HLayout1 = QtWidgets.QHBoxLayout(frame1)
    HLayout1.setContentsMargins(0, 0, 0, 0)
    HLayout1.setSpacing(0)
    HLayout1.addWidget(self.ui.increasePrecisionToolButton,0)
    HLayout1.addWidget(self.ui.decreasePrecisionToolButton,1)
    frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
    frame1.setLayout(HLayout1)
    self.ui.statusBar.addPermanentWidget(frame1)
    # Frame for text size tools
    frame1 = QtWidgets.QFrame()
    HLayout1 = QtWidgets.QHBoxLayout(frame1)
    HLayout1.setContentsMargins(0, 0, 0, 0)
    HLayout1.setSpacing(0)
    HLayout1.addWidget(self.ui.increaseTextsToolButton,0)
    HLayout1.addWidget(self.ui.decreaseTextsToolButton,1)
    frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
    frame1.setLayout(HLayout1)
    self.ui.statusBar.addPermanentWidget(frame1)
    # Frame for grid tools
    frame1 = QtWidgets.QFrame()
    HLayout1 = QtWidgets.QHBoxLayout(frame1)
    HLayout1.setContentsMargins(0, 0, 0, 0)
    HLayout1.setSpacing(5)
    HLayout1.addWidget(self.ui.snapOnToolButton,0)
    HLayout1.addWidget(QtWidgets.QLabel('dx'),1)
    HLayout1.addWidget(self.ui.dxSpinBox,2)
    HLayout1.addWidget(QtWidgets.QLabel('dy'),3)
    HLayout1.addWidget(self.ui.dySpinBox,4)
    HLayout1.addWidget(QtWidgets.QLabel('Nx'),5)
    HLayout1.addWidget(self.ui.nxSpinBox,6)
    HLayout1.addWidget(QtWidgets.QLabel('Ny'),7)
    HLayout1.addWidget(self.ui.nySpinBox,8)
    frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,QtWidgets.QSizePolicy.Fixed)
    frame1.setLayout(HLayout1)
    self.ui.statusBar.addPermanentWidget(frame1)
    # Label de coordenadas del mouse en el canvas
    self.ui.coordsLabel = QtWidgets.QLabel()
    self.ui.statusBar.addWidget(self.ui.coordsLabel)
