from PyQt5 import QtWidgets

def agrupar_botones(self):
    # Grupo de creacion de estructura
    self.canvasmode_group = QtWidgets.QActionGroup(self)
    self.canvasmode_group.addAction(self.ui.actionPickAll)
    self.canvasmode_group.addAction(self.ui.actionPickNodos)
    self.canvasmode_group.addAction(self.ui.actionPickBarras)
    self.canvasmode_group.addAction(self.ui.actionPan)
    self.canvasmode_group.addAction(self.ui.actionZoom2Rect)
    self.canvasmode_group.addAction(self.ui.actionDrawNodo)
    self.canvasmode_group.addAction(self.ui.actionDrawBarra)
    self.ui.actionPan.setChecked(True)
    # Grupo de windows layout
    self.window_layout = QtWidgets.QActionGroup(self)
    self.window_layout.addAction(self.ui.actionTabbed_View)
    self.window_layout.addAction(self.ui.actionTile_View)
    self.window_layout.addAction(self.ui.actionCascade_View)
    self.ui.actionTabbed_View.setChecked(True)
    # Grupo de arbol selector
    self.treebuttons_group = QtWidgets.QButtonGroup(self)
    self.treebuttons_group.addButton(self.ui.selectedToolButton)
    self.treebuttons_group.addButton(self.ui.nodosToolButton)
    self.treebuttons_group.addButton(self.ui.barrasToolButton)
    self.treebuttons_group.addButton(self.ui.propiedadesToolButton)
    self.ui.selectedToolButton.setChecked(True)
    # Grupo de botones del frame de carga de estructura
    self.estructurabuttons_group = QtWidgets.QButtonGroup(self)
    self.estructurabuttons_group.addButton(self.ui.nodosButtonDraw)
    self.estructurabuttons_group.addButton(self.ui.barrasButtonDraw)
    self.estructurabuttons_group.addButton(self.ui.barrasButtonRel)
    self.estructurabuttons_group.addButton(self.ui.nodosButtonVinculo)
    self.estructurabuttons_group.addButton(self.ui.barrasButtonProps)
    self.ui.nodosButtonDraw.setChecked(True)
    # Grupo de botones del frame de barras
    self.accionesbuttons_group = QtWidgets.QButtonGroup(self)
    self.accionesbuttons_group.addButton(self.ui.nodosButtonCarga)
    self.accionesbuttons_group.addButton(self.ui.barrasButtonCarga)
    self.accionesbuttons_group.addButton(self.ui.barrasButtonTemp)
    self.accionesbuttons_group.addButton(self.ui.nodosButtonCed)
    self.ui.nodosButtonCarga.setChecked(True)