"""
Modulo resolucion paso a paso por método de las flexibilidades

creado por: equipo Atenea-UBATIC
"""
from .atenea2dgui import NodoGUI, BarraGUI, EstructuraGUI
import numpy as np
# from .Atenea2d.analysis1el import metodo_flexibilidades
import matplotlib.lines as mlines

def sistemas_flexibilidades(estructura, qinc=[], ext=[]):
    """
    """
    #Inicializar diccionario con los sistemas del metodo de las flexibilidades
    sistemas_flex={}
    # if not qinc:
    #     qinc = estructura.qinc
    #Inicializar diccionario con los indices de cada sistema
    indice_sist={'qinc':[],'ext':[]}
    for n in range(1,len(qinc)+1):
        indice_sist['qinc'].append(n)
    for n in range(1,len(ext)+1):
        indice_sist['ext'].append(n+len(qinc))
    #Sistema0
    nodos, barras = crear_barrasynodos_flex(estructura, ext, qinc)
    #Cargas la nueva estructura con las cargas de la estructura original
    for nodo in nodos.values():
        nodo.P = estructura.nodos[nodo.indice].P
        nodo.ced = estructura.nodos[nodo.indice].ced
    for barra in barras.values():
        barra.w = estructura.barras[barra.indice].w
        barra.assign_w(estructura.barras[barra.indice].w)
        barra.e0 = estructura.barras[barra.indice].e0
        barra.assign_e0(estructura.barras[barra.indice].e0)
    sistema = EstructuraGUI(nodos, barras)
    #Append al diccionario el sistema 0
    sistemas_flex[0] = sistema
    # Cargar la nueva estructura con las cargas equivalentes a las cargas
    # unitarias
    # relativas segun cual sea el indice del sistema
    counter = 0  # Counter para contar la posicion en la lista xcols
    for i in indice_sist['qinc']:
        nodos, barras = crear_barrasynodos_flex(estructura, ext, qinc)
        sistema = EstructuraGUI(nodos, barras)
        #Cargar la nueva estructura con las cargas de la estruc
        cargar_sistemas_xcols(sistema, estructura.bx, i)
        #Imponerle q unitario en la coordenada que corresponde
        sistema.Q_ini = [0]*estructura.nq
        sistema.Q_ini[qinc[counter]-1] = 1
        #Guardar que q fue liberado en el sistema i
        sistema.int = qinc[counter]
        #Append al diccionario el sistema i de xcols
        sistemas_flex[i] = sistema
        counter += 1
     # Counter para identificar en que DOFd se debe aplicar una fuerza unitaria
    counter = 0
    for i in indice_sist['ext']:
        nodos, barras = crear_barrasynodos_flex(estructura, ext, qinc)
        # Crear estructura
        sistema = EstructuraGUI(nodos, barras)
        # Agregar cargas a nodos
        for nodo in sistema.nodos.values():
            if ext[counter] in nodo.DOF:
                dof = nodo.DOF.index(ext[counter])
                nodo.P[dof] = 1
        # Reescribir estructura con cargas actualizadas
        sistema = EstructuraGUI(nodos, barras)
        #Guardar que DOF fue liberado en en el sistema i
        sistema.ext = ext[counter]
        #Append al diccionario el sistema i de ext
        sistemas_flex[i] = sistema
        counter +=1
    return sistemas_flex

def crear_barrasynodos_flex(estructura, ext=[], qinc=[]):
    """
        Funcion que crea la estructura con los releases elegidos automaticamente por el meteodo de las flexibilidades
    :param estructura:
    :param qinc: lista con las fuerzas basica (q) a liberar especificadas por el usuario
    :param ext: lista con los DOFd a liberar especificados por el usuario
    :return:
    """
    # Inicializar diccionario de nodos y barras para luego construir la nueva estructura
    nodos_flex = {}
    barras_flex = {}
    k = 1

    for nodo in estructura.nodos.values():
        nodo_flex = NodoGUI([nodo.coords[0], nodo.coords[1]], k)
        #condicion de vinculo
        nodo_flex.restr = nodo.restr[:]
        #liberar vinculacion externa
        if ext:
            if any(elem in ext for elem in nodo.DOF):
                if nodo.DOF[0] in ext:
                    nodo_flex.restr[0]=0
                if nodo.DOF[1] in ext:
                    nodo_flex.restr[1]=0
                if len(nodo.DOF) == 3:
                    if nodo.DOF[2] in ext:
                        nodo_flex.restr[2] = 0
        # Append el nuevo elemento nodo al diccionario
        nodos_flex[k] = nodo_flex
        k += 1
    k = 1
    last_q = 0 # Variable que guarda la fuerza basica del elemento barra i-1 en la iteracion
    for barra in estructura.barras.values():
        #indice de nodos inicial y final
        i = barra.nodos[0].indice
        f = barra.nodos[1].indice
        #crear barra
        barra_flex = BarraGUI(k, [nodos_flex[i], nodos_flex[f]], barra.ElemName)
        #Cargar la barra con material, seccion y releases
        barra_flex.mat = barra.mat
        barra_flex.seccion = barra.seccion
        #Liberar esfuerzos internos xcol
        rel = barra.rel[:]
        counter = 1  # Counter para saber en donde cargar el release
        if any(elem in qinc for elem in barra.qid):
            for q in barra.qid:
                if q in qinc:
                    if counter == 2:
                        rel[1] = 1  # rel momento inicial
                        barra_flex.new_rel[1] = 1  #Atributo de la barra que indica los releases agregados en el metodo de las flexibilidades
                    if counter == 3:
                        rel[2] = 1  # rel momento final
                        barra_flex.new_rel[2] = 1
                    if counter == 1:
                        rel[0] = 1  # rel normal
                        barra_flex.new_rel[0] = 1
                counter += 1
        barra_flex.assign_rel(rel)
        barras_flex[k] = barra_flex
        k += 1
    # estructura_flex = EstructuraGUI(nodos_flex, barras_flex)
    return nodos_flex, barras_flex

def check_statics_sistemas(estructura, ext, qinc):
    B = estructura.B
    Bf = estructura.Bf
    # indices q sin releases
    cid = [i - 1 for i in estructura.cid]
    # grados de libertad libres
    nf = len(estructura.DOFf) + len(ext)
    # Excluir releases en matriz estatica y cinemática
    Bf_cid = Bf[:, cid]
    B_cid = B[:, cid]

    # Ordenar ext y qinc
    ext = sorted(ext)
    qinc = sorted(qinc)
    # Agregar filas de reacciones de vinculo redundantes
    ext = [x - 1 for x in ext][:]  # restar 1 para indexar
    Be = np.vstack((Bf_cid, B_cid[ext, :]))
    # Dividir entre columnas de fuerzas internas redundantes
    xcols = [cid.index(x-1) for x in qinc]
    icols = [a for a in range(np.size(Bf_cid, 1)) if a not in xcols]
    icols.sort()
    bi = Be[:, icols]  # Matriz B del isostatico

    nqi = np.size(bi, 1)
    # Rango de matriz estática
    if np.size(bi) != 0:
        rank = np.linalg.matrix_rank(bi)  # rango de la matriz B
    else:
        rank = 0
    B_min_dim = min(np.shape(bi))  # Mínima dimensión de Bf
    B_full_rank = (rank == B_min_dim)  # chequear si Bf es de rango completo
    # Grado de estaticidad (nqs - nfs)
    grado_est = np.int(nqi - nf)
    if (grado_est == 0 and B_full_rank):
        tipo = 'isostatico'
        grado = grado_est
    elif (grado_est > 0 and B_full_rank):
        tipo = 'hiperestatico'
        grado = grado_est
    else:
        tipo = 'inestable'
        grado = rank - estructura.nf
    return tipo, grado

def cargar_sistemas_xcols(sistema, bx, indice_sist):
    # grados de libertad libres sin releases
    doff = [i - 1 for i in sistema.DOFf]
    P = np.reshape(bx[:, indice_sist - 1], (len(bx[:, indice_sist - 1]), 1))
    sistema.P[doff] = -P
    sistema.Pf = -P

