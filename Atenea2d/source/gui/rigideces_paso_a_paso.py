"""
Modulo resolucion paso a paso por método de las rigideces

creado por: equipo Atenea-UBATIC
"""
from .atenea2dgui import NodoGUI, BarraGUI, EstructuraGUI
from ..core.analysis1el import metodo_rigideces
import numpy as np

def rigideces_paso_a_paso(estructura, DOFf):
    #Generar diccionarios de nodos y barras
    nodos, barras = crear_nodosybarras(estructura)
    #Cargar la estructura
    if DOFf == 0:
        for nodo in nodos.values():
            # Cargar los nodos con fuerzas y cedimientos de vinculo
            nodo.P = estructura.nodos[nodo.indice].P
            nodo.ced = estructura.nodos[nodo.indice].ced
        for barra in barras.values():
            # Cargar la barra con fuerzas y cedimientos de vinculo
            barra.assign_w(estructura.barras[barra.indice].w)
            barra.assign_e0(estructura.barras[barra.indice].e0)
    else:
        nodos = cargar_cedimientos(estructura,nodos,DOFf)

    sistema = EstructuraGUI(nodos, barras)
    return sistema


def cargar_cedimientos(estructura,nodos,DOFf):
    """
    :param estructura: estructura original
    :param nodos: diccionario de nodos a los cuales se les va a cargar el cedimiento de vinculo
    :param DOFf: DOFf en el cual debe aplicarse el cedimiento de vinculo
    :return: diccionario de nodos, con el nodo con cedimiento de vinculo en el DOFf correspondiente
    """
    # Cargar el cedimiento de vinculo unitario correspondiente
    for nodo in nodos.values():
        if len(estructura.nodos[nodo.indice].DOF) == 3:
            if DOFf == estructura.nodos[nodo.indice].DOF[0]:
                nodo.ced = [1, 0, 0]
                break
            elif DOFf == estructura.nodos[nodo.indice].DOF[1]:
                nodo.ced = [0, 1, 0]
                break
            elif DOFf == estructura.nodos[nodo.indice].DOF[2]:
                nodo.ced = [0, 0, 1]
                break
        else:
            if DOFf == estructura.nodos[nodo.indice].DOF[0]:
                nodo.ced = [1, 0]
                break
            elif DOFf == estructura.nodos[nodo.indice].DOF[1]:
                nodo.ced = [0, 1]
                break
    return nodos

def crear_nodosybarras(estructura):
    """
    Funcion que crea un diccionario con nodos con todos sus grados de libertad restringidos, y un diccionario de barras
    """
    # Inicializar diccionario de nodos y barras para luego construir la nueva estructura
    nodos_rig = {}
    barras_rig = {}
    k = 1
    for nodo in estructura.nodos.values():
        nodo_rig = NodoGUI([nodo.coords[0], nodo.coords[1]], k)
        # restringir los grados de libertad del nodo
        if len(nodo.DOF) == 3:
            nodo_rig.restr = [1, 1, 1]
        else:
            nodo_rig.restr = [1, 1]

        # Append el nuevo elemento nodo al diccionario
        nodos_rig[k] = nodo_rig
        k += 1
    k = 1
    for barra in estructura.barras.values():
        #indice de nodos inicial y final
        i = barra.nodos[0].indice
        f = barra.nodos[1].indice
        #crear barra
        barra_rig = BarraGUI(k, [nodos_rig[i], nodos_rig[f]], barra.ElemName)
        #Cargar la barra con material, seccion y releases
        barra_rig.mat = barra.mat
        barra_rig.seccion = barra.seccion
        barra_rig.assign_rel(barra.rel)
        #Append el nuevo elemento barra al diccionario
        barras_rig[k] = barra_rig
        k += 1
    # estructura_rig = EstructuraGUI(nodos_rig, barras_rig)
    return nodos_rig, barras_rig

