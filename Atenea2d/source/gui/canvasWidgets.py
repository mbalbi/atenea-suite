# modulo canvasWidgets.py
"""
Este modulo define las clases que agregan interactividad al pyqt canvas definido
en el módulo interactiveCanvas.py

creado por: equipo Atenea-UBATIC
"""
import numpy as np

import matplotlib.lines as mlines
from matplotlib.patches import FancyArrowPatch, FancyArrow
from ..core.graph2d import FrameBar, Arrow2D

from .selectorWidgets import RectangleSelector

from PyQt5.QtCore import QObject, pyqtSignal, Qt

def findObject(objectsList, targetObject):
    pos = 0
    for object in objectsList:
        if object == targetObject:
            return True, pos
        pos += 1
    return False, None

class Picker(QObject):
    """
    Clase que agrega la capacidad de seleccionar objetos (childs) del axes.
    Sólo permite seleccionar objetos nodo (patch.Rectangle) y objetos barra
    (mlines.Line2D)
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - nodos_list [list]: lista de nodos en el canvas
        - barras_list [list]: lista de barras en el canvas
        
    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - objectType: modo de funcionamiento
        - rs: RectangleSelector widget definido en módulo SelectorWidgets.py.
              Define la capacidad de dibujar un rectángulo cuando se arrastra
              el mouse
        - list_picks [list]: Lista con objeto clickeados por el mouse. El
                             matplotlib, por defecto, selecciona todo los objetos
                             que están en las coordenadas del click. Para un click
                             debería quedarme solo con un objeto.
        - selected_objects [list]: Lista de objetos seleccionados. 
        - prev_objects [list]: Lista de objetos previamente seleccionados
        - consecutive [bool]: Bool que define si el siguiente objeto seleccionado
                              se agrega al anterior o lo reemplaza
        - ignore_release [bool]: Bool para ignorar el callback cuando se suelta
                                 el click del mouse
    
    Métodos:
        - onPick(): Callback del 'pick_event'. Define la lista list_picks
        - onRelease(): Callback del evento 'mouse release'. Actualiza lista de 
                     objetos seleccionados y los colorea. Vuelve al color original
                     la lista de objetos previamente seleccionados. Emite pyqt
                     signal 'picked'
        - colorSelected(): Pinta de color rojo los objetos en selected_objects y
                         vuelve al color original los objetos en prev_objects
        - onKeyPress(): Callback de 'key press'. Define la funcionalida de
                      seleccionar objetos consecutivos usando 'shift' y de
                      eliminar objetos usando 'delete'
        - onKeyRelease(): Callback de 'key release'. Deja de seleccionar objetos
                        consecutivos
        - object_select_callback(): callback del RectangleSelector. Agrega a 
                                  selected_objects todos los objetos que estan
                                  completamente adentro del rectangulo y los
                                  colorea.
        - connect(objectType): Conecta los callbacks con el modo objectType
        - disconnect(): Desconecta los callbacks

    Signals:
        - picked: Se emite al final del callback 'onRelease'
        
    """
    picked = pyqtSignal(list,list)
    delete = pyqtSignal()
    cleared = pyqtSignal()
    
    def __init__(self, ax, nodos_list={}, barras_list={}, nodecolor='b',
                 barracolor='k', selectcolor='r', key_self='self'):
        """
        
        objectType: 'all', 'nodos', 'barras'
        """
        super().__init__()
        
        self.ax = ax
        self.fig = ax.get_figure()
        
        self.rs = RectangleSelector(ax, self.object_select_callback,
                           drawtype='box', useblit=True, button=[1], 
                           minspanx=5, minspany=5, spancoords='pixels', 
                           interactive=False)

        self.list_picks = []
        self.selected_objects = []
        self.prev_obj = []
        self.nodos_list = nodos_list
        self.barras_list = barras_list
        self.consecutive = False # Flag que dice si se seleccionan objetos seguidos
        self.ignore_onrelease = False
        self.nodecolor = nodecolor
        self.barracolor = barracolor
        self.selectcolor = selectcolor
        self.key_self = key_self
        
        self.connect('all')
        self.disconnect()
        
    def onPick(self, event):
        """
        Guarda todos los artistas pickeados y los guarda para posterior uso
        """
        if not event.mouseevent.button == 1: return
        self.list_picks.append(event.artist)
        
    def onRelease(self, event):
        """
        Este event handler se necesita ya que en los nodos, el pick event
        se ejecuta dos veces agarrando la barra y el nodo.
        EL programa requiere que en esos casos, se seleccione solo el nodo.
        """
        # De la lista de objetos pickeados me quedo con el nodo o con el primero
        # (si son varias lineas)
        if self.ignore_onrelease:
            self.ignore_onrelease = False
            return
        obj = None
        for pick in self.list_picks:
            npoints = len(pick.get_xdata())
            if npoints == 1:
                obj = pick
                break # Priorizar nodos sobre barras
            if npoints > 1:
                found_barra, pos_barra = self.findLineinBar(pick)
                obj = self.barras_list[pos_barra].artists[self.key_self][0]
        # Actualizo lista de objetos seleccionados
        if self.consecutive:
            if obj:
                self.selected_objects += [obj]
        elif not self.consecutive:
            self.prev_obj = self.selected_objects
            if obj:
                self.selected_objects = [obj]
            else:
                self.selected_objects = []
            
        # Coloreo listo de objetos seleccionados y vuelvo al color original
        # lista de objetos previos
        self.colorSelected()
        # Reseteo lista de objetos pickeados
        self.list_picks = []
        
        # Envío senal con objetos seleccionados
        nodos_indices, barras_indices = self.findIndices()
        self.picked.emit(nodos_indices, barras_indices)

    def colorSelected(self):
        """
        Función para pintar de rojo todos los objetos seleccionados
        """
        # Volver al color original objetos previos
        for prev_obj in self.prev_obj:
            if type(prev_obj) is FrameBar:
                prev_obj.set_color(self.barracolor)
            elif (type(prev_obj) is FancyArrowPatch) or\
                 (type(prev_obj) is FancyArrow):
                prev_obj.set_color(self.nodecolor)
            elif type(prev_obj) is mlines.Line2D:
                prev_obj.set_markeredgecolor(self.nodecolor)
                if self.objectType == 'releases':
                    prev_obj.set_markerfacecolor('white')
                else:
                    prev_obj.set_markerfacecolor(self.nodecolor)
        self.fig.canvas.draw()
        # Colorear objetos seleccionados
        for obj in self.selected_objects:
            if type(obj) is FrameBar:
                obj.set_color(self.selectcolor)
            elif (type(obj) is FancyArrowPatch) or (type(obj) is FancyArrow):
                obj.set_color(self.selectcolor)
            elif type(obj) is mlines.Line2D:
                obj.set_markeredgecolor(self.selectcolor)
                obj.set_markerfacecolor(self.selectcolor)

        self.fig.canvas.draw()

    def clearSelected(self):
        self.prev_obj = self.selected_objects
        self.selected_objects = []
        self.colorSelected()
        self.cleared.emit()

    def onKeyPress(self, event, override=False):
        """
        Si la tecla shift está apretada, entonces se seleccionan varios
        seguidos
        """
        barras_deleted = []
        nodos_deleted = []
        if not override:
            if event.inaxes != self.ax: return
        if event.key=='shift':
            self.consecutive = True
        elif event.key=='delete':
            self.delete.emit()
            # for object in self.selected_objects:
            #     # object.remove()
            #     if findObject(self.nodos_list, object)[0]: # if object is node
            #         barras_found = self.findBars(object)
            #         for barra in barras_found:
            #             barras_deleted.append(barra)
            #             self.barras_list.remove(barra)
            #             if barra in self.selected_objects:  # remover selected objects
            #                 self.selected_objects.remove(barra)
            #         self.nodos_list.remove(object)
            #         nodos_deleted.append(object)
            #     elif findObject(self.barras_list, object)[0]:  # if object is bar
            #         barras_deleted.append(object)
            #         self.barras_list.remove(object)
            # if barras_deleted:
            #     self.bardeleted.emit(barras_deleted)
            # if nodos_deleted:
            #     self.nodedeleted.emit()
        else:
            return

    def onKeyRelease(self,event):
        """
        Si la tecla shift está apretada, entonces se seleccionan varios
        seguidos
        """
        if event.inaxes != self.ax:
            return
        if event.key == 'shift':
            self.consecutive = False
        elif event.key=='delete':
            # self.delete.emit()
            return
        else:
            return

    def object_select_callback(self, eclick, erelease):
        # Ignore if there was dragging
        if ((eclick.xdata==erelease.xdata) and (eclick.ydata==erelease.ydata)):
            return
        # Rectangle geometry
        xmin, xmax, ymin, ymax = self.rs.extents
        # Obtener todos los objetos pickeables
        pickable_children = [child for child in self.ax.get_children() if
                             child.get_picker()]
        # Ver cuáles objetos caen dentro del rectangulo
        inside_objects = []
        for child in pickable_children:
            npoints = len(child.get_xdata())
            if npoints == 1:
                xcenter, ycenter = child.get_data()
                if (xcenter>xmin and xcenter<xmax) and \
                   (ycenter>ymin and ycenter<ymax):
                    inside_objects.append(child)
            if npoints > 1:
                line_coords = child.get_xydata()
                is_in = True
                for center in line_coords:
                    if ((center[0]<xmin or center[0]>xmax) or (
                                            center[1]<ymin or center[1]>ymax)):
                        is_in = False
                if is_in:
                    found_barra, pos_barra = self.findLineinBar(child)
                    inside_objects.append(
                        self.barras_list[pos_barra].artists[self.key_self][0])

        # Agrego a objectos seleccionados
        if self.consecutive:
            self.selected_objects += inside_objects
        elif not self.consecutive:
            self.prev_obj = self.selected_objects
            self.prev_obj_color = [obj.get_color() for obj in self.prev_obj]
            self.selected_objects = inside_objects
        # Coloreo listo de objetos seleccionados y vuelvo al color original
        # lista de objetos previos
        self.colorSelected()
        # Ignorar OnRelease callback
        self.ignore_onrelease = True
        
        # Envío senal con objetos seleccionados
        nodos_indices, barras_indices = self.findIndices()
        self.picked.emit(nodos_indices, barras_indices)
    
    def findIndices(self):
        nodos_indices = []
        barras_indices = []
        for object in self.selected_objects:
            artists_list = [a.artists[self.key_self][0] for a in \
                            self.barras_list.values()]
            keys_list = [a for a in self.barras_list.keys()]
            found_barra, pos_barra = findObject(artists_list, object)
            if found_barra:
                pos_barra = keys_list[pos_barra]
                barras_indices.append(pos_barra)
            
            artists_list = [a.artists[self.key_self][0] for a in \
                            self.nodos_list.values()]
            keys_list = [a for a in self.nodos_list.keys()]
            found_nodo, pos_nodo = findObject(artists_list, object)
            if found_nodo:
                pos_nodo = keys_list[pos_nodo]
                nodos_indices.append(pos_nodo)
        return nodos_indices, barras_indices

    def findLineinBar(self, object):
        k = 0
        found_barra = False
        for key, barra in self.barras_list.items():
            axis_list = [a.baraxis for a in barra.artists[self.key_self]]
            is_barra = findObject(axis_list, object)
            if is_barra[0]:
                found_barra = True
                pos_barra = key
                return found_barra, pos_barra
            k += 1
        return found_barra, None    

    def findBars(self, node):
        """
        Encontrar barras que concurren al nodo 'node'
        """
        node_center = node.get_xydata()[0]
        barras_found = []
        for barra in self.barras_list:
            bar_data = barra.get_xydata()
            nodei_center = bar_data[0,:]
            nodef_center = bar_data[1,:]
            if np.array_equal(node_center, nodei_center) or np.array_equal(
                    node_center, nodef_center):
                barras_found.append(barra)
        return barras_found

    def setPickable(self, objectType):
        self.objectType = objectType
        if objectType == 'nodos':
            picker_nodos = 5
            picker_barras = False
            picker_releases = False
        elif objectType == 'barras':
            picker_nodos = False
            picker_barras = 5
            picker_releases = False
        elif objectType == 'releases':
            picker_nodos = False
            picker_barras = False
            picker_releases = 5
        elif objectType == 'all':
            picker_nodos = 5
            picker_barras = 5
            picker_releases = False

        for nodo in self.nodos_list.values():
            nodo.set_picker(picker_nodos, key=self.key_self)
        for barra in self.barras_list.values():
            barra.set_picker(picker_barras, key=self.key_self)
            # for rel in barra.artists[self.key_self][0].rel:
            #     rel.set_picker(picker_releases)

    def connect(self, objectType):
        self.setPickable(objectType)
        self._cidpick = self.fig.canvas.mpl_connect(
                            'pick_event', self.onPick)
        self._cidrelease = self.fig.canvas.mpl_connect(
                            'button_release_event', self.onRelease)
        self._cidkeypress = self.fig.canvas.mpl_connect(
                            'key_press_event', self.onKeyPress)
        self._cidkeyrelease = self.fig.canvas.mpl_connect(
                            'key_release_event', self.onKeyRelease)
        if objectType is 'releases':
            self.rs.disconnect()
        else:
            self.rs.connect()
       
    def disconnect(self):
        self.fig.canvas.mpl_disconnect(self._cidpick)
        self.fig.canvas.mpl_disconnect(self._cidrelease)
        self.fig.canvas.mpl_disconnect(self._cidkeypress)
        self.fig.canvas.mpl_disconnect(self._cidkeyrelease)
        self.rs.disconnect()

class PickerRel(QObject):
    """
    Clase que agrega la capacidad de seleccionar objetos (childs) del axes.
    Sólo permite seleccionar objetos nodo (patch.Rectangle) y objetos barra
    (mlines.Line2D)

    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - nodos_list [list]: lista de nodos en el canvas
        - barras_list [list]: lista de barras en el canvas

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - objectType: modo de funcionamiento
        - rs: RectangleSelector widget definido en módulo SelectorWidgets.py.
              Define la capacidad de dibujar un rectángulo cuando se arrastra
              el mouse
        - list_picks [list]: Lista con objeto clickeados por el mouse. El
                             matplotlib, por defecto, selecciona todo los objetos
                             que están en las coordenadas del click. Para un click
                             debería quedarme solo con un objeto.
        - selected_objects [list]: Lista de objetos seleccionados.
        - prev_objects [list]: Lista de objetos previamente seleccionados
        - consecutive [bool]: Bool que define si el siguiente objeto seleccionado
                              se agrega al anterior o lo reemplaza
        - ignore_release [bool]: Bool para ignorar el callback cuando se suelta
                                 el click del mouse

    Métodos:
        - onPick(): Callback del 'pick_event'. Define la lista list_picks
        - onRelease(): Callback del evento 'mouse release'. Actualiza lista de
                     objetos seleccionados y los colorea. Vuelve al color original
                     la lista de objetos previamente seleccionados. Emite pyqt
                     signal 'picked'
        - colorSelected(): Pinta de color rojo los objetos en selected_objects y
                         vuelve al color original los objetos en prev_objects
        - onKeyPress(): Callback de 'key press'. Define la funcionalida de
                      seleccionar objetos consecutivos usando 'shift' y de
                      eliminar objetos usando 'delete'
        - onKeyRelease(): Callback de 'key release'. Deja de seleccionar objetos
                        consecutivos
        - object_select_callback(): callback del RectangleSelector. Agrega a
                                  selected_objects todos los objetos que estan
                                  completamente adentro del rectangulo y los
                                  colorea.
        - connect(objectType): Conecta los callbacks con el modo objectType
        - disconnect(): Desconecta los callbacks

    Signals:
        - picked: Se emite al final del callback 'onRelease'

    """
    picked = pyqtSignal(dict, dict)
    cleared = pyqtSignal()

    def __init__(self, ax, nodos_list=[], barras_list=[],
                 nodecolor='b', barracolor='k', selectcolor='r', 
                 key_self='self'):
        """

        objectType: 'all', 'nodos', 'barras'
        """
        super().__init__()

        self.ax = ax
        self.fig = ax.get_figure()

        self.list_picks = []
        self.selected_objects = []
        self.prev_obj = []
        self.nodos_list = nodos_list
        self.barras_list = barras_list
        self.consecutive = True  # Flag que dice si se seleccionan objetos seguidos
        self.ignore_onrelease = False
        self.nodecolor = nodecolor
        self.barracolor = barracolor
        self.selectcolor = selectcolor
        self.key_self = key_self

        self.connect()
        self.disconnect()

    def onPick(self, event):
        """
        Guarda todos los artistas pickeados y los guarda para posterior uso
        """
        if not event.mouseevent.button == 1: return
        self.list_picks.append(event.artist)

    def onRelease(self, event):
        """
        Este event handler se necesita ya que en los nodos, el pick event
        se ejecuta dos veces agarrando la barra y el nodo.
        EL programa requiere que en esos casos, se seleccione solo el nodo.
        """

        # obj = []
        if not self.list_picks:
            self.clearSelected()
            qinc = {}
            ext = {}
            self.picked.emit(qinc,ext)
            return
        if self.list_picks:
            obj = [self.list_picks[0]]

        # Actualizo lista de objetos seleccionados
        if obj[0] not in self.selected_objects:
            self.selected_objects += obj
        # self.selected_objects = self.list_picks
        # Coloreo listo de objetos seleccionados y vuelvo al color original
        # lista de objetos previos
        self.colorSelected()
        # Reseteo lista de objetos pickeadoss)
        self.list_picks = []

        # Envío senal con objetos seleccionados
        qinc, ext = self.findIndices() # cambiar para hallar q y rs
        self.picked.emit(qinc, ext)

    def colorSelected(self):
        """
        Función para pintar de rojo todos los objetos seleccionados
        """
        # Volver al color original objetos previos
        for prev_obj in self.prev_obj:
            if (type(prev_obj) is FancyArrowPatch) or (type(prev_obj) is \
                                                                Arrow2D):
                prev_obj.set_color(self.nodecolor)
            if type(prev_obj) is mlines.Line2D:
                prev_obj.set_markeredgecolor(self.barracolor)
                prev_obj.set_markerfacecolor('white')
        # Colorear objetos seleccionados
        for obj in self.selected_objects:
            if (type(obj) is FancyArrowPatch) or (type(obj) is Arrow2D):
                obj.set_color(self.selectcolor)
            if type(obj) is mlines.Line2D:
                obj.set_markeredgecolor(self.selectcolor)
                # obj.set_markerfacecolor(self.selectcolor)
                obj.set_zorder(obj.zorder+1)

        self.fig.canvas.draw()

    def clearSelected(self):
        self.prev_obj = self.selected_objects
        self.selected_objects = []
        self.list_picks = []
        self.colorSelected()
        self.cleared.emit()

    def findIndices(self):
        qinc = {} #dic: keys hace referencia la indice de barra, value hace referencia
        # al indice del q 0,1,2
        ext = {} #dic: keys hace referencia al indice de nodo, value hace 
        # referencia al indice del DOF 0,1,2
        for obj in self.selected_objects:
            # Click en una flecha
            if (type(obj) is FancyArrowPatch) or (type(obj) is Arrow2D): 
            # Si ya se clikeo antes en una flecha que corresponde al mismo nodo 
            # que la flecha actual
                for nodo in self.nodos_list.values():
                    arrows_list = []
                    counter = 0
                    for i in range(len(nodo.restr)):
                        if nodo.restr[i] != 0:
                            arrows_list.append( 
                                nodo.artists['reacciones_fl'][counter].flecha )
                            counter += 1
                        else:
                            arrows_list.append( 0 )
                    # arrows_list = [arrow.flecha for arrow in \
                    #                nodo.artists['reacciones_fl']]
                    if obj in arrows_list:
                        if nodo.indice in ext.keys():
                            ext[nodo.indice].append(arrows_list.index(obj))
                        else:
                            ext[nodo.indice] = [arrows_list.index(obj)]
            if type(obj) is mlines.Line2D:
                for barra in self.barras_list.values():
                    rel_list = barra.artists[self.key_self][0].rel
                    if obj in rel_list:
                        if barra.indice in qinc.keys():
                            qinc[barra.indice].append(rel_list.index(obj))
                        else:
                            qinc[barra.indice] = [rel_list.index(obj)]
        return qinc, ext

    def setPickable(self):

        picker_nodos = False
        picker_barras = False
        picker_releases = 5

        for nodo in self.nodos_list.values():
            nodo.set_picker(picker_nodos)
        for barra in self.barras_list.values():
            barra.set_picker(picker_barras)
            for rel in barra.artists[self.key_self][0].rel:
                rel.set_picker(picker_releases)

    def connect(self):
        # self.setPickable()
        self._cidpick = self.fig.canvas.mpl_connect(
            'pick_event', self.onPick)
        self._cidrelease = self.fig.canvas.mpl_connect(
            'button_release_event', self.onRelease)

    def disconnect(self):
        self.fig.canvas.mpl_disconnect(self._cidpick)
        self.fig.canvas.mpl_disconnect(self._cidrelease)


class NodeDrawer(QObject):
    """
    Clase que agrega la capacidad de dibujar nodos al hacer click izquierdo
    del mouse. Los nodos creados son almacenados en una lista de nodos como
    objetos patch.Rectangle. 
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - nodos_list [list]: Lista de nodos existentes en canvas
        - grid [dict]: {'xgrid':xgrid,'ygrid':ygrid} donde xgrid e ygrid
                               son np.arrays con las coordenadas de los puntos
                               de la grilla
        - horizOn [bool]: Bool para dibujar línea horizontal de mouse snapping
        - vertOn [bool]: Bool para dibujar línea vertical de mouse snapping
        - useblit [bool]: Bool to use blit function to update canvas (faster)
        - nodeprops [dict]: Dict con propiedades del rectángulo de los nodos.
                            {'nodeside':0.2,'nodecolor':'b'}
        - **kwargs [dict]: key word arguments que definen las propiedades de las
                           líneas verticales y horizontales del mouse
        
    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - canvas: figure canvas
        - nodeprops [dict]: mismo del argumento
        - grid [dict]: mismo del argumento
        - visible [bool]: visibilidad de líneas hor y ver del mouse
        - horizOn [bool]: mismo del argumento
        - vertOn [bool]: mismo del argumento
        - useblit [bool]: mismo del argumento
        - lineh [mpl object]: Línea horizontal de matplotlib
        - linev [mpl object]: Línea vertical de matplotlib
        - background [mpl object]: canvas background from copy_from_bbox
        - needclear [bool]: define si hay que hacer canvas.draw()
        - snapon [bool]: Define si hay snapping a los puntos de la grilla
        - nodos [list]: Lista de patch.Rectangle
        - nodos_xcoord [list]: Lista con las coordenadas x de los nodos
        - nodos_ycoord [list]: Lista con las coordenadas y de los nodos
        - xsnap [float]: coordenada x del snap
        - ysnap [float]: coordenada y del snap
        
    
    Métodos:
        - clear(): Actualiza el background y invisibiliza las lineas hor y vert
        - onmove(): Actualiza posición de líneas del cursor (con o sin snap)
        - _update(): Actualiza lo que se ve en el canvas
        - on_release(): Crea y dibuja el nodo en las coordenadas (con o sin snap)
                        donde se soltó el mouse
        - connect(): Conecta los callbacks
        - disconnect(): Desconecta los callbacks
        
    Signals:
        - created: Se emite luego de crear el objeto nodo (patch.Rectangle)

    """
    created = pyqtSignal(tuple)
    
    def __init__(self, ax, grid=False, horizOn=True, vertOn=True, boxOn=True,
                 useblit=True, markersize=6, **lineprops):
        """
        Add a cursor to *ax*.  If ``useblit=True``, use the backend-dependent
        blitting features for faster updates.  *lineprops* is a dictionary of
        line properties.
        """
        # AxesWidget.__init__(self, ax)
        super().__init__()
        
        self.ax = ax
        self.fig = ax.get_figure()
        self.canvas = self.fig.canvas

        # Connect events
        self.connect()
        self.disconnect()
        
        # self.nodeprops = nodeprops
        self.grid = grid
        
        self.visible = True
        self.horizOn = horizOn
        self.vertOn = vertOn
        self.boxOn = boxOn
        self.useblit = useblit and self.canvas.supports_blit

        if self.useblit:
            lineprops['animated'] = True
        self.lineh = ax.axhline(ax.get_ybound()[0], visible=False, **lineprops)
        self.linev = ax.axvline(ax.get_xbound()[0], visible=False, **lineprops)
        self.box = self.ax.plot(0,0,'s',fillstyle='none',
                                markersize=markersize+2,
                                markeredgecolor='k', animated=True)[0]

        self.background = None
        self.needclear = False
        if grid:
            self.snapon = True
        else:
            self.snapon = False
        
        self.xsnap = 0
        self.ysnap = 0

    def clear(self, event):
        """clear the cursor"""
        if self.useblit:
            self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.linev.set_visible(False)
        self.lineh.set_visible(False)
        self.box.set_visible(False)

    def onmove(self, event):
        """on mouse motion draw the cursor if visible"""
        if not self.canvas.widgetlock.available(self):
            return
        if event.inaxes != self.ax:
            self.linev.set_visible(False)
            self.lineh.set_visible(False)
            self.box.set_visible(False)

            if self.needclear:
                self.canvas.draw()
                self.needclear = False
            return
        self.needclear = True
        if not self.visible:
            return
        
        x = event.xdata
        y = event.ydata
        if self.grid and self.snapon:
            # Get closest grid point
            indx = (np.abs(np.array(self.grid['xgrid'])-x)).argmin()
            self.xsnap = self.grid['xgrid'][indx]
            indy = (np.abs(np.array(self.grid['ygrid'])-y)).argmin()
            self.ysnap = self.grid['ygrid'][indy]
        else:
            self.xsnap = x
            self.ysnap = y

        self.linev.set_xdata((self.xsnap, self.xsnap))
        self.lineh.set_ydata((self.ysnap, self.ysnap))
        self.box.set_data(self.xsnap,self.ysnap)
        self.linev.set_visible(self.visible and self.vertOn)
        self.lineh.set_visible(self.visible and self.horizOn)
        self.box.set_visible(self.visible and self.boxOn)
        
        self._update()

    def _update(self):

        if self.useblit:
            if self.background is not None:
                self.canvas.restore_region(self.background)
            self.ax.draw_artist(self.linev)
            self.ax.draw_artist(self.lineh)
            self.ax.draw_artist(self.box)
            self.canvas.blit(self.ax.bbox)
        else:
            self.canvas.draw_idle()

        return False

    def on_release(self,event):
        # Solo crear nodo con click izquierdo
        if not event.button == 1:
            return
        # Draw node
        node_center = (self.xsnap,self.ysnap)
        self.created.emit(node_center)
        
        # Update cursor lines and canvas
        self.linev.set_xdata((self.xsnap, self.xsnap))
        self.lineh.set_ydata((self.ysnap, self.ysnap))
        self.box.set_data(self.xsnap, self.ysnap)
        self.linev.set_visible(self.visible and self.vertOn)
        self.lineh.set_visible(self.visible and self.horizOn)
        self.box.set_visible(self.visible)
        self.canvas.draw()
        self._update()

    def connect(self):
        """connect events"""
        self.canvas.draw()
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self._cidmotion = self.canvas.mpl_connect('motion_notify_event',
                                                  self.onmove)
        self._ciddraw = self.canvas.mpl_connect('draw_event', self.clear)
        self._cidrelease = self.canvas.mpl_connect('button_release_event',
                                                   self.on_release)
        self.needclear = True

    def disconnect(self):
        """disconnect events"""
        self.canvas.mpl_disconnect(self._cidmotion)
        self.canvas.mpl_disconnect(self._ciddraw)
        self.canvas.mpl_disconnect(self._cidrelease)

##
class BarDrawer(QObject):
    """
    Clase que agrega la capacidad de dibujar barras entre nodos (únicamente)
    al clickear y arrastrar el mouse.
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - barras_list [list]: Lista de barras existentes en canvas
        - grid [dict]: {'xgrid':xgrid,'ygrid':ygrid} donde xgrid e ygrid
                               son np.arrays con las coordenadas de los puntos
                               de la grilla
        - horizOn [bool]: Bool para dibujar línea horizontal de mouse snapping
        - vertOn [bool]: Bool para dibujar línea vertical de mouse snapping
        - useblit [bool]: Bool to use blit function to update canvas (faster)
        - barprops [dict]: Dict con propiedades del rectángulo de las barras.
                           {'framewidth':2,'framestyle':'-','color':'k'}
        - **kwargs [dict]: key word arguments que definen las propiedades de las
                           líneas verticales y horizontales del mouse
        
    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - canvas: figure canvas
        - barprops [dict]: mismo del argumento
        - grid [dict]: mismo del argumento
        - visible [bool]: visibilidad de líneas hor y ver del mouse
        - horizOn [bool]: mismo del argumento
        - vertOn [bool]: mismo del argumento
        - useblit [bool]: mismo del argumento
        - lineh [mpl object]: Línea horizontal de matplotlib
        - linev [mpl object]: Línea vertical de matplotlib
        - background [mpl object]: canvas background from copy_from_bbox
        - needclear [bool]: define si hay que hacer canvas.draw()
        - draw_line [bool]: Indica si hay que dibujar la barra o no
        - barras [list]: Lista de mlines.Line2D
        - xsnap [float]: coordenada x del snap
        - ysnap [float]: coordenada y del snap
        
    Métodos:
        - clear(): Actualiza el background y invisibiliza las lineas hor y vert
        - onmove(): Actualiza posición de líneas del cursor (con o sin snap), y
                    de la barra y sus puntos inicial y final si draw_line=True
        - _update(): Actualiza lo que se ve en el canvas
        - on_press(): Crea la barra con puntos incial y final donde se hizo el
                      click
        - on_release(): Crea y dibuja la barra en las coordenadas de la grilla
                        de nodos
        - connect(newgrid): Actualiza la grilla de nodos y conecta los callbacks
        - disconnect(): Desconecta los callbacks
        
    Signals:
        - created: Se emite luego de crear el objeto nodo (patch.Rectangle)

    """
    # created = pyqtSignal(mlines.Line2D)
    created = pyqtSignal(tuple)

    def __init__(self, ax, grid,  horizOn=True, vertOn=True, boxOn=True,
                 useblit=True, markersize=6,
                 barprops={'framewidth':2,'framestyle':'-','color':'k'},
                 **lineprops):
        """
        Add a cursor to *ax*.  If ``useblit=True``, use the backend-dependent
        blitting features for faster updates.  *lineprops* is a dictionary of
        line properties.
        """
        # AxesWidget.__init__(self, ax)
        super().__init__()

        self.ax = ax
        self.fig = ax.get_figure()
        self.canvas = self.fig.canvas

        # Se inicializa la barra sin releases
        self.releases = 0
        self.elemname = '2DFrame'

        # Connect events
        self.connect(grid, self.releases)
        self.disconnect()
        
        self.barprops = barprops
        self.grid = grid
        
        self.visible = True
        self.horizOn = horizOn
        self.vertOn = vertOn
        self.boxOn = boxOn
        self.useblit = useblit and self.canvas.supports_blit

        if self.useblit:
            lineprops['animated'] = True
        self.lineh = ax.axhline(ax.get_ybound()[0], visible=False, **lineprops)
        self.linev = ax.axvline(ax.get_xbound()[0], visible=False, **lineprops)
        self.box = self.ax.plot(0, 0, 's', fillstyle='none', 
                                markersize=markersize+2,
                                markeredgecolor='k', animated=True)[0]

        self.background = None
        self.needclear = False
        self.draw_line = False
        self.xsnap = 0
        self.ysnap = 0

    def clear(self, event):
        """clear the cursor"""
        # if self.ignore(event):
        #     return
        if self.useblit:
            self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.linev.set_visible(False)
        self.lineh.set_visible(False)
        self.box.set_visible(False)

    def onmove(self, event):
        """on mouse motion draw the cursor if visible"""
        # if self.ignore(event):
        #     return
        if not self.canvas.widgetlock.available(self):
            return
        if event.inaxes != self.ax:
            self.linev.set_visible(False)
            self.lineh.set_visible(False)
            self.box.set_visible(False)

            if self.needclear:
                self.canvas.draw()
                self.needclear = False
            return
        self.needclear = True
        if not self.visible:
            return
        
        x = event.xdata
        y = event.ydata
        if self.grid:
            # Get closest of the individual points
            ind = ((np.array(self.grid['xgrid'])-x)**2 + \
                   (np.array(self.grid['ygrid'])-y)**2).argmin()
            self.xsnap = self.grid['xgrid'][ind]
            self.ysnap = self.grid['ygrid'][ind]            
        else:
            self.xsnap = x
            self.ysnap = y

        self.linev.set_xdata((self.xsnap, self.xsnap))
        self.lineh.set_ydata((self.ysnap, self.ysnap))
        self.box.set_data(self.xsnap, self.ysnap)
        # self.box.set_xy((self.xsnap - 0.2/ 2,
        #                  self.ysnap - 0.2/ 2))
        self.linev.set_visible(self.visible and self.vertOn)
        self.lineh.set_visible(self.visible and self.horizOn)
        self.box.set_visible(self.visible and self.boxOn)
        
        if self.draw_line:
            # Update bar position
            dx = x - self.pointi_center[0]
            dy = y - self.pointi_center[1]
            self.pointf_center = (self.pointi_center[0]+dx,
                                  self.pointi_center[1]+dy)
            self.baraxis.set_data([self.pointi_center[0],self.pointf_center[0]],
                                  [self.pointi_center[1],self.pointf_center[1]])
        
        self._update()

    def _update(self):

        if self.useblit:
            if self.background is not None:
                self.canvas.restore_region(self.background)
            self.ax.draw_artist(self.linev)
            self.ax.draw_artist(self.lineh)
            self.ax.draw_artist(self.box)
            if self.draw_line:
                self.baraxis.draw_artist()
                # self.ax.draw_artist(self.baraxis)
            self.canvas.blit(self.ax.bbox)
        else:
            self.canvas.draw_idle()

        return False
    
    def on_press(self,event):
        """
        On press of mouse button a node is drawn
        """
        # if self.ignore(event):
        #     return
        if not self.canvas.widgetlock.available(self):
            return
        if event.inaxes != self.ax:
            self.linev.set_visible(False)
            self.lineh.set_visible(False)

            if self.needclear:
                self.canvas.draw()
                self.needclear = False
            return
        self.needclear = True
        if not self.visible:
            return
        if not event.button == 1: # Solo crear barra con click izquierdo
            return

        # Set initial node coordinates
        self.pointi_center = (self.xsnap,self.ysnap)
        
        # Set final node coordinates
        self.pointf_center = self.pointi_center
        
        # Create bar-axis line (without drawing)
        self.createBar(self.pointi_center, self.pointf_center, self.elemname)
        self.baraxis.set_animated(True)
        
        # Set line drawer flag
        self.draw_line = True
        
    def on_release(self,event):
        if not event.button == 1: # Solo crear barra con click izquierdo
            return
        self.linev.set_xdata((self.xsnap, self.xsnap))
        self.lineh.set_ydata((self.ysnap, self.ysnap))
        self.linev.set_visible(self.visible and self.vertOn)
        self.lineh.set_visible(self.visible and self.horizOn)
        # Update baraxis
        self.pointf_center = (self.xsnap, self.ysnap)
        self.baraxis.set_data((self.pointi_center[0], self.pointf_center[0]),
                              (self.pointi_center[1], self.pointf_center[1]))
        self.baraxis.set_animated(False)
        # Create bar only if initial and final point are different
        if self.pointi_center != self.pointf_center:
            # Add bar to bar list
            # self.barras_list.append(self.baraxis)
            # self.barras_list.pop()
            # Emit signal
            # self.created.emit(self.baraxis)
            self.baraxis.remove()
            self.created.emit((self.pointi_center, self.pointf_center,
                               self.elemname))
        else:
            self.baraxis.remove()
            # Eliminate last added baraxis
            # del self.barras_list[len(self.barras_list)-1]

        self.canvas.draw()
        self.draw_line = False
        self._update()

    def createBar(self, pointi_center, pointf_center, elemname = '2DFrame',
                  zorder=1):
        # Create bar-axis line (without drawing)
        self.baraxis = FrameBar(self.ax,
                                [pointi_center[0],pointf_center[0]],
                                [pointi_center[1],pointf_center[1]],
                                linewidth=self.barprops['framewidth'],
                                color=self.barprops['color'],
                                elemname = elemname, zorder=zorder)
        # self.baraxis.set_picker(5)
        self.canvas.draw_idle()
        # self.barras_list.append(self.baraxis)

        # if emit:
        #     # self.barras_list.append(self.baraxis)
        #     self.baraxis.set_animated(False)
        #     # Emit signal
        #     self.created.emit(self.baraxis)

    def connect(self, newgrid, elemname='2DFrame'):
        """connect events"""
        self.elemname = elemname
        self.grid = newgrid
        self.canvas.draw()
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self._cidmotion = self.canvas.mpl_connect('motion_notify_event',
                                                  self.onmove)
        self._ciddraw = self.canvas.mpl_connect('draw_event', self.clear)
        self._cidpress = self.canvas.mpl_connect('button_press_event',
                                                 self.on_press)
        self._cidrelease = self.canvas.mpl_connect('button_release_event',
                                                   self.on_release)
        self.needclear = True

    def disconnect(self):
        """disconnect events"""
        self.canvas.mpl_disconnect(self._cidmotion)
        self.canvas.mpl_disconnect(self._ciddraw)
        self.canvas.mpl_disconnect(self._cidpress)
        self.canvas.mpl_disconnect(self._cidrelease)

## =============================================================================
class Zoom:
    """
    Clase que agrega la capacidad de hacer zoom in y out con el scroll del mouse
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib
        - base_scale [float]: Define la escala para agrandar o achicar el zoom
        
    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - base_scale: mismo del argumento
        - cur_xlim: axes xlim
        - cur_ylim: axes ylim
        
        
    Métodos:
        - zoom(): Callback del 'scroll event'. Agranda y achica los limites del
                  canvas
        - connect(): Conecta los callbacks
        - disconnect(): Desconecta los callbacks
        
    Signals:
        - 

    """
    def __init__(self, ax, base_scale = 1.3):
        self.cur_xlim = None
        self.cur_ylim = None
        self.ax = ax
        self.base_scale = base_scale
        
        self.fig = ax.get_figure() # get the figure of interest
        
        self.connect()
        self.disconnect()
        
    def zoom(self, event):

        cur_xlim = self.ax.get_xlim()
        cur_ylim = self.ax.get_ylim()

        xdata = event.xdata # get event x location
        ydata = event.ydata # get event y location

        if event.button == 'down':
            # deal with zoom in
            scale_factor = self.base_scale
        elif event.button == 'up':
            # deal with zoom out
            scale_factor = 1 / self.base_scale
        else:
            # deal with something that should never happen
            scale_factor = 1
            print (event.button)

        new_width = (cur_xlim[1] - cur_xlim[0]) * scale_factor
        new_height = (cur_ylim[1] - cur_ylim[0]) * scale_factor

        relx = (cur_xlim[1] - xdata)/(cur_xlim[1] - cur_xlim[0])
        rely = (cur_ylim[1] - ydata)/(cur_ylim[1] - cur_ylim[0])

        self.ax.set_xlim([xdata-new_width*(1-relx), xdata+new_width*(relx)])
        self.ax.set_ylim([ydata-new_height*(1-rely), ydata+new_height*(rely)])
        self.ax.figure.canvas.draw()
        
    def connect(self):
        self._cidscroll = self.fig.canvas.mpl_connect('scroll_event', self.zoom)
        
    def disconnect(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidscroll)
        
class Pan:
    """
    Clase que agrega la capacidad de panear en el canvas arrastrando el click
    del mouse
    
    Argumentos:
        - ax [matplotlib.figure.axes]: objeto de ejes de matplotlib

    Atributos:
        - ax: objeto de ejes de matplotlib
        - fig: figure de matplotlib correspondiente al self.ax
        - press [set]: set con x0, y0, event.xdata, event.ydata
        - xpress [float]: Coordenada x cuando se clickea
        - ypress [float]: Coordenada y cuando se clickea
        - keypress [False o str]: nombre de la tecla que activa el pan. Si
                                  keypress=False, entonces el pan está siempre
                                  activado

    Métodos:
        - onKeyPress(): Callback del 'key press event'. Llama a connect_pan()
        - onKeyRelease(): Callback del 'key release event'. Llama a
                          disconnect_pan()
        - onPress(): Lee límites del gráfico y punto donde se presionó el mouse
        - onRelease(): Actualiza el canvas
        - onMotion(): Redefine límites del gráfico
        - connect_pan(): Conecta los callback de mouse press,release y motion
        - disconnect_pan(): Desconecta los callback de mouse press,release y motion
        - connect(keypress): Conecta los callbacks
        - disconnect(): Desconecta los callbacks
        
    Signals:
        - 

    """
    def __init__(self,ax):
        self.ax = ax
        self.press = None
        self.xpress = None
        self.ypress = None
        self.x0 = None
        self.y0 = None
        
        self.fig = self.ax.get_figure() # get the figure of interest
        
        # El Objeto se inicializa desconectado
        self.connect()
        self.disconnect()


    def onKeyPress(self,event):
        if event.inaxes != self.ax: return
        if not event.key == self.keypress: return
        self.connect_pan()
        
    def onKeyRelease(self,event):
        if not event.key == self.keypress: return
        self.disconnect_pan()
    
    def onPress(self,event):
        if event.inaxes != self.ax: return
        if not event.button == 1: return
        self.cur_xlim = self.ax.get_xlim()
        self.cur_ylim = self.ax.get_ylim()
        self.press = event.xdata, event.ydata
        self.xpress, self.ypress = self.press

    def onRelease(self,event):
        self.press = None
        self.fig.canvas.draw_idle()

    def onMotion(self,event):
        if self.press is None: return
        if event.inaxes != self.ax: return
        dx = event.xdata - self.xpress
        dy = event.ydata - self.ypress
        self.cur_xlim -= dx
        self.cur_ylim -= dy
        self.ax.set_xlim(self.cur_xlim)
        self.ax.set_ylim(self.cur_ylim)

        self.fig.canvas.draw_idle()
        
    def connect(self, keypress=False):
        """connect events"""
        self.keypress = keypress
        if not self.keypress:
            self.connect_pan()
            return
        self._cidkeypress = self.fig.canvas.mpl_connect('key_press_event',
                                                        self.onKeyPress)
        self._cidkeyrelease = self.fig.canvas.mpl_connect('key_release_event',
                                                        self.onKeyRelease)
    
    def disconnect(self):
        if not self.keypress:
            self.disconnect_pan()
            return
        self.fig.canvas.mpl_disconnect(self._cidkeypress)
        self.fig.canvas.mpl_disconnect(self._cidkeyrelease)
        
    def connect_pan(self):
        self._cidmotion = self.fig.canvas.mpl_connect('motion_notify_event',
                                                  self.onMotion)
        self._cidpress = self.fig.canvas.mpl_connect('button_press_event',
                                                 self.onPress)
        self._cidrelease = self.fig.canvas.mpl_connect('button_release_event',
                                                   self.onRelease)
                                                   
    def disconnect_pan(self):
        """disconnect events"""
        self.fig.canvas.mpl_disconnect(self._cidmotion)
        self.fig.canvas.mpl_disconnect(self._cidpress)
        self.fig.canvas.mpl_disconnect(self._cidrelease)



