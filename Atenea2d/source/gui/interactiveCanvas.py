# modulo interactiveCanvas.py
"""
Este modulo define la clase "AteneaCanvasQt" que es un widget de Pyqt para el
builder Qt. 

creado por: equipo Atenea-UBATIC
"""
import numpy as np

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.patches import Polygon
from matplotlib.text import Text
from matplotlib.lines import Line2D
from ..core.graph2d import Arrow2D

from PyQt5.QtCore import Qt, QSize

from .canvasWidgets import NodeDrawer, BarDrawer, Zoom, Pan, Picker, PickerRel
from .selectorWidgets import RectangleSelector
from ..core.graph2d import flechaAtenea

class AteneaCanvasQt(FigureCanvas):
    """
    Matplotlib qt backend canvas qidget con clases para la interacción con el
    usuario para insertar dentro de un Qt GUI.
    
    Argumentos:
        - parent [QWidget]: QWidget que está una jerarquía arriba en el GUI
        - layout [QLayout widget]: layout widget donde estará inserto el canvas
        - grid [False / list]: False para crear el canvas sin grilla de puntos.
                               Lista [dx,dy,Nx,Ny] para crear una grilla de 
                               Nx x Ny puntos espaciados dx y dy respectivamente
        - QSizePolicy [QWidget.QsizePolicy]: QsizePolicy object
        - coordsLabel [QLabel]: QLabel donde actualizar la posición del cursor
                                dentro del canvas
        - zoom_base_scale [float]: multplicador para definir el zoom interactivo
        
    Atributos:
        - fig [matplotlib.figure.Figure]: Matplotlib figure object
        - coordsLabel [QLabel]: QLabel donde actualizar la posición del cursor
                                dentro del canvas
        - ax [fig.axes]: objeto axes de matplotlib figure
        - grid [False or list]: False para crear el canvas sin grilla de puntos.
                                Lista [dx,dy,Nx,Ny] para crear una grilla de 
                                Nx x Ny puntos espaciados dx y dy respectivamente
        - gridon [bool]: Define si la grilla está visible o no
        - grid_flatten [dict]: {'xgrid':xgrid,'ygrid':ygrid} donde xgrid e ygrid
                               son np.arrays con las coordenadas de los puntos
                               de la grilla
        - interactions [dict]: Diccionario con todos los objetos que agregan 
                               interactividad al objeto axes (self.ax).
                               'nodedrawer': dibuja nodos con el mouse
                               'bardrawer': dibuja barras entre nodos existentes
                               'pan': panea clickeando el mouse y arrastrando
                               'zoom': hace zoom usando el scroll
                               'pickall': selecciona objetos del axes
                               'picknodes': selecciona objetos del axes, solo nodos
                               'pickbars': selecciona objetos del axes, solo barras
        - nodos [list]: Lista de patch.Rectanfgle de todos los nodos dibujados 
                        en el canvas
        - barras [list]: Lista de mlines.Line2d de todas las barras dibujadas
                         en el canvas 
         
    Metodos:
        - onMotion(event): Define el evento de escribir las coordenadas del mouse
                           durante el movimiento del mouse
        - enablePan(event): Define el evento de habilitar el modo 'pan' cuando
                            se oprime la tecla 'control'
        - disablePan(event): Define el evento de deshabilitar el 'pan' y volver
                             al modo previo cuando se suelta la tecla 'control'
        - setMode(mode, QCursor): conecta el modo indicado en el string 'mode'
                                  y le asigna el cursor QCursor
        - disconnectAll(): Desconecta todas las funcionalidades
        - setBounds(bounds): Setea los límites del canvas
        - drawGrid(grid): Dibuja la grilla de puntos definida por el list grid
        - setGridOn(gridon): Prende y apaga la visibilidad de la grilla según
                             el boolean 'gridon'
        - clearCanvas(): Elimina todos las barras y nodos
        
    """
    def __init__(self, parent=None, layout=None, grid=False, QSizePolicy=None,
                 coordsLabel=None, nodos={}, barras={}, zoom_base_scale=1.3,
                 nodeprops={'marker':'s', 'markersize':6, 'color':'b'},
                 barprops={'framewidth':2,'framecolor':'k'},
                 selectcolor='r', originArrows=True, key_self='self'):
        
        
        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5        
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        FigureCanvas.setSizePolicy(self,QSizePolicy,QSizePolicy)
        # Detect key press in canvas
        FigureCanvas.setFocusPolicy(self, Qt.ClickFocus)
        FigureCanvas.setFocus(self)
        # Detect mouse movement in canvas
        self.setMouseTracking(True)
        
        # Label widget para escribir coordenadas del mouse en statusbar
        self.coordsLabel = coordsLabel

        # Initialize and format axes properties
        self.ax = self.fig.add_subplot(111)
        self.ax.set_frame_on(True)
        self.ax.set_aspect('equal', adjustable='datalim', anchor='C')
        # self.ax.minorticks_on()
        self.ax.tick_params(axis='both', which = 'both', direction = 'out',
                labelsize=4, pad=0, length=4, width=0.2,
                bottom=True, top=True, left=True, right=True,
                labelbottom=True, labeltop=True, labelleft=True, labelright=True)
        for axis in ['top', 'bottom', 'left', 'right']:
            self.ax.spines[axis].set_linewidth(0.2)
        # create origin arrows
        self.originArrows = originArrows
        self.originArrowsArtists = []
        if originArrows:
            self.createOriginArrows()

        # Set grid
        self.grid = grid
        self.gridlines = []
        if grid:
            self.gridon = True
            self.drawGrid(self.grid)
            self.setBounds([-1, 7, -1, 7])
        elif not grid: # default bounds
            self.setBounds([-1, 7, -1, 7])
            self.grid_flatten = False
            self.gridon = False
        
        # Initialize barras and nodos lists
        self.nodos = nodos
        self.barras = barras
        self.nodeprops = nodeprops
        self.barprops = barprops
        self.selectcolor = selectcolor
        self.initial_children = self.ax.get_children()

        # Create interactive capabilities
        self.interactions = {}
        self.interactions['nodedrawer'] = NodeDrawer(self.ax,
                                            grid=self.grid_flatten,
                                            markersize=nodeprops['markersize'],
                                            linestyle='--', linewidth=.3,
                                            color='k')
        self.interactions['bardrawer']  = BarDrawer(self.ax,
                                            grid=self.grid_flatten,
                                            markersize=nodeprops['markersize'],
                                            barprops=barprops,
                                            linestyle='--', linewidth=.3,
                                            color='k')
        self.interactions['pan']        = Pan(self.ax)
        self.interactions['zoom']       = Zoom(self.ax,zoom_base_scale)
        self.interactions['picker_rel']     = PickerRel(self.ax, self.nodos,
                                                        self.barras,
                                                        nodeprops['color'],
                                                        barprops['color'],
                                                        selectcolor,
                                                        key_self=key_self)
        self.interactions['picker']     = Picker(self.ax, self.nodos,
                                                 self.barras,
                                                 nodeprops['color'],
                                                 barprops['color'],
                                                 selectcolor=selectcolor,
                                                 key_self=key_self)
        self.interactions['zoom2rect']  = RectangleSelector(self.ax,
                            self.zoom2rect, drawtype='box', useblit=True,
                            button=[1], minspanx=5, minspany=5,
                            spancoords='pixels', interactive=False,
                            rectprops=dict(facecolor='blue',edgecolor='black',
                                           alpha=0.2, fill=False))
        self.setMode('pan',Qt.PointingHandCursor)
        
        # Set pan mode with control key
        self.fig.canvas.mpl_connect('key_press_event',self.enablePan)
        self.fig.canvas.mpl_connect('key_release_event',self.disablePan)
        self.fig.canvas.mpl_connect('motion_notify_event',self.onMotion)
        
        # draw
        self.fig.canvas.draw_idle()
        
    def updateNodosyBarras(self, key_self, nodos, barras):
        self.key_self = key_self
        self.nodos = nodos
        self.barras = barras
        self.interactions['picker_rel'].nodos_list = nodos
        self.interactions['picker_rel'].barras_list = barras
        self.interactions['picker_rel'].key_self = key_self
        self.interactions['picker'].nodos_list = nodos
        self.interactions['picker'].barras_list = barras
        self.interactions['picker'].key_self = key_self
        
    def onMotion(self,event):
        if event.inaxes != self.ax: 
            if self.coordsLabel:
                self.coordsLabel.setText("Coordenadas del mouse: x= y= ") 
                self.coordsLabel.update()
            return
        mousex = event.xdata
        mousey = event.ydata
        # Chequear si hay snapping en node y bardrawers
        if self.current_mode == 'nodedrawer':
            mousex = self.interactions['nodedrawer'].xsnap
            mousey = self.interactions['nodedrawer'].ysnap
        elif self.current_mode == 'bardrawer':
            mousex = self.interactions['bardrawer'].xsnap
            mousey = self.interactions['bardrawer'].ysnap 
        # Update del label
        if self.coordsLabel:
            self.coordsLabel.setText(
                    "Coordenadas del mouse: x=%04.2f y=%04.2f " % (mousex, mousey)) 
            self.coordsLabel.update()
    
    def enablePan(self,event):
        """
        Habilitar paneo cuando se apreta la tecla control
        """
        if event.inaxes != self.ax: return
        if not event.key == 'control': return
        prev_mode = self.current_mode
        prev_cursor = self.current_cursor
        self.setMode('pan', Qt.OpenHandCursor)
        self.current_mode = prev_mode
        self.current_cursor = prev_cursor
        
    def disablePan(self,event):
        """
        Habilitar paneo cuando se apreta la tecla control
        """
        if event.inaxes != self.ax: return
        if not event.key == 'control': return
        self.setMode(self.current_mode, self.current_cursor)
        
    def setMode(self, mode, QCursor=None, **kwargs):
        self.current_mode = mode
        self.current_cursor = QCursor
        # Setear cursor
        if QCursor:
            self.setCursor(QCursor)
        # Reiniciar modos
        self.disconnectAll()
        # Setear modo
        if mode == 'pan':
            self.interactions['pan'].connect()
            self.interactions['zoom'].connect()
        elif mode == 'nodedrawer':
            self.interactions['nodedrawer'].connect()
            self.interactions['zoom'].connect()
        elif mode == 'bardrawer':
            # Update grid to node coordinates
            nodos_xcoord, nodos_ycoord = self.getNodosCoords()
            nodegrid = {'xgrid':nodos_xcoord, 'ygrid':nodos_ycoord}
            if nodegrid['xgrid']==[]:
                nodegrid = False
                return
            # Connect bar drawer with grid of nodes
            self.interactions['bardrawer'].connect(nodegrid, **kwargs)
            self.interactions['zoom'].connect()
        elif mode == 'pickall':
            self.interactions['picker'].connect('all')
            self.interactions['zoom'].connect()
        elif mode == 'picknodes':
            self.interactions['picker'].connect('nodos')
            self.interactions['zoom'].connect()
        elif mode == 'pickbars':
            self.interactions['picker'].connect('barras')
            self.interactions['zoom'].connect()
        elif mode == 'pickreleases':
            self.interactions['picker_rel'].connect()
            self.interactions['zoom'].connect()
        elif mode == 'zoom2rect':
            self.interactions['zoom2rect'].connect()
            self.interactions['zoom'].connect()

    def disconnectAll(self):
        for interaction in self.interactions.values():
            interaction.disconnect()

    def setBounds(self, axis_bounds):
        self.ax.axis(axis_bounds)

    def drawGrid(self, grid, show=True, **gridprops):
        sx = grid[0]
        sy = grid[1]
        nx = grid[2]
        ny = grid[3]
        xgrid_pos = [i*sx for i in range(0,int(np.ceil(nx/2))+1)]
        xgrid_neg = [i*sx - nx//2*sx for i in range(1,(nx-len(xgrid_pos))+1)]
        ygrid_pos = [i*sy for i in range(0,int(np.ceil(ny/2))+1)]
        ygrid_neg = [i*sy - ny//2*sy for i in range(1,(ny-len(ygrid_pos))+1)]
        xgrid = xgrid_neg + xgrid_pos
        ygrid = ygrid_neg + ygrid_pos
        self.grid_flatten = {'xgrid':xgrid,'ygrid':ygrid}
        [X,Y] = np.meshgrid(self.grid_flatten['xgrid'],self.grid_flatten['ygrid'])
        self.gridlines = self.ax.plot(X, Y, linestyle='', marker='.',
                                      markersize=.5, color='r', visible=show)

    def setGridOn(self):
        status = not self.gridon
        for gridline in self.gridlines:
            gridline.set_visible(status)
        self.interactions['nodedrawer'].snapon = status
        self.gridon = status
        self.fig.canvas.draw()
    
    def updateGrid(self, grid, show=True):
        # Clear old grid
        for gridline in self.gridlines:
            gridline.remove()
        # Draw new grid
        self.drawGrid(grid, show=show)
        self.fig.canvas.draw()
        # Update grid in nodedrawer
        self.interactions['nodedrawer'].grid = self.grid_flatten

    def clearCanvas(self, grid=False, nodos={}, barras={}):
        # eliminar todos los artistas del ax
        self.ax.clear()
        # Propiedades de los ticks
        # self.ax.minorticks_on()
        self.ax.tick_params(axis='both', which = 'both', direction = 'out',
                labelsize=4, pad=0, length=4,
                bottom=True, top=True, left=True, right=True,
                labelbottom=True,labeltop=True,labelleft=True,labelright=True)
        for axis in ['top', 'bottom', 'left', 'right']:
            self.ax.spines[axis].set_linewidth(0.2)
        # Limites
        self.setBounds([-1, 7, -1, 7])
        # Dibujar grilla de nuevo
        if grid:
            self.drawGrid(self.grid)
        self.fig.canvas.draw_idle()
        # Create origin arrows
        if self.originArrows:
            self.createOriginArrows()
        # reset nodes and bars list
        self.nodos = nodos
        self.barras =  barras
        self.initial_children = self.ax.get_children()
        self.interactions['nodedrawer'].nodos_list = self.nodos
        self.interactions['bardrawer'].barras_list = self.barras
        self.interactions['picker'].nodos_list = self.nodos
        self.interactions['picker'].barras_list = self.barras
        self.interactions['picker_rel'].nodos_list = self.nodos
        self.interactions['picker_rel'].barras_list = self.barras

    def getNodosCoords(self):
        nodos_xcoords = []
        nodos_ycoords = []
        for nodo in self.nodos.values():
            x,y = nodo.coords
            nodos_xcoords.append(x)
            nodos_ycoords.append(y)
            # for artist in nodo.artists['p']:
            #     vertices = artist.flecha._verts
            #     nodos_xcoords.extend(vertices[0])
            #     nodos_ycoords.extend(vertices[1])
            # for artist in nodo.artists['c']:
            #     vertices = artist.flecha._verts
            #     nodos_xcoords.extend(vertices[0])
            #     nodos_ycoords.extend(vertices[1])
        return nodos_xcoords, nodos_ycoords
    
    def findNodebyCoords(self, coords):
        # Actualizar lista de nodos
        nodos_xcoords, nodos_ycoords = self.getNodosCoords()
        # Buscar nodo 
        for i in range(len(nodos_xcoords)):
            nodo = (nodos_xcoords[i],nodos_ycoords[i])
            if nodo == coords:
                return i 
        return []

    def getChildrenExtent(self):
        """
        Obtener límites de los nodos
        """
        xcoords = []
        ycoords = []
        for child in self.ax.get_children():
            if child not in self.initial_children:
                if type(child) is Line2D:
                    xcoords.extend(child.get_data()[0])
                    ycoords.extend(child.get_data()[1])
                elif type(child) is Arrow2D:
                    xcoords.extend(child._verts[0])
                    xcoords.extend(child._verts[1])
                elif type(child) is Polygon:
                    xcoords.extend(list(child.get_xy()[:,0]))
                    ycoords.extend(list(child.get_xy()[:,1]))
                elif type(child) is Text:
                    xcoords.append(child.get_position()[0])
                    ycoords.append(child.get_position()[1])
        # nodos_xcoords, nodos_ycoords = self.getNodosCoords()
        xmin = min(xcoords)
        xmax = max(xcoords)
        ymin = min(ycoords)
        ymax = max(ycoords)
        return xmin, xmax, ymin, ymax

    def getNodosExtent(self):
        """
        Obtener límites de los nodos
        """
        nodos_xcoords, nodos_ycoords = self.getNodosCoords()
        xmin = min(nodos_xcoords)
        xmax = max(nodos_xcoords)
        ymin = min(nodos_ycoords)
        ymax = max(nodos_ycoords)
        return xmin, xmax, ymin, ymax

    def zoom2extent(self, margin=0.05):
        tol = 0.01
        if self.nodos:
            xmin, xmax, ymin, ymax = self.getNodosExtent()
            if (xmin == xmax) and (ymin == ymax):
                return
            xmargin = (xmax-xmin)*margin + tol
            ymargin = (ymax-ymin)*margin + tol
            dx = xmax-xmin+2*xmargin
            dy = ymax-ymin+2*ymargin
            # Respetar constrained layout
            figure_ratio = self.fig.get_size_inches()[1] / \
                           self.fig.get_size_inches()[0]
            if dy/dx > figure_ratio:
                dx = dy/figure_ratio
                new_margin = (dx - (xmax-xmin))/2
                self.setBounds([xmin-new_margin, xmax+new_margin,
                                ymin-ymargin, ymax+ymargin])
            else:
                dy = dx*figure_ratio
                new_margin = (dy - (ymax-ymin))/2
                self.setBounds([xmin-xmargin, xmax+xmargin,
                                ymin-new_margin, ymax+new_margin])
            self.fig.canvas.draw_idle()

    def zoom2rect(self, eclick, erelease, margin=0.05):
        # Ignore if there was no dragging
        if (eclick.xdata == erelease.xdata) and (
            eclick.ydata == erelease.ydata):
            return
        # Rectangle geometry
        xmin, xmax, ymin, ymax = self.interactions['zoom2rect'].extents
        xmargin = (xmax - xmin) * margin
        ymargin = (ymax - ymin) * margin
        self.setBounds([xmin - xmargin, xmax + xmargin,
                        ymin - ymargin, ymax + ymargin])
        self.fig.canvas.draw_idle()

    def createOriginArrows(self):
        self.flecha_x = flechaAtenea(self.ax, [0,0], [1,0], label='x',
                                     fontsize=8, length=0.3, reverse=False,
                                     offset=0, lw=1, color='r',
                                     visible=True, zorder=1, transform=True)
        self.flecha_x.set_arrowstyle('fancy, head_length=5, head_width=3')
        self.originArrowsArtists.append(self.flecha_x)
        self.flecha_y = flechaAtenea(self.ax, [0,0], [0,1], label='y',
                                     fontsize=8, length=0.3, reverse=False,
                                     offset=0, lw=1, color='r',
                                     visible=True, zorder=1, transform=True)
        self.flecha_y.set_arrowstyle('fancy, head_length=5, head_width=3')
        self.originArrowsArtists.append(self.flecha_y)

class BarrasDisplayCanvas(FigureCanvas):
    """
    """
    def __init__(self, parent=None, layout=None, QSizePolicy=None):
        # Create Qt Widget with Matplotlib Backend
        # Create figure
        self.fig = Figure(constrained_layout=True, dpi=150)
        # Create canvas backedn for pyqt5
        FigureCanvas.__init__(self, self.fig)
        # Set parent widget of canvas
        self.setParent(parent)
        # Add canvas to layout
        layout.addWidget(self)
        # Set that canvas expand with layout
        FigureCanvas.setSizePolicy(self, QSizePolicy, QSizePolicy)
        FigureCanvas.setMaximumHeight(self, 250)
        FigureCanvas.setMinimumHeight(self, 1)

        # Initialize and format axes properties
        self.ax = self.fig.add_subplot(111)
        self.ax.set_frame_on(False)
        self.ax.get_xaxis().set_visible(False)
        self.ax.get_yaxis().set_visible(False)
        self.ax.axis('off')
        
        # Permitir modo paneo
        # self.pan = Pan(self.ax)
        # self.pan.connect()
