# modulo atenea2dgui.py
"""
Este modulo define la clases de nodos barras y estructura con interfaz gráfica

creado por: equipo Atenea-UBATIC
"""

from ..core.bar2d import Nodo, Barra
from ..core.model2d import Estructura
from ..core.graph2d import *

class NodoGUI(Nodo):
    """
    """
    def __init__(self, coords, indice):

        # Inicializar superclase
        Nodo.__init__(self, coords, indice)

        # Propiedad para contener artistas de matplotlib
        self.artists = {'self': [], 'v': [], 'p': [], 'c': [], 'index': [],
                        'reacciones': []}

    def plot(self, axes, fontsize, indice_visible, keys=['self','index'],
             picker=True, **kwargs):
        self.artists[keys[0]] = plot_nodo(self, axes, **kwargs)
        self.artists[keys[1]] = plot_indice_nodo(self, axes, fontsize,
                                                 indice_visible)
        if picker:
            self.set_picker(key=keys[0])

    def plot_vinculos(self, axes, key='v', visible=True, length=1, zorder=1,
                      color='r'):
        self.artists[key] = plot_vinculos(self, axes, visible=visible,
                                          length=length, zorder=zorder,
                                          color=color)

    def plot_cedimientos(self, axes, color, fontsize=8, visible=True,
                         zorder=1, length=1, key='c'):
        self.artists[key] = plot_cedimientos(self, axes, color=color,
                                             fontsize=fontsize, visible=visible,
                                             zorder=zorder, length=length)

    def plot_fuerzas(self, axes, fontsize=8, visible=True, color='b', length=1,
                     key='p', zorder=1):
        self.artists[key] = plot_fuerzas_nodales(self, axes, fontsize=fontsize,
                                                 length=length, color=color,
                                                 visible=visible, zorder=zorder)

    def plot_reacciones(self, axes, fontsize=8, color='g', length=1, R=None,
                        visible=True, zorder=1, key='reacciones', precision=3):
        self.artists[key] = plot_reacciones(self, axes, fontsize=fontsize,
                                            color=color, length=length,
                                            visible=visible, zorder=zorder,
                                            precision=precision, R=R)

    def set_picker(self, radius=5, key='self'):
        for artist in self.artists[key]:
            artist.set_picker(radius)

    def clean(self, types):
        """
        Eliminar los artistas que correspondan del nodo.
        mpde: string que indica si se quieren eliminar artistas de la carga de
        estructura, o de los resultados dicts: lista de todos los artists y textos
        asociados que se quieran borrar
        """
        for type in types:
            for artist in self.artists[type]:
                artist.remove()
            self.artists[type] = []

class BarraGUI(Barra):
    """
    """
    def __init__(self, indice, nodos, ElemName, rel=[], w={}, e0=[],
                 data={'seccion':{},'mat':{}}):

        # Inicializar superclase
        Barra.__init__(self,indice,nodos,ElemName,rel,w,e0,data)

        # Propiedad para contener artistas de matplotlib
        self.artists = {'self':[],'w': [], 'e':[], 'ejes': [],
                        'index':[], 'M':[],'Q':[], 'N':[], 'M_loc':[],
                        'Q_loc':[], 'N_loc':[], 'deformada':[], 'q_ini':[]}

    def plot(self, axes, fontsize, indice_visible, ejes_visible, barra_color='k',
             ejes_color='y', keys=['self','ejes','index'], zorder=1, picker=True,
             linewidth=1):
        self.artists[keys[0]] = plot_barra(self, axes, linewidth=linewidth,
                                           color=barra_color, zorder=zorder)
        self.artists[keys[1]] = plot_ejes_locales(self, axes, fontsize=fontsize,
                                                  color=ejes_color,
                                                  visible=ejes_visible,
                                                  zorder=zorder)
        self.artists[keys[2]] = plot_indice_barra(self, axes, fontsize=fontsize,
                                                  visible=indice_visible)
        if picker:
            self.set_picker(key=keys[0])

    def plot_fuerzas(self, axes, fontsize=8, length=1, key='w', color='b',
                     visible=True, zorder=1):
        self.artists[key] = plot_fuerzas_distribuidas(self, axes,
                                fontsize=fontsize, color=color, visible=visible,
                                zorder=zorder, length=length)

    def plot_q_ini(self, axes, q, key=['q_ini'], fontsize=8, length=1,
                   color='m', zorder=1, linewidth=1):
        self.artists[key] = plot_barra_q_ini(axes, self, q, color=color,
                                             length=length, fontsize=fontsize,
                                             zorder=zorder, linewdith=linewidth)

    def plot_temperatura(self, axes, fontsize=8, colors=['b','r'],
                         zorder=1, visible=True, key='e'):
        self.artists[key] = plot_temperatura(self, axes, fontsize=fontsize,
                                             colors=colors, zorder=zorder,
                                             visible=visible)

    def plot_diagramas(self, axes, diagrama, x, linewidth=1, precision=3,
                       fontsize=8, colors=['r','b']):
        self.artists[diagrama+'_loc'] = plot_diagramas_local(self, axes,
                                                diagrama, x,
                                                linewidth=linewidth,
                                                precision=precision,
                                                fontsize=fontsize,
                                                colors=colors)

    def set_picker(self, radius=5, key='self'):
        for artist in self.artists[key]:
            artist.set_picker(radius)

    def clean(self, types):
        """
        Eliminar los artistas que correspondan de la barra.
        """
        for type in types:
            for artist in self.artists[type]:
                artist.remove()
            self.artists[type]=[]

class EstructuraGUI(Estructura):
    """
    """
    def __init__(self, nodos, barras):

        # Inicializar superclase
        Estructura.__init__(self, nodos, barras)

    def plot_estructura(self, axes, keys, fontsize, length, indice_visible,
                        ejes_visible, colors, zorder, vinculos=True,
                        acciones=True, picker=True):
        # Plotear nodos
        for nodo in self.nodos.values():
            nodo.plot(axes, fontsize, indice_visible, keys=keys['nodo'],
                      color=colors['nodo'], marker='s', markersize=5,
                      zorder=2, picker=picker)
            if vinculos:
                nodo.plot_vinculos(axes, key=keys['v'], visible=True,
                                   length=length['v'], color=colors['v'])
        # Plotear barras
        for barra in self.barras.values():
            barra.plot(axes, fontsize=fontsize, indice_visible=indice_visible,
                       ejes_visible=ejes_visible, barra_color=colors['barra'],
                       ejes_color=colors['ejes'], keys=keys['barra'],
                       zorder=1, picker=picker, linewidth=1)
        if acciones:
            self.plot_acciones(axes, keys, fontsize, length, colors, zorder)

    def plot_acciones(self, axes, keys, fontsize, length, colors, zorder):
        for nodo in self.nodos.values():
            nodo.plot_fuerzas(axes, fontsize=fontsize, visible=True,
                              color=colors['p'], length=length['p'],
                              key=keys['p'], zorder=zorder['p'])
            nodo.plot_cedimientos(axes, color=colors['c'], fontsize=fontsize,
                                  visible=True, zorder=zorder['c'],
                                  length=length['c'], key=keys['c'])
        # Plotear barras
        for barra in self.barras.values():
            barra.plot_fuerzas(axes, fontsize=fontsize, length=length['w'],
                                key=keys['w'], color=colors['w'],
                                zorder=zorder['w'])
            barra.plot_temperatura(axes, fontsize=fontsize,
                                    colors=colors['e'], zorder=zorder['e'],
                                    visible=True, key=keys['e'])

    def plot_Q_ini(self, axes, key, fontsize, length, color, zorder, linewidth):
        q_indices = [i for i, j in enumerate(self.Q_ini) if j != 0]
        for q_index in q_indices:
            for barra in self.barras.values():
                q = [self.Q_ini[a-1] for a in barra.qid]
                if any(q)!=0:
                    barra.plot_q_ini(axes, q, key=key, fontsize=fontsize,
                                     length=length, color=color, zorder=zorder,
                                     linewidth=linewidth)

    def plot_results(self, axes, colors, scale_def=1, precision=3, fontsize=8,
                     length=1, zorder={'def':1, 'diag':2, 'reacciones':1},
                     deformada=True):
        if deformada:
            # Plotear deformada
            self.plot_deformada(axes, scale_def, linewidth=1, color=colors['def'],
                                zorder=zorder['def'])
        # Plotear diagramas
        self.plot_diagramas(axes, linewidth=1, zorder=zorder['diag'],
                            colors=colors['diag'], precision=precision,
                            fontsize=fontsize, scale_d=1)
        # Plotear reacciones
        self.plot_reacciones(axes, fontsize=fontsize, color=colors['reacciones'],
                             length=length, zorder=zorder['reacciones'],
                             precision=precision)

    def plot_deformada(self, axes, scale_def, linewidth=1, color='b', zorder=1,
                       visible=True):
        plot_deformada(self, axes, scale_def, linewidth=linewidth, color=color,
                       zorder=zorder, visible=visible)

    def plot_diagramas(self, axes, linewidth=1, zorder=1, colors=['b','r'],
                       precision=3, fontsize=8, scale_d=1):
        solicitaciones = ['M','Q','N']
        for diagrama in solicitaciones:
            plot_diagramas_global(self, axes, diagrama, linewidth=linewidth,
                           zorder=zorder, colors=colors, scale_d=scale_d,
                           precision=precision, fontsize=fontsize)

    def plot_reacciones(self, axes, fontsize=8, color='g', length=1, visible=True,
                        zorder=1, key='reacciones', precision=3, R=None):
        for nodo in self.nodos.values():
            nodo.plot_reacciones(axes, fontsize=fontsize, color=color,
                             length=length, visible=visible, zorder=zorder,
                             precision=precision, R=R)

    def set_diagramas_visible(self, diagrama, visible=True):
        for barra in self.barras.values():
            for sol in diagrama:
                for artist in barra.artists[sol]:
                    artist.set_visible(visible)

    def set_barras_visible(self, keys, visible=True):
        for key in keys:
            for barra in self.barras.values():
                for artist in barra.artists[key]:
                    artist.set_visible(visible)

    def set_nodos_visible(self, keys, visible=True):
        for key in keys:
            for nodo in self.nodos.values():
                for artist in nodo.artists[key]:
                    artist.set_visible(visible)

    def set_nodos_picker(self, radius=5, key='self'):
        for nodo in self.nodos.values():
            nodo.set_picker(radius, key)

    def set_barras_picker(self, radius=5, key='self'):
        for barra in self.barras.values():
            barra.set_picker(radius, key)

    def remove_barra_artists(self, keys):
        """
        """
        for barra in self.barras.values():
            barra.clean(keys)

    def remove_nodo_artists(self, keys):
        for nodo in self.nodos.values():
            nodo.clean(keys)


