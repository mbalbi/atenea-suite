# -*- coding: utf-8 -*-

import os, sys
from datetime import datetime
import numpy as np

from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5.QtWidgets import QFileDialog, QStyleFactory, QMessageBox
from PyQt5.QtCore import Qt

# os.system("C:\\Users\\Mariano\\AppData\\Local\\Programs\\Python\\Python37\\Scripts\\pyuic5
# -o source\\gui\\mainwindow.py source\\gui\\mainwindow.ui")ñ

from Atenea3d.source.gui.mainwindow import *
from Atenea3d.source.gui.interactiveCanvas import AteneaCanvasQt, \
    BarrasDisplayCanvas
from Atenea3d.source.gui.atenea3dgui import NodoGUI, BarraGUI, EstructuraGUI
from Atenea3d.source.gui.dialogs import customDialog, runDialog, about_dialog
from Atenea3d.source.gui.dialogs import saveDialog, errorDialog

from Atenea3d.source.core.postprocess import read_txt, save_txt
from Atenea3d.source.core.analysis1el import metodo_rigideces, \
    resolver_isostatico
import Atenea3d.source.core.graph3d as graph
from Atenea3d.source.core.auxiliar import to_precision

import matplotlib as mpl
from mpl_toolkits.mplot3d import art3d


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


class AteneaForm(QMainWindow):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.resizeDocks([self.ui.arbolDock, self.ui.estructuraDock,
                          self.ui.cargasDock, self.ui.resultadosDock],
                         [1, 1, 1, 1], Qt.Horizontal)

        # Custom icons
        self.lock_icon = QtGui.QIcon()
        self.lock_icon.addPixmap(QtGui.QPixmap(
            resource_path("icons/lock_icon.png")),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.unlock_icon = QtGui.QIcon()
        self.unlock_icon.addPixmap(QtGui.QPixmap(
            resource_path("icons/unlock_icon.png")),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)

        # Validacion de input para todos los qlineedits numericos
        self.DoubleValidation()

        # Parámetros globales del modelo
        self.nodos = {}
        self.barras = {}
        self.selected_nodos = {}
        self.selected_barras = {}
        self.Estructura = None
        self.materiales = {}
        self.secciones = {}
        self.filename = None
        self.saved = False
        self.precision = 3
        self.length_flechas = 1

        # Propiedades gráficas de la estructura
        self.nodo_graph_props = {'color': 'blue', 'marker': 's',
                                 'markersize': 5,
                                 'zorder': 2}
        self.barra_graph_props = {'color': 'black', 'lw': 1, 'zorder': 1}
        self.zorder = {'nodos': 2, 'barras': 1, 'w': 3, 'P': 4, 'ced': 5}
        # Tamaño de flechas y textos
        self.fontsize = 8

        # String que indica en que modo de results
        self.results_mode = None
        self.showtextsfuerzas = True
        self.showtextsresults = True
        self.showlocal = False
        self.showIndex = False

        # Setear que las columnas del arbol se autoexpandan
        self.updateTreeNodos()
        self.updateTreeBarras()

        # Dialogs
        self.error_dialog = errorDialog()
        # self.error_dialog = QMessageBox()
        # self.error_dialog.setIcon(QMessageBox.Critical)
        # self.error_dialog.setStandardButtons(QMessageBox.Ok)
        # self.error_dialog.setWindowTitle('Error')
        self.mat_dialog = customDialog(['Nombre (sin espacios)',
                                        'Módulo de elasticidad [E]',
                                        'Coeficiente de Poisson [mu]',
                                        'Coeficiente de expansion termica [l]'],
                                       'Crear nuevo material',
                                       description='Ej. valores numéricos: 2.45, 24500, 24.5e3')
        self.seccion_dialog = customDialog(['Nombre (sin espacios)', 'Área [A]',
                                            'Momento de inercia y [Iy]',
                                            'Momento de inercia z [Iz]',
                                            'Momento de inercia polar [It]',
                                            'Ángulo de rotación de la secicón [a]'],
                                           'Crear nueva sección',
                                           description='Ej. valores numéricos: 2.45, 24500, 24.5e3')
        self.savedialog = saveDialog()

        # Crear status bar
        self.createStatusBar()

        ## Agrupar action buttons
        # Grupo de creacion de estructura
        self.drawing_group = QtWidgets.QActionGroup(self)
        self.drawing_group.addAction(self.ui.actionPickAll)
        self.drawing_group.addAction(self.ui.actionPickNodos)
        self.drawing_group.addAction(self.ui.actionPickBarras)
        self.drawing_group.addAction(self.ui.actionPan)
        self.drawing_group.addAction(self.ui.actionRotate)
        self.ui.actionRotate.setChecked(True)
        # Grupo de windows layout
        self.window_layout = QtWidgets.QActionGroup(self)
        self.window_layout.addAction(self.ui.actionTabbed_View)
        self.window_layout.addAction(self.ui.actionTile_View)
        self.window_layout.addAction(self.ui.actionCascade_View)
        self.ui.actionTabbed_View.setChecked(True)
        # Grupo de resultados
        self.ui.resultadosDock.setEnabled(False)
        # self.results_group = QtWidgets.QButtonGroup(self)
        # self.results_group.addButton(self.ui.deformadaToolButton)
        # self.results_group.addButton(self.ui.momentoToolButton)
        # self.results_group.addButton(self.ui.corteToolButton)
        # self.results_group.addButton(self.ui.normalToolButton)
        # self.results_group.addButton(self.ui.reaccionesToolButton)
        # Menu desplegable de momentos tool button
        self.ui.momentoToolButton.setPopupMode(
            QtWidgets.QToolButton.InstantPopup)
        # self.ui.momentoToolButton.setToolButtonStyle(
        #                 Qt.ToolButtonTextBesideIcon)
        self.ui.momentomenu = QtWidgets.QMenu()
        self.ui.Mymenu = self.ui.momentomenu.addAction('My')
        self.ui.Mymenu.setCheckable(True)
        self.ui.Mzmenu = self.ui.momentomenu.addAction('Mz')
        self.ui.Mzmenu.setCheckable(True)
        self.ui.Mtmenu = self.ui.momentomenu.addAction('Mt')
        self.ui.Mtmenu.setCheckable(True)
        self.ui.momentoToolButton.setMenu(self.ui.momentomenu)
        # Menu desplegable de corte tool button
        self.ui.corteToolButton.setPopupMode(
            QtWidgets.QToolButton.InstantPopup)
        self.ui.cortemenu = QtWidgets.QMenu()
        self.ui.Qymenu = self.ui.cortemenu.addAction('Qy')
        self.ui.Qymenu.setCheckable(True)
        self.ui.Qzmenu = self.ui.cortemenu.addAction('Qz')
        self.ui.Qzmenu.setCheckable(True)
        self.ui.corteToolButton.setMenu(self.ui.cortemenu)

        # Grupo de arbol selector
        self.treebuttons_group = QtWidgets.QButtonGroup(self)
        self.treebuttons_group.addButton(self.ui.selectedToolButton)
        self.treebuttons_group.addButton(self.ui.nodosToolButton)
        self.treebuttons_group.addButton(self.ui.barrasToolButton)
        self.treebuttons_group.addButton(self.ui.propiedadesToolButton)
        self.ui.selectedToolButton.setChecked(True)
        # Grupo de botones del frame de carga de estructura
        self.estructurabuttons_group = QtWidgets.QButtonGroup(self)
        self.estructurabuttons_group.addButton(self.ui.nodosButtonDraw)
        self.estructurabuttons_group.addButton(self.ui.barrasButtonDraw)
        self.estructurabuttons_group.addButton(self.ui.barrasButtonRel)
        self.estructurabuttons_group.addButton(self.ui.nodosButtonVinculo)
        self.estructurabuttons_group.addButton(self.ui.barrasButtonProps)
        self.ui.nodosButtonDraw.setChecked(True)

        # Grupo de botones del frame de barras
        self.accionesbuttons_group = QtWidgets.QButtonGroup(self)
        self.accionesbuttons_group.addButton(self.ui.nodosButtonCarga)
        self.accionesbuttons_group.addButton(self.ui.barrasButtonCarga)
        self.accionesbuttons_group.addButton(self.ui.barrasButtonTemp)
        self.accionesbuttons_group.addButton(self.ui.nodosButtonCed)
        self.ui.nodosButtonCarga.setChecked(True)

        # Crear subwindows de carga de estructura y de resultados
        # Matplotlib canvas to create structure
        self.ui.resultadosSW = self.ui.mdiArea.addSubWindow(
            self.ui.resultadosSubwindow)
        # self.ui.resultadosSW.setWindowIcon(
        #                 QtGui.QIcon('source/resources/Atenea3d_icon.png'))
        self.ui.resultadosSW.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(1, 1)))
        self.ui.resultscanvas = AteneaCanvasQt(
            parent=self.ui.resultadosSubwindow,
            layout=self.ui.horizontalLayout_resultados,
            grid=False,
            QSizePolicy=QtWidgets.QSizePolicy.Expanding,
            coordsLabel=self.coordsLabel,
            nodos=self.nodos, barras=self.barras,
            nodecolor="b", barracolor="k",
            key_self='self_res'
            )
        #  Matplotlib canvas for carga de estructura
        self.ui.cargaSW = self.ui.mdiArea.addSubWindow(self.ui.cargaSubwindow)
        # self.ui.cargaSW.setWindowIcon(
        #                 QtGui.QIcon('source/resources/Atenea3d_icon.png'))
        self.ui.cargaSW.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(1, 1)))
        self.ui.loadcanvas = AteneaCanvasQt(parent=self.ui.cargaSubwindow,
                                            layout=self.ui.horizontalLayout_carga,
                                            grid=False,
                                            QSizePolicy=QtWidgets.QSizePolicy.Expanding,
                                            coordsLabel=self.coordsLabel,
                                            nodos=self.nodos,
                                            barras=self.barras,
                                            nodecolor="b", barracolor="k",
                                            selectcolor='r', originArrows=False
                                            )

        # Canvas para solicitaciones de barras individuales
        self.ui.barrascanvas = BarrasDisplayCanvas(
            parent=self.ui.frameCanvasContainer,
            layout=self.ui.horizontalLayout_solicitaciones,
            QSizePolicy=QtWidgets.QSizePolicy.Expanding)
        # Set subwindows view actions
        self.ui.actionTabbed_View.triggered.connect(self.Tabbed_View)
        self.ui.actionCascade_View.triggered.connect(self.cascadeArrange)
        self.ui.actionTile_View.triggered.connect(self.tileArrange)

        # Set text action
        self.ui.actionFuerzas.triggered.connect(self.showFuerzas)
        self.ui.actionIndices.triggered.connect(self.showIndices)
        self.ui.actionEjes_Locales.triggered.connect(self.showEjesLocales)

        ## action buttons de display del canvas

        # Setear modos de los docks iniciales
        self.setEstructuraMode()
        self.setAccionesMode()

        # Seleccionar modo paneo y seleccion
        self.ui.actionPan.triggered.connect(self.setCanvasMode)
        self.ui.actionRotate.triggered.connect(self.setCanvasMode)
        self.ui.actionPickAll.triggered.connect(self.setCanvasMode)
        self.ui.actionPickNodos.triggered.connect(self.setCanvasMode)
        self.ui.actionPickBarras.triggered.connect(self.setCanvasMode)

        # Conectar actions de docks
        self.ui.actionArbol.triggered.connect(self.openDocks)
        self.ui.actionEstructura.triggered.connect(self.openDocks)
        self.ui.actionAcciones.triggered.connect(self.openDocks)
        self.ui.actionResultados.triggered.connect(self.openDocks)
        self.ui.actionArbol.setChecked(True)
        self.ui.actionEstructura.setChecked(True)
        self.ui.actionAcciones.setChecked(True)
        self.ui.actionResultados.setChecked(False)
        self.openDocks()
        self.splitDockWidget(self.ui.estructuraDock, self.ui.cargasDock,
                             Qt.Vertical)
        self.ui.estructuraDock.raise_()

        ## Action buttons de zoom
        # Zoom to extent
        self.ui.actionZoom2Extent.triggered.connect(
            lambda: self.zoom2extent(0.1))
        # # Cambiar view del 3d
        # self.views_icon = QtGui.QIcon()
        # self.views_icon.addPixmap(QtGui.QPixmap(
        #                     resource_path("icons/views_icon.png")),
        #                     QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.ui.viewsButton = QtWidgets.QToolButton()
        # self.ui.viewsButton.setIcon(self.views_icon)
        # self.ui.viewsButton.setIconSize(QtCore.QSize(30,30))
        # self.ui.viewsButton.setPopupMode(QtWidgets.QToolButton.InstantPopup)
        # # self.ui.viewsButton.setAutoRaise(True)
        # self.ui.viewsmenu = QtWidgets.QMenu()
        # self.ui.actionViewXY = self.ui.viewsmenu.addAction('x-y')
        # self.ui.actionViewXZ = self.ui.viewsmenu.addAction('x-z')
        # self.ui.actionViewYZ = self.ui.viewsmenu.addAction('y-z')
        # self.ui.actionViewISO = self.ui.viewsmenu.addAction('iso')
        # self.ui.viewsButton.setMenu(self.ui.viewsmenu)
        # self.ui.mainToolBar.addWidget(self.ui.viewsButton)
        self.ui.actionViewXY.triggered.connect(lambda: self.setView('xy'))
        self.ui.actionViewXZ.triggered.connect(lambda: self.setView('xz'))
        self.ui.actionViewYZ.triggered.connect(lambda: self.setView('yz'))
        self.ui.actionViewISO.triggered.connect(lambda: self.setView('iso'))

        ## Action buttons de ayuda
        self.ui.actionSobre_el_software.triggered.connect(about_dialog)

        ## actions buttons de carga de estructura
        # Seleccionar modo dibujar nodos
        self.ui.nodosButtonDraw.clicked.connect(self.setEstructuraMode)
        # Seleccionar modo agregar vinculos
        self.ui.nodosButtonVinculo.clicked.connect(self.setEstructuraMode)
        # Seleccionar modo dibujar barras
        self.ui.barrasButtonDraw.clicked.connect(self.setEstructuraMode)
        self.ui.VigaRadioButton.toggled.connect(self.setEstructuraMode)
        self.ui.ReticuladoRadioButton.toggled.connect(self.setEstructuraMode)
        # Seleccionar modo de ingresar releases
        self.ui.barrasButtonRel.clicked.connect(self.setEstructuraMode)
        # Seleccionar modo cargar secciones y materiales
        self.ui.barrasButtonProps.clicked.connect(self.setEstructuraMode)

        # Seleccionar modo cargas cargas puntuales
        self.ui.nodosButtonCarga.clicked.connect(self.setAccionesMode)
        # Seleccionar modo cargas cargas distribuidas
        self.ui.radioButtonTernaLocal.toggled.connect(self.changeWlabels)
        self.ui.radioButtonTernaGlobal.toggled.connect(self.changeWlabels)
        self.ui.barrasButtonCarga.clicked.connect(self.setAccionesMode)
        # Seleccionar modo cargas de temperatura
        self.ui.barrasButtonTemp.clicked.connect(self.setAccionesMode)
        # Seleccionar modo cargas de cedimiento de vinculo
        self.ui.nodosButtonCed.clicked.connect(self.setAccionesMode)

        # Leer Material
        self.ui.addMatButton.clicked.connect(self.readMaterial)
        self.ui.modMatButton.clicked.connect(self.modifyMaterial)
        # Leer Sección
        self.ui.addSecButton.clicked.connect(self.readSeccion)
        self.ui.modSecButton.clicked.connect(self.modifySeccion)

        ## action buttons de correr esrtuctura
        # Correr estructura
        self.ui.actionRun.triggered.connect(self.runEstructura)
        # Desbloquear carga de estructura
        self.ui.actionUnlock.triggered.connect(self.unlockCargaMode)

        ## action buttons new, save, load
        # Cargar estructura
        self.ui.actionOpen.triggered.connect(self.openFileDialog)
        # Guardar estructura
        self.ui.actionSave.triggered.connect(self.saveFileDialog)
        # Guardar estructura como
        self.ui.actionSaveAs.triggered.connect(self.saveAsFileDialog)
        # Nuevo archivo
        self.ui.actionNew.triggered.connect(lambda: self.newCanvas(dialog=True))

        ## action buttons de ver resultados
        # Seleccionar modo ploteo de deformada
        self.ui.deformadaToolButton.clicked.connect(self.plotResults)
        # Seleccionar modo ploteo de Momentos
        # self.ui.momentoToolButton.clicked.connect(self.setMomentoMode)
        self.ui.Mymenu.triggered.connect(self.plotResults)
        self.ui.Mzmenu.triggered.connect(self.plotResults)
        self.ui.Mtmenu.triggered.connect(self.plotResults)
        # Seleccionar modo ploteo de Corte
        # self.ui.corteToolButton.clicked.connect(self.setCorteMode)
        self.ui.Qymenu.triggered.connect(self.plotResults)
        self.ui.Qzmenu.triggered.connect(self.plotResults)
        # Seleccionar modo ploteo de Normales
        self.ui.normalToolButton.clicked.connect(self.plotResults)
        # Seleccionar modo ploteo de Reacciones
        self.ui.reaccionesToolButton.clicked.connect(self.plotResults)

        # action button de resultados de barras individuales
        self.ui.pushButton_max.clicked.connect(
                            lambda: self.show_max_barra_selected('max'))
        self.ui.pushButton_min.clicked.connect(
                            lambda: self.show_max_barra_selected('min'))

        # Group button del tree view outline
        self.ui.selectedToolButton.clicked.connect(self.updateTreeSelected)
        self.ui.nodosToolButton.clicked.connect(self.updateTreeNodos)
        self.ui.barrasToolButton.clicked.connect(self.updateTreeBarras)
        self.ui.propiedadesToolButton.clicked.connect(
            self.updateTreePropiedades)
        self.ui.deleteActionsToolButton.clicked.connect(self.cleanAcciones)
        self.ui.deleteToolButton.clicked.connect(self.deleteAll)

        ## Interacción entre canvas de carga
        # Completar text browsers cuando se selecciona un nodo
        self.ui.loadcanvas.interactions['picker'].picked.connect(
            self.addSelectedObjects)
        self.ui.loadcanvas.interactions['picker'].cleared.connect(
            self.clearSelectedObjects)

        ## Interacción entre canvas de resultados y frame de resultados
        # Completar tabla y grafico cuando se seleccinoan objetos
        self.ui.resultscanvas.interactions['picker'].picked.connect(
            self.addSelectedObjects2Results)
        self.ui.resultscanvas.interactions['picker'].cleared.connect(
            self.clearSelectedObjects)
        # Cambiar de barra cuando se modifica el combo box
        self.ui.barrasComboBox.currentIndexChanged.connect(
            self.plot_barra_selected)
        # Mover slider de posición de barra
        self.ui.barpositionSlider.valueChanged.connect(self.plot_barra_selected)
        # Setear posicion del slider desde la caja de texto
        # self.ui.textEditBarPosition.textChanged.connect(self.move_position_slider)

        ## Crear nodos dando como input las coordenadas
        self.ui.pushButtonCreateNode.clicked.connect(self.readNodo)

        ## Crear barras dando como input el nodo inicial y final y el tipo
        self.ui.pushButtonCreateBar.clicked.connect(self.readBarra)

        ## Elminar objetos seleccionados
        self.ui.loadcanvas.interactions["picker"].delete.connect(
            self.deleteSelectedObjects)

        ## Vinculos frame
        # Push button: agregar vinculos
        self.ui.pushButtonVinculos.clicked.connect(self.readVinculos)

        # Push button: agregar cedimientos de vinculo
        self.ui.pushButtonCed.clicked.connect(self.readCedimientos)

        ## Cargas Puntuales frame
        # Push button: agregar cargas
        self.ui.pushButtonCargasP.clicked.connect(self.readCargasP)

        ## Cargas distribuidas frame
        # Push button: agregar cargas
        self.ui.pushButtonCargasW.clicked.connect(self.readCargasW)

        ## Propiedades frame
        # Push button: agregar propiedades
        self.ui.pushButtonPropiedades.clicked.connect(self.addProperties)

        ## Barras frame: Articulaciones
        # Push button: agregar articulaciones
        self.ui.pushButtonArticulaciones.clicked.connect(self.readReleases)

        ## Barras frame: Articulaciones
        # Push button: agregar articulaciones
        self.ui.pushButtonTemp.clicked.connect(self.readCargase0)

        ## Deformada frame
        # Push button: cambiar escala
        self.ui.scaleSlider.sliderMoved.connect(self.changeScale)
        self.ui.textEditDeformadaScale.editingFinished.connect(
            self.changeScale_position)

        ## Botones de seleccionar all
        self.ui.allnodosButton.clicked.connect(self.selectAllNodos)
        self.ui.allbarrasButton.clicked.connect(self.selectAllBarras)

    def setView(self, view):
        self.ui.loadcanvas.setView(view)
        self.ui.resultscanvas.setView(view)

    def openDocks(self):
        # Dock de acciones
        if self.ui.actionAcciones.isChecked():
            self.ui.cargasDock.setVisible(True)
        else:
            self.ui.cargasDock.setVisible(False)
        # Dock de estructura
        if self.ui.actionEstructura.isChecked():
            self.ui.estructuraDock.setVisible(True)
        else:
            self.ui.estructuraDock.setVisible(False)
        # Dock del arbol
        if self.ui.actionArbol.isChecked():
            self.ui.arbolDock.setVisible(True)
        else:
            self.ui.arbolDock.setVisible(False)
        # Dock de resultados
        if self.ui.actionResultados.isChecked():
            self.ui.resultadosDock.setVisible(True)
        else:
            self.ui.resultadosDock.setVisible(False)

    def changeWlabels(self):
        # Leer terna
        if self.ui.radioButtonTernaGlobal.isChecked():
            self.ui.label_18.setText('wyi')
            self.ui.label_19.setText('wyf')
        elif self.ui.radioButtonTernaLocal.isChecked():
            self.ui.label_18.setText('wzi')
            self.ui.label_19.setText('wzf')

    def cleanAcciones(self):
        # Borrar cargas puntuales
        self.addCargasP(self.selected_nodos, [0, 0, 0, 0, 0, 0])
        # Borrar cedimientos de vinculo
        self.addCedimientos(self.selected_nodos, [0, 0, 0, 0, 0, 0])
        # Borrar cargas distribuidas
        self.addCargasW(self.selected_barras,
                        {'wxi': 0, 'wyi': 0, 'wzi': 0, 'wxf': 0, 'wyf': 0,
                         'wzf': 0,
                         'terna': 'local'})
        # Borrar deformaciones no-mecánicas
        self.addCargase0(self.selected_barras, [0, 0, 0])

    def readCargasW(self):
        # Leer cargas
        w = {'wxi': 0, 'wyi': 0, 'wzi': 0, 'wxf': 0, 'wyf': 0, 'wzf': 0,
             'terna': 'local'}
        try:
            wxi = self.ui.textEditWxi.displayText()
            if str(wxi) != '':
                w['wxi'] = float(wxi)
            wyi = self.ui.textEditWyi.displayText()
            if str(wyi) != '':
                w['wyi'] = float(wyi)
            wzi = self.ui.textEditWzi.displayText()
            if str(wzi) != '':
                w['wzi'] = float(wzi)
            wxf = self.ui.textEditWxf.displayText()
            if str(wxf) != '':
                w['wxf'] = float(wxf)
            wyf = self.ui.textEditWyf.displayText()
            if str(wyf) != '':
                w['wyf'] = float(wyf)
            wzf = self.ui.textEditWzf.displayText()
            if str(wzf) != '':
                w['wzf'] = float(wzf)
            # Leer terna
            if self.ui.radioButtonTernaGlobal.isChecked():
                w['terna'] = 'global'
            elif self.ui.radioButtonTernaLocal.isChecked():
                w['terna'] = 'local'
        except Exception as ex:
            self.error_dialog.error_catch(
                'Error en ingreso de información', ex)
            return
        barras = self.selected_barras
        self.addCargasW(barras, w)

    def readCargase0(self):
        # Leer deformaciones
        e0 = [0, 0, 0]
        try:
            # baricentrica
            e00 = self.ui.textEdite01.displayText()
            if str(e00) != '':
                e0[0] = float(e00)
            # gradiente y
            e01 = self.ui.textEdite02.displayText()
            if str(e01) != '':
                e0[1] = float(e01)
            # gradiente z
            e02 = self.ui.textEdite03.displayText()
            if str(e02) != '':
                e0[2] = float(e02)
        except Exception as ex:
            self.error_dialog.error_catch(
                'Error en ingreso de información', ex)
            return
        barras = self.selected_barras
        self.addCargase0(barras, e0)

    def addCargase0(self, barras, e0):
        # agregar restricciones a los nodos seleccionados
        # table = self.ui.tableCargasw
        for barra in barras.values():
            barra.clean(['e'])
            barra.assign_e0(e0)
            barra.plot_temperatura(self.ui.loadcanvas.ax, self.fontsize,
                                   visible=self.ui.actionFuerzas.isChecked())
        self.ui.loadcanvas.fig.canvas.draw_idle()

        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def addCargasW(self, barras, w):
        # agregar restricciones a los nodos seleccionados
        for barra in barras.values():
            barra.clean(['w'])
            barra.assign_w(w)
            barra.plot_fuerzas(self.ui.loadcanvas.ax, self.fontsize,
                               color="b",
                               visible=self.ui.actionFuerzas.isChecked(),
                               length=self.length_flechas,
                               zorder=self.zorder['w'])
        self.ui.loadcanvas.fig.canvas.draw_idle()

        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def readReleases(self):
        # Leer restricciones
        rel = [0, 0, 0, 0, 0, 0]
        if self.ui.checkBoxIzq.isChecked():
            rel[1] = 1
            rel[2] = 1
        if self.ui.checkBoxDer.isChecked():
            rel[4] = 1
            rel[5] = 1

        barras = self.selected_barras
        self.addReleases(barras, rel)

    def addReleases(self, barras, rel):
        for i, barra in barras.items():
            if barra.ElemName == '2DTruss':
                break
            barra.assign_rel(rel)
            barra.artists["self"][0].add_releases(rel)
        self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def readCargasP(self):
        # Leer cargas
        P = [0, 0, 0, 0, 0, 0]
        try:
            Px = self.ui.textEditCargasPx.displayText()
            if str(Px) != '':
                P[0] = float(Px)
            Py = self.ui.textEditCargasPy.displayText()
            if str(Py) != '':
                P[1] = float(Py)
            Pz = self.ui.textEditCargasPz.displayText()
            if str(Pz) != '':
                P[2] = float(Pz)
            Mx = self.ui.textEditCargasMx.displayText()
            if str(Mx) != '':
                P[3] = float(Mx)
            My = self.ui.textEditCargasMy.displayText()
            if str(My) != '':
                P[4] = float(My)
            Mz = self.ui.textEditCargasMz.displayText()
            if str(Mz) != '':
                P[5] = float(Mz)
        except Exception as ex:
            self.error_dialog.error_catch(
                'Error en ingreso de información', ex)
            return
        nodos = self.selected_nodos
        self.addCargasP(nodos, P)

    def addCargasP(self, nodos, P):
        # agregar restricciones a los nodos seleccionados
        # table = self.ui.tableCargasP
        for nodo in nodos.values():
            nodo.clean(['p'])
            nodo.P = P
            nodo.plot_fuerzas(self.ui.loadcanvas.ax, self.fontsize,
                              colors=["m", "m"],
                              visible=self.ui.actionFuerzas.isChecked(),
                              length=self.length_flechas,
                              zorder=self.zorder['P'])
            self.ui.loadcanvas.fig.canvas.draw_idle()

        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def readVinculos(self):
        # Leer restricciones
        BOUN = [0, 0, 0, 0, 0, 0]
        if self.ui.checkBoxRx.isChecked():
            BOUN[0] = 1
        if self.ui.checkBoxRy.isChecked():
            BOUN[1] = 1
        if self.ui.checkBoxRz.isChecked():
            BOUN[2] = 1
        if self.ui.checkBoxMx.isChecked():
            BOUN[3] = 1
        if self.ui.checkBoxMy.isChecked():
            BOUN[4] = 1
        if self.ui.checkBoxMz.isChecked():
            BOUN[5] = 1

        nodos = self.selected_nodos
        self.addVinculos(nodos, BOUN)

    def addVinculos(self, nodos, BOUN):
        # agregar restricciones a los nodos seleccionados
        # table = self.ui.tableVinculos
        for nodo in nodos.values():
            nodo.clean(['v'])
            nodo.restr = BOUN
            nodo.plot_vinculos(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                               tipo="vinculo", color='r',
                               length=self.length_flechas,
                               visible=True, zorder=1)
        self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def readCedimientos(self):
        # Leer cedimientos
        ced = [0, 0, 0, 0, 0, 0]
        try:
            if self.ui.textEditCedux.isEnabled():
                cedx = self.ui.textEditCedux.text()
                if str(cedx) != '':
                    ced[0] = float(cedx)
            if self.ui.textEditCeduy.isEnabled():
                cedy = self.ui.textEditCeduy.text()
                if str(cedy) != '':
                    ced[1] = float(cedy)
            if self.ui.textEditCeduz.isEnabled():
                cedz = self.ui.textEditCeduz.text()
                if str(cedz) != '':
                    ced[2] = float(cedz)
            if self.ui.textEditCedgirox.isEnabled():
                girox = self.ui.textEditCedgirox.text()
                if str(girox) != '':
                    ced[3] = float(girox)
            if self.ui.textEditCedgiroy.isEnabled():
                giroy = self.ui.textEditCedgiroy.text()
                if str(giroy) != '':
                    ced[4] = float(giroy)
            if self.ui.textEditCedgiroz.isEnabled():
                giroz = self.ui.textEditCedgiroz.text()
                if str(giroz) != '':
                    ced[5] = float(giroz)
        except Exception as ex:
            self.error_dialog.error_catch(
                'Error en ingreso de información', ex)
            return
        nodos = self.selected_nodos
        self.addCedimientos(nodos, ced)

    def addCedimientos(self, nodos, ced):
        # agregar cedimientos de vínculo a los nodos seleccionados
        for nodo in nodos.values():
            nodo.clean(['c'])
            nodo.ced = ced
            nodo.plot_cedimientos(self.ui.loadcanvas.ax, self.fontsize,
                                  self.length_flechas, key='c', color="c",
                                  visible=self.ui.actionFuerzas.isChecked(),
                                  zorder=self.zorder['ced'])
            self.ui.loadcanvas.fig.canvas.draw_idle()
        # Actualizar arbol de outline
        self.updateTreeSelected(expand=True)

    def createStatusBar(self):
        # Add to the status bar
        # Tool Button de mejorar precision
        self.ui.increasePrecisionToolButton = QtWidgets.QToolButton()
        self.ui.increasePrecisionToolButton.setToolTip(
            'Incrementar decimales de resultados')
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(resource_path("icons/increase_precision_icon.png")),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.increasePrecisionToolButton.setIcon(icon)
        self.ui.increasePrecisionToolButton.setIconSize(QtCore.QSize(15, 15))
        self.ui.increasePrecisionToolButton.setPopupMode(
            QtWidgets.QToolButton.DelayedPopup)
        self.ui.increasePrecisionToolButton.setAutoRaise(True)
        self.ui.increasePrecisionToolButton.setArrowType(QtCore.Qt.NoArrow)
        self.ui.increasePrecisionToolButton.setObjectName(
            "increaseArrowsToolButton")
        self.ui.increasePrecisionToolButton.setChecked(True)
        self.ui.increasePrecisionToolButton.clicked.connect(
            lambda: self.modifyPrecision(1))
        # Tool Button de achicar precision
        self.ui.decreasePrecisionToolButton = QtWidgets.QToolButton()
        self.ui.decreasePrecisionToolButton.setToolTip(
            'Reducir decimales de resultados')
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(resource_path("icons/decrease_precision_icon.png")),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.decreasePrecisionToolButton.setIcon(icon)
        self.ui.decreasePrecisionToolButton.setIconSize(QtCore.QSize(15, 15))
        self.ui.decreasePrecisionToolButton.setPopupMode(
            QtWidgets.QToolButton.DelayedPopup)
        self.ui.decreasePrecisionToolButton.setAutoRaise(True)
        self.ui.decreasePrecisionToolButton.setArrowType(QtCore.Qt.NoArrow)
        self.ui.decreasePrecisionToolButton.setObjectName(
            "decreaseArrowsToolButton")
        self.ui.decreasePrecisionToolButton.setChecked(True)
        self.ui.decreasePrecisionToolButton.clicked.connect(
            lambda: self.modifyPrecision(-1))
        # Tool Button de agrandar textos
        self.ui.increaseTextsToolButton = QtWidgets.QToolButton()
        self.ui.increaseTextsToolButton.setToolTip('Aumentar tamaño de textos')
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(resource_path("icons/increase_letter_icon.png")),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.increaseTextsToolButton.setIcon(icon)
        self.ui.increaseTextsToolButton.setIconSize(QtCore.QSize(15, 15))
        self.ui.increaseTextsToolButton.setPopupMode(
            QtWidgets.QToolButton.DelayedPopup)
        self.ui.increaseTextsToolButton.setAutoRaise(True)
        self.ui.increaseTextsToolButton.setArrowType(QtCore.Qt.NoArrow)
        self.ui.increaseTextsToolButton.setObjectName(
            "increaseArrowsToolButton")
        self.ui.increaseTextsToolButton.setChecked(True)
        self.ui.increaseTextsToolButton.clicked.connect(
            lambda: self.modifyTextsSize(1))
        # Tool Button de achicar textos
        self.ui.decreaseTextsToolButton = QtWidgets.QToolButton()
        self.ui.decreaseTextsToolButton.setToolTip('Reducir tamaño de textos')
        icon = QtGui.QIcon()
        icon.addPixmap(
            QtGui.QPixmap(resource_path("icons/decrease_letter_icon.png")),
            QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.decreaseTextsToolButton.setIcon(icon)
        self.ui.decreaseTextsToolButton.setIconSize(QtCore.QSize(15, 15))
        self.ui.decreaseTextsToolButton.setPopupMode(
            QtWidgets.QToolButton.DelayedPopup)
        self.ui.decreaseTextsToolButton.setAutoRaise(True)
        self.ui.decreaseTextsToolButton.setArrowType(QtCore.Qt.NoArrow)
        self.ui.decreaseTextsToolButton.setObjectName(
            "decreaseArrowsToolButton")
        self.ui.decreaseTextsToolButton.setChecked(True)
        self.ui.decreaseTextsToolButton.clicked.connect(
            lambda: self.modifyTextsSize(-1))
        # Tool Button del grid snap
        self.ui.snapOnToolButton = QtWidgets.QToolButton()
        self.ui.snapOnToolButton.setToolTip('Prender/Apagar grilla del canvas')
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(resource_path("icons/grid_icon.png")),
                       QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.ui.snapOnToolButton.setIcon(icon)
        self.ui.snapOnToolButton.setIconSize(QtCore.QSize(15, 15))
        self.ui.snapOnToolButton.setCheckable(True)
        self.ui.snapOnToolButton.setPopupMode(
            QtWidgets.QToolButton.DelayedPopup)
        self.ui.snapOnToolButton.setAutoRaise(True)
        self.ui.snapOnToolButton.setArrowType(QtCore.Qt.NoArrow)
        self.ui.snapOnToolButton.setObjectName("selectedToolButton")
        self.ui.snapOnToolButton.setChecked(False)
        self.ui.snapOnToolButton.clicked.connect(self.setGridOn)

        # Add widgets
        # Frame for precision tools
        frame1 = QtWidgets.QFrame()
        HLayout1 = QtWidgets.QHBoxLayout(frame1)
        HLayout1.setContentsMargins(0, 0, 0, 0)
        HLayout1.setSpacing(0)
        HLayout1.addWidget(self.ui.increasePrecisionToolButton, 0)
        HLayout1.addWidget(self.ui.decreasePrecisionToolButton, 1)
        frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
                             QtWidgets.QSizePolicy.Fixed)
        frame1.setLayout(HLayout1)
        self.ui.statusBar.addPermanentWidget(frame1)
        # Frame for text size tools
        frame1 = QtWidgets.QFrame()
        HLayout1 = QtWidgets.QHBoxLayout(frame1)
        HLayout1.setContentsMargins(0, 0, 0, 0)
        HLayout1.setSpacing(0)
        HLayout1.addWidget(self.ui.increaseTextsToolButton, 0)
        HLayout1.addWidget(self.ui.decreaseTextsToolButton, 1)
        frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
                             QtWidgets.QSizePolicy.Fixed)
        frame1.setLayout(HLayout1)
        self.ui.statusBar.addPermanentWidget(frame1)
        # Frame for grid tools
        frame1 = QtWidgets.QFrame()
        HLayout1 = QtWidgets.QHBoxLayout(frame1)
        HLayout1.setContentsMargins(0, 0, 0, 0)
        HLayout1.setSpacing(5)
        HLayout1.addWidget(self.ui.snapOnToolButton, 0)
        frame1.setSizePolicy(QtWidgets.QSizePolicy.Fixed,
                             QtWidgets.QSizePolicy.Fixed)
        frame1.setLayout(HLayout1)
        self.ui.statusBar.addPermanentWidget(frame1)
        # Label de coordenadas del mouse en el canvas
        self.coordsLabel = QtWidgets.QLabel()
        self.ui.statusBar.addWidget(self.coordsLabel)

    def define_w_size(self):
        # Obtener extent de nodos y barras de la estructura en loadcanvas
        xmin, xmax, ymin, ymax, zmin, zmax = self.ui.loadcanvas.getNodesExtent()
        xdif = xmax - xmin
        ydif = ymax - ymin
        zdif = zmax - zmin
        self.wy_arrowstyle['length'] = max(xdif, ydif, zdif) / 12
        self.wy_arrowstyle['head_length'] = 12 * self.wy_arrowstyle['length']
        self.wy_arrowstyle['head_width'] = 6 * self.wy_arrowstyle['length']
        self.wy_arrowstyle['tail_width'] = 3 * self.wy_arrowstyle['length']

    def modifyTextsSize(self, step):
        self.fontsize = self.fontsize + step
        # Update en nodos
        for nodo in self.nodos.values():
            for artist_list in nodo.artists.values():
                for artist in artist_list:
                    if (type(artist) is art3d.Text3D) or \
                            (type(artist) is graph.FlechaAtenea3d):
                        artist.set_fontsize(self.fontsize)
        # Update en barras
        for barra in self.barras.values():
            for artist_list in barra.artists.values():
                for artist in artist_list:
                    if (type(artist) is art3d.Text3D) or \
                            (type(artist) is mpl.text.Text) or \
                            (type(artist) is graph.FlechaAtenea3d) or \
                            (type(artist) is graph.cargadistribuidaAtenea):
                        artist.set_fontsize(self.fontsize)

        self.ui.loadcanvas.fig.canvas.draw_idle()
        self.ui.resultscanvas.fig.canvas.draw_idle()
        self.ui.barrascanvas.fig.canvas.draw_idle()

    def modifyPrecision(self, step):
        if self.Estructura:
            self.precision += step
            # Modificar diagramas
            self.changeScale()
            self.completeDisplacementTable()
            # Modificar diagrama barra seleccionada
            if (self.ui.momentoToolButton.isChecked()) or \
                    (self.ui.corteToolButton.isChecked()) or \
                    (self.ui.normalToolButton.isChecked()):
                self.plot_barra_selected()

    def readNodo(self):
        node_center = [0, 0, 0]
        # Leer coordenadas de widgets
        try:
            node_center_x = self.ui.textEditCoordx.displayText()
            if str(node_center_x) != '':
                node_center[0] = float(node_center_x)
            node_center_y = self.ui.textEditCoordy.displayText()
            if str(node_center_y) != '':
                node_center[1] = float(node_center_y)
            node_center_z = self.ui.textEditCoordz.displayText()
            if str(node_center_z) != '':
                node_center[2] = float(node_center_z)
        except Exception as ex:
            self.error_dialog.error_catch(
                'Error en ingreso de información', ex)
            return
        self.addNodo(node_center)

    def addNodo(self, node_center):
        # Crear objeto nodo de atenea
        k = len(self.nodos)
        nodo = NodoGUI(node_center, k + 1)
        self.nodos[k + 1] = nodo
        # Plotear nodo
        nodo.plot(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                  indice_visible=self.ui.actionIndices.isChecked(),
                  **self.nodo_graph_props)
        self.ui.loadcanvas.fig.canvas.draw_idle()
        # Update barras combobox
        self.ui.comboBoxNodo1.addItem(str(k + 1))
        self.ui.comboBoxNodo2.addItem(str(k + 1))
        # Update tree
        self.updateTreeNodos()
        # Zoom to extent
        self.ui.loadcanvas.zoom2extent(margin=0.1)

    def barrasennodo(self, nodo):
        barras2del = {}
        for key, barra in self.barras.items():
            if nodo in barra.nodos:
                barras2del[key] = barra
        return barras2del

    def deleteNodo(self, nodo):
        # Encontrar cuales son las barras Atenea que se deben eliminar
        barras2del = self.barrasennodo(nodo)
        for barra in barras2del.values():
            self.deleteBarra(barra)
        # Limpiar artistas de nodo
        nodo.clean(['self', 'v', 'p', 'c', 'index'])
        index = nodo.indice
        # Eliminar objeto nodo
        del self.nodos[index]
        # Modificar
        for i in range(index + 1, len(self.nodos) + 2):
            self.nodos[i - 1] = self.nodos.pop(i)
            self.nodos[i - 1].indice = i - 1
            self.nodos[i - 1].artists['index'][0].set_text(
                '[' + str(i - 1) + ']')
        # Actualizar ComboBox
        self.ui.comboBoxNodo1.clear()
        self.ui.comboBoxNodo2.clear()
        for nodo in self.nodos.values():
            self.ui.comboBoxNodo1.addItem(str(nodo.indice))
            self.ui.comboBoxNodo2.addItem(str(nodo.indice))
        # Actualizar arbol
        self.updateTreeNodos()
        # Resetear estructura y results canvas si los hay
        if self.Estructura:
            self.reset_Results_Canvas()

    def readBarra(self):
        # Leer nodo inicial y final de los widgets
        nodei_number = float(self.ui.comboBoxNodo1.currentText())
        nodef_number = float(self.ui.comboBoxNodo2.currentText())
        ElemName = None
        if nodei_number != nodef_number:
            # Coordenadas de los nodos y tipo de barra
            if self.ui.VigaRadioButton.isChecked():
                ElemName = '3DFrame'
            if self.ui.ReticuladoRadioButton.isChecked():
                ElemName = '3DTruss'
        self.addBarra(nodei_number, nodef_number, ElemName)

    def addBarra(self, nodei_number, nodef_number, ElemName):
        if nodei_number != nodef_number:
            # Crear objeto barra de atenea
            k = len(self.barras)
            barra = BarraGUI(k + 1, [self.nodos[nodei_number],
                                     self.nodos[nodef_number]], ElemName)
            self.barras[k + 1] = barra
            # Plotear barra
            barra.plot(self.ui.loadcanvas.ax, fontsize=self.fontsize,
                       indice_visible=self.ui.actionIndices.isChecked(),
                       ejes_visible=self.ui.actionEjes_Locales.isChecked(),
                       length=self.length_flechas * 0.5, ejes_color='y',
                       **self.barra_graph_props)

            # Update tamaño de flechas
            self.length_flechas = graph.update_ax_arrows(self.ui.loadcanvas.ax,
                                                         self.nodos,
                                                         self.barras,
                                                         0.3)
            aux = graph.update_ax_arrows(self.ui.resultscanvas.ax,
                                         self.nodos, self.barras,
                                         0.3)
            # UPDATE TREE
            self.updateTreeBarras()
            self.ui.loadcanvas.fig.canvas.draw_idle()

    def deleteBarra(self, barra):
        # Encontrar cuales son las barras Atenea que se deben eliminar
        index = barra.indice  # Lista con los indices de las barras a eliminar
        barra.clean(['self', 'w', 'e', 'ejes', 'index'])
        # Eliminar elemnames de los nodos extremos de la barra
        for nodo in barra.nodos:
            nodo.elemnames.remove(barra.ElemName)
        # Eliminar objeto barra
        del self.barras[index]
        # Modificar
        for i in range(index + 1, len(self.barras) + 2):
            self.barras[i - 1] = self.barras.pop(i)
            self.barras[i - 1].indice = i - 1
            self.barras[i - 1].artists['index'][0].set_text(
                '[' + str(i - 1) + ']')
        # Acutalizar arbol
        self.updateTreeBarras()
        # Resetear estructura y results canvas si los hay
        if self.Estructura:
            self.reset_Results_Canvas()

    def deleteSelectedObjects(self):
        # Des-seleccionar a todas las barras que ya van a eliminar los nodos
        for nodo in self.selected_nodos.values():
            barras2del = self.barrasennodo(nodo)
            for index in barras2del.keys():
                if index in self.selected_barras.keys():
                    del self.selected_barras[index]

        for nodo in self.selected_nodos.values():
            self.deleteNodo(nodo)
        for barra in self.selected_barras.values():
            self.deleteBarra(barra)
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def deleteAll(self):
        # Llamar al key press delete event del canvas
        event = mpl.backend_bases.KeyEvent('delete',
                                           self.ui.loadcanvas.fig.canvas,
                                           'delete', x=0, y=0)
        self.ui.loadcanvas.interactions['picker'].onKeyPress(event,
                                                             override=True)
        self.ui.loadcanvas.fig.canvas.draw_idle()

    def clearSelectedObjects(self):
        self.addSelectedObjects({}, {})

    def getSelectedObjects(self, nodos_indices, barras_indices):
        selected_nodos = {}
        selected_barras = {}
        for index in nodos_indices:
            nodo = self.nodos[index]
            selected_nodos[index] = nodo
        for index in barras_indices:
            barra = self.barras[index]
            selected_barras[index] = barra
        self.selected_nodos = selected_nodos
        self.selected_barras = selected_barras

    def selectAllNodos(self):
        if not self.Estructura:
            self.ui.loadcanvas.interactions['picker'].clearSelected()
        if self.Estructura:
            self.ui.resultscanvas.interactions['picker'].clearSelected()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()
        nodos_indices = [i for i in self.nodos.keys()]
        if not self.Estructura:
            selected_artists = [nodo.artists['self'][0] for nodo in
                                self.nodos.values()]
            self.ui.loadcanvas.interactions[
                'picker'].selected_objects = selected_artists
            self.ui.loadcanvas.interactions['picker'].list_picks = []
            self.ui.loadcanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects(nodos_indices, [])
        if self.Estructura:
            selected_artists = [nodo.artists['self_res'][0] for nodo in
                                self.nodos.values()]
            self.ui.resultscanvas.interactions[
                'picker'].selected_objects = selected_artists
            self.ui.resultscanvas.interactions['picker'].list_picks = []
            self.ui.resultscanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects2Results(nodos_indices, [])

    def selectAllBarras(self):
        if not self.Estructura:
            self.ui.loadcanvas.interactions['picker'].clearSelected()
        if self.Estructura:
            self.ui.resultscanvas.interactions['picker'].clearSelected()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()
        barras_indices = [i for i in self.barras.keys()]
        if not self.Estructura:
            selected_artists = [barra.artists['self'][0] for barra in
                                self.barras.values()]
            self.ui.loadcanvas.interactions[
                'picker'].selected_objects = selected_artists
            self.ui.loadcanvas.interactions['picker'].list_picks = []
            self.ui.loadcanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects([], barras_indices)
        if self.Estructura:
            selected_artists = [barra.artists['self_res'][0] for barra in \
                                self.barras.values()]
            self.ui.resultscanvas.interactions[
                'picker'].selected_objects = selected_artists
            self.ui.resultscanvas.interactions['picker'].list_picks = []
            self.ui.resultscanvas.interactions['picker'].colorSelected()
            self.addSelectedObjects2Results([], barras_indices)

    def addSelectedObjects2Edits(self):
        # nodos seleccionados
        nodos_text = ''
        for index in self.selected_nodos.keys():
            nodos_text += str(index)
            nodos_text += ', '
        # barras seleccionadas
        barras_text = ''
        for index in self.selected_barras.keys():
            barras_text += str(index)
            barras_text += ', '
        # Textos de nodos seleccionados
        self.ui.textEditSelectedNodos.setText(nodos_text)
        # Textos de barras seleccionadas
        self.ui.textEditSelectedBarras.setText(barras_text)

    def addSelectedObjects(self, nodos_indices, barras_indices):
        self.getSelectedObjects(nodos_indices, barras_indices)
        # Completar text edits y tablas
        self.addSelectedObjects2Edits()
        # Updatear arbol
        self.updateTreeSelected(expand=True)
        self.ui.selectedToolButton.setChecked(True)
        # Habilitar edits de cedimientos de vinculo
        # Chequear restricciones de vínculo en cada nodo
        restrux = [0]
        restruy = [0]
        restruz = [0]
        restrgx = [0]
        restrgy = [0]
        restrgz = [0]
        if self.selected_nodos:
            restrux = [a.restr[0] for a in self.selected_nodos.values()]
            restruy = [a.restr[1] for a in self.selected_nodos.values()]
            restruz = [a.restr[2] for a in self.selected_nodos.values()]
            restrgx = [a.restr[3] for a in self.selected_nodos.values() if \
                       len(a.restr) == 6]
            restrgy = [a.restr[4] for a in self.selected_nodos.values() if \
                       len(a.restr) == 6]
            restrgz = [a.restr[5] for a in self.selected_nodos.values() if \
                       len(a.restr) == 6]
        self.ui.textEditCedux.setEnabled(all(restrux))
        self.ui.textEditCeduy.setEnabled(all(restruy))
        self.ui.textEditCeduz.setEnabled(all(restruz))
        self.ui.textEditCedgirox.setEnabled(all(restrgx))
        self.ui.textEditCedgiroy.setEnabled(all(restrgy))
        self.ui.textEditCedgiroz.setEnabled(all(restrgz))

    def addSelectedObjects2Results(self, nodos_indices, barras_indices):
        # Objetos seleccionados
        self.getSelectedObjects(nodos_indices, barras_indices)
        # Completar text edits y tablas
        self.addSelectedObjects2Edits()
        # Completar tabla de desplazamientos nodales
        self.completeDisplacementTable()
        # Completar combobox de barras
        self.completeBarrasCombobox()
        # Update tree selected
        self.updateTreeSelected(expand=True)

    def completeBarrasCombobox(self):
        # Limpiar combo box
        combo_list = []
        self.ui.barrasComboBox.clear()
        # COmpletar combo box
        for index in self.selected_barras.keys():
            item = str(index)
            combo_list.append(item)
            self.ui.barrasComboBox.addItem(item)
        # Elegir primero de la lista
        self.ui.barrasComboBox.setCurrentIndex(0)
        self.plot_barra_selected()

    def plot_barra_selected(self):
        # Clear axes
        self.ui.barrascanvas.ax.clear()
        # Leer posición del slider
        value = self.ui.barpositionSlider.value()
        # Plot
        index = self.ui.barrasComboBox.currentText()
        solicitacion = []
        if index:
            barra = self.barras[int(index)]
            x_pos = ((value - 1) * barra.long / 100)
            if self.ui.Mymenu.isChecked():
                solicitacion = 'My'
                barra.plot_diagramas(self.ui.barrascanvas.ax, solicitacion,
                                     x_pos, precision=self.precision,
                                     fontsize=self.fontsize)
            if self.ui.Mzmenu.isChecked():
                solicitacion = 'Mz'
                barra.plot_diagramas(self.ui.barrascanvas.ax, solicitacion,
                                     x_pos, precision=self.precision,
                                     fontsize=self.fontsize)
            if self.ui.Mtmenu.isChecked():
                solicitacion = 'Mt'
                barra.plot_diagramas(self.ui.barrascanvas.ax, solicitacion,
                                     x_pos, precision=self.precision,
                                     fontsize=self.fontsize)
            if self.ui.Qymenu.isChecked():
                solicitacion = 'Qy'
                barra.plot_diagramas(self.ui.barrascanvas.ax, solicitacion,
                                     x_pos, precision=self.precision,
                                     fontsize=self.fontsize)
            if self.ui.Qzmenu.isChecked():
                solicitacion = 'Qz'
                barra.plot_diagramas(self.ui.barrascanvas.ax, solicitacion,
                                     x_pos, precision=self.precision,
                                     fontsize=self.fontsize)
            if self.ui.normalToolButton.isChecked():
                solicitacion = 'N'
                barra.plot_diagramas(self.ui.barrascanvas.ax, solicitacion,
                                     x_pos, precision=self.precision,
                                     fontsize=self.fontsize)
            # Actualizar line edit
            self.ui.textEditBarPosition.setText(to_precision(x_pos, 3))
        self.ui.barrascanvas.ax.invert_yaxis()
        self.ui.barrascanvas.draw_idle()
            
    def show_max_barra_selected(self, extreme='max'):
        index = self.ui.barrasComboBox.currentText()
        if index:
            barra = self.barras[int(index)]
            solicitaciones = []
            if self.ui.Mymenu.isChecked():
                solicitaciones.append('My')
            if self.ui.Mzmenu.isChecked():
                solicitaciones.append('Mz')
            if self.ui.Mtmenu.isChecked():
                solicitaciones.append('Mt')
            if self.ui.Qymenu.isChecked():
                solicitaciones.append('Qy')
            if self.ui.Qzmenu.isChecked():
                solicitaciones.append('Qz')
            if self.ui.normalToolButton.isChecked():
                solicitaciones.append('N')
            values = []
            positions = []
            for solicitacion in solicitaciones:
                pos, val = barra.find_maxmin_internal_forces(solicitacion,extreme)
                values.append(val)
                positions.append(pos)
            if values:
                if extreme == 'max': index = values.index(max(values))
                elif extreme == 'min': index = values.index(min(values))
                position = positions[index]
                value = round((position*100/barra.long)+1)
                self.ui.barpositionSlider.setSliderPosition(value)

    # def show_max_barra_selected(self):
    #     index = self.ui.barrasComboBox.currentText()
    #     if index:
    #         barra = self.barras[int(index)]
    #         if self.ui.momentoToolButton.isChecked():
    #             solicitacion = 'Mz'
    #         if self.ui.corteToolButton.isChecked():
    #             solicitacion = 'Qz'
    #         if self.ui.normalToolButton.isChecked():
    #             solicitacion = 'N'
    #         position = barra.find_maxmin_internal_forces(solicitacion, 'max')
    #         value = round((position * 100 / barra.long) + 1)
    #         self.ui.barpositionSlider.setSliderPosition(value)

    # def show_min_barra_selected(self):
    #     index = self.ui.barrasComboBox.currentText()
    #     if index:
    #         barra = self.barras[int(index)]
    #         if self.ui.momentoToolButton.isChecked():
    #             solicitacion = 'Mz'
    #         if self.ui.corteToolButton.isChecked():
    #             solicitacion = 'Qz'
    #         if self.ui.normalToolButton.isChecked():
    #             solicitacion = 'N'
    #         position = barra.find_maxmin_internal_forces(solicitacion, 'min')
    #         value = round((position * 100 / barra.long) + 1)
            # self.ui.barpositionSlider.setSliderPosition(value)

    def completeDisplacementTable(self):
        """
        Completar tabla con desplazamientos nodales al seleccionar nodos

        """
        # Limpiar tabla
        self.ui.tableWidget.setRowCount(0)
        # Agregar a tabla
        for index, nodo in self.selected_nodos.items():
            # Create row
            numRows = self.ui.tableWidget.rowCount()
            self.ui.tableWidget.insertRow(numRows)
            # Add items to row
            q = []
            headers = ["Nodo"]
            if self.ui.deformadaToolButton.isChecked():
                q.extend(nodo.U)
                headers.extend(["dx", "dy", "dz", "giro x", "giro y", "giro z"])
                self.ui.label_6.setText("Desplazamientos nodales:")
            if self.ui.reaccionesToolButton.isChecked():
                q.extend(nodo.R)
                headers.extend(["Rx", "Ry", "Rz", "Mx", "My", "Mz"])
                self.ui.label_6.setText("Reacciones de vínculo:")
            # Escribir headers
            self.ui.tableWidget.setColumnCount(len(headers))
            self.ui.tableWidget.setHorizontalHeaderLabels(headers)
            # Llenar columnas
            i = str(index)
            item = QtWidgets.QTableWidgetItem(i)
            item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
            self.ui.tableWidget.setItem(numRows, 0, item)
            for i in range(len(q)):
                d = to_precision(q[i], self.precision)
                item = QtWidgets.QTableWidgetItem(d)
                item.setTextAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
                self.ui.tableWidget.setItem(numRows, i + 1, item)
            # Resize table to contents
            header = self.ui.tableWidget.horizontalHeader()
            header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
            for i in range(len(q)):
                header.setSectionResizeMode(i + 1,
                                            QtWidgets.QHeaderView.ResizeToContents)

    # Windows view
    def Tabbed_View(self):
        self.ui.mdiArea.setViewMode(1)

    def cascadeArrange(self):
        self.ui.mdiArea.setViewMode(0)
        self.ui.mdiArea.cascadeSubWindows()

    def tileArrange(self):
        self.ui.mdiArea.setViewMode(0)
        self.ui.mdiArea.tileSubWindows()

    def openFileDialog(self, filename=None):
        if filename:
            self.filename = filename
        if not filename:
            filename = QFileDialog.getOpenFileName(self, 'Open file',
                                                   filter='Atenea3d(*.a3d)')
            if not filename[0]:
                return
            self.filename = filename[0]
        self.setWindowTitle(
            'Atenea3d (beta) - ' + os.path.basename(self.filename[0]) + \
            ' [' + str(datetime.now().strftime('%H:%M')) + ']')
        if os.path.exists(self.filename):
            # Resetear resultados y estructura
            if self.Estructura:
                self.reset_Results_Canvas()
            # Inicializar elementos de la estructura
            self.nodos = {}
            self.barras = {}
            self.Estructura = None
            self.ui.seccionlistWidget.clear()
            self.ui.matlistWidget.clear()
            # Eliminar todos los artists del canvas
            self.ui.loadcanvas.clearCanvas(self.nodos, self.barras,
                                           self.ui.snapOnToolButton.isChecked())
            self.ui.resultscanvas.clearCanvas(self.nodos, self.barras,
                                              self.ui.snapOnToolButton.isChecked())
            # Leer metadata de estructura desde el archivo
            metadata = read_txt(self.filename)
            # Cargar estructura
            self.loadEstructura(metadata)
            self.ui.loadcanvas.draw_idle()
            # Estructura guardada
            self.saved = True

    def saveFileDialog(self):
        # Obtener nombre y directorio de archivo nuevo
        if not self.saved:
            filename = QFileDialog.getSaveFileName(self, 'Save file',
                                                   filter='Atenea3d(*.a3d)')
            self.filename = filename[0]
            self.saved = True
        if self.filename:
            # Modificar window title
            self.setWindowTitle('Atenea3d (beta) - ' +
                                os.path.basename(self.filename) +
                                ' [' + str(datetime.now().strftime('%H:%M')) +
                                ']')
            # Guardarlo en el archivo
            save_txt(self.filename, self.nodos, self.barras, self.materiales,
                     self.secciones)

    def saveAsFileDialog(self):
        # Obtener nombre y directorio de archivo nuevo
        self.saved = False
        self.saveFileDialog()

    def readMaterial(self):
        """
        Leer los inputs del material: módulo de elasticidad y tensión de
        fluencia.
        Se agregan a las opciones del combo box.
        """
        # comboMat_list = []
        ret, ok = self.mat_dialog.exec_()
        if ok:
            try:
                item = str(ret[0])
                self.materiales[ret[0]] = {'name': ret[0], 'E': float(ret[1]),
                                           'mu': float(ret[2]),
                                           'l': float(ret[3])}
                # comboMat_list.append(item)
                self.ui.matlistWidget.addItem(item)
            except Exception as ex:
                self.Estructura = None
                self.error_dialog.error_catch(
                    'Chequear que esté bien cargada la información', ex)
                return
            # Actualizar arbol
            self.updateTreePropiedades()

    def modifyMaterial(self):
        """

        :return:
        """
        # Leer material seleccionado
        if not self.ui.matlistWidget.currentItem():
            self.error_dialog.setText('No hay material seleccionado')
            self.error_dialog.exec_()
            return
        item = self.ui.matlistWidget.currentItem()
        itemMat = self.ui.matlistWidget.currentItem().text()
        old_mat = self.materiales[itemMat]
        # Abrir dialog de carga de material con input del material seleccionado
        ret, ok = self.mat_dialog.exec_(values=[itemMat, str(old_mat['E']),
                                                str(old_mat['mu']),
                                                str(old_mat['l'])])
        # Reescribir material seleccionado
        if ok:
            # Borrar material seleccionado
            del self.materiales[itemMat]
            self.materiales[ret[0]] = {'name': ret[0], 'E': float(ret[1]),
                                       'mu': float(ret[2]), 'l': float(ret[3])}
            item.setText(str(ret[0]))
        # Actualizar barras con el material correspondiente
        for barra in self.barras.values():
            if barra.mat:
                if barra.mat['name'] == itemMat:
                    barra.mat['name'] = ret[0]
                    barra.mat['E'] = self.materiales[ret[0]]['E']
                    barra.mat['mu'] = self.materiales[ret[0]]['mu']
                    barra.mat['l'] = self.materiales[ret[0]]['l']

        # Update del tree
        self.updateTreePropiedades()
        # Resetear estructura y results canvas si los hay
        if self.Estructura:
            self.reset_Results_Canvas()

    def readSeccion(self):
        """
        Lee los inputs de la sección: área y momento de inercia.
        Se agregan a las opciones del combo box.
        """
        # comboSec_list = []
        ret, ok = self.seccion_dialog.exec_()
        if ok:
            try:
                item = str(ret[0])
                # comboSec_list.append(item)
                self.secciones[ret[0]] = {'name': ret[0], 'A': float(ret[1]),
                                          'Iy': float(ret[2]),
                                          'Iz': float(ret[3]),
                                          'Jt': float(ret[4]),
                                          'alpha': float(ret[5])}
                self.ui.seccionlistWidget.addItem(item)
            except Exception as ex:
                self.Estructura = None
                self.error_dialog.error_catch(
                    'Chequear que esté bien cargada la información', ex)
                return
            # Actualizar arbol
            self.updateTreePropiedades()

    def modifySeccion(self):
        """

        :return:
        """
        # Leer sección seleccionada
        if not self.ui.seccionlistWidget.currentItem():
            self.error_dialog.setText('No hay sección seleccionada')
            self.error_dialog.exec_()
            return
        item = self.ui.seccionlistWidget.currentItem()
        itemSec = self.ui.seccionlistWidget.currentItem().text()
        old_sec = self.secciones[itemSec]
        # Abrir dialog de carga de material con input de la seccion seleccionada
        ret, ok = self.seccion_dialog.exec_(
            values=[itemSec, str(old_sec['A']), str(old_sec['Iy']),
                    str(old_sec['Iz']), str(old_sec['Jt']),
                    str(old_sec['alpha'])])
        # Reescribir seccion seleccionada
        if ok:
            # Borrar seccion seleccionada
            del self.secciones[itemSec]
            self.secciones[ret[0]] = {'name': ret[0], 'A': float(ret[1]),
                                      'Iy': float(ret[2]), 'Iz': float(ret[3]),
                                      'Jt': float(ret[4]),
                                      'alpha': float(ret[5])}
            item.setText(str(ret[0]))
        # Actualizar barras con la seccion correspondiente
        for barra in self.barras.values():
            if len(barra.seccion) > 1:  # si hay seccion asignada
                if barra.seccion['name'] == itemSec:
                    barra.seccion['name'] = ret[0]
                    barra.seccion['A'] = self.secciones[ret[0]]['A']
                    barra.seccion['Iy'] = self.secciones[ret[0]]['Iy']
                    barra.seccion['Iz'] = self.secciones[ret[0]]['Iz']
                    barra.seccion['Jt'] = self.secciones[ret[0]]['Jt']
                    barra.seccion['alpha'] = self.secciones[ret[0]]['alpha']
                    # Actualizar ejes locales
                    barra.update()
                    barra.clean(['ejes'])
                    barra.plot_ejes_locales( self.ui.loadcanvas.ax, self.fontsize,
                                length=self.length_flechas, color='y',
                                visible=self.ui.actionEjes_Locales.isChecked(),
                                zorder=self.barra_graph_props['zorder'] + 1)

        # Update del tree
        self.updateTreePropiedades()
        # Resetear estructura y results canvas si los hay
        if self.Estructura:
            self.reset_Results_Canvas()

    def addProperties(self):
        """
        Se le asignan las propiedades seleccionadas en los combobox a las
        barras seleccionadas.
        """
        barras = self.selected_barras
        data = {}
        if not self.ui.matlistWidget.currentItem():
            self.error_dialog.setText('No hay material seleccionado')
            self.error_dialog.exec_()
            return
        itemMat = self.ui.matlistWidget.currentItem().text()
        if not self.ui.seccionlistWidget.currentItem():
            self.error_dialog.setText('No hay sección seleccionada')
            self.error_dialog.exec_()
            return
        itemSec = self.ui.seccionlistWidget.currentItem().text()

        mat = self.materiales[itemMat]
        sec = self.secciones[itemSec]
        data['mat'] = {"name": mat["name"], 'E': mat['E'], 'mu': mat['mu'],
                       'l': mat['l']}
        data['seccion'] = {'name': sec["name"], 'A': sec['A'], 'Iy': sec['Iy'],
                           'Iz': sec['Iz'], 'Jt': sec['Jt'],
                           'alpha': sec['alpha']}
        for barra in barras.values():
            barra.assign_data(data)
            # Actualizar ejes locales
            barra.update()
            barra.clean(['ejes'])
            barra.plot_ejes_locales( self.ui.loadcanvas.ax, self.fontsize,
                                length=self.length_flechas, color='y',
                                visible=self.ui.actionEjes_Locales.isChecked(),
                                zorder=self.barra_graph_props['zorder'] + 1)

        # Update del tree
        self.updateTreeSelected(expand=True)
        # Resetear estructura y results canvas si los hay
        if self.Estructura:
            self.reset_Results_Canvas()

    def loadEstructura(self, metadata):
        """
        Cargar estructura desde metadata
        """

        # Crear nodos
        for coords in metadata['XYZ'].values():
            self.addNodo(coords)
        # Agregar restricciones de vínculo
        for i, boun in metadata['BOUN'].items():
            self.addVinculos({i: self.nodos[i]}, boun)
        # Agregar cargas nodales
        for i, P in metadata['P'].items():
            self.addCargasP({i: self.nodos[i]}, P)
        # Crear barras
        for i, nodos in metadata['CON'].items():
            ElemName = metadata['ElemName'][i]
            self.addBarra(nodos[0], nodos[1], ElemName)
        # Agregar releases
        for i, rel in metadata['rel'].items():
            self.addReleases({i: self.barras[i]}, rel)
        # Agregar cargas distribuidas
        for i, w in metadata['w'].items():
            self.addCargasW({i: self.barras[i]}, w)
        # Agregar deformaciones distribuidas
        for i, e0 in metadata['e0'].items():
            self.addCargase0({i: self.barras[i]}, e0)
        # Agregar cedimientos de vinculo
        for i, ced in metadata['ced'].items():
            self.addCedimientos({i: self.nodos[i]}, ced)
        # Crear secciones
        for i in metadata['secciones'].keys():
            seccion = metadata['secciones'][i]
            self.secciones[i] = seccion
            self.ui.seccionlistWidget.addItem(str(i))
        # Crear materiales
        for i in metadata['materiales'].keys():
            material = metadata['materiales'][i]
            self.materiales[i] = material
            self.ui.matlistWidget.addItem(str(i))
        # Agregar propiedades a las barras
        for i, data in metadata['ElemData'].items():
            sec = None
            mat = None
            if data['seccion']:
                sec = self.secciones[data['seccion']]
            if data['mat']:
                mat = self.materiales[data['mat']]
            self.barras[i].assign_data({'seccion': sec, 'mat': mat})

        # Hacer zoom sobre la estructura
        self.zoom2extent(margin=0.1)

        # Update tree nodos
        self.updateTreeNodos()
        self.updateTreeBarras()

    def error_catch(self, info, ex):
        """

        :return:
        """
        template = "Ocurrió un error de tipo {0}. Argumentos:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        self.error_dialog.setText(info)
        self.error_dialog.setInformativeText(message)
        self.error_dialog.exec_()
        return

    def runEstructura(self):
        try:
            self.Estructura = EstructuraGUI(self.nodos, self.barras)
        except Exception as ex:
            self.Estructura = None
            self.error_dialog.error_catch('Error al armar la estructura', ex)
            return
        dialog = runDialog(self.Estructura)
        met, ok = dialog.exec_()
        if ok:
            self.met = met
            # Correr estructura
            if met == 0:
                try:
                    resolver_isostatico(self.Estructura)
                except Exception as ex:
                    self.Estructura = None
                    self.error_dialog.error_catch(
                        'Error corriendo el método de equilibrio de fuerzas',
                        ex)
                    return
            elif met == 1:
                # Chequear que todas las barras tengan mat y sec
                for barra in self.barras.values():
                    if (not barra.mat) or (not barra.seccion):
                        self.Estructura = None
                        self.error_dialog.error_catch(
                            'Faltan propiedades en la barra {}'.format(
                                barra.indice))
                        return
                try:
                    metodo_rigideces(self.Estructura)
                except Exception as ex:
                    self.Estructura = None
                    self.error_dialog.error_catch(
                        'Error corriendo el método de rigideces', ex)
                    return
            elif met == 2:
                # Chequear que todas las barras tengan mat y sec
                for barra in self.barras.values():
                    if (not barra.mat) or (not barra.seccion):
                        self.Estructura = None
                        self.error_dialog.error_catch(
                            'Faltan propiedades en la barra {}'.format(
                                barra.indice))
                        return
                try:
                    metodo_rigideces(self.Estructura)
                except Exception as ex:
                    self.Estructura = None
                    self.error_dialog.error_catch(
                        'Error corriendo el método de flexibilidades', ex)
                    return
            elif met == -1:
                self.Estructura = None
                return
            # Habilitar botones de resultados
            self.lockCargaMode()
            # Plotear resultados
            for nodo in self.nodos.values():
                nodo.plot(self.ui.resultscanvas.ax, fontsize=self.fontsize,
                          indice_visible=self.ui.actionIndices.isChecked(),
                          keys=['self_res', 'index_res'],
                          **self.nodo_graph_props)
            for barra in self.barras.values():
                barra.plot(self.ui.resultscanvas.ax, fontsize=self.fontsize,
                           indice_visible=self.ui.actionIndices.isChecked(),
                           ejes_visible=self.ui.actionEjes_Locales.isChecked(),
                           length=self.length_flechas, ejes_color='y',
                           keys=['self_res', 'ejes_res', 'index_res'],
                           **self.barra_graph_props)
            self.Estructura.plot_reacciones(self.ui.resultscanvas.ax,
                                            self.fontsize,
                                            precision=self.precision)
            self.Estructura.plot_diagramas(self.ui.resultscanvas.ax,
                                           precision=self.precision,
                                           fontsize=self.fontsize)
            # Setear valores del slider
            if met != 0: self.changeScale_Slider_parameters()
            # Plotear
            self.plotResults()

        else:
            self.Estructura = None

    def lockCargaMode(self):
        """
        Bloquea los botones de carga de estructura
        """
        # Marcar como activo el action de lock
        self.ui.actionUnlock.setEnabled(True)
        self.ui.actionUnlock.setIcon(self.lock_icon)
        # Habilitar botones de resultados y deshabilitar botones de carga
        self.ui.cargasDock.setEnabled(False)
        self.ui.estructuraDock.setEnabled(False)
        self.ui.resultadosDock.setEnabled(True)
        self.ui.actionSave.setEnabled(False)
        self.ui.actionSaveAs.setEnabled(False)
        self.ui.actionOpen.setEnabled(False)
        self.ui.actionNew.setEnabled(False)
        self.ui.actionRun.setEnabled(False)
        self.ui.actionEstructura.setEnabled(False)
        self.ui.actionAcciones.setEnabled(False)
        self.ui.deleteActionsToolButton.setEnabled(False)
        self.ui.deleteToolButton.setEnabled(False)
        self.ui.actionResultados.setEnabled(True)
        # Set deformada mode por default
        if self.met == 0:
            self.ui.deformadaToolButton.setVisible(False)
            self.ui.deformadaToolButton.setChecked(False)
            self.ui.momentoToolButton.setChecked(True)
            self.ui.frame_9.setVisible(False)
        else:
            self.ui.deformadaToolButton.setVisible(True)
            self.ui.deformadaToolButton.setChecked(True)
            self.ui.frame_9.setVisible(True)
        self.ui.actionPickNodos.setChecked(True)
        self.ui.resultscanvas.setMode('picknodes',
                                      QtGui.QCursor(
                                          QtCore.Qt.PointingHandCursor))
        # self.ui.frameSolicitaciones.setVisible(False)
        # self.ui.frameDesplazamientos.setVisible(True)
        self.ui.actionResultados.setChecked(True)
        self.ui.actionEstructura.setChecked(False)
        self.ui.actionAcciones.setChecked(False)
        self.ui.resultscanvas.interactions['picker'].clearSelected()
        self.openDocks()
        # Deshabilitar función de eliminar nodos
        self.ui.loadcanvas.interactions["picker"].delete.disconnect()

    def unlockCargaMode(self):
        """
        Resetea los resultados y la estructura y habilita los botones
        de carga de estructura
        """
        # File Dialog pop-up
        warning = QtWidgets.QMessageBox.warning(self,
                                                'Desbloquear modo de carga',
                                                'Al debloquear el modo de carga se perderán los resultados',
                                                QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
        if warning == QtWidgets.QMessageBox.Ok:
            # Marcar como activo el action de lock
            self.ui.actionUnlock.setEnabled(False)
            self.ui.actionUnlock.setIcon(self.unlock_icon)
            # Resetear resultados y estructura
            if self.Estructura:
                self.reset_Results_Canvas()
            # Habilitar botones de carga y deshabilitar botones de resultados
            self.ui.cargasDock.setEnabled(True)
            self.ui.estructuraDock.setEnabled(True)
            self.ui.resultadosDock.setEnabled(False)
            self.ui.actionSave.setEnabled(True)
            self.ui.actionSaveAs.setEnabled(True)
            self.ui.actionOpen.setEnabled(True)
            self.ui.actionNew.setEnabled(True)
            self.ui.actionRun.setEnabled(True)
            self.ui.actionEstructura.setEnabled(True)
            self.ui.actionAcciones.setEnabled(True)
            self.ui.actionResultados.setEnabled(False)
            self.ui.deleteActionsToolButton.setEnabled(True)
            self.ui.deleteToolButton.setEnabled(True)
            self.setEstructuraFrame()
            self.zoom2extent()
            self.ui.actionResultados.setChecked(False)
            self.ui.actionEstructura.setChecked(True)
            self.ui.actionAcciones.setChecked(True)
            # Limpiar canvas de resultados
            self.ui.tableWidget.clear()
            self.ui.resultscanvas.clearCanvas(nodos=self.nodos,
                                              barras=self.barras)
            self.ui.loadcanvas.interactions['picker'].clearSelected()
            # Borrar estructura
            self.Estructura = None
            self.openDocks()
            # Conectar función de eliminar nodos
            self.ui.loadcanvas.interactions["picker"].delete.connect(
                                    self.deleteSelectedObjects)

    def changeScale(self, value=None):
        # Definir escala y plotear
        if not value:
            value = self.ui.scaleSlider.value()
        self.scale_def = 10 ** (value / 100)
        self.ui.textEditDeformadaScale.setText(to_precision(self.scale_def, 3))
        self.Estructura.remove_barra_artists(['deformada'])
        self.Estructura.plot_deformada(self.ui.resultscanvas.ax,
                                       scale_def=self.scale_def, linewidth=1,
                                       color='g')
        self.Estructura.set_barras_visible('deformada', True)
        self.ui.resultscanvas.draw_idle()

    def changeScale_position(self, scale=None):
        if not scale:
            # Leer valor
            scale = float(self.ui.textEditDeformadaScale.text())
        # Obtener posicion en slider
        value = np.log10(scale) * 100
        # Mover slider
        self.ui.scaleSlider.setValue(value)
        # Cambiar escala
        self.changeScale(value)

    def changeScale_Slider_parameters(self):
        self.ui.scaleSlider.setMinimum(-500)
        self.ui.scaleSlider.setMaximum(500)
        self.ui.scaleSlider.setSliderPosition(0)
        self.ui.textEditDeformadaScale.setEnabled(True)
        self.changeScale_position(scale=1)

    def plotResults(self, zoom=True):
        # Mostrar subwindow de resultados
        self.ui.mdiArea.setActiveSubWindow(self.ui.resultadosSW)
        # Plotear barras y nodos
        cursor = QtCore.Qt.PointingHandCursor
        # Zoom a la estructura
        if zoom:
            self.zoom2extent(margin=0.1)
        # Actualizar textEdit con la escala de la deformada
        # self.ui.textEditDeformadaScale.setText(to_precision(self.scale_def,3))
        # Generar lista de artistas asociados a los resultados y plotear
        self.Estructura.set_barras_visible('deformada', False)
        self.Estructura.set_nodos_visible('reacciones', False)
        self.Estructura.set_diagramas_visible(
            ['Mt', 'My', 'Mz', 'Qy', 'Qz', 'N'], False)
        if self.ui.Mymenu.isChecked():
            self.Estructura.set_diagramas_visible(['My'])
            self.plot_barra_selected()
        if self.ui.Mzmenu.isChecked():
            self.Estructura.set_diagramas_visible(['Mz'])
            self.plot_barra_selected()
        if self.ui.Mtmenu.isChecked():
            self.Estructura.set_diagramas_visible(['Mt'])
            self.plot_barra_selected()
        if self.ui.Qymenu.isChecked():
            self.Estructura.set_diagramas_visible(['Qy'])
            self.plot_barra_selected()
        if self.ui.Qzmenu.isChecked():
            self.Estructura.set_diagramas_visible(['Qz'])
            self.plot_barra_selected()
        if self.ui.normalToolButton.isChecked():
            self.Estructura.set_diagramas_visible('N')
            self.plot_barra_selected()
        if self.ui.deformadaToolButton.isChecked():
            self.Estructura.remove_barra_artists(['deformada'])
            self.Estructura.plot_deformada(self.ui.resultscanvas.ax,
                                           scale_def=self.scale_def,
                                           linewidth=1,
                                           color='g')
            self.Estructura.set_barras_visible('deformada', True)
        if self.ui.reaccionesToolButton.isChecked():
            # self.ui.resultscanvas.setMode('picknodes', QtGui.QCursor(cursor))
            self.Estructura.set_nodos_visible('reacciones', True)
        self.completeDisplacementTable()
        self.completeBarrasCombobox()
        self.ui.resultscanvas.draw_idle()

    def showFuerzas(self):
        state = self.ui.actionFuerzas.isChecked()
        # Nodos
        artist_list = ['p', 'c']
        for nodo in self.nodos.values():
            for list in artist_list:
                for artist in nodo.artists[list]:
                    artist.set_visible(state)
        # Barras
        artist_list = ['w', 'e']
        for barra in self.barras.values():
            for list in artist_list:
                for artist in barra.artists[list]:
                    artist.set_visible(state)

        self.ui.loadcanvas.draw_idle()
        self.showtextsfuerzas = state

    def showTextResults(self):
        state = not (self.showtextsresults)

        if self.ui.reaccionesToolButton.isChecked():
            for nodo in self.nodos.values():
                for artist in nodo.artists['reacciones']:
                    if type(artist) is mpl.text.Text:
                        artist.set_visible(state)
        else:
            for barra in self.barras.values():
                if self.ui.normalToolButton.isChecked():
                    for artist in barra.artists['N']:
                        if (type(artist) is mpl.text.Text):
                            artist.set_visible(state)
                if self.ui.corteToolButton.isChecked():
                    for artist in barra.artists['Qy']:
                        if (type(artist) is mpl.text.Text):
                            artist.set_visible(state)
                    for artist in barra.artists['Qz']:
                        if (type(artist) is mpl.text.Text):
                            artist.set_visible(state)
                if self.ui.momentoToolButton.isChecked():
                    for artist in barra.artists['Mx']:
                        if (type(artist) is mpl.text.Text):
                            artist.set_visible(state)
                    for artist in barra.artists['My']:
                        if (type(artist) is mpl.text.Text):
                            artist.set_visible(state)
                    for artist in barra.artists['Mz']:
                        if (type(artist) is mpl.text.Text):
                            artist.set_visible(state)

        self.ui.resultscanvas.draw_idle()
        self.showtextsresults = state

    def showEjesLocales(self):
        state = self.ui.actionEjes_Locales.isChecked()
        # Barras
        for barra in self.barras.values():
            for artist in barra.artists['ejes']:
                artist.set_visible(state)
            if self.Estructura:
                for artist in barra.artists['ejes_res']:
                    artist.set_visible(state)
        self.ui.loadcanvas.draw_idle()
        self.ui.resultscanvas.draw_idle()
        self.showlocal = state

    def showIndices(self):
        state = self.ui.actionIndices.isChecked()
        # Barras
        for barra in self.barras.values():
            for text in barra.artists['index']:
                text.set_visible(state)
            if self.Estructura:
                for text in barra.artists['index_res']:
                    text.set_visible(state)
        # Nodos
        for nodo in self.nodos.values():
            for text in nodo.artists['index']:
                text.set_visible(state)
            if self.Estructura:
                for text in nodo.artists['index_res']:
                    text.set_visible(state)
        self.ui.loadcanvas.draw_idle()
        # Results canvas
        for child in self.ui.resultscanvas.ax.get_children():
            if (type(child) is mpl.text.Text):
                child.set_visible(state)
        self.ui.resultscanvas.draw_idle()
        self.showlocal = state

    def setEstructuraFrame(self):
        self.ui.mdiArea.setActiveSubWindow(self.ui.cargaSW)
        self.openDocks()
        self.setEstructuraMode()

    def setAccionesFrame(self):
        self.ui.mdiArea.setActiveSubWindow(self.ui.cargaSW)
        self.openDocks()
        self.setAccionesMode()

    def setCanvasMode(self):
        # Clear selections
        # self.ui.loadcanvas.interactions['picker'].clearSelected()
        # self.ui.resultscanvas.interactions['picker'].clearSelected()
        # Check button clicked
        if self.ui.actionPan.isChecked():
            cursor = QtCore.Qt.ClosedHandCursor
            self.ui.loadcanvas.setMode("pan", QtGui.QCursor(cursor))
            self.ui.resultscanvas.setMode("pan", QtGui.QCursor(cursor))
        elif self.ui.actionPickAll.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode('pickall', QtGui.QCursor(cursor))
            self.ui.resultscanvas.setMode('pickall', QtGui.QCursor(cursor))
        elif self.ui.actionPickNodos.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("picknodes", QtGui.QCursor(cursor))
            self.ui.resultscanvas.setMode("picknodes", QtGui.QCursor(cursor))
        elif self.ui.actionPickBarras.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("pickbars", QtGui.QCursor(cursor))
            self.ui.resultscanvas.setMode("pickbars", QtGui.QCursor(cursor))
        elif self.ui.actionRotate.isChecked():
            cursor = QtCore.Qt.PointingHandCursor
            self.ui.loadcanvas.setMode("rotate", QtGui.QCursor(cursor))
            self.ui.resultscanvas.setMode("rotate", QtGui.QCursor(cursor))

    def setEstructuraMode(self):
        # Clear selections
        # self.ui.loadcanvas.interactions['picker'].clearSelected()
        # Check button clicked
        if self.ui.nodosButtonDraw.isChecked():
            self.setNodedrawerMode()
        elif self.ui.barrasButtonDraw.isChecked():
            self.setBardrawerMode()
        if self.ui.barrasButtonRel.isChecked():
            self.setReleasesMode()
        elif self.ui.nodosButtonVinculo.isChecked():
            self.setVinculosMode()
        elif self.ui.barrasButtonProps.isChecked():
            self.setSeccionMode()

    def setNodedrawerMode(self):
        self.ui.frameNodoDraw.setVisible(True)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeNodos()
        self.ui.actionPan.setChecked(True)
        self.setCanvasMode()

    def setBardrawerMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(True)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPan.setChecked(True)
        self.setCanvasMode()

    def setVinculosMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(True)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeNodos()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()

    def setReleasesMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(True)
        self.ui.frameBarraProps.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()

    def setSeccionMode(self):
        self.ui.frameNodoDraw.setVisible(False)
        self.ui.frameBarraDraw.setVisible(False)
        self.ui.frameNodoVinculo.setVisible(False)
        self.ui.frameBarraRel.setVisible(False)
        self.ui.frameBarraProps.setVisible(True)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()

    def setAccionesMode(self):
        # Clear selections
        # self.ui.loadcanvas.interactions['picker'].clearSelected()
        # Check button clicked
        if self.ui.nodosButtonCarga.isChecked():
            self.setCargasPMode()
        elif self.ui.barrasButtonCarga.isChecked():
            self.setCargasWMode()
        elif self.ui.barrasButtonTemp.isChecked():
            self.setTempMode()
        elif self.ui.nodosButtonCed.isChecked():
            self.setCedMode()

    def setCargasPMode(self):
        self.ui.frameNodoCarga.setVisible(True)
        self.ui.frameBarraCarga.setVisible(False)
        self.ui.frameBarraTemp.setVisible(False)
        self.ui.frameNodoCed.setVisible(False)
        # self.updateTreeNodos()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()

    def setCargasWMode(self):
        self.ui.frameNodoCarga.setVisible(False)
        self.ui.frameBarraCarga.setVisible(True)
        self.ui.frameBarraTemp.setVisible(False)
        self.ui.frameNodoCed.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()

    def setTempMode(self):
        self.ui.frameNodoCarga.setVisible(False)
        self.ui.frameBarraCarga.setVisible(False)
        self.ui.frameBarraTemp.setVisible(True)
        self.ui.frameNodoCed.setVisible(False)
        # self.updateTreeBarras()
        self.ui.actionPickBarras.setChecked(True)
        self.setCanvasMode()

    def setCedMode(self):
        self.ui.frameNodoCarga.setVisible(False)
        self.ui.frameBarraCarga.setVisible(False)
        self.ui.frameBarraTemp.setVisible(False)
        self.ui.frameNodoCed.setVisible(True)
        # self.updateTreeNodos()
        self.ui.actionPickNodos.setChecked(True)
        self.setCanvasMode()

    def zoom2extent(self, margin=0.1):
        if self.ui.cargaSW is self.ui.mdiArea.activeSubWindow():
            self.ui.loadcanvas.zoom2extent(margin)
        elif self.ui.resultadosSW is self.ui.mdiArea.activeSubWindow():
            self.ui.resultscanvas.zoom2extent(margin)

    def setGridOn(self):
        gridon = not self.ui.loadcanvas.gridon
        # Apagar o prender grilla
        self.ui.loadcanvas.setGridOn(gridon)
        self.ui.resultscanvas.setGridOn(gridon)

    def newCanvas(self, dialog=True):
        # Save dialog
        if dialog:
            ok = self.savedialog.exec_()
            if ok == 1:  # Yes
                self.saveFileDialog()
            elif ok == 0:  # No
                pass
            elif ok == 2:  # Cancel
                return
        # Inicializar elementos de la estructura
        self.nodos = {}
        self.barras = {}
        self.Estructura = None
        # Eliminar todos los artists del canvas
        self.ui.loadcanvas.clearCanvas(self.nodos, self.barras)
        self.ui.resultscanvas.clearCanvas(self.nodos, self.barras)
        self.ui.actionPan.setChecked(True)
        self.setCanvasMode()
        # Borrar los Combo Box de createBarra
        self.ui.comboBoxNodo1.clear()
        self.ui.comboBoxNodo2.clear()
        # Deshabilitar botones de resultados
        self.ui.resultadosDock.setEnabled(False)
        # Eliminar secciones y materiales creados
        self.materiales = {}
        self.ui.matlistWidget.clear()
        self.secciones = {}
        self.ui.seccionlistWidget.clear()
        # Resetear guardado y nombre del file
        self.filename = None
        self.saved = False
        self.setWindowTitle('Atenea3d (beta)')
        # Vaciar los trees
        self.updateTreeNodos()
        self.updateTreeBarras()
        self.updateTreePropiedades()

    def reset_Results_Canvas(self):
        """
        Resetear el results canvas y eliminar la Estructura
        """
        self.Estructura.remove_barra_artists(
            ['deformada', 'My', 'Mz', 'Mt', 'Qy', 'Qz', 'N'])
        self.Estructura.remove_nodo_artists(['reacciones'])
        self.Estructura = None
        self.ui.resultscanvas.clearCanvas(self.nodos, self.barras)

    def writeNodoBranch(self, parent, nodo):
        parent.setText(1, str(nodo.coords))
        # crear coordenadas child
        self.addTreeBranch(parent, 'Coords.', ['x', 'y', 'z'], nodo.coords)
        # crear Vinculos child
        self.addTreeBranch(parent, 'Vínculos',
                           ['Rx', 'Ry', 'Rz', 'Mx', 'My', 'Mz'],
                           nodo.restr)
        # crear Cargas child
        self.addTreeBranch(parent, 'Cargas',
                           ['Px', 'Py', 'Pz', 'Mx', 'My', 'Mz'],
                           nodo.P)
        # crear Cedimientos child
        self.addTreeBranch(parent, 'Cedimientos', ['ux', 'uy', 'uz',
                                                   'giro x', 'giro y',
                                                   'giro z'], nodo.ced)

    def updateTreeNodos(self):
        self.ui.nodosToolButton.setChecked(True)
        self.ui.treeWidget.clear()
        for i, nodo in self.nodos.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Nodo {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                resource_path('icons/arbolnodo_icon.png')))
            self.writeNodoBranch(parent, nodo)

    def writeBarraBranch(self, parent, barra):
        parent.setText(1, barra.ElemName)
        # crear nodos child
        self.addTreeBranch(parent, 'Nodos', ['Inicial', 'Final', 'Longitud'],
                           [barra.nodos[0].indice, barra.nodos[1].indice,
                            barra.long])
        # crear releases child
        if barra.ElemName == '3DFrame':
            self.addTreeBranch(parent, 'Releases',
                               ['inicial y', 'inicial y', 'final y', 'final z'],
                               [barra.rel[1], barra.rel[2], barra.rel[4],
                                barra.rel[5]])
        elif barra.ElemName == '3DTruss':
            self.addTreeBranch(parent, 'Releases',
                               ['inicial y', 'inicial y', 'final y', 'final z'],
                               [1, 1, 1, 1])
        # crear Cargas child
        self.addTreeBranch(parent, 'Carga',
                           ['wxi', 'wyi', 'wzi', 'wxf', 'wyf', 'wzi', 'Terna'],
                           [v for v in barra.w.values()])
        # crear deformaciones child
        self.addTreeBranch(parent, 'Temperatura',
                           ['baricentrica', 'gradiente y', 'gradiente z'],
                           barra.e0)
        # crear material child
        self.addTreeBranch(parent, 'Material',
                           [a for a in barra.mat.keys()],
                           [a for a in barra.mat.values()])
        # crear seccion child
        if len(barra.seccion.keys()) == 1:
            self.addTreeBranch(parent, 'Seccion', [], [])
        else:
            self.addTreeBranch(parent, 'Seccion',
                               [a for a in barra.seccion.keys()],
                               [a for a in barra.seccion.values()])

    def updateTreeBarras(self):
        self.ui.barrasToolButton.setChecked(True)
        self.ui.treeWidget.clear()

        for i, barra in self.barras.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Barra {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                resource_path('icons/arbolbarra_icon.png')))
            self.writeBarraBranch(parent, barra)

    def writePropsBranch(self, parent, name, props):
        keys = [v for v in props.keys() if v != 'name']
        self.addTreeBranch(parent, name, keys,
                           [props[key] for key in keys])

    def updateTreePropiedades(self):
        self.ui.propiedadesToolButton.setChecked(True)
        self.ui.treeWidget.clear()
        self.Material_item = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
        self.Material_item.setIcon(0, QtGui.QIcon(
            resource_path('icons/seccion_icon.png')))
        self.Material_item.setText(0, 'Materiales')
        for nombre, material in self.materiales.items():
            parent = self.Material_item
            self.writePropsBranch(parent, nombre, material)

        self.Seccion_item = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
        self.Seccion_item.setIcon(0, QtGui.QIcon(
            resource_path('icons/seccion_icon.png')))
        self.Seccion_item.setText(0, 'Secciones')
        for nombre, seccion in self.secciones.items():
            parent = self.Seccion_item
            self.writePropsBranch(parent, nombre, seccion)

    def updateTreeSelected(self, expand=False):
        self.ui.selectedToolButton.setChecked(True)
        self.ui.treeWidget.clear()
        for i, nodo in self.selected_nodos.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Nodo {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                resource_path('icons/arbolnodo_icon.png')))
            self.writeNodoBranch(parent, nodo)
            self.expandAll(parent, expand)
        for i, barra in self.selected_barras.items():
            parent = QtWidgets.QTreeWidgetItem(self.ui.treeWidget)
            parent.setText(0, 'Barra {}'.format(i))
            parent.setIcon(0, QtGui.QIcon(
                resource_path('icons/arbolbarra_icon.png')))
            self.writeBarraBranch(parent, barra)
            self.expandAll(parent, expand)

    def addTreeBranch(self, parent, child_title, text, data):
        child = QtWidgets.QTreeWidgetItem(parent)
        child.setText(0, child_title)
        for i in range(len(data)):
            item = QtWidgets.QTreeWidgetItem(child)
            item.setText(0, text[i])
            item.setText(1, str(data[i]))
            # item.setFlags(item.flags() | QtCore.Qt.ItemIsEditable)

    def expandAll(self, treeItem, expand):
        """
        Expande o colapsa todos los children de un tree widget item
        :return:
        """
        treeItem.setExpanded(expand)
        childCount = treeItem.childCount()
        for childNo in range(0, childCount):
            child = treeItem.child(childNo)
            if expand:  # if expanding, do that first (wonky animation otherwise)
                child.setExpanded(expand)
            subChildCount = child.childCount()
            if subChildCount > 0:
                self.expandAll(child, expand)
            if not expand:  # if collapsing, do it last (wonky animation otherwise)
                child.setExpanded(expand)

    def DoubleValidation(self):
        onlyDouble = QtGui.QDoubleValidator()
        # Coordenadas nodales
        self.ui.textEditCoordx.setValidator(onlyDouble)
        self.ui.textEditCoordy.setValidator(onlyDouble)
        self.ui.textEditCoordz.setValidator(onlyDouble)
        # Cedimientos de vinculo
        self.ui.textEditCedux.setValidator(onlyDouble)
        self.ui.textEditCeduy.setValidator(onlyDouble)
        self.ui.textEditCeduz.setValidator(onlyDouble)
        self.ui.textEditCedgirox.setValidator(onlyDouble)
        self.ui.textEditCedgiroy.setValidator(onlyDouble)
        self.ui.textEditCedgiroz.setValidator(onlyDouble)
        # Cargas nodales
        self.ui.textEditCargasPx.setValidator(onlyDouble)
        self.ui.textEditCargasPy.setValidator(onlyDouble)
        self.ui.textEditCargasPz.setValidator(onlyDouble)
        self.ui.textEditCargasMx.setValidator(onlyDouble)
        self.ui.textEditCargasMy.setValidator(onlyDouble)
        self.ui.textEditCargasMz.setValidator(onlyDouble)
        # Cargas distribuidas
        self.ui.textEditWxi.setValidator(onlyDouble)
        self.ui.textEditWxf.setValidator(onlyDouble)
        self.ui.textEditWyi.setValidator(onlyDouble)
        self.ui.textEditWyf.setValidator(onlyDouble)
        self.ui.textEditWzi.setValidator(onlyDouble)
        self.ui.textEditWzf.setValidator(onlyDouble)
        # Deformaciones distribuidas
        self.ui.textEdite01.setValidator(onlyDouble)
        self.ui.textEdite02.setValidator(onlyDouble)
        self.ui.textEdite03.setValidator(onlyDouble)
        # self.ui.textEdite04.setValidator(onlyDouble)
        # self.ui.textEdite05.setValidator(onlyDouble)
        # self.ui.textEdite06.setValidator(onlyDouble)


if __name__ == "__main__":
    SCREEN_BORDER = 50
    app = 0
    app = QApplication(sys.argv)
    QApplication.setStyle(QStyleFactory.create('Plastique'))
    w = AteneaForm()
    # Windows size
    rect = QApplication.desktop().availableGeometry()
    w.setGeometry(rect.x() + SCREEN_BORDER,
                  rect.y() + SCREEN_BORDER,
                  rect.width() - 2 * SCREEN_BORDER,
                  rect.height() - 2 * SCREEN_BORDER)
    w.setMinimumSize(900, 600)
    # Window title and icon
    w.setWindowTitle('Atenea3d (beta)')
    w.setWindowIcon(QtGui.QIcon(
        resource_path('icons/Atenea3d_icon.png')))
    # Abrir archivo
    if len(sys.argv) == 2:
        w.openFileDialog(sys.argv[1])
    w.show()
    app.setWindowIcon(QtGui.QIcon(
        resource_path('icons/Atenea3d_icon.png')))
    sys.exit(app.exec_())
